<?php

namespace App\Imports;

use App\Models\Guru;
use App\Models\User;
use App\Models\TahunPelajaran;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class GuruImport implements ToCollection, ToModel, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //
    }
    public function model(array $row){
        // dd($row);
        $tahun =  TahunPelajaran::where('status', 'aktif')->first();
        // $id = $tahun -> id;
        User::create([
            'name' => $row['nama'],
            'email' => $row['nip'],
            'password' => password_hash('guru', PASSWORD_DEFAULT),
            'pass' => 'guru',
            'role_id' => 3,
            'id_tahun' => $tahun -> id
        ]);
        $id_user = User::max('id');
        return new Guru([
            'id_tahun' => $tahun -> id,
            'nama'    => $row['nama'],
            'nip'    => $row['nip'],
            'jenkel'    => $row['jenkel'],
            'alamat'    => $row['alamat'],
            'telp'    => $row['telp'],
            'id_user' => $id_user
        ]);
    }
}
