<?php

namespace App\Imports;

use App\Models\Perusahaan;
use App\Models\TahunPelajaran;
use App\Models\ActivityLog;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportPerusahaan implements ToCollection, ToModel, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //
    }
    public function model(array $row){
        // dd($row['mou']);
        $tahun =  TahunPelajaran::where('status', 'aktif')->first();
        // $id = $tahun -> id;


        return new Perusahaan([
            'id_tahun' => $tahun -> id,
            'nama'    => $row['nama'],
            'alamat'    => $row['alamat'],
            'lokasi'    => $row['map'],
            'email'    => $row['email'],
            'fax'    => $row['fax'],
            'telp'    => $row['telp'],
            'website'    => $row['website'],
            'mou'    => $row['mou'],
            'kuota'    => $row['kuota'],
        ]);
    }
}
