<?php

namespace App\Imports;

use App\Models\Siswa;
use App\Models\TahunPelajaran;
use App\Models\ActivityLog;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportSiswa implements ToCollection, ToModel, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        // // dd($collection);
        // foreach ($collection as $item) {
        //     // dd(gettype($item['induk']));
        //     // Siswa::insert([
        //     // 'nis' => $item['induk'],
        //     // 'nama' => $item['nama_siswa'],
        //     // 'jenkel' => $item['lp']

        //     // ]);

        //     DB::table('siswas')->insert([
        //         'nis' => $item['induk'],
        //         'nama' => $item['nama_siswa'],
        //         'jenkel' => $item['lp']

        //     ]);
        // }

    }

    public function model(array $row){
        // dd($row);
        $tahun =  TahunPelajaran::where('status', 'aktif')->first();
        // dd($tahun -> id);

        return new Siswa([
            'id_tahun' => $tahun -> id,
            'nis'     => $row['nis'],
            'nama'    => $row['nama'],
            'jenkel'    => $row['jenkel']

        ]);
    }
}
