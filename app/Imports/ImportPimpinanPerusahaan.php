<?php

namespace App\Imports;

use App\Models\TahunPelajaran;
use App\Models\PimpinanPerusahaan;
use App\Models\ActivityLog;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportPimpinanPerusahaan implements ToCollection, ToModel, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //
    }
    public function model(array $row){
        // dd($row);
        $tahun =  TahunPelajaran::where('status', 'aktif')->first();
        // dd($tahun -> id);

        return new PimpinanPerusahaan([
            'id_tahun' => $tahun -> id,
            'nama'    => $row['nama_pemimpin'],
            'nip'    => $row['nip_pemimpin'],
            'pangkat'    => $row['pangkat'],
            'jenkel'    => $row['jenkel_pemimpin']
        ]);
    }
}
