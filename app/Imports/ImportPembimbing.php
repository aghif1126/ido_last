<?php

namespace App\Imports;

use App\Models\TahunPelajaran;
use Illuminate\Support\Collection;
use App\Models\PembimbingPerusahaan;
use App\Models\ActivityLog;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ImportPembimbing implements ToCollection, ToModel, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //
    }
    public function model(array $row){
        // dd($row);
        $tahun =  TahunPelajaran::where('status', 'aktif')->first();
        // dd($tahun -> id);

        return new PembimbingPerusahaan([
            'id_tahun' => $tahun -> id,
            'nama'    => $row['nama_pembimbing'],
            'nip'    => $row['nip_pembimbing'],
            'telp'    => $row['telp_pembimbing'],
            'jenkel'    => $row['jenkel_pembimbing']
        ]);
    }
}
