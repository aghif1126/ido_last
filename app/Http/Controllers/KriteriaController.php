<?php

namespace App\Http\Controllers;

use App\Models\Kriteria;
use Illuminate\Http\Request;
use App\Models\TahunPelajaran;
use App\Models\PembimbingPerusahaan;
use App\Models\ActivityLog;
use Illuminate\Support\Facades\Auth;

class KriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // dd(Auth::user() -> id);
        $pembimbing = PembimbingPerusahaan::where('id_user', Auth::user() -> id)-> first();
        // dd($pembimbing -> id);
        $kriteria = Kriteria::where('id_pembimbing', $pembimbing -> id) -> get();
        // dd($kriteria);

        return view('pembimbing.kriteria', ['pembimbing' => $pembimbing], compact('kriteria'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = null;
        $pembimbing = PembimbingPerusahaan::where('id_user', Auth::user() -> id)-> first();
        return view('pembimbing.add-kriteria', compact('pembimbing', 'data'));

    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request);
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        Kriteria::create([
            'id_pembimbing' => $request -> id_pembimbing,
            'nama_kriteria' => $request -> nama_kriteria,
            'id_tahun' => $tahun ->id
        ]);

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/pembimbing/kriteria');
    }

    /**
     * Display the specified resource.
     */
    public function show(Kriteria $kriteria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Kriteria $kriteria)
    {
        $data = $kriteria;
        $pembimbing = PembimbingPerusahaan::where('id_user', Auth::user() -> id)-> first();
        return view('pembimbing.add-kriteria', compact('pembimbing', 'data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Kriteria $kriteria)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($request);
        $kriteria -> update($request -> except('_token'));

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/pembimbing/kriteria');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Kriteria $kriteria)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($kriteria);
        $kriteria -> delete();

        $activitylog = ActivityLog::all()->last();

        // $activitylog->ip = $request->getClientIp();
        // $activitylog->id_tahun = $tahun->id;

        // $activitylog->save();

        return redirect('/pembimbing/kriteria');
    }
}
