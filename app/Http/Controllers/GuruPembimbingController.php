<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\Absen;
use App\Models\Kelas;
use App\Models\Nilai;
use App\Models\Siswa;
use App\Models\Catatan;
use App\Models\Kriteria;
use App\Models\Aktivitas;
use App\Models\Monitoring;
use App\Models\Perusahaan;
use Illuminate\Http\Request;
use App\Models\TahunPelajaran;
use App\Models\PembimbingPerusahaan;
use App\Models\User;
use App\Models\ActivityLog;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class GuruPembimbingController extends Controller
{
    public function index()
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        $guru = $tahun->guru->where('id_user', Auth::user()->id)->first();
        $user = User::findorfail($guru -> id_user);
        $perusahaan = $guru -> perusahaan;
        // dd($guru);
        return view('gurupembimbing.dashboard', ['perusahaan' => $perusahaan,'guru' => $guru, 'user' => $user, 'tahun' => $tahun]);
    }

    public function profile()
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        $guru = $tahun->guru->where('id_user', Auth::user()->id)->first();
        $user = User::findorfail($guru -> id_user);
        $perusahaan = $guru -> perusahaan;
        // dd($guru);
        return view('gurupembimbing.profile', ['perusahaan' => $perusahaan,'guru' => $guru, 'user' => $user, 'tahun' => $tahun]);
    }

    public function editpassword(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $user = User::where('id', $request->id)->first();
        $user->password = password_hash($request->password, PASSWORD_DEFAULT);
        $user->pass = $request->password;
        $user->save();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/logout');
    }

    public function perusahaan()
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        $guru = $tahun->guru->where('id_user', Auth::user()->id)->first();
        $jurusan = $tahun -> jurusan;
        return view('gurupembimbing.perusahaan', ['guru' => $guru, 'tahun' => $tahun, 'jurusan' => $jurusan]);
    }
    public function detailsiswa(Request $request)
    {
        // dd($request);
        $guru = Guru::where('id_user', auth()->user()->id) -> first();
        $siswa = Siswa::findorFail($request -> id_siswa);
        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni", "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
        $tanggal = date('Y-m-d');
        $all_nilai = [];
        for ($i=0; $i < count($siswa -> perusahaan -> all()); $i++) {
            // $nilai = Nilai::where('id_siswa', $siswa -> nis)->where('id_perusahaan', $siswa -> perusahaan[$i] -> id_perusahaan)->first();
            // array_push($all_nilai, $nilai);
        }
        // $catatan = Catatan::where('id_siswa', $siswa -> nis)->orderby('id_perusahaan')->get();
        $catatan = $siswa -> catatan;
        $riwayat = Aktivitas::where('id_siswa' , $siswa -> nis)->get();
        $absen = Absen::where('id_siswa', $siswa -> nis) -> get();

        foreach ($riwayat as $r ) {
            $r -> hari = date('l', strtotime($r -> tanggal));
        }

        $month = 12;
        $tahun = 2023;
        $sekarang = $bulan[$month];
        $perusahaan = Perusahaan::findorfail($request -> id_perusahaan);
        // dd($perusahaan);
        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni", "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
        $kriteria = Kriteria::all();
        // dd($absen);


        return view('gurupembimbing.detail-siswa', ['guru' => $guru, 'siswa' => $siswa, 'perusahaan' => $perusahaan, 'bulan' => $bulan, 'absen' => $absen,
        'sekarang' => $sekarang, 'kriteria' => $kriteria, 'tahun' => $tahun, 'nilai' => $siswa -> nilai, 'catatan' => $catatan, 'riwayat' => $riwayat
    ]);
    }
    public function detailpembimbing($id)
    {

        $pembimbing = PembimbingPerusahaan::findorfail($id);
        $guru = Guru::where('id_user', auth()->user()->id) -> first();

        return view('gurupembimbing.detail-pembimbing', [ 'pembimbing' => $pembimbing, 'guru' => $guru]);
    }

    public function kelas()
    {

    }

    public function detailperusahaan($id,$idjur)
    {
        // dd($idjur)
        $perusahaan = Perusahaan::findOrFail($id);
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        $guru = $tahun->guru->where('id_user', Auth::user()->id)->first();
        $siswa = $perusahaan -> siswa -> where('confirm', 'Sudah') -> where('id_jurusan', $idjur)-> all();
        $pembimbing = $perusahaan -> pembimbing -> where('id_jurusan', $idjur) -> first();
        // dd($pembimbing);


        return view('gurupembimbing.detail-perusahaan', ['pembimbing' => $pembimbing,'guru' => $guru, 'perusahaan' => $perusahaan, 'siswa' => $siswa, 'idjur' => $idjur]);
    }



    public function monitoring()
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        $guru = $tahun->guru->where('id_user', Auth::user()->id)->first();
        $perusahaan = $guru -> perusahaan;
        return view('gurupembimbing.monitoring', ['perusahaan' => $perusahaan, 'guru' => $guru]);
    }

    public function form()
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        $guru = $tahun->guru->where('id_user', Auth::user()->id)->first();
        $perusahaan = $guru -> perusahaan;
        // dd($perusahaan[2] -> detailperusahaan);
        $tanggal = date('Y-m-d');

        $tz = 'Asia/Jakarta';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
        $jam = $dt->format('H:i:s');
        $tes = [$tanggal, $jam];
        $wm = implode(' ', $tes);
        // dd($perusahaan->jurusan[0]->detailjurusan);
        return view('gurupembimbing.form', ['guru' => $guru, 'perusahaan' => $perusahaan, 'wm' => $wm]);
    }

    public function storeform(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $img = $request->photo;

        $image_parts = explode(";base64,", $img);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        // dd($image_type_aux);

        $image_base64 = base64_decode($image_parts[1]);
        $fileName = uniqid() . '.png';

        $file = $fileName;
        // dd($fileName);
        Storage::disk('public_uploads_monitoring')->put($file, $image_base64);
        $id_perusahaan = explode("-",$request->id_perusahaan)[0];
        $id_jurusan = explode("-",$request->id_perusahaan)[1];
        // dd($request);
        Monitoring::create([
            'kehadiran' => $request->kehadiran,
            'sikap' => $request->sikap,
            'kompetensi' => $request->kompetensi,
            'tanggal' => $request->tanggal,
            'id_guru' => $request->id_guru,
            'id_perusahaan' => $id_perusahaan,
            'id_jurusan' => $id_jurusan,
            'photo' => $fileName,
            'id_tahun' => $tahun -> id
        ]);

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/pembimbing/dashboard');
    }

    public function riwayat()
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        $guru = $tahun->guru->where('id_user', Auth::user()->id)->first();
        $riwayat = Monitoring::where('id_guru', $guru -> id)->get();
        // dd($riwayat);
        foreach ($riwayat as $r ) {
            $r -> hari = date('l', strtotime($r -> tanggal));
        }
        return view('gurupembimbing.riwayat', ['guru' => $guru, 'riwayat' => $riwayat]);
    }
}
