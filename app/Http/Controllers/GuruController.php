<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\User;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Jurusan;
use App\Models\Kaprog;
use App\Models\Perusahaan;
use Illuminate\Http\Request;
use App\Models\KonsentrasiKeahlian;
use App\Models\SiswaPerusahaan;
use App\Models\TahunPelajaran;
use App\Models\ActivityLog;
use App\Models\GuruPerusahaan;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function dashboard(){
        $jurusan = Perusahaan::all();
        // $p = Jurusan::first();
        // dd($p->jurusanperusahaan->perusahaan);
        return view('dashboard', ['perusahaan' => $jurusan]);
    }

    public function index(Request $request)
    {
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $siswa = $tahun_pelajaran -> siswa;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $siswa = $tahun_pelajaran -> siswa;
            $tahun = TahunPelajaran::get();

        }

        // dd($request->getClientgetClientIp());

        $log = Activity::where('id_tahun', $tahun_pelajaran->id)->get();
        // $log = Activity::where('description', 'updated')->first();

        // foreach ($log as $key => $value) {
        //     $contoh = $log->properties['attributes']['nama'];
        // }
        // $log1 = explode('-',$log);
        // dd($contoh);
        // dd($tahun_pelajaran->tahun);
        $perusahaan = $tahun_pelajaran->perusahaan;
        $user =$tahun_pelajaran->users;
        $konsentrasi = $tahun_pelajaran->konsentrasi;
        $jurusan = $tahun_pelajaran->jurusan;
        $kelas = $tahun_pelajaran->kelas;
        $guru = $tahun_pelajaran->guru;
        $pembimbing = $tahun_pelajaran->pembimbing;


        $sudah = 0;
            foreach ($siswa as $item) {
                if ($item ->perusahaan != "[]") {
                    // echo $item -> perusahaan;
                   $sudah += 1;
                }
            }
            $belum = $siswa -> count() - $sudah;
            $jumlah_siswa = Siswa::all()->count();
            $confirm = 0;
            $tunggu = 0;
            foreach ($siswa as $item) {
                if ($item ->perusahaan != "[]") {
                    // echo $item -> perusahaan;
                    $perusahaan = $item -> perusahaan -> all();
                    // dd(count($perusahaan));
                    foreach ($perusahaan as $value) {
                        if ($value -> confirm == "Sudah") {
                            $confirm += 1;
                        }else{
                            $tunggu += 1;
                        }
                    }
                }
            }
            foreach ($kelas as $item ) {
                $item -> sudah = 0;
                $item -> belum = 0;
                $item -> confirm = 0;
                $item -> tunggu = 0;
            }

        $siswanya = SiswaPerusahaan::all()->groupBy('id_perusahaan');
        $perusahaannya = Perusahaan::all();


            $atos = 0;
            foreach ($guru as $item) {
                if ($item ->perusahaan != "[]") {
                   $atos += 1;
                }
            }
            $acan = $guru -> count() - $atos;
            foreach ($guru as $item) {
                if ($item ->perusahaan != "[]") {
                    $perusahaan = $item -> perusahaan -> all();
                }
            }
            foreach ($kelas as $item ) {
                $item -> atos = 0;
                $item -> acan = 0;
            }
            // dd($confirm);
        return view('guru.dashboard', [
            'siswa' => $siswa,
            'kelas' => $kelas,
            'user' => $user,
            'konsentrasi' => $konsentrasi,
            'jurusan' => $jurusan,
            'perusahaan' => $perusahaan,
            'guru' => $guru,
            'pembimbing' => $pembimbing,
            'tahun' => $tahun,
            'tahun_pelajaran' => $tahun_pelajaran,
            'sudah' => $sudah,
            'belum' => $belum,
            'confirm' => $confirm,
            'jumlah_siswa' => $jumlah_siswa,
            'tunggu' => $tunggu,
            'siswanya' => $siswanya,
            'perusahaannya' => $perusahaannya,
            'atos' => $atos,
            'acan' => $acan,
            'log' => $log,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('guru.guru.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($activitylog);
        User::create([
            'name' => $request -> nama,
            'email' => $request -> nip,
            'password' => password_hash('guru', PASSWORD_DEFAULT),
            'pass' => 'guru',
            'role_id' => 3,
            'id_tahun' => $tahun -> id
        ]);

        $id_user = User::max('id');
        Guru::create([
            'nip' => $request -> nip,
            'nama' => $request -> nama,
            'alamat' => $request -> alamat,
            'telp' => $request -> telp,
            'jenkel' => $request -> jenkel,
            'id_tahun' => $tahun -> id,
            'id_user' => $id_user
        ]);
        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('guru/show-guru');
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $guru = $tahun_pelajaran -> guru;
            $jurusan = $tahun_pelajaran -> jurusan;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $guru = $tahun_pelajaran -> guru;
            $jurusan = $tahun_pelajaran -> jurusan;
            $tahun = TahunPelajaran::get();

        }

            return view('guru.guru.show', ['guru'=>$guru, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran, 'jurusan' => $jurusan]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {


        $guru = Guru::findorFail($id);
        $perusahaan = perusahaan::all();
        $jurusan = Jurusan::all();

        return view('guru.guru.edit', ['guru' => $guru, 'perusahaan' => $perusahaan, 'jurusan' => $jurusan]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Guru $guru)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $flight = Guru::findorFail($request->id);
        $flight->id = $request->id;
        $flight->nama = $request->nama;
        $flight->nip = $request->nip;
        $flight->alamat = $request->alamat;
        $flight->telp = $request->telp;
        $flight->jenkel = $request->jenkel;
        $flight->status = $request->status;

        $flight->save();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/detail-guru/'.$request->id);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete()
    {
        $pembimbing = Guru::all();
        // $keyword = $request->keyword;

        // $pembimbing = Guru::where('nama', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('nip', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('jenkel', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('alamat', 'LIKE', '%' . $keyword . '%')
        //     ->orderby('nama')
        //     ->paginate(15);

        return view('guru.guru.delete', ['guru' => $pembimbing]);
    }
    public function destroy(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        Guru::findorFail($request->id)->delete();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/show-guru');
    }

    public function detail($id)
    {
        $guru = Guru::findorFail($id);
        // dd($guru -> perusahaan[0] -> detailperusahaan);
        return view("guru.guru.detail")->with("guru", $guru);
    }

     public function tambahperusahaan(Request $request)
    {
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $guru = $tahun_pelajaran -> guru -> find($request -> id);
            $perusahaan = $tahun_pelajaran -> perusahaan;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $guru = $tahun_pelajaran -> guru -> find($request -> id);
            $perusahaan = $tahun_pelajaran -> perusahaan ;
            $tahun = TahunPelajaran::get();

        }



        return view('guru.guru.add-perusahaan', ['guru' => $guru , 'perusahaan' => $perusahaan, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran]);
    }

    // public function tambahperusahaanapi(Request $request)
    // {
        // $guru = Guru::findOrFail($request -> id);
        // $perusahaan = perusahaan::where('id_guru', null)->get()->load('jurusan');

        // foreach ($perusahaan as $key) {
        //     $jurusan = $key->jurusan;
        // }

        // dd($perusahaan);

        // return response()->json(['perusahaan' => $perusahaan]);

        // dd($jurusan);

        // return response()->json(compact('perusahaan', "jurusan"));
        // dd($j);

        // return response()->json(compact('perusahaan'));
        // $perusahaan = perusahaan::where('id_guru', null) -> get();
        // dd($perusahaan);

        // $list = $perusahaan->where(function($query) use($keyword){
        //     return $query
        //         ->where('nama' , 'LIKE', '%'. $keyword .'%')
        //         ->orWhere('alamat' , 'LIKE', '%'. $keyword .'%')
        //         ->orWhereHas('jurusan', function($query) use($keyword){
        //             $query->where('nama', 'LIKE', '%' . $keyword . '%');
        //     });
        // })
        // ->get();
        // dd($list);
        // $selectedItems = session('selectedItems', []);
        // dd($perusahaan -> firstItem());
        // return view('guru.guru.add-perusahaan', ['guru' => $guru , 'perusahaan' => $perusahaan], compact('selectedItems'));

    // }


    public function storeperusahaan(Request $request)
    {
        // dd($request);
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        // for ($i=0; $i < count($request -> perusahaan); $i++) {
        // $perusahaan = perusahaan::FindOrFail($request -> perusahaan[$i]);
        // $perusahaan -> id_guru = $request -> id_guru;
        // $perusahaan -> save();
        // }

        foreach ($request -> perusahaan as $key => $item) {
            $id_perusahaan = explode('-', $item)[0];
            $id_jurusan = explode('-', $item)[1];
            // dd($item);
            GuruPerusahaan::insert([
                'id_guru' => $request -> id_guru,
                'id_perusahaan' => $id_perusahaan,
                'id_jurusan' => $id_jurusan
            ]);
        }

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        $id = $request -> id_guru;
        return redirect('/guru/edit-guru/'.$id);

    }

    public function hapusperusahaan(Request $request)
    {
        $guru = Guru::findOrFail($request -> id);
        return view('guru.guru.delete-perusahaan', ['guru' => $guru]);
    }

    public function destroyperusahaan(Request $request)
    {
        // dd($request);
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $perusahaan = GuruPerusahaan::where('id_perusahaan',$request -> id_perusahaan) -> where('id_guru', $request -> id_guru) -> first();
        $perusahaan->delete();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        $id = $request -> id_guru;
        return redirect('/guru/edit-guru/'.$id);
    }
    public function backup_db(Request $request)
    {


         //ENTER THE RELEVANT INFO BELOW
         $mysqlHostName      = env('DB_HOST');
         $mysqlUserName      = env('DB_USERNAME');
         $mysqlPassword      = env('DB_PASSWORD');
         $DbName             = env('DB_DATABASE');
         $backup_name        = "mybackup.sql";
         $tables             = array( "users","gurus","perusahaans","pimpinan_perusahaans","pembimbing_perusahaans","jurusan","kelas","konsentrasi_keahlian","catatans","jurusan_perusahaans","monitorings","siswa_perusahaans","absen","kriteria", "nilais", 'siswas'); //here your tables...

         $connect = new \PDO("mysql:host=$mysqlHostName;dbname=$DbName;charset=utf8", "$mysqlUserName", "$mysqlPassword",array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
         $get_all_table_query = "SHOW TABLES";
         $statement = $connect->prepare($get_all_table_query);
         $statement->execute();
         $result = $statement->fetchAll();


         $output = '';
         foreach($tables as $table)
         {
        //   $output .= "DROP TABLE IF EXISTS `$table`;\n";

          $show_table_query = "SHOW CREATE TABLE " . $table . "";
          $statement = $connect->prepare($show_table_query);
          $statement->execute();
          $show_table_result = $statement->fetchAll();

          foreach($show_table_result as $show_table_row)
          {
        //    $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
          }
          $select_query = "SELECT * FROM " . $table . "";
          $statement = $connect->prepare($select_query);
          $statement->execute();
          $total_row = $statement->rowCount();

          for($count=0; $count<$total_row; $count++)
          {
           $single_result = $statement->fetch(\PDO::FETCH_ASSOC);
           $table_column_array = array_keys($single_result);
           $table_value_array = array_values($single_result);
           $output .= "\nINSERT INTO $table (";
           $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
        //    foreach ($table_value_array as $key => $item) {
        //     if($item){
        //     }else {
        //         $table_value_array[$key] = 0;

        //     }
        //    }
           $dummy = implode("','", $table_value_array) ;
        //    str_replace("''", '0', $dummy);
           $output .= "'" . $dummy . "');\n";
        //    str_replace("'',", 'NULL,', $output);
          }
         }
         $password = password_hash('admin', PASSWORD_DEFAULT);
    // $output .= "\nINSERT INTO users (id, name, email, email_verified_at, password, pass, created_at, updated_at, role_id, id_tahun) VALUES ('1','admin','admin@gmail.com','2023-02-02 23:10:10','".$password."','admin','2023-11-20 05:49:02','2023-11-20 05:49:02','1','2');";
//  dd($output);
         $file_name = 'database_backup_on_' . date('y-m-d') . '.sql';
         $file_handle = fopen($file_name, 'w+');
         fwrite($file_handle, $output);
         fclose($file_handle);

        // Set header untuk memberitahu browser cara menangani file
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($file_name));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file_name));

        // Bersihkan buffer output dan kirimkan file ke browser
        ob_clean();
        flush();
        readfile($file_name);

        // Hapus file setelah diunduh
        unlink($file_name);

        return redirect()->back();

    }

    public function restore_db(Request $request)
    {
        // dd($request->file('file'));
        $sqlFile = $request->file('file');
        $id_awal_user = User::max('id');
        if($id_awal_user == 1){
            // $id_awal_user = 1;
            if ($sqlFile) {
                $content = file_get_contents($sqlFile);
             $tables  = array("users","gurus","perusahaans","pimpinan_perusahaans","pembimbing_perusahaans","jurusan","kelas","konsentrasi_keahlian","catatans","jurusan_perusahaans","monitorings","siswa_perusahaans","absen","kriteria", "nilais", "siswas"); //here your tables...

                // $tables = ["tahun_pelajaran","roles","users","gurus","perusahaans","pimpinan_perusahaans","pembimbing_perusahaans","jurusan","kelas","konsentrasi_keahlian","catatans","jurusan_perusahaans","monitorings","siswa_perusahaans","absen","kriteria", "nilais"];

                // Nonaktifkan kunci asing
                DB::statement('SET FOREIGN_KEY_CHECKS=0');

                // Hapus tabel yang sudah ada
                foreach ($tables as $table) {
                    if (Schema::hasTable($table)) {
                DB::statement('DELETE FROM '.$table.';');
                        // Schema::dropIfExists($table);
                    }
                }


                // Jalankan perintah SQL dari file
                DB::unprepared($content);

                // Aktifkan kembali kunci asing
                DB::statement('SET FOREIGN_KEY_CHECKS=1');

                $user = User::all();

                foreach ($user as $item) {
                    $data_user = User::where('email', $item->email)->get();
                    $cek_user = $data_user ->count();

                    if ($cek_user > 1) {
                        $data_user->max()->delete();
                    }
                }

                return redirect()
                    ->back();
            }
        }else{
            if ($sqlFile) {
                $content = file_get_contents($sqlFile);
             $tables  = array("users","gurus","perusahaans","pimpinan_perusahaans","pembimbing_perusahaans","jurusan","kelas","konsentrasi_keahlian","catatans","jurusan_perusahaans","monitorings","siswa_perusahaans","absen","kriteria", "nilais", "siswas"); //here your tables...

                // $tables = ["tahun_pelajaran","roles","users","gurus","perusahaans","pimpinan_perusahaans","pembimbing_perusahaans","jurusan","kelas","konsentrasi_keahlian","catatans","jurusan_perusahaans","monitorings","siswa_perusahaans","absen","kriteria", "nilais"];

                // Nonaktifkan kunci asing
                DB::statement('SET FOREIGN_KEY_CHECKS=0');

                // Hapus tabel yang sudah ada
                foreach ($tables as $table) {
                    if (Schema::hasTable($table)) {
                DB::statement('DELETE FROM '.$table.';');
                        // Schema::dropIfExists($table);
                    }
                }


                // Jalankan perintah SQL dari file
                DB::unprepared($content);

                // Aktifkan kembali kunci asing
                DB::statement('SET FOREIGN_KEY_CHECKS=1');


                $id_akhir_user = User::max('id');

                for ($i=$id_awal_user + 1; $i <= $id_akhir_user; $i++) {
                    $user = User::find($i);
                    if ($user) {
                        $user -> delete();
                    }
                }


                return redirect()
                    ->back();
            }
        }



        return redirect()
            ->back();
    }
}
