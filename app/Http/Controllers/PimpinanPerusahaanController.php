<?php

namespace App\Http\Controllers;

use App\Models\PimpinanPerusahaan;
use App\Models\Perusahaan;
use App\Models\Role;
use App\Models\TahunPelajaran;
use App\Models\ActivityLog;
use Illuminate\Http\Request;

class PimpinanPerusahaanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
    //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function pilih_perusahaan()
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        $perusahaan = $tahun -> perusahaan -> all();
        // $perusahaan = Perusahaan::paginate(15);
        return view('guru.pimpinan.pilih-perusahaan', ['perusahaan' => $perusahaan]);


    }
    public function create(Request $request)
    {
        // dd($request);
        // $tahun = TahunPelajaran::where('status', 'aktif')->first();
        $perusahaan = Perusahaan::find($request -> id_perusahaan);
        // $perusahaan = Perusahaan::paginate(15);
        return view('guru.pimpinan.add', ['perusahaan' => $perusahaan]);


    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request);
        $tahun =TahunPelajaran::where('status', 'aktif')->first();
        PimpinanPerusahaan::create([
            'nama' => $request -> nama,
            'nip' => $request -> nip,
            'pangkat' => $request -> pangkat,
            'id_perusahaan' => $request -> id_perusahaan,
            'id_tahun' => $tahun -> id
        ]);

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('guru/show-pimpinan');
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $pimpinanPerusahaan = $tahun_pelajaran -> pimpinan_perusahaan;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $pimpinanPerusahaan = $tahun_pelajaran -> pimpinan_perusahaan;
            $tahun = TahunPelajaran::get();

        }

        return view('guru.pimpinan.show', ['pimpinanPerusahaan' => $pimpinanPerusahaan, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $role = Perusahaan::all();
        $perusahaan = Perusahaan::paginate(15);
        $pimpinanPerusahaan = PimpinanPerusahaan::findorFail($id);
        return view('guru.pimpinan.edit', ['pimpinanPerusahaan' => $pimpinanPerusahaan, 'role' => $role, 'perusahaan' => $perusahaan]);

        // $pimpinanPerusahaan = pimpinanPerusahaan::findorFail($id);
        // return view('guru.pimpinan.edit', ['pimpinanPerusahaan' => $pimpinanPerusahaan]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        //    dd($request);
        $flight = pimpinanPerusahaan::findorFail($request->id);
        $flight->nama = $request->nama;
        $flight->nip = $request->nip;
        $flight->jenkel = $request->jenkel;
        $flight->pangkat = $request->pangkat;
        $flight->id_perusahaan = $request->id_perusahaan;

        $flight->save();

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/detail-pimpinan/'.$request->id);
    }

    public function detail($id)
    {
        $p = PimpinanPerusahaan::findorFail($id);
        // $pimpinanPerusahaan = PimpinanPerusahaan::all();
        return view('guru.pimpinan.detail', ['pimpinanPerusahaan' => $p]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $pimpinanPerusahaan = pimpinanPerusahaan::findorFail($request->id)->delete();

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();
        // $pimpinanPerusahaan = pimpinanPerusahaan::paginate(15);
        return redirect('/guru/show-pimpinan');
    }

    public function delete()
    {
        $pimpinanPerusahaan = PimpinanPerusahaan::all();
        // $keyword = $request->keyword;

        // $pimpinanPerusahaan = PimpinanPerusahaan::Where('nama', 'LIKE', '%' . $keyword . '%')
        // ->orWhere('nip', 'LIKE', '%' . $keyword . '%')
        // ->orWhere('jenkel', 'LIKE', '%' . $keyword . '%')
        // ->orWhere('pangkat', 'LIKE', '%' . $keyword . '%')
        // ->orWhereHas('perusahaan', function($query) use($keyword){
        //     $query->where('nama', 'LIKE', '%' . $keyword . '%');
        // })
        // ->orderBy('nama')
        // ->paginate(15);
        return view('guru.pimpinan.delete', ['pimpinanPerusahaan' => $pimpinanPerusahaan]);
    }
}
