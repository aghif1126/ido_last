<?php

namespace App\Http\Controllers;

use PDF;
use DateTime;
use DateTimeZone;
use Carbon\Carbon;
use App\Models\Role;
use App\Models\Guru;
use App\Models\User;
use App\Models\Absen;
use App\Models\Kelas;
use App\Models\Nilai;
use App\Models\Siswa;
use App\Models\Catatan;
use App\Models\Jurusan;
use App\Models\Kriteria;
use App\Models\Aktivitas;
use App\Models\Perusahaan;
use App\Models\SiswaPerusahaan;
use App\Models\KonsentrasiKeahlian;
use App\Models\TahunPelajaran;
use App\Models\ActivityLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $siswa = Siswa::where('id_user', Auth::user()->id) -> first();

        // dd($siswa -> alamat);
        if ($siswa -> alamat == "Tidak tersedia") {
            return redirect('/siswa/form');
        }
        elseif ($siswa -> kelas == "Tidak tersedia") {
            return redirect('/siswa/form');
        }
        elseif ($siswa -> jenkel == "Tidak tersedia") {
            return redirect('/siswa/form');
        }
        elseif ($siswa -> telp == "Tidak tersedia") {
            return redirect('/siswa/form');
        }
        elseif ($siswa -> tanggal_lahir == "Tidak tersedia") {
            return redirect('/siswa/form');
        }
        elseif ($siswa -> nama_ortu == "Tidak tersedia") {
            return redirect('/siswa/form');
        }
        elseif ($siswa -> alamat_ortu == "Tidak tersedia") {
            return redirect('/siswa/form');
        }
        elseif ($siswa -> telp_ortu == "Tidak tersedia") {
            return redirect('/siswa/form');
        }
        else {

        $perusahaan = SiswaPerusahaan::where('id_siswa', $siswa -> nis)-> where('periode', 'LIKE', '%'.'Desember'.'%')->first();

        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni", "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
        $tanggal = date('Y-m-d');
        $month = 12;

        $tahun = 2023;
        $sekarang = $bulan[$month];

            return view('siswa.dashboard', ['perusahaan' => $perusahaan ,'siswa' => $siswa, 'sekarang' => $sekarang, 'tahun' => $tahun, 'bulan' => $bulan]);

        }
    }

    public function form(Request $request)
    {
        $siswa = Siswa::where('id_user', Auth::user()->id) -> first();
        $kelas = Kelas::all();
        return view('siswa.form', ['siswa' => $siswa, 'kelas' =>$kelas]);
    }

    public function profile()
    {
        $siswa = Siswa::where('id_user', Auth::user()->id)->first();
        $user = User::where('id', $siswa->id_user)->first();

        // dd($user);
        // $siswa = Siswa::findorFail($id);
        // dd($siswa -> perusahaan);
        $perusahaan = Perusahaan::all();
        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni", "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
        $tanggal = date('Y-m-d');
        // $user = User::where('id', Auth::user()->id);

        // $month = intval(explode('-', $tanggal)[1]);
        $month = 12;
        // $tahun = intval(explode('-', $tanggal)[0]);
        $tahun = 2023;
        $sekarang = $bulan[$month];
        // dd($sekarang);

        return view('siswa.profile', ['siswa' => $siswa, 'sekarang' => $sekarang, 'tahun' => $tahun, 'bulan' => $bulan, 'user' => $user]);
    }

    public function aktivitas()
    {
        $siswa = Siswa::where('id_user', Auth::user()->id)->first();
        // dd($siswa->perusahaan->first()->id);
        $perusahaan = SiswaPerusahaan::where('id_siswa', $siswa -> nis)-> where('periode', 'LIKE', '%'.'Desember'.'%')->get();
        // dd($perusahaan[0] -> detail);
        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni",
        "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");

        $tanggal = date('Y-m-d');
        // $tanggal = "-";

        $tz = 'Asia/Jakarta';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
        $jam = $dt->format('H:i:s');
        $tes = [$tanggal, $jam];
        $wm = implode(' ', $tes);

        // dd(implode(' ', $tes));

        // $month = intval(explode('-', $tanggal)[1]);
        $month = 12;
        // $tahun = intval(explode('-', $tanggal)[0]);
        $tahun = 2023;
        $sekarang = $bulan[$month];

        $absen = $siswa -> absen -> where('tanggal', $tanggal)-> first();
        $status = $siswa->absen->where('status', 'Sakit')->first();
        $status1 = $siswa->absen->where('status', 'Izin')->first();
        // dd($absen);
        return view('siswa.aktivitas', ['siswa' => $siswa, 'sekarang' => $sekarang,
        'tahun' => $tahun, 'bulan' => $bulan, 'perusahaan' => $perusahaan -> first(), 'absen' => $absen, 'status' => $status, 'status1' => $status1, 'tanggal' => $tanggal, 'wm' => $wm]);
    }

    public function absen(Request $request){
        // dd($request);
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        $tanggal = date('Y-m-d');
        $jam = date('H:i:s');
        // $jam[1] = intval($jam[1]) + 7;
        // dd($tanggal);
        // dd($jam);
        $tz = 'Asia/Jakarta';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
        $jam = $dt->format('H:i:s');
        // dd($jam);

        // dd();
        $daftar_bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni",
        "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
        $bulan = $daftar_bulan[explode("-", $tanggal)[1]];
        // dd($bulan);


        if ($request -> status == "Hadir") {

            $tgl = intval(explode('-', $tanggal)[2]);
            $img = $request->photo;
            $folderPath = "uploads";

            $image_parts = explode(";base64,", $img);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        // dd($image_type_aux);

        $image_base64 = base64_decode($image_parts[1]);
        $fileName = uniqid() . '.png';

        $file = $folderPath . $fileName;
        // dd($fileName);
        Storage::disk('public_uploads_absen')->put($file, $image_base64);

        // dd('Image uploaded successfully: '.$fileName);
        Absen::create([
            'status' => $request->status,
            'tanggal' => $tanggal,
            'bulan' => $bulan,
            'jam_masuk' => $jam,
            'id_siswa' => $request->id_siswa,
            'poto_masuk' => $fileName,
            'id_tahun' => $tahun -> id,
            'tgl' => $tgl,
            'id_perusahaan' => $request -> id_perusahaan,
        ]);
    } else{
        $tgl = intval(explode('-', $tanggal)[2]);
        Absen::create([
            'status' => $request->status,
            'tanggal' => $tanggal,
            'bulan' => $bulan,
            'jam_masuk' => $jam,
            'id_siswa' => $request->id_siswa,
            'poto_masuk' => '',
            'id_tahun' => $tahun -> id,
            // 'photo' => $fileName
            'tgl' => $tgl,
            'id_perusahaan' => $request -> id_perusahaan,
        ]);
    }

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/siswa/aktivitas');
    }

    public function absenpulang(Request $request){
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $tanggal = date('Y-m-d');
        $jam = date('H:i:s');
        // $jam[1] = intval($jam[1]) + 7;
        // dd($tanggal);
        // dd($jam);
        $tz = 'Asia/Jakarta';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
        $jam = $dt->format('H:i:s');
        // dd($jam);
        $siswa = Absen::where('tanggal', $tanggal)->where('id_siswa', $request->id_siswa)->first();

        // dd($siswa);

        $img = $request->photo;
        $folderPath = "uploads";

        $image_parts = explode(";base64,", $img);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        // dd($image_type_aux);

        $image_base64 = base64_decode($image_parts[1]);
        $fileName = uniqid() . '.png';

        $file = $folderPath . $fileName;
        // dd($fileName);
        Storage::disk('public_uploads_absenpulang')->put($file, $image_base64);

        // $siswa->jam_pulang = $jam;
        // $siswa -> poto_pulang = $fileName;
        $siswa -> update([
            'jam_pulang' => $jam,
            'poto_pulang' => $fileName
        ]);

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/siswa/aktivitas');
    }

    public function isi_aktivitas(Request $request)
    {
        // dd($request);
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        Aktivitas::create([
            'id_siswa' => $request -> id_siswa,
            'id_perusahaan' => $request -> id_perusahaan,
            'tanggal' => $request ->tanggal,
            'kegiatan' => $request ->kegiatan,
            'mulai' => $request -> mulai,
            'selesai' => $request -> selesai,
            'id_tahun' => $tahun -> id
        ]);

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/siswa/dashboard');
    }
    public function riwayat()
    {
        $siswa = Siswa::where('id_user', Auth::user()->id)->first();
        // $perusahaan = SiswaPerusahaan::where('id_siswa', $siswa -> nis)-> where('periode', 'LIKE', '%'.'Januari'.'%')->first();
        $riwayat = Aktivitas::where('id_siswa' , $siswa -> nis)->get();
        // dd($riwayat);
        $absen = Absen::where('id_siswa', $siswa -> nis) -> get();
        // dd($perusahaan);
        // $date = ($riwayat[0]['tanggal']);
        // $newDate = date('l', strtotime($date));
        // dd($newDate);

        // echo $riwayat[0];
        foreach ($riwayat as $r ) {
            $r -> hari = date('l', strtotime($r -> tanggal));
        }
        // dd($absen[0]);
        return view('siswa.riwayat', ['riwayat' => $riwayat, 'siswa' => $siswa], compact('absen'));
    }

    public function perusahaan()
    {
        $siswa = Siswa::where('id_user', Auth::user()->id)->first();
        $perusahaan = SiswaPerusahaan::where('id_siswa', $siswa -> nis)
            ->where('status', 'diterima')
            ->where('periode', 'LIKE', '%'.'Januari'.'%')
            ->orWhere('periode', 'LIKE', '%'.'Februari'.'%')
            ->orWhere('periode', 'LIKE', '%'.'Maret'.'%')
            ->orWhere('periode', 'LIKE', '%'.'April'.'%')
            ->orWhere('periode', 'LIKE', '%'.'Mei'.'%')
            ->orWhere('periode', 'LIKE', '%'.'Juni'.'%')
            ->orWhere('periode', 'LIKE', '%'.'Juli'.'%')
            ->orWhere('periode', 'LIKE', '%'.'Agustus'.'%')
            ->orWhere('periode', 'LIKE', '%'.'September'.'%')
            ->orWhere('periode', 'LIKE', '%'.'October'.'%')
            ->orWhere('periode', 'LIKE', '%'.'November'.'%')
            ->orWhere('periode', 'LIKE', '%'.'Desember'.'%')
            ->get();
        // dd($perusahaan);
        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni",
        "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
        // dd($perusahaan);
        // dd($siswa -> perusahaan -> first() -> detail -> pembimbing);

        return view('siswa.perusahaan', ['siswa' => $siswa, 'perusahaan' => $perusahaan -> first(), 'bulan' => $bulan]);
    }
    public function nilai()
    {
        $siswa = Siswa::where('id_user', Auth::user()->id)->first();
        // dd($siswa -> perusahaan -> where('periode', 'LIKE','%'. 'Januari'.'%') -> all());
        $perusahaan = SiswaPerusahaan::where('id_siswa', $siswa -> nis)-> where('periode', 'LIKE', '%'.'Oktober'.'%')->get();
        // dd($perusahaan[0] -> detail -> pembimbing ->  );
        // if ($perusahaan != "[]") {
        //     $nilai = $siswa -> nilai -> where('id_perusahaan', $perusahaan -> first() -> id_perusahaan);
        //     return view('siswa.nilai', ['siswa' => $siswa, 'perusahaan' => $perusahaan[0], 'nilai' => $nilai]);
        // }else{
        //     $nilai = null;
        // }
        // $kriteria = [];
        // dd($perusahaan)
        if ($perusahaan) {
            $kriteria = Kriteria::all();
            // dd($a);
        }else{
            $kriteria = null;
        }
    // dd($kriteria);
        // $kriteria = Kriteria::where('id_pembimbing', $perusahaan -> detail ->  pembimbing -> id) -> get();
        return view('siswa.nilai', ['siswa' => $siswa, 'perusahaan' => $perusahaan, 'nilai' => $siswa -> nilai], compact('kriteria'));

        // dd($nilai);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $guru = $tahun -> guru -> all();
        $perusahaan = $tahun -> perusahaan -> all();
        $kelas = $tahun -> kelas -> all();
        return view('guru.siswa.add-siswa', ['guru' => $guru, 'perusahaan' => $perusahaan, 'kelas' => $kelas]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif') -> first();
        $id = User::max('id')+1;
        $siswa = Siswa::create([
            'nis' => $request -> nis,
            'nama' => $request -> nama,
            'jenkel' => $request -> jenkel,
            'id_kelas' => $request -> id_kelas,
            'id_tahun' => $tahun -> id,
            // 'id_user' => $id
        ]);
        // dd($siswa);
        $siswa -> id_user = $id;
        $siswa -> save();

        // $siswa = Siswa::wherenull('id_user')->first();
        // $siswa -> nis_user = $id;

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();


        return redirect('guru/show-siswa');
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
         // dd($request -> tahun);
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $kelas = $tahun_pelajaran -> kelas;
            $siswa = $tahun_pelajaran -> siswa;
            $tahun = TahunPelajaran::get();

            $sudah = 0;
            foreach ($siswa as $item) {
                if ($item ->perusahaan != "[]") {
                    // echo $item -> perusahaan;
                   $sudah += 1;
                }
            }
            $belum = $siswa -> count() - $sudah;
            $jumlah_siswa = $siswa -> count();
            $confirm = 0;
            $tunggu = 0;
            foreach ($siswa as $item) {
                if ($item ->perusahaan != "[]") {
                    // echo $item -> perusahaan;
                    $perusahaan = $item -> perusahaan -> all();
                    // dd(count($perusahaan));
                    foreach ($perusahaan as $value) {
                        if ($value -> confirm == "Sudah") {
                            $confirm += 1;
                        }else{
                            $tunggu += 1;
                        }
                    }
                }
            }
            foreach ($kelas as $item ) {
                $item -> sudah = 0;
                $item -> belum = 0;
                $item -> confirm = 0;
                $item -> tunggu = 0;
            }


            foreach ($kelas as $kls) {

                $kls -> siswa = $siswa -> where('id_kelas', $kls->id);
                // dd($kls);
                foreach ($kls -> siswa as $item) {
                    if ($item ->perusahaan != "[]") {
                       $kls -> sudah += 1;
                    }else{
                        $kls -> belum += 1;
                    }
                }
                foreach ($kls -> siswa as $item) {
                    if ($item ->perusahaan != "[]") {
                        // echo $item -> perusahaan;
                        $perusahaan = $item -> perusahaan -> all();
                        // dd(count($perusahaan));
                        foreach ($perusahaan as $value) {
                            if ($value -> confirm == "Sudah") {
                                $kls -> confirm  += 1;
                            }else{
                                $kls -> tunggu += 1;
                            }
                        }
                    }
                }
                // $kls -> confirm = $siswa -> where('id_kelas', $kls->id)->where('confirm', 'Sudah');
                // $kls -> tunggu = $siswa -> where('id_kelas', $kls->id)->Where('confirm', 'Belum');
                $kls -> jumlah_siswa = $siswa -> where('id_kelas', $kls->id) -> count();
            }

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $kelas = $tahun_pelajaran -> kelas;
            $siswa = $tahun_pelajaran -> siswa;
            $tahun = TahunPelajaran::get();

            $sudah = 0;
            foreach ($siswa as $item) {
                if ($item ->perusahaan != "[]") {
                    // echo $item -> perusahaan;
                   $sudah += 1;
                }
            }
            $belum = $siswa -> count() - $sudah;
            $jumlah_siswa = count($siswa);
            $confirm = 0;
            $tunggu = 0;
            foreach ($siswa as $item) {
                if ($item ->perusahaan != "[]") {
                    // echo $item -> perusahaan;
                    $perusahaan = $item -> perusahaan -> all();
                    // dd(count($perusahaan));
                    foreach ($perusahaan as $value) {
                        if ($value -> confirm == "Sudah") {
                            $confirm += 1;
                        }else{
                            $tunggu += 1;
                        }
                    }
                }
            }
            foreach ($kelas as $item ) {
                $item -> sudah = 0;
                $item -> belum = 0;
                $item -> confirm = 0;
                $item -> tunggu = 0;
            }


            foreach ($kelas as $kls) {

                $kls -> siswa = $siswa -> where('id_kelas', $kls->id);
                // dd($kls);
                foreach ($kls -> siswa as $item) {
                    if ($item ->perusahaan != "[]") {
                       $kls -> sudah += 1;
                    }else{
                        $kls -> belum += 1;
                    }
                }
                foreach ($kls -> siswa as $item) {
                    if ($item ->perusahaan != "[]") {
                        // echo $item -> perusahaan;
                        $perusahaan = $item -> perusahaan -> all();
                        // dd(count($perusahaan));
                        foreach ($perusahaan as $value) {
                            if ($value -> confirm == "Sudah") {
                                $kls -> confirm  += 1;
                            }else{
                                $kls -> tunggu += 1;
                            }
                        }
                    }
                }
                // $kls -> confirm = $siswa -> where('id_kelas', $kls->id)->where('confirm', 'Sudah');
                // $kls -> tunggu = $siswa -> where('id_kelas', $kls->id)->Where('confirm', 'Belum');
                $kls -> jumlah_siswa = $siswa -> where('id_kelas', $kls->id) -> count();
            }
        }
        // dd($siswa ->where('id_kelas', 2)-> max());
        return view('guru.siswa.show-siswa', [
        'sudah' => $sudah,
        'belum' => $belum,
        'jumlah' => $jumlah_siswa,
        'kelas' => $kelas,
        'confirm' => $confirm,
        'tahun' => $tahun,
        'tahun_pelajaran' => $tahun_pelajaran,
        'tunggu' => $tunggu,
        'siswa' => $siswa
    ]);
    }

    public function detail($id)
    {
        $siswa = Siswa::findorFail($id);
        // dd($siswa -> perusahaan);
        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni", "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
        $tanggal = date('Y-m-d');
        $all_nilai = [];

        // $catatan = Catatan::where('id_siswa', $siswa -> nis)->whereNotNull('id_perusahaan')->orderby('id_perusahaan')->get();
        // $all_perusahaan = $siswa -> perusahaan;
        // dd($all_perusahaan);
        // $catatan =[];
        $absen = $siswa->absen;
        $riwayat = $siswa->aktivitas;

        // echo $riwayat[0];
        foreach ($absen as $a ) {
            $a -> hari = date('l', strtotime($a -> tanggal));
        }

        // $month = intval(explode('- ', $tanggal)[1]);
        $month = 12;
        // $tahun = intval(explode('-', $tanggal)[0]);
        $tahun = 2023;
        $sekarang = $bulan[$month];
        $kriteria = Kriteria::all();
        // dd($siswa -> catatan);
        return view('guru.siswa.detail-siswa', ['siswa' => $siswa, 'bulan' => $bulan,
        'sekarang' => $sekarang, 'tahun' => $tahun, 'kriteria' => $kriteria, 'nilai' => $siswa -> nilai, 'catatan' => $siswa -> catatan, 'riwayat' => $riwayat, 'absen' => $absen
    ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $siswa = Siswa::findorFail($id);
        $guru = Guru::all();
        $perusahaan = Perusahaan::all();
        $kelas = Kelas::all();
        $jurusan = Jurusan::all();
        $konsentrasi = KonsentrasiKeahlian::all();
        // dd($siswa -> guru);
        return view('guru.siswa.edit-siswa', [
            'siswa' => $siswa,
            'guru' => $guru,
            'perusahaan' => $perusahaan,
            'kelas' => $kelas,
            'jurusan' => $jurusan,
            'konsentrasi' => $konsentrasi

        ]);

    }

    public function ubah(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $flight = Siswa::find($request->nis);

        $flight->nis = $request->nis;
        $flight->nama = $request->nama;
        $flight->id_kelas = $request->kelas;
        $flight->telp = $request->telp;
        $flight->jenkel = $request->jenkel;
        $flight->alamat = $request->alamat;
        $flight->tanggal_lahir = $request->tanggal_lahir;
        // $flight->periode = $request->periode;


        $flight->save();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/detail-siswa/'. $request -> nis);

    }

    /**
     * Update the specified resource in storage.
     */




    public function editpassword(Request $request){
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($request);
        // $siswa = Siswa::where('id_user', Auth::user()->id)->first();
        $user = User::where('id', $request->id)->first();
        // $user->name = $request->name;
        $user->password = password_hash($request->password, PASSWORD_DEFAULT);
        $user->pass = $request->password;
        // $user->email = $request->email;
        // $user->role_id = $request->role_id;

        $user->save();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/logout');
    }





    public function poto(Request $request, Siswa $siswa){
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $foto_file = $request->file('foto');
        // $foto_ekstensi = $foto_file->extension();
        $foto_nama = date('ymdhis').'.'.$foto_file->extension();
        $foto_file->move(public_path('uploads/profil'), $foto_nama);
        $flight = Siswa::find($request->nis);
        // dd($flight);

        $flight->foto = $foto_nama;
        $flight->save();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/siswa/dashboard');
    }

    public function update(Request $request, Siswa $siswa)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($request);
        $flight = Siswa::find($request->nis);

        // $flight->semester = $request->semester;
        // $flight->tahun_pelajaran = $request->tahun_pelajaran;
        // $flight->periode = $request->periode;
        // $flight->foto = $foto_nama;
        // $flight->foto = $request->foto;

        $flight->telp = $request->telp;
        $flight->alamat = $request->alamat;
        $flight->tanggal_lahir = $request->tanggal_lahir;
        $flight->alamat_ortu = $request->alamat_ortu;
        $flight->telp_ortu = $request->telp_ortu;
        $flight->nama_ortu = $request->nama_ortu;

        $flight->save();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect(route('siswa-index'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete()
    {
        // $keyword = $request->keyword;

        $profil = Siswa::all();

        // $profil = Siswa::where('nama', 'LIKE', '%' . $keyword . '%')
        //     ->orWhereHas('kelas', function($query) use($keyword){
        //         $query->where('nama', 'LIKE', '%' . $keyword . '%');
        //     })
        //     ->orWhere('jenkel', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('alamat', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('semester', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('tahun_pelajaran', 'LIKE', '%' . $keyword . '%')

        //     ->orderBy('nama')
        //     ->paginate(15);

        return view('guru.siswa.delete-siswa', ['siswa' => $profil]);
    }

    public function destroy(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($request->id);
        Nilai::where('id_siswa', $request -> nis) -> delete();
        Siswa::findorFail($request->nis)->delete();
        $profil = Siswa::all();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return view('guru.siswa.delete-siswa', ['siswa' => $profil]);
    }

    public function kelas(){
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $kelas = $tahun -> kelas -> all();

        return view('guru.siswa.pilih-kelas', ['kelas' => $kelas]);
    }

    // public function generatepdf(){
    //     $riwayat = Aktivitas::paginate(15);
    //     $siswa = Siswa::paginate(15);

    //     $pdf = PDF::loadView('siswa.riwayat', array('riwayat'=>$riwayat, 'siswa'=>$siswa));

    //     return $pdf->download('Laporan_Kegiatan.pdf');
    // }

    // public function absen(){

    // }


    // public function ubahpassword(Request $request){
    //     $user = DB::table('users')->where('id', $request->id)->update([
    //         'password' => $request->password
    //     ]);
    //     return redirect('/siswa/dashboard', compact('user'));
    // }
}
