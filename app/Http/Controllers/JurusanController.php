<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\Jurusan;
use Illuminate\Http\Request;
use App\Models\TahunPelajaran;
use App\Models\KonsentrasiKeahlian;
use App\Models\ActivityLog;

class JurusanController extends Controller
{
    public function create()
    {
        $role = Guru::Where('status', 'kaprog')->get();
        return view('guru.jurusan.add', ['kaprog' => $role]);
    }

    public function store(Request $request)
    {
        $tahun =TahunPelajaran::where('status', 'aktif')->first();

        Jurusan::create([
            'nama' => $request['nama'],
            'id_tahun' => $tahun -> id
        ]);

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        // jurusan::create($request->except('_token'));
        return redirect('guru/show-jurusan');
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $jurusan = $tahun_pelajaran -> jurusan;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $jurusan = $tahun_pelajaran -> jurusan;
            $tahun = TahunPelajaran::get();

        }

        return view('guru.jurusan.show', ['jurusan' => $jurusan, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $jurusan = Jurusan::findorFail($id);
        $kaprog = Guru::Where('status', 'kaprog')->get();
        return view('guru.jurusan.edit', ['jurusan' => $jurusan, 'kaprog' => $kaprog]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        // dd($request);
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $flight = Jurusan::findorFail($request->id);
        $flight->nama = $request->nama;
        $flight->id_guru = $request->id_kaprog;
        $flight->save();

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/detail-jurusan/'.$request->id);
    }

    /**
     * Remove the specified resource from storage.
     */
    // public function destroy(Request $request)
    // {
    //     $jurusan = Jurusan::findorFail($request->id)->delete();
    //     // $jurusan = jurusan::paginate(15);
    //     return redirect('/guru/show-jurusan');
    // }
    // public function delete()
    // {
    //     $jurusan = Jurusan::paginate(15);
    //     return view('pembimbing.delete', ['jurusan' => $jurusan]);
    // }
    public function detail($id)
    {
        $jurusan = Jurusan::findorFail($id);
        $konsentrasi = KonsentrasiKeahlian::all()->where('id_jurusan', $id);
        // dd($jurusan -> kaprog);
        return view('guru.jurusan.detail', ['jurusan' => $jurusan, 'id' => $id, 'konsentrasi' => $konsentrasi]);
    }
}