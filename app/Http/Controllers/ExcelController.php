<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Absen;
use App\Models\Siswa;
use App\Models\Perusahaan;
use App\Imports\GuruImport;
use App\Imports\ImportGuru;
use App\Models\ActivityLog;
use App\Exports\ExportSiswa;
use App\Imports\ImportSiswa;
use Illuminate\Http\Request;
use App\Models\TahunPelajaran;
use App\Imports\ImportPembimbing;
use App\Imports\ImportPerusahaan;
use App\Models\JurusanPerusahaan;
use App\Models\PimpinanPerusahaan;
use App\Models\PembimbingPerusahaan;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Response;
use App\Imports\ImportPimpinanPerusahaan;

class ExcelController extends Controller
{
    public function import(Request $request)
    {

        $tahun = TahunPelajaran::where('status', 'aktif') -> first();
        // dd($tahun->id);
        Excel::import(new ImportSiswa, $request->file('file'));
        $siswa = Siswa::where('id_kelas', null)->get();
        // dd($siswa);
        foreach ($siswa as $item) {

            $item -> id_user = User::where('email', $item -> nis) -> first() -> id;
            $item -> id_kelas = $request -> id_kelas;
            $item -> id_tahun = $tahun-> id;
            $item -> save();
         }

         $activitylog = ActivityLog::all()->last();

         $activitylog->ip = $request->getClientIp();
         $activitylog->id_tahun = $tahun->id;

         $activitylog->save();

        return redirect('/guru/show-siswa')->with('success', 'All good!');
    }

    public function export(){
        return Excel::download(new ExportSiswa, 'absen.xlsx');
    }

    public function format(){
        // dd('d');
        $path = public_path('template_import_siswa.xlsx');
        $fileName = 'template_import_siswa.xlsx';

        // return Response::download($path, $fileName, ['Content-Type: xlsx']);
    }
    public function format_perusahaan(){
        // dd('d');
        $path = public_path('template_import_perusahaan.xlsx');
        $fileName = 'template_import_perusahaan.xlsx';

        return Response::download($path, $fileName, ['Content-Type: xlsx']);
    }
    public function format_guru(){
        // dd('d');
        $path = public_path('template_import_guru.xlsx');
        $fileName = 'template_import_guru.xlsx';


        return Response::download($path, $fileName, ['Content-Type: xlsx']);
    }

    public function import_perusahaan(Request $request)
    {
        // dd($request);
        $tahun = TahunPelajaran::where('status', 'aktif') -> first();
        $max_id_perusahaan = $tahun -> perusahaan -> max('id');
        if($max_id_perusahaan){
            $max_id_perusahaan += 1;
        }else{
            $max_id_perusahaan = 1;

        }
        // dd($max_id_perusahaan);
        Excel::import(new ImportPerusahaan, $request->file('file'), $request -> id_jurusan);
        $new_id_perusahaan = $tahun -> perusahaan -> max('id');

        // dd($request -> file('file'));
        Excel::import(new ImportPembimbing, $request->file('file'));
        $pembimbing = PembimbingPerusahaan::whereNull('id_perusahaan')->get();
        // dd($pembimbing);
        foreach ($pembimbing as $item) {
            $perusahaan = Perusahaan::find($max_id_perusahaan);
            $item -> id_perusahaan = $max_id_perusahaan;
            $item -> id_jurusan = $request -> id_jurusan;
            $item -> save();
            JurusanPerusahaan::create([
                'kuota' => $perusahaan -> kuota,
                'id_perusahaan' => $max_id_perusahaan,
                'id_jurusan' => $request -> id_jurusan,
                'id_tahun' => $tahun -> id
            ]);
            User::create([
                'id_tahun' => $tahun -> id,
                'name' => $item -> nama,
                'email' => $item -> nip,
                'password' => password_hash('pembimbing', PASSWORD_DEFAULT),
                'pass' => 'pembimbing',
                'role_id' => '2'
            ]);
            $item -> id_user = User::where('email', $item -> nip) -> first() -> id;
            $item -> save();
            $max_id_perusahaan += 1;
        }
        $max_id_perusahaan = $tahun -> perusahaan -> max('id') + 1;
        // dd($max_id_perusahaan);
        Excel::import(new ImportPimpinanPerusahaan, $request->file('file'));
        $pimpinan = PimpinanPerusahaan::whereNull('id_perusahaan')->get();
        // dd($pimpinan);
        foreach ($pimpinan as $item) {
            $item -> id_perusahaan = $max_id_perusahaan;
            $item -> save();
            $max_id_perusahaan += 1;
        }

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();
        return redirect()->back();
    }
    public function import_guru(Request $request)
    {
        // dd($request -> file('file'));
        $tahun = TahunPelajaran::where('status', 'aktif') -> first();

        // dd($max_id_perusahaan);
        Excel::import(new GuruImport, $request->file('file'));
        // $new_id_perusahaan = $tahun -> perusahaan -> max('id');
        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();


        return redirect('/guru/show-guru')->with('success', 'All good!');
    }
}
