<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\ActivityLog;
use App\Models\User;
use App\Models\Perusahaan;
use Illuminate\Http\Request;
use App\Models\TahunPelajaran;
use App\Models\PembimbingPerusahaan;

class PembimbingController extends Controller
{
    // public function show(){
    //     $pembimbing = PembimbingPerusahaan::all();
    //     return view('guru.pembimbing.show', compact('pembimbing'));
    // }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */



    public function pilih_perusahaan()
    {
        $tahun = TahunPelajaran::where('status', 'aktif') -> first();
        $role = $tahun -> Perusahaan;
        return view('guru.pembimbing.pilih-perusahaan', ['perusahaan' => $role,]);
    }
    public function create(Request $request)
    {
        // dd($request);
        // $role = Perusahaan::all();
        $perusahaan = Perusahaan::find($request -> id_perusahaan);
        // dd($perusahaan -> pembimbing);
        return view('guru.pembimbing.add', [ 'perusahaan' => $perusahaan]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request);
        $tahun =TahunPelajaran::where('status', 'aktif')->first();

        User::create([
            'id_tahun' => $tahun -> id,
            'name' => $request -> nama,
            'email' => $request -> nip,
            'password' => password_hash('pembimbing', PASSWORD_DEFAULT),
            'pass' => 'pembimbing',
            'role_id' => '2'
        ]);
        $id_user = User::max('id');
        PembimbingPerusahaan::create([
            'nama' => $request -> nama,
            'nip' => $request -> nip,
            'alamat' => $request -> alamat,
            'telp' => $request -> telp,
            'jenkel' => $request -> jenkel,
            'id_perusahaan' => $request -> id_perusahaan,
            'id_jurusan' => $request -> id_jurusan,
            'id_tahun' => $tahun -> id,
            'id_user' => $id_user
        ]);

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('guru/show-pembimbing');
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $pembimbing = $tahun_pelajaran -> pembimbing -> all();
            $jurusan = $tahun_pelajaran -> jurusan;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $pembimbing = $tahun_pelajaran -> pembimbing -> all();
            $jurusan = $tahun_pelajaran -> jurusan;
            $tahun = TahunPelajaran::get();
            // dd($pembimbing[0] );

        }
        // $keyword = $request->keyword;
        // $pembimbing = PembimbingPerusahaan::Where('nama', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('nip', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('alamat', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('telp', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('jenkel', 'LIKE', '%' . $keyword . '%')
        //     ->orWhereHas('perusahaan', function($query) use($keyword){
        //         $query->where('nama', 'LIKE', '%' . $keyword . '%');
        //     })
        //     ->orderBy('nama')->paginate(15);

            return view('guru.pembimbing.show', ['jurusan' => $jurusan,'pembimbing'=>$pembimbing, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $role = Perusahaan::all();
        $perusahaan = Perusahaan::paginate(15);
        $pembimbing = PembimbingPerusahaan::findorFail($id);
        return view('guru.pembimbing.edit', ['pembimbing' => $pembimbing, 'role' => $role, 'perusahaan' => $perusahaan]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($request);
        $flight = PembimbingPerusahaan::findorFail($request->id);
        $flight->id = $request->id;
        $flight->nama = $request->nama;
        $flight->nip = $request->nip;
        $flight->alamat = $request->alamat;
        $flight->telp = $request->telp;
        $flight->jenkel = $request->jenkel;
        $flight->id_perusahaan = $request->id_perusahaan;
        $flight->id_user = $request->id_user;

        $flight->save();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/detail-pembimbing/'.$request->id);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $pembimbing = PembimbingPerusahaan::findorFail($request->id)->delete();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/show-pembimbing');

    }
    public function delete()
    {
        $pembimbing = PembimbingPerusahaan::all();
        // $keyword = $request->keyword;

        // $pembimbing = PembimbingPerusahaan::where('nama', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('nip', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('alamat', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('telp', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('jenkel', 'LIKE', '%' . $keyword . '%')
        //     ->orWhereHas('perusahaan', function($query) use($keyword){
        //         $query->where('nama', 'LIKE', '%' . $keyword . '%');
        //     })
        //     ->orderBy('nama')
        //     ->paginate(15);
        return view('guru.pembimbing.delete', ['pembimbing' => $pembimbing]);
    }
    public function detail($id)
    {
        $pembimbing = PembimbingPerusahaan::findorFail($id);

        return view("guru.pembimbing.detail")->with("pembimbing", $pembimbing);
    }
}
