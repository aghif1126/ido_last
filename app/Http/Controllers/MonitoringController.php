<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\Monitoring;
use App\Models\Perusahaan;
use Illuminate\Http\Request;
use App\Models\TahunPelajaran;
use App\Models\ViewMonitoring;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\PembimbingPerusahaan;

class MonitoringController extends Controller
{
    public function show(Request $request){
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            // $monitoring = $tahun_pelajaran -> monitoring;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            // $monitoring = $tahun_pelajaran -> monitoring;
            $tahun = TahunPelajaran::get();

        }
        $monitoring = ViewMonitoring::all();
        // dd($monitoring);
        $guru = Guru::all();
        $perusahaan = Perusahaan::all();

        return view('guru.monitoring.show-monitoring', ['monitoring' => $monitoring, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran, 'guru' =>$guru, 'perusahaan' => $perusahaan]);
    }
    public function downloadmonitoring(Request $request){
        // dd($request);
        $guru = Guru::find($request -> id_guru);
        $perusahaan = Perusahaan::findorfail($request -> id_perusahaan);
        $monitoring = Monitoring::where('id_perusahaan', $request->id_perusahaan)
        ->where('id_guru', $request -> id_guru)
        ->where('tanggal', $request -> tanggal)
        ->first();
        $pembimbing = $perusahaan->pembimbing->where('id_jurusan', $monitoring->id_jurusan)->first();
        // dd($guru -> id);
        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni",
        "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
        $hari = array(
            'Sun' => 'Minggu',
            'Mon' => 'Senin',
            'Tue' => 'Selasa',
            'Wed' => 'Rabu',
            'Thu' => 'Kamis',
            'Fri' => 'Jumat',
            'Sat' => 'Sabtu'
        );

        // dd($bulan[intval(explode('-', $monitoring->tanggal)[1])]);
        // dd($pembimbing);
        $pdf = PDF::loadView('pdf.monitoring', ['guru'=>$guru, 'pembimbing' => $pembimbing, 'monitoring'=>$monitoring,
         'bulan'=>$bulan, 'perusahaan' => $perusahaan, 'hari' => $hari])->setPaper('A4', 'portrait');

        return $pdf->stream('JURNAL_KEGIATAN_SISWA_PRAKTEK_KERJA_LAPANGAN_(PKL).pdf');
    }
}
