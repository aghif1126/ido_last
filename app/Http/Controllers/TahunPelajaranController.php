<?php

namespace App\Http\Controllers;

use App\Models\TahunPelajaran;
use App\Models\ActivityLog;
use Illuminate\Http\Request;

class TahunPelajaranController extends Controller
{
    /**
     * Display a listing of the resource.
     */

     public function index()
     {
     //
     }

     /**
      * Show the form for creating a new resource.
      */
     public function create()
     {
        //  $role = Perusahaan::all();
        //  $perusahaan = Perusahaan::paginate(15);
         return view('guru.tahun-pelajaran.add', []);


         // $role = TahunPelajaran::all();
         // $tahun-pelajaran = TahunPelajaran::all();
         // return view('guru.tahun-pelajaran.add');
     }

     /**
      * Store a newly created resource in storage.
      */
     public function store(Request $request)
     {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($request);
         TahunPelajaran::create($request->except('_token', 'submit'));

         $activitylog = ActivityLog::all()->last();

         $activitylog->ip = $request->getClientIp();
         $activitylog->id_tahun = $tahun->id;

         $activitylog->save();

         return redirect('guru/show-tahun-pelajaran');
     }

     /**
      * Display the specified resource.
      */
     public function show()
     {
         $tahun = TahunPelajaran::all();
         return view('guru.tahun-pelajaran.show', ['tahun' => $tahun]);
     }

     /**
      * Show the form for editing the specified resource.
      */
     public function edit($id)
     {
        //  $role = Perusahaan::all();
        //  $perusahaan = Perusahaan::paginate(15);
         $tahun = TahunPelajaran::findorFail($id);
         return view('guru.tahun-pelajaran.edit', ['tahun' => $tahun]);

         // $tahun = TahunPelajaran::findorFail($id);
         // return view('guru.tahun-pelajaran.edit', ['tahun' => $tahun]);
     }

     /**
      * Update the specified resource in storage.
      */
     public function update(Request $request)
     {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
         //    dd($request);
         $flight = TahunPelajaran::findorFail($request->id);
         $flight->tahun = $request->tahun;
         $flight->save();

         $activitylog = ActivityLog::all()->last();

         $activitylog->ip = $request->getClientIp();
         $activitylog->id_tahun = $tahun->id;

         $activitylog->save();

         return redirect('/guru/show-tahun-pelajaran/');
     }
     public function ganti_status($id)
     {
            // dd($request);
         $flight = TahunPelajaran::where('status', 'aktif')->first();
        //  dd($flight);
         $flight -> status = "tidak aktif";

         $flight->save();

         $tahun = TahunPelajaran::findorfail($id);
         $tahun -> status = 'aktif';
         $tahun -> save();
        //  dd($tahun);
        // $activitylog = ActivityLog::all()->last();

        // $activitylog->ip = $request->getClientIp();
        // $activitylog->id_tahun = $flight->id;

        // $activitylog->save();

         return redirect('/guru/show-tahun-pelajaran/');
     }

    //  public function detail($id)
    //  {
    //      $p = TahunPelajaran::findorFail($id);
    //      // $tahun = TahunPelajaran::all();
    //      return view('guru.tahun-pelajaran.detail', ['tahun' => $p]);
    //  }

     /**
      * Remove the specified resource from storage.
      */
     public function destroy(Request $request)
     {
        $tahun1 = TahunPelajaran::where('status', 'aktif')->first();

         $tahun = TahunPelajaran::findorFail($request->id)->delete();

         $activitylog = ActivityLog::all()->last();

         $activitylog->ip = $request->getClientIp();
         $activitylog->id_tahun = $tahun->id;

         $activitylog->save();

         return redirect('/guru/show-tahun-pelajaran');
     }

     public function delete()
     {
         $tahun = TahunPelajaran::all();

         return view('guru.tahun-pelajaran.delete', ['tahun' => $tahun]);
     }
}
