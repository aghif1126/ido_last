<?php

namespace App\Http\Controllers;

use App\Models\Kelas;

use App\Models\Siswa;
use App\Models\Jurusan;
use App\Models\Perusahaan;
use App\Models\ActivityLog;
use Illuminate\Http\Request;
use App\Models\TahunPelajaran;
use Illuminate\Support\Facades\DB;
use App\Models\KonsentrasiKeahlian;

class KelasController extends Controller
{
    public function create()
    {
        $role = Kelas::all();
        $jurusan = KonsentrasiKeahlian::all();
        return view('guru.kelas.add', ['jurusan' => $jurusan]);
    }

    public function store(Request $request)
    {
        $tahun =TahunPelajaran::where('status', 'aktif')->first();
        Kelas::create([
            'nama' => $request -> nama,
            'id_tahun' => $tahun -> id,
            'id_konsentrasi_keahlian' => $request -> id_konsentrasi_keahlian,
        ]);

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        // kelas::create($request->except('_token'));
        return redirect('guru/show-kelas');
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
             $kelas = DB::select(

                'CALL show_kelas('.$tahun_pelajaran -> id.')'

             );
            // $kelas = $tahun_pelajaran -> kelas;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $kelas = DB::select(

                'CALL show_kelas('.$tahun_pelajaran -> id.')'

             );
            $tahun = TahunPelajaran::get();

        }



            return view('guru.kelas.show', ['kelas'=>$kelas, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $kelas = Kelas::findorFail($id);
        $konsentrasi = KonsentrasiKeahlian::all();
        return view('guru.kelas.edit', ['kelas' => $kelas, 'konsentrasi' => $konsentrasi]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($request);
        $flight = Kelas::findorFail($request->id);
        $flight->nama = $request->nama;
        $flight->id_konsentrasi_keahlian = $request->id_konsentrasi_keahlian;
        $flight->save();

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/detail-kelas/'.$request->id);
    }

    /**
     * Remove the specified resource from storage.
     */
    // public function destroy(Request $request)
    // {
    //     $kelas = Kelas::findorFail($request->id)->delete();
    //     // $kelas = kelas::paginate(15);
    //     return redirect('/guru/show-kelas');

    // }
    // public function delete()
    // {
    //     $kelas = Kelas::all();
    //     $konsentrasi = KonsentrasiKeahlian::all();
    //     return view('guru.kelas.delete', ['kelas' => $kelas, 'konsentrasi' => $konsentrasi]);
    // }
    public function detail($id)
    {
        $kelas = Kelas::findorFail($id);
        $kelas -> sudah = 0;
        $kelas -> belum = 0;
        $kelas -> confirm = 0;
        $kelas -> tunggu = 0;
        $kelas -> siswa = Siswa::where('id_kelas', $kelas->id)-> get();
            // dd($kelas);
            foreach ($kelas -> siswa as $item) {
                if ($item ->perusahaan != "[]") {
                   $kelas -> sudah += 1;
                }else{
                    $kelas -> belum += 1;
                }
            }
            foreach ($kelas -> siswa as $item) {
                if ($item ->perusahaan != "[]") {
                    // echo $item -> perusahaan;
                    $perusahaan = $item -> perusahaan -> all();
                    // dd(count($perusahaan));
                    foreach ($perusahaan as $value) {
                        if ($value -> confirm == "Sudah") {
                            $kelas -> confirm  += 1;
                        }else{
                            $kelas -> tunggu += 1;
                        }
                    }
                }
            }
        // $siswa = Siswa::where('id_kelas', $kelas->id);
        // $sudah = Siswa::where('id_kelas', $kelas->id)->where('status','Sudah')->count();
        // $belum = Siswa::where('id_kelas', $kelas->id)->where('status','Belum')->count();

        return view('guru.kelas.detail', ['kelas' => $kelas, 'id' => $id, 'siswa' => $kelas -> siswa,
        'sudah' => $kelas -> sudah, 'belum' => $kelas -> belum
    ]);
    }

    public function siswa($id, Request $request)
    {
        // $contoh = TahunPelajaran::first()->kelas;
        // dd($contoh);
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            $kelas = $tahun_pelajaran -> kelas -> find($id);
            // dd($kelas);
            if (is_null($kelas)) {
                $siswa = $tahun_pelajaran -> siswa -> where('id_kelas', null);
            }else{
                $siswa = $tahun_pelajaran -> siswa -> where('id_kelas', $kelas->id);
            }
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $kelas = $tahun_pelajaran -> kelas -> find($id);
            if (is_null($kelas)) {
                $siswa = $tahun_pelajaran -> siswa -> where('id_kelas', null);
            }else{
                $siswa = $tahun_pelajaran -> siswa -> where('id_kelas', $kelas->id);
            }
            $tahun = TahunPelajaran::get();
        }

            $bulan['1'] = "Januari";
            $bulan['2'] = "Februari";
            $bulan['3'] = "Maret";
            $bulan['4'] = "April";
            $bulan['5'] = "Mei";
            $bulan['6'] = "Juni";
            $bulan['7'] = "Juli";
            $bulan['8'] = "Agustus";
            $bulan['9'] = "September";
            $bulan["10"] = "Oktober";
            $bulan["11"] = "November";
            $bulan["12"] = "Desember";


            // dd($bulan);
        // dd(explode('-', $list[0]->perusahaan[0]-> awal_periode ));
        return view('guru.kelas.siswa', ['kelas' => $kelas, 'id' => $id, 'siswa' => $siswa, 'bulan' => $bulan, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran]);
    }
}
