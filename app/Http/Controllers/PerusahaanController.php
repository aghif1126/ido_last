<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use App\Models\JurusanPerusahaan;
use App\Models\PembimbingPerusahaan;
use App\Models\Perusahaan;
use App\Models\PimpinanPerusahaan;
use App\Models\Siswa;
use App\Models\TahunPelajaran;
use App\Models\ActivityLog;
use Illuminate\Http\Request;

class PerusahaanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $jurusan = Jurusan::all();
        return view('guru.perusahaan.add', ['jurusan' => $jurusan]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request);
        $tahun =TahunPelajaran::where('status', 'aktif')->first();
        Perusahaan::create([
            'nama' => $request['nama'],
            'alamat' => $request['alamat'],
            'email' => $request['email'],
            'website' => $request['website'],
            'fax' => $request['fax'],
            'telp' => $request['telp'],
            'mou' => $request['mou'],
            'lokasi' => $request['lokasi'],
            'kuota' => $request['kuota'],
            'id_tahun' => $tahun -> id
        ]);
        $id_perusahaan = Perusahaan::max('id');
        // dd($id_perusahaan);

            JurusanPerusahaan::create([
                'id_perusahaan' => $id_perusahaan,
                'id_jurusan' => $request -> id_jurusan1,
                'id_tahun' => $tahun -> id,
                'kuota' => $request -> kuota
            ]);

        // if (($request -> id_jurusan2) != "Jurusan") {
        //     JurusanPerusahaan::create([
        //         'id_perusahaan' => $id_perusahaan,
        //         'id_jurusan' => $request -> id_jurusan2,
        //         'id_tahun' => $tahun -> id
        //     ]);
        // }
        // if (($request -> id_jurusan3) != "Jurusan") {
        //     JurusanPerusahaan::create([
        //         'id_perusahaan' => $id_perusahaan,
        //         'id_jurusan' => $request -> id_jurusan3,
        //         'id_tahun' => $tahun -> id
        //     ]);
        // }

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('guru/show-perusahaan');
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $perusahaan = $tahun_pelajaran -> perusahaan;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $perusahaan = $tahun_pelajaran -> perusahaan;
            $tahun = TahunPelajaran::get();
        }

        $jurusan = $tahun_pelajaran -> jurusan -> all();
        // dd();

            return view('guru.perusahaan.show', ['perusahaan'=>$perusahaan, 'tahun' => $tahun, 'tahun_pelajaran'=>$tahun_pelajaran, 'jurusan' => $jurusan   ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $perusahaan = Perusahaan::findorFail($id);
        return view('guru.perusahaan.edit', ['perusahaan' => $perusahaan, 'id' => $id]);
    }
    public function tambahjurusan($id)
    {
        $perusahaan = Perusahaan::findorFail($id);
        $list_jurusan = Jurusan::all();
        return view('guru.perusahaan.tambah-jurusan', ['perusahaan' => $perusahaan, 'jurusan' => $list_jurusan]);
    }
    public function storejurusan(Request $request)
    {
        // dd($request);
        $tahun =TahunPelajaran::where('status', 'aktif')->first();

        JurusanPerusahaan::create([
            'kuota' => $request -> kuota,
            'id_perusahaan' => $request -> id_perusahaan,
            'id_jurusan' => $request -> id_jurusan,
            'id_tahun' => $tahun -> id
        ]);

        return redirect('/guru/edit-perusahaan/'. $request -> id_perusahaan);
    }
    public function hapusjurusan($id)
    {
        $perusahaan = Perusahaan::findorFail($id);
        $list_jurusan = Jurusan::all();
        // dd($perusahaan -> jurusan[0] -> detailjurusan);
        return view('guru.perusahaan.delete-jurusan', ['perusahaan' => $perusahaan, 'jurusan' => $list_jurusan]);
    }
    public function destroyjurusan(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        JurusanPerusahaan::where('id_perusahaan', $request -> id_perusahaan)->where('id_jurusan', $request -> id_jurusan)->delete();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/edit-perusahaan/'.$request -> id_perusahaan.'');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($request);
        $flight = Perusahaan::findorFail($request->id);
        $flight->nama = $request->name;
        $flight->alamat = $request->alamat;
        $flight->email = $request->email;
        $flight->lokasi = $request->lokasi;
        $flight->fax = $request->fax;
        $flight->telp = $request->telp;
        $flight->website = $request->website;
        // $flight->kuota = $request->kuota;


        $flight->save();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/detail-perusahaan/'.$request->id);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        JurusanPerusahaan::where('id_perusahaan', $request->id)->delete();
        PembimbingPerusahaan::where('id_perusahaan', $request->id)->delete();
        PimpinanPerusahaan::where('id_perusahaan', $request->id)->delete();
        Perusahaan::findorFail($request->id)->delete();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        // $perusahaan = Perusahaan::paginate(15);
        return redirect('/guru/show-perusahaan');

    }
    public function delete()
    {
        $perusahaan = Perusahaan::all();
        return view('guru.perusahaan.delete', ['perusahaan' => $perusahaan]);
    }
    public function detail($id)
    {
        $perusahaan = Perusahaan::findorFail($id);
        $total = $perusahaan -> siswa -> count();
        return view('guru.perusahaan.detail', ['perusahaan' => $perusahaan,
        'id' => $id, 'siswa' => $perusahaan -> siswa,
        'total' => $total]);
    }
}
