<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Jurusan;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\KonsentrasiKeahlian;
use App\Models\TahunPelajaran;
use App\Models\ActivityLog;

class KonsentrasiController extends Controller
{
    public function create()
    {
        $role = Jurusan::all();
        return view('guru.konsentrasi.add', ['jurusan' => $role]);
    }

    public function store(Request $request)
    {
        $tahun =TahunPelajaran::where('status', 'aktif')->first();

        KonsentrasiKeahlian::create([
            'nama' => $request -> nama,
            'id_jurusan' => $request -> id_jurusan,
            'id_tahun' => $tahun -> id
        ]);

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        // konsentrasi::create($request->except('_token'));
        return redirect('guru/show-konsentrasi');
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $konsentrasi = $tahun_pelajaran -> konsentrasi;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $konsentrasi = $tahun_pelajaran -> konsentrasi;
            $tahun = TahunPelajaran::get();

        }

        return view('guru.konsentrasi.show', ['konsentrasi' => $konsentrasi, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $konsentrasi = KonsentrasiKeahlian::findorFail($id);
        $jurusan = Jurusan::all();
        return view('guru.konsentrasi.edit', ['konsentrasi' => $konsentrasi, 'jurusan' => $jurusan]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($request);
        $flight = KonsentrasiKeahlian::findorFail($request->id);
        $flight->nama = $request->nama;
        $flight->id_jurusan = $request->id_jurusan;
        $flight->save();

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/detail-konsentrasi/'.$request->id);
    }

    /**
     * Remove the specified resource from storage.
     */
    // public function destroy(Request $request)
    // {
    //     $konsentrasi = KonsentrasiKeahlian::findorFail($request->id)->delete();
    //     // $konsentrasi = konsentrasi::paginate(15);
    //     return redirect('/guru/show-konsentrasi');

    // }
    // public function delete()
    // {
    //     $konsentrasi = KonsentrasiKeahlian::paginate(15);
    //     return view('guru.konsentrasi.delete', ['konsentrasi' => $konsentrasi]);
    // }
    public function detail($id)
    {
        $konsentrasi = KonsentrasiKeahlian::findorFail($id);
        $kelas = Kelas::where('id_konsentrasi_keahlian', $id)->get();
        return view('guru.konsentrasi.detail', [
        'konsentrasi' => $konsentrasi,
        'id' => $id,
        'kelas' => $kelas
    ]);
    }
}