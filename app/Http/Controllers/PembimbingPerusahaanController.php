<?php

namespace App\Http\Controllers;

use App\Models\Absen;
use App\Models\Nilai;
use App\Models\Siswa;
use App\Models\Catatan;
use App\Models\Kriteria;
use App\Models\Aktivitas;
use App\Models\Perusahaan;
use Illuminate\Http\Request;
use App\Models\TahunPelajaran;
use App\Models\SiswaPerusahaan;
use Illuminate\Support\Facades\DB;
use App\Http\Middleware\Pembimbing;
use App\Models\PembimbingPerusahaan;
use App\Models\User;
use App\Models\ActivityLog;
use Illuminate\Support\Facades\Auth;

class PembimbingPerusahaanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $tanggal = date("Y-m")."-01";
        // $tanggal ;
        // $tanggal[2] ="01";
        // dd($tanggal);
        $data_pembimbing = PembimbingPerusahaan::all()->where('id_user' , Auth::user()->id)->first();
        // dd($data_pembimbing -> id_jurusan);
        $user = User::where('id', $data_pembimbing->id_user)->first();
        $siswa = SiswaPerusahaan::where('id_perusahaan', $data_pembimbing -> perusahaan -> id) -> where('confirm', 'Sudah')->where('id_jurusan', $data_pembimbing -> id_jurusan)-> get();
        // dd($siswa);
        $periode = [];
        foreach ($siswa as $item ) {
            $awal = intval(explode('-', $item -> awal_periode)[1]);
            $akhir = intval(explode('-', $item -> akhir_periode)[1]);
            $item -> periode = [];
            if ($akhir < $awal) {
                array_push($periode, $akhir);
                for ($i=$akhir; $i > 0; $i--) {
                    $akhir += 1;
                    array_push($periode, $akhir);
                }
                for ($i=1; $i <= $awal ; $i++) {
                    array_push($periode, $i);
                }
            }
            else{

                $selisih = $akhir - $awal + 1;

                for ($i=$awal; $i <= $selisih + $awal -1 ; $i++) {

                    array_push($periode, $i);
                }

            }

        }
        // dd($siswa);
        return view('pembimbing.dashboard', ['pembimbing' => $data_pembimbing, 'siswa' => $siswa, 'user' => $user]);
    }

    public function editpassword(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $user = User::where('id', $request->id)->first();
        $user->password = password_hash($request->password, PASSWORD_DEFAULT);
        $user->pass = $request->password;
        $user->save();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/logout');
    }

    public function catatan($id)
    {
        $siswa = Siswa::FindOrFail($id);
        $data_pembimbing = PembimbingPerusahaan::all()->where('id_user' , Auth::user()->id)->first();
        $catatan = Catatan::where('id_siswa', $id)->where('id_perusahaan', $data_pembimbing -> perusahaan -> id)->get();
        // dd($catatan);

        return view('pembimbing.catatan', ['siswa' => $siswa, 'catatan' => $catatan, 'pembimbing' =>  $data_pembimbing]);

    }

    public function addCatatan($id){
        $siswa = Siswa::findOrFail($id);
        $data_pembimbing = PembimbingPerusahaan::all()->where('id_user' , Auth::user()->id)->first();


        return view('pembimbing.add-catatan', ['siswa' => $siswa, 'id_perusahaan' => $data_pembimbing -> perusahaan -> id, 'pembimbing' => $data_pembimbing]);
    }

    public function storeCatatan(Request $request){
        // dd($request->id_perusahaan);
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        Catatan::create([
            'id_siswa' => $request->id_siswa,
            'id_perusahaan' => $request->id_perusahaan,
            'catatan' => $request->catatan,
            'tanggal' => $request->tanggal,
            'id_tahun' => $tahun -> id
        ]);

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();
        // Catatan::create($request->except('_token', 'submit'));

        return redirect('/pembimbing/catatan/'.$request -> id_siswa);
    }

    public function editCatatan($id){
        $catatan = Catatan::Find($id);
        // dd($catatan);
        $data_pembimbing = PembimbingPerusahaan::all()->where('id_user' , Auth::user()->id)->first();


        return view('pembimbing.edit-catatan', ['catatan' => $catatan, 'pembimbing' => $data_pembimbing]);
    }

    public function updateCatatan(Request $request){
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $catatan = Catatan::Find($request->id);

        $catatan->catatan = $request->catatan;
        $catatan->tanggal = $request->tanggal;

        $catatan->save();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/pembimbing/dashboard');
    }

    public function deleteCatatan($id){
        $siswa = Siswa::FindOrFail($id);
        $data_pembimbing = PembimbingPerusahaan::all()->where('id_user' , Auth::user()->id)->first();
        $catatan = Catatan::where('id_siswa', $id)->where('id_perusahaan', $data_pembimbing -> perusahaan -> id)->get();
        // $siswa = Siswa::findOrFail($id);
        // $catatan = Catatan::where('id_siswa', $id);
        // dd($catatan);

        return view('pembimbing.delete-catatan', ['catatan' => $catatan, 'siswa' => $siswa]);
    }

    public function nilai($id)
    {
        $siswa = Siswa::FindOrFail($id);
        $pembimbing = PembimbingPerusahaan::where('id_user' , Auth::user()->id)->first();
        $nilai = Nilai::where('id_pembimbing', $pembimbing -> id) -> where('id_siswa', $siswa -> nis)-> first();
        $kriteria = Kriteria::where('id_pembimbing', $pembimbing -> id) -> get();
        // dd($nilai -> );
        // dd(($nilai));
        // if (is_null($nilai)){
        //     Nilai::insert([
        //         'id_siswa' => $siswa -> id,
        //         'id_perusahaan' => $pembimbing -> perusahaan -> id
        //     ]);
        // }
        return view('pembimbing.nilai', ['siswa' => $siswa, 'nilai' => $nilai], compact('kriteria', 'pembimbing'));
    }

    public function addNilai($id){
        $siswa = Siswa::FindOrFail($id);
        $pembimbing = PembimbingPerusahaan::all()->where('id_user' , Auth::user()->id)->first();
        $nilai = $siswa -> nilai -> where('id_perusahaan', $pembimbing -> perusahaan -> id) -> first();

        return view('pembimbing.penilaian', ['siswa'=>$siswa ,'nilai' => $nilai]);
    }

    public function storeNilai(Request $request){
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $data_pembimbing = PembimbingPerusahaan::all()->where('id_user' , Auth::user()->id)->first();

        $nilai = Nilai::where('id_siswa', $request -> id)->where('id_perusahaan', $data_pembimbing -> perusahaan -> id)->first();
        // dd($request -> id);
        if (is_null($nilai)){
            Nilai::create([
           'penampilan' => $request->penampilan,
           'komitmen' => $request->komitmen,
           'kesopanan' => $request->kesopanan,
           'kreativitas' => $request->kreativitas,
           'kerjasamatim' => $request->kerjasamatim,
           'disiplin' => $request->disiplin,
           'penguasaan_keilmuan' => $request->penguasaan_keilmuan,
           'kemampuan_identifikasi' => $request->kemampuan_identifikasi,
           'kemampuan_alternatif' => $request->kemampuan_alternatif,
           'keterampilan' => $request->keterampilan,
           'inovasi' => $request->inovasi,
           'produktivitas' => $request->produktivitas,
           'penguasaan_alatkerja' => $request->penguasaan_alatkerja,
           'id_perusahaan' => $data_pembimbing -> perusahaan -> id,
           'id_siswa' => $request -> id

            ]);
        }
        else{
            // $nilai = Nilai::Find($request->id);

            $nilai->penampilan = $request->penampilan;
            $nilai->komitmen = $request->komitmen;
            $nilai->kesopanan = $request->kesopanan;
            $nilai->kreativitas = $request->kreativitas;
            $nilai->kerjasamatim = $request->kerjasamatim;
            $nilai->disiplin = $request->disiplin;
            $nilai->penguasaan_keilmuan = $request->penguasaan_keilmuan;
            $nilai->kemampuan_identifikasi = $request->kemampuan_identifikasi;
            $nilai->kemampuan_alternatif = $request->kemampuan_alternatif;
            $nilai->keterampilan = $request->keterampilan;
            $nilai->inovasi = $request->inovasi;
            $nilai->produktivitas = $request->produktivitas;
            $nilai->penguasaan_alatkerja = $request->penguasaan_alatkerja;
            $nilai->id_perusahaan = $data_pembimbing -> perusahaan -> id;

            $nilai->save();
        }

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/pembimbing/nilai/'.$request -> id);
    }

    public function editNilai($id){
        $siswa = Siswa::FindOrFail($id);
        $pembimbing = PembimbingPerusahaan::where('id_user' , Auth::user()->id)->first();
        $nilai = Nilai::where('id_pembimbing', $pembimbing -> id) -> where('id_siswa', $siswa -> nis) -> get();
        // dd($nilai);
        $kriteria = Kriteria::where('id_pembimbing', $pembimbing -> id) -> get();

        return view('pembimbing.editnilai', ['siswa' => $siswa, 'nilai' => $nilai, 'pembimbing' => $pembimbing], compact('kriteria'));
    }

    public function updateNilai(Request $request){
        // dd($request);
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        $data_pembimbing = PembimbingPerusahaan::all()->where('id_user' , Auth::user()->id)->first();
        $nilai = $request -> nilai;
        $kriteria = $request -> kriteria;
        $id = $request -> id_siswa;
        $siswa = Siswa::find($request -> id_siswa);
        // dd($siswa -> nilai -> all());
        if($siswa -> nilai -> all()){

            foreach($nilai as $key => $nilai){

                foreach($siswa -> nilai as $item){
            if($item -> id_kriteria == $kriteria[$key]){
                $item->point = $nilai;
                $item -> save();
            }
        }
    }
    }else{

    for ($i=0; $i < count($nilai); $i++) {
        Nilai::create([
            'id_siswa' => $id,
            'id_kriteria' => $kriteria[$i],
            'point' => intval($nilai[$i]),
            'id_pembimbing' => $data_pembimbing -> id,
            'id_tahun' => $tahun -> id

        ]);
    }

}


        // $nilai = Nilai::where('id_siswa', $request -> id)->where('id_perusahaan', $data_pembimbing -> perusahaan -> id)->first();
        // dd($request -> id);
        // if (is_null($nilai)){
        //     Nilai::insert([
        //    'penampilan' => $request->penampilan,
        //    'komitmen' => $request->komitmen,
        //    'kesopanan' => $request->kesopanan,
        //    'kreativitas' => $request->kreativitas,
        //    'kerjasamatim' => $request->kerjasamatim,
        //    'disiplin' => $request->disiplin,
        //    'penguasaan_keilmuan' => $request->penguasaan_keilmuan,
        //    'kemampuan_identifikasi' => $request->kemampuan_identifikasi,
        //    'kemampuan_alternatif' => $request->kemampuan_alternatif,
        //    'keterampilan' => $request->keterampilan,
        //    'inovasi' => $request->inovasi,
        //    'produktivitas' => $request->produktivitas,
        //    'penguasaan_alatkerja' => $request->penguasaan_alatkerja,
        //    'id_perusahaan' => $data_pembimbing -> perusahaan -> id,
        //    'id_siswa' => $request -> id

        //     ]);
        // }
        // else{
        //     // $nilai = Nilai::Find($request->id);

        //     $nilai->penampilan = $request->penampilan;
        //     $nilai->komitmen = $request->komitmen;
        //     $nilai->kesopanan = $request->kesopanan;
        //     $nilai->kreativitas = $request->kreativitas;
        //     $nilai->kerjasamatim = $request->kerjasamatim;
        //     $nilai->disiplin = $request->disiplin;
        //     $nilai->penguasaan_keilmuan = $request->penguasaan_keilmuan;
        //     $nilai->kemampuan_identifikasi = $request->kemampuan_identifikasi;
        //     $nilai->kemampuan_alternatif = $request->kemampuan_alternatif;
        //     $nilai->keterampilan = $request->keterampilan;
        //     $nilai->inovasi = $request->inovasi;
        //     $nilai->produktivitas = $request->produktivitas;
        //     $nilai->penguasaan_alatkerja = $request->penguasaan_alatkerja;
        //     $nilai->id_perusahaan = $data_pembimbing -> perusahaan -> id;

        //     $nilai->save();
        // }


        return redirect('/pembimbing/nilai/'.$request -> id_siswa);
    }

    public function detailsiswa($id)
    {
        // dd($request);
        $siswa = Siswa::findorFail($id);
        $pembimbing = PembimbingPerusahaan::where('id_user', Auth::user() -> id)-> first();
        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni", "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
        $tanggal = date('Y-m-d');
        // $all_nilai = [];
        // for ($i=0; $i < count($siswa -> perusahaan -> all()); $i++) {
        //     $nilai = Nilai::where('id_siswa', $siswa -> id)->where('id_perusahaan', $siswa -> perusahaan[$i] -> id_perusahaan)->first();
        //     array_push($all_nilai, $nilai);
        // }
        // $catatan = Catatan::where('id_siswa', $siswa -> id)->whereNotNull('id_perusahaan')->orderby('id_perusahaan')->get();
        $riwayat = Aktivitas::where('id_siswa' , $siswa -> nis)->get();
        $absen = Absen::where('id_siswa' , $siswa -> nis)->get();
        // dd($absen);
        foreach ($riwayat as $r ) {
            $r -> hari = date('l', strtotime($r -> tanggal));
        }

        $month = 12     ;
        $tahun = 2023;
        $sekarang = $bulan[$month];
        // $perusahaan = Perusahaan::findorfail($request -> id_perusahaan);
        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni", "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");

        return view('pembimbing.detail-siswa',[ 'siswa' => $siswa, 'bulan' => $bulan,
        'sekarang' => $sekarang, 'tahun' => $tahun,   'riwayat' => $riwayat, 'absen' => $absen, 'pembimbing' => $pembimbing
    ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(PembimbingPerusahaan $pembimbingPerusahaan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PembimbingPerusahaan $pembimbingPerusahaan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, PembimbingPerusahaan $pembimbingPerusahaan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($request);
        // $manggil = Catatan::where('id', $request->name)->get();
        Catatan::where('id', $request->id_catatan)->delete();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect()->back();
    }

}
