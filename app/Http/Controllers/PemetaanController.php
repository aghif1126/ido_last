<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Perusahaan;
use App\Models\ActivityLog;
use Illuminate\Http\Request;
use App\Models\TahunPelajaran;
use App\Models\SiswaPerusahaan;
use App\Models\JurusanPerusahaan;

class PemetaanController extends Controller
{

    public function index(Request $request)
    {
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $kelas = $tahun_pelajaran -> kelas;
            $siswa = $tahun_pelajaran -> siswa;
            $tahun = TahunPelajaran::get();

            $sudah = 0;
            foreach ($siswa as $item) {
                if ($item ->perusahaan != "[]") {
                    // echo $item -> perusahaan;
                   $sudah += 1;
                }
            }
            $belum = $siswa -> count() - $sudah;
            $jumlah_siswa = $siswa -> count();
            $confirm = 0;
            $tunggu = 0;
            foreach ($siswa as $item) {
                if ($item ->perusahaan != "[]") {
                    // echo $item -> perusahaan;
                    $perusahaan = $item -> perusahaan -> all();
                    // dd(count($perusahaan));
                    foreach ($perusahaan as $value) {
                        if ($value -> confirm == "Sudah") {
                            $confirm += 1;
                        }else{
                            $tunggu += 1;
                        }
                    }
                }
            }
            foreach ($kelas as $item ) {
                $item -> sudah = 0;
                $item -> belum = 0;
                $item -> confirm = 0;
                $item -> tunggu = 0;
            }


            foreach ($kelas as $kls) {

                $kls -> siswa = $siswa -> where('id_kelas', $kls->id);
                // dd($kls);
                foreach ($kls -> siswa as $item) {
                    if ($item ->perusahaan != "[]") {
                       $kls -> sudah += 1;
                    }else{
                        $kls -> belum += 1;
                    }
                }
                foreach ($kls -> siswa as $item) {
                    if ($item ->perusahaan != "[]") {
                        // echo $item -> perusahaan;
                        $perusahaan = $item -> perusahaan -> all();
                        // dd(count($perusahaan));
                        foreach ($perusahaan as $value) {
                            if ($value -> confirm == "Sudah") {
                                $kls -> confirm  += 1;
                            }else{
                                $kls -> tunggu += 1;
                            }
                        }
                    }
                }
                // $kls -> confirm = Siswa::where('id_kelas', $kls->id)->where('confirm', 'Sudah');
                // $kls -> tunggu = Siswa::where('id_kelas', $kls->id)->Where('confirm', 'Belum');
                $kls -> jumlah_siswa = $siswa -> where('id_kelas', $kls->id) -> count();
            }

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $kelas = $tahun_pelajaran -> kelas;
            $siswa = $tahun_pelajaran -> siswa;
            $tahun = TahunPelajaran::get();

            $sudah = 0;
            foreach ($siswa as $item) {
                if ($item ->perusahaan != "[]") {
                    // echo $item -> perusahaan;
                   $sudah += 1;
                }
            }
            $belum = $siswa -> count() - $sudah;
            $jumlah_siswa = $siswa ->count();
            $confirm = 0;
            $tunggu = 0;
            foreach ($siswa as $item) {
                if ($item ->perusahaan != "[]") {
                    // echo $item -> perusahaan;
                    $perusahaan = $item -> perusahaan -> all();
                    // dd(count($perusahaan));
                    foreach ($perusahaan as $value) {
                        if ($value -> confirm == "Sudah") {
                            $confirm += 1;
                        }else{
                            $tunggu += 1;
                        }
                    }
                }
            }
            foreach ($kelas as $item ) {
                $item -> sudah = 0;
                $item -> belum = 0;
                $item -> confirm = 0;
                $item -> tunggu = 0;
            }


            foreach ($kelas as $kls) {

                $kls -> siswa =$siswa -> where('id_kelas', $kls->id);
                // dd($kls);
                foreach ($kls -> siswa as $item) {
                    if ($item ->perusahaan != "[]") {
                       $kls -> sudah += 1;
                    }else{
                        $kls -> belum += 1;
                    }
                }
                foreach ($kls -> siswa as $item) {
                    if ($item ->perusahaan != "[]") {
                        // echo $item -> perusahaan;
                        $perusahaan = $item -> perusahaan -> all();
                        // dd(count($perusahaan));
                        foreach ($perusahaan as $value) {
                            if ($value -> confirm == "Sudah") {
                                $kls -> confirm  += 1;
                            }else{
                                $kls -> tunggu += 1;
                            }
                        }
                    }
                }
                // $kls -> confirm = Siswa::where('id_kelas', $kls->id)->where('confirm', 'Sudah');
                // $kls -> tunggu = Siswa::where('id_kelas', $kls->id)->Where('confirm', 'Belum');
                $kls -> jumlah_siswa = $siswa -> where('id_kelas', $kls->id) -> count();
            }
        }
        // dd($kelas);
        return view('guru.pemetaan.dashboard', [
        'sudah' => $sudah,
        'belum' => $belum,
        'jumlah_siswa' => $jumlah_siswa,
        'kelas' => $kelas,
        'confirm' => $confirm,
        'tahun' => $tahun,
        'tahun_pelajaran' => $tahun_pelajaran,
        'tunggu' => $tunggu
    ]);
    }

    // public function pemetaan($id)
    // {
    //     return view('guru.pemetaan.pemetaan');
    // }
    public function pilih(Request $request)
    {
        // dd($request);
        $siswa  = Siswa::find($request -> siswa[0]);
        $id_jurusan = $siswa -> kelas -> konsentrasi -> jurusan -> id;
        // dd($id_jurusan);
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $perusahaan = $tahun_pelajaran -> perusahaan;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $perusahaan = $tahun_pelajaran -> perusahaan;
            $tahun = TahunPelajaran::get();
        }
        // dd($id);
        return view('guru.pemetaan.perusahaan', ['id_jurusan' => $id_jurusan,'perusahaan' => $perusahaan, 'id_siswa' => $request -> siswa, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran]);
    }

    public function satu_pilih($id, Request $request)
    {
        // dd($request);
        $siswa  = Siswa::find($id);
        $id_jurusan = $siswa -> kelas -> konsentrasi -> jurusan -> id;
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $perusahaan = $tahun_pelajaran -> perusahaan;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $perusahaan = $tahun_pelajaran -> perusahaan;
            $tahun = TahunPelajaran::get();
        }
        $id_siswa = [$id];
        // dd($id);
        return view('guru.pemetaan.perusahaan', ['id_jurusan' => $id_jurusan, 'perusahaan' => $perusahaan, 'id_siswa' => $id_siswa, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran]);
    }

    public function confirm(Request $request)
    {
        // dd($request);
        return view('guru.pemetaan.periode', ['id_siswa' => $request -> id_siswa, 'id_perusahaan' => $request -> id_perusahaan]);
    }
    public function periode(Request $request)
    {

        // dd($request);
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        $awal = ($request -> awal_periode."-01");
        $a = intval(explode('-', $request -> awal_periode)[1]);
        $b = intval(explode('-', $request -> akhir_periode)[1]);
        // dd($a);
        $akhir = ($request -> akhir_periode."-01");
        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni",
        "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");

        // dd($bulan);
        $periode = 0;
        if ($a < $b){
        $periode = $b - $a + 1;
        // dd($periode);
        }
        else{
            $periode = 13 - $a + $b;
            // dd($periode);
        }
        $all_periode = [];
        // dd($periode);
        $start = $a;
        for ($i=$start; $i < $periode + $start; $i++) {

            // if($a == 13){
            //     $j = $a -12;
            // }
            if ($a > 12) {
                $j = $a - 12;

                // dd($a);
            }else{
                $j = $a;

            }
            array_push($all_periode, $bulan[$j]);
            $a += 1;


        }
        $periode = implode('-', $all_periode);
        // dd($request->id_siswa);
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        foreach ($request -> id_siswa as $key) {
            $siswa = Siswa::findorfail($key);
            $id_jurusan = $siswa -> kelas -> konsentrasi -> jurusan -> id;
            $perusahaan = Perusahaan::find($request->id_perusahaan);
            $jurusan_perusahaan =  $perusahaan -> jurusan -> where('id_jurusan', $id_jurusan) -> first() ;
            $jurusan_perusahaan = JurusanPerusahaan::find($jurusan_perusahaan -> id);
            if($jurusan_perusahaan -> kuota > 0){
            SiswaPerusahaan::create([
                'id_siswa' => $key,
                'id_perusahaan' => $request -> id_perusahaan,
                'awal_periode' => $awal,
                'akhir_periode' => $akhir,
                'periode' => $periode,
                'status' => 'Sudah',
                'id_jurusan' => $id_jurusan,
                'id_tahun' => $tahun -> id
            ]);
            $jurusan_perusahaan ->  kuota -= 1 ;
            $jurusan_perusahaan -> save();
           }
        }

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();




        return redirect('/guru/detail-kelas/'.$siswa -> kelas -> id.'/siswa');
    }


    public function konfirmasi(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($request);
        $siswa = SiswaPerusahaan::where('id_siswa' ,$request->id_siswa)-> where('id_perusahaan' ,$request->id_perusahaan)-> first();

        $siswa -> confirm = 'Sudah';

        $siswa -> save();

        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/detail-perusahaan/'.$request->id_perusahaan);
    }
    public function batalkan(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($request);
        $siswa = SiswaPerusahaan::where('id_siswa' ,$request->id_siswa)-> where('id_perusahaan' ,$request->id_perusahaan)-> get();
        // dd($siswa);
        $id_jurusan = $siswa[0] -> id_jurusan;
        foreach ($siswa as $item ) {
            $item -> delete();
        }
        $perusahaan = Perusahaan::find($request->id_perusahaan);
        $jurusan_perusahaan =  $perusahaan -> jurusan -> where('id_jurusan', $id_jurusan) -> first() ;
        // dd($jurusan_perusahaan);
        $jurusan_perusahaan = JurusanPerusahaan::find($jurusan_perusahaan -> id);
        $jurusan_perusahaan ->  kuota += 1 ;
        $jurusan_perusahaan -> save();
        $activitylog = ActivityLog::all()->last();

        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/detail-perusahaan/'.$request->id_perusahaan);
    }
}
