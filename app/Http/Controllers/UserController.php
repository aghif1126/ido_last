<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\Role;
use App\Models\User;
use App\Models\Siswa;
use App\Models\Kaprog;
use App\Models\TahunPelajaran;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\PembimbingPerusahaan;
use App\Models\ActivityLog;

class UserController extends Controller
{
    public function login(Request $request){

    }

    public function pilih(){
        return view('guru.user.pilih');
    }

    public function createGuru($id)
    {
        $role = Role::all();
        $user = Guru::findorFail($id);
        return view('guru.user.addGuru', ['role' => $role, 'user' => $user, 'id' => $id]);
    }

    public function createSiswa($id)
    {
        $role = Role::all();
        $user = Siswa::findorFail($id);
        return view('guru.user.addSiswa', ['role' => $role, 'user' => $user, 'id' => $id]);
    }

    public function createPembimbing($id)
    {
        $role = Role::all();
        $user = PembimbingPerusahaan::findorFail($id);
        return view('guru.user.addPembimbing', ['role' => $role, 'user' => $user, 'id' => $id]);
    }

    public function createKaprog($id)
    {
        $role = Role::all();
        $user = Kaprog::findorFail($id);
        return view('guru.user.addKaprog', ['role' => $role, 'user' => $user, 'id' => $id]);
    }

    public function pilihGuru(Request $request)
    {
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $guru = $tahun_pelajaran -> guru;
            $role = $tahun_pelajaran -> role;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $guru = $tahun_pelajaran -> guru;
            $role = $tahun_pelajaran -> role;
            $tahun = TahunPelajaran::get();
        }

        return view('guru.user.guru', ['role' => $role, 'guru' => $guru, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran]);
    }

    public function pilihSiswa(Request $request)
    {
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $siswa = $tahun_pelajaran -> siswa;
            $role = $tahun_pelajaran -> role;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $siswa = $tahun_pelajaran -> siswa;
            $role = $tahun_pelajaran -> role;
            $tahun = TahunPelajaran::get();
        }

        // foreach ($siswa as $value) {
        //     dd($value->perusahaan);
        //     $value;
        // }


        return view('guru.user.siswa', ['role' => $role, 'siswa' => $siswa, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran]);
    }

    public function pilihPembimbing(Request $request)
    {
        if ($request -> tahun) {
            $tahun_pelajaran = TahunPelajaran::findorfail($request->tahun);
            // dd($tahun);
            $pembimbing = $tahun_pelajaran -> pembimbing;
            $role = $tahun_pelajaran -> role;
            $tahun = TahunPelajaran::get();

        }else{
            $tahun_pelajaran = TahunPelajaran::where('status', 'aktif')->first();
            $pembimbing = $tahun_pelajaran -> pembimbing;
            $role = $tahun_pelajaran -> role;
            $tahun = TahunPelajaran::get();
        }

        return view('guru.user.pembimbing', ['role' => $role, 'pembimbing' => $pembimbing, 'tahun'=>$tahun, 'tahun_pelajaran'=>$tahun_pelajaran]);
    }

    public function pilihKaprog()
    {
        $role = Role::all();
        $kaprog = Kaprog::where('id_user', null)->paginate(15);
        return view('guru.user.kaprog', ['role' => $role, 'kaprog' => $kaprog]);
    }

    // public function KaprogStore(Request $request)
    // {
    //     User::insert([
    //         'name' => $request['name'],
    //         'email' => $request['email'],
    //         'email_verified_at' => $request[''],
    //         'password' => password_hash($request['pass'], PASSWORD_DEFAULT),
    //         'pass' => $request['pass'],
    //         'role_id' => $request['role_id'],
    //         'created_at' => Carbon::now(),
    //         'updated_at' => Carbon::now()

    //     ]);
    //     // dd($request);
    //     $kaprog = Kaprog::FindOrFail($request -> id);
    //     $kaprog -> id_user = User::max('id');
    //     $kaprog -> save();

    //     // User::pilih($request->except('_token'));
    //     return redirect('guru/show-user');
    // }
    public function GuruStore(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'email_verified_at' => $request[''],
            'password' => password_hash($request['pass'], PASSWORD_DEFAULT),
            'pass' => $request['pass'],
            'role_id' => $request['role_id'],
            'id_tahun' => $tahun -> id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);
        // dd($request);
        $kaprog = Guru::findorfail($request -> id);

        $kaprog -> id_user = User::max('id');
        $kaprog -> save();

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        // User::pilih($request->except('_token'));
        return redirect('guru/show-user');
    }
    public function PembimbingStore(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'email_verified_at' => $request[''],
            'password' => password_hash($request['pass'], PASSWORD_DEFAULT),
            'pass' => $request['pass'],
            'role_id' => $request['role_id'],
            'id_tahun' => $tahun -> id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);
        $kaprog = PembimbingPerusahaan::findorfail($request -> id);

        $kaprog -> id_user = User::max('id');
        $kaprog -> save();

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        // User::pilih($request->except('_token'));
        return redirect('guru/show-user');
    }
    public function SiswaStore(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'email_verified_at' => $request[''],
            'password' => password_hash($request['pass'], PASSWORD_DEFAULT),
            'pass' => $request['pass'],
            'role_id' => $request['role_id'],
            'id_tahun' => $tahun -> id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);
        // dd($request);
        $siswa = Siswa::findorfail($request -> id);
        $siswa -> id_user = User::max('id');
        $siswa -> save();

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        // User::pilih($request->except('_token'));
        return redirect('guru/show-user');
    }

    /**
     * Display the specified resource.
     */
    public function show()
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        $user = $tahun -> users -> all();


        return view('guru.user.show', ['user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $user = User::findorFail($id);
        $role = Role::all();

        return view('guru.user.edit', ['user' => $user, 'role' => $role]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();
        // dd($request);
        $flight = User::findorFail($request->id);
        $flight->name = $request->name;
        $flight->pass = $request->pass;
        $flight->email = $request->email;
        $flight->role_id = $request->role_id;

        $flight->save();

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        return redirect('/guru/detail-user/'.$request->id);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $tahun = TahunPelajaran::where('status', 'aktif')->first();

        $id = $request -> id;

        $siswa = Siswa::where('id_user', $id)->first();
        // dd(is_null($siswa));
        if (is_null($siswa) != true) {
            $siswa -> id_user = null;
            $siswa -> save();
        }

        $Guru = Guru::where('id_user', $id)->first();
        if (is_null($Guru) != true) {
            $Guru -> id_user = null;
            $Guru -> save();
        }

        $PembimbingPerusahaan = PembimbingPerusahaan::where('id_user', $id)->first();
        if (is_null($PembimbingPerusahaan) != true) {
            $PembimbingPerusahaan -> id_user = null;
            $PembimbingPerusahaan -> save();
        }
        // $Kaprog = Kaprog::where('id_user', $id)->first();
        // if (is_null($Kaprog) != true) {
        //     $Kaprog -> id_user = null;
        //     $Kaprog -> save();
        // }

        $user = User::findorFail($request->id)->delete();

        $activitylog = ActivityLog::all()->last();
        
        $activitylog->ip = $request->getClientIp();
        $activitylog->id_tahun = $tahun->id;

        $activitylog->save();

        // $user = User::paginate(15);
        return redirect('/guru/show-user');

    }
    public function delete()
    {
        $user = User::all();

        return view('guru.user.delete', ['user' => $user]);
    }
    public function detail($id)
    {
        $user = User::findorFail($id);
        return view('guru.user.detail', ['user' => $user, 'id' => $id]);
    }
}
