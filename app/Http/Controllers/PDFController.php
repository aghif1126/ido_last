<?php

namespace App\Http\Controllers;

use App\Models\Guru;
// use PDF;
use App\Models\Siswa;
use App\Models\Kelas;
use App\Models\Kriteria;
use App\Models\Jurusan;
use App\Models\Aktivitas;
use App\Models\Monitoring;
use App\Models\Absen;
use App\Models\Perusahaan;
use App\Models\SiswaPerusahaan;
use App\Models\PembimbingPerusahaan;
use App\Models\TahunPelajaran;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Auth;

class PDFController extends Controller
{
    //ingfo: stream buat preview dan download buat download

    public function monitoring(Request $request){
        $guru = Guru::where('id_user', $request -> id_user)-> first();
        $perusahaan = Perusahaan::findorfail($request -> id_perusahaan);
        $monitoring = Monitoring::where('id_perusahaan', $request->id_perusahaan)
        ->where('id_guru', $guru -> id)
        ->where('tanggal', $request -> tanggal)
        ->first();
        $pembimbing = $perusahaan->pembimbing->where('id_jurusan', $monitoring->id_jurusan)->first();
        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni",
        "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
        $hari = array(
            'Sun' => 'Minggu',
            'Mon' => 'Senin',
            'Tue' => 'Selasa',
            'Wed' => 'Rabu',
            'Thu' => 'Kamis',
            'Fri' => 'Jumat',
            'Sat' => 'Sabtu'
        );
        // dd($perusahaan->pembimbing->where('id_jurusan', $monitoring->id_jurusan));
        // dd($bulan[intval(explode('-', $monitoring->tanggal)[1])]);

        $pdf = PDF::loadView('pdf.monitoring', ['guru'=>$guru, 'pembimbing' => $pembimbing, 'monitoring'=>$monitoring,
         'bulan'=>$bulan, 'perusahaan' => $perusahaan, 'hari' => $hari])->setPaper('A4', 'portrait');

        return $pdf->stream('JURNAL_KEGIATAN_SISWA_PRAKTEK_KERJA_LAPANGAN_(PKL).pdf');
    }

    public function riwayatmonitoring($id){

        $monitoring = Monitoring::findorfail($id);
        $guru = $monitoring->guru;
        $perusahaan = $monitoring->perusahaan;
        dd($monitoring->perusahaan);
        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni",
        "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
        $hari = array(
            'Sun' => 'Minggu',
            'Mon' => 'Senin',
            'Tue' => 'Selasa',
            'Wed' => 'Rabu',
            'Thu' => 'Kamis',
            'Fri' => 'Jumat',
            'Sat' => 'Sabtu'
        );

        // dd($bulan[intval(explode('-', $monitoring->tanggal)[1])]);


        $pdf = PDF::loadView('pdf.monitoring', ['pembimbing'=>$guru, 'monitoring'=>$monitoring,
         'bulan'=>$bulan, 'perusahaan' => $perusahaan, 'hari' => $hari])->setPaper('A4', 'portrait');

        return $pdf->stream('JURNAL_KEGIATAN_SISWA_PRAKTEK_KERJA_LAPANGAN_(PKL).pdf');
    }

    public function jurnalsiswa(Request $request){
        // dd($request);
        $id_perusahaan = $request -> id_perusahaan;
        $siswa = Siswa::findorfail($request -> id_siswa);
        // $perusahaan = SiswaPerusahaan::where('id_siswa' , $siswa -> nis)->where('id_perusahaan', $id_perusahaan)->first();
        $perusahaan = SiswaPerusahaan::find($request -> id_perusahaan);
        // dd($perusahaan);
        $riwayat = Aktivitas::where('id_siswa' , $siswa -> nis)->get();

        $bulan = array("1"=>"Januari", "2"=>"Februari", "3"=>"Maret", "4"=>"April", "5"=>"Mei", "6"=>"Juni",
        "7"=>"Juli", "8"=>"Agustus", "9"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
        // dd($siswa->nilai);
        $bulan_sekarang = intval(date('m', time()));
        // dd($bulan[$bulan_sekarang]);

        $bulan[$bulan_sekarang] = "November";

        $siswa_perusahaan = SiswaPerusahaan::where('id_siswa', $siswa -> nis)-> where('periode', 'LIKE', '%'. $bulan[$bulan_sekarang] .'%')->first();
        $perusahaan = Perusahaan::findorfail($id_perusahaan);
        // dd($perusahaan);
        $kriteria = Kriteria::where('id_pembimbing', $request -> id_pembimbing) -> get();
        $pembimbing = PembimbingPerusahaan::findorfail($request -> id_pembimbing);
        $all_bulan = Absen::all()->groupBy('bulan');


        $pdf = PDF::loadView('pdf.jurnal-siswa', ['riwayat'=>$riwayat, 'siswa'=>$siswa, 'pembimbing' => $pembimbing,
        'bulan'=>$bulan, 'perusahaan' => $perusahaan, 'siswa_perusahaan' => $siswa_perusahaan, 'kriteria' => $kriteria, 'nilai' => $siswa -> nilai, 'all_bulan' => $all_bulan])->setPaper('A4', 'portrait');

        foreach ($riwayat as $r ) {
            $r -> hari = date('l', strtotime($r -> tanggal));
        }

        return $pdf->stream('JURNAL_'.$siswa -> nama.'_(PKL).pdf');
    }

    public function rekappemetaan(Request $request){
        // dd($request);
        $tahun_pelajaran = TahunPelajaran::findorfail($request->id_tahun);
        $tahunrekap = $tahun_pelajaran->tahun;
        $perusahaan = $tahun_pelajaran->perusahaan;

        // dd($perusahaan->siswa);
        $pdf = PDF::loadView('pdf.rekap-pemetaan', ['tahunrekap'=>$tahunrekap, 'perusahaan'=>$perusahaan]);

        return $pdf->stream('rekap-pemetaan');

        // return view('pdf.rekap-pemetaan', ['tahunrekap'=>$tahunrekap, 'perusahaan'=>$perusahaan]);
    }

    public function formatriwayat(){
        $siswa = Siswa::where('id_user', Auth::user()->id)->first();
        $riwayat = Aktivitas::where('id_siswa' , $siswa -> nis)->get();

        $pdf = PDF::loadView('pdf.format-siswa-riwayat', ['riwayat'=>$riwayat, 'siswa'=>$siswa])->setPaper('A4', 'landscape');

        foreach ($riwayat as $r ) {
            $r -> hari = date('l', strtotime($r -> tanggal));
        }

        return $pdf->stream('Laporan_Kegiatan.pdf');
    }

    public function formatnilai(){
        $siswa = Siswa::where('id_user', Auth::user()->id)->first();

        $pdf = PDF::loadView('pdf.format-siswa-nilai', ['siswa' => $siswa])->setPaper('A4', 'portrait');

        return $pdf->stream('Laporan_Penilaian.pdf');
    }

    public function formatcover(){
        $pdf = PDF::loadView('pdf.format-cover');
        // return view('pdf.format-cover');
        return $pdf->stream('Cover_Jurnal.pdf');
    }

    public function formatidentitas(){
        $pdf = PDF::loadView('pdf.format-identitas');

        return $pdf->stream('identitas');
    }

    public function formattempatpkl(){
        $pdf = PDF::loadView('pdf.format-tempatpkl');

        return $pdf->stream('tempatpkl');
    }

    public function formatcatatan(){
        $pdf = PDF::loadView('pdf.format-catatan');

        return $pdf->stream('catatan');
    }

    public function formatcoverlaporan(){
        $pdf = PDF::loadView('pdf.format-coverlaporan');

        return $pdf->stream('coverlaporan');
    }

    public function formatcatatanmonitoring(){
        $pdf = PDF::loadView('pdf.format-catatanmonitoring');

        return $pdf->stream('catatanmonitoring');
    }

    public function formatmonitoring(){
        $pdf = PDF::loadView('pdf.format-monitoring');

        return $pdf->stream('monitoring');
    }

    public function formatabsen(){
        $pdf = PDF::loadView('pdf.format-absen');

        return $pdf->stream('absen');
    }
}
