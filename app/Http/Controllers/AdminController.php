<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\User;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Kaprog;
use App\Models\Jurusan;
use App\Models\Perusahaan;
use Illuminate\Http\Request;
use App\Models\KonsentrasiKeahlian;

class AdminController extends Controller
{
    public function index(){
        $siswa = Siswa::all();
        $perusahaan = Perusahaan::all();
        $user = User::all();
        $konsentrasi = KonsentrasiKeahlian::all();
        $jurusan = Jurusan::all();
        $kelas = Kelas::all();
        $guru = Guru::all();
        $kaprog = Kaprog::all();

        return view('admin.dashboard', [
            'siswa' => $siswa,
            'perusahaan' => $perusahaan,
            'user' => $user,
            'konsentrasi' => $konsentrasi,
            'jurusan' => $jurusan,
            'kelas' => $kelas,
            'guru' => $guru,
            'kaprog' => $kaprog
        ]);

    }
    public function showkaprog()
    {
        $kaprog = Kaprog::all();
        // $keyword = $request->keyword;
        // $kaprog = Kaprog::Where('nama', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('nip', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('jenkel', $keyword)
        //     ->orWhere('alamat', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('telp', 'LIKE', '%' . $keyword . '%')
        //     ->orderBy('nama')->paginate(15);

        return view('guru.kap rog.show', ['kaprog'=>$kaprog]);
    }

    public function createkaprog()
    {
        $kaprog = Kaprog::all();
        return view("guru.kaprog.add")->with("kaprog", $kaprog);
    }

    public function storekaprog(Request $request)
    {
        Kaprog::create($request->except("_token", "submit"));
        return redirect("guru/show-kaprog");
    }

    public function detailkaprog($id)
    {
        $kaprog = Kaprog::findorFail($id);

        return view("guru.kaprog.detail")->with("kaprog", $kaprog);
    }

    public function editkaprog($id)
    {
        $kaprog = Kaprog::findorFail($id);

        return view("guru.kaprog.edit")->with("kaprog", $kaprog);
    }

    public function updatekaprog(Request $request)
    {
        $proses = Kaprog::find($request->id);

        $proses->nip = $request->nip;
        $proses->nama = $request->nama;
        $proses->jenkel = $request->jenkel;
        $proses->alamat = $request->alamat;
        $proses->telp = $request->telp;

        $proses->save();

        return redirect("guru/show-kaprog");
    }

    public function deletekaprog(Request $request)
    {
        $kaprog = Kaprog::all();
        // $keyword = $request->keyword;
        // $kaprog = Kaprog::Where('nama', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('nip', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('jenkel', $keyword)
        //     ->orWhere('alamat', 'LIKE', '%' . $keyword . '%')
        //     ->orWhere('telp', 'LIKE', '%' . $keyword . '%')
        //     ->orderBy('nama')->paginate(15);

        return view("guru.kaprog.delete")->with("kaprog", $kaprog);
    }

    public function destroykaprog(Request $request)
    {
        Kaprog::findorFail($request->id)->delete();
        return redirect("guru/show-kaprog");
    }

}
