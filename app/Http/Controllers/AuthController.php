<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GrahamCampbell\ResultType\Success;
use Illuminate\Support\Facades\Session;


class AuthController extends Controller
{
    public function index()
    {
        if (Auth::user()->role->id == 4) {
            return redirect()->intended('/siswa/dashboard');
        }else if (Auth::user()->role->id == 3) {
            return redirect()->intended('/guru/pembimbing/dashboard');
        }
        else if (Auth::user()->role->id == 2) {
            return redirect()->intended('/pembimbing/dashboard');
        }
        else if (Auth::user()->role->id == 1) {
            return redirect()->intended('/guru/dashboard');
        }
    }
    public function login()
    {
        return view('login');
    }

    public function validasi(Request $request)
    {

        $credentials = $request->validate([
            'email' => ['required'],
            'password' => ['required'],
        ]);
        // dd('s');
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            // echo(Auth::user()->role->);
            if (Auth::user()->role->id == 4) {
                return redirect()->intended('/siswa/dashboard');
            }else if (Auth::user()->role->id == 3) {
                return redirect()->intended('/guru/pembimbing/dashboard');
            }
            else if (Auth::user()->role->id == 2) {
                return redirect()->intended('/pembimbing/dashboard');
            }
            else if (Auth::user()->role->id == 1) {
                return redirect()->intended('/guru/dashboard');
            }else{
                return redirect()->intended('/');
            }

        }

        // return back()->withErrors([
        //     'email' => 'The provided credentials do not match our records.',
        // ])->onlyInput('email');
        Session::flash('status', 'failed');
        Session::flash('message', 'Password atau Email salah');
        return redirect('/log-in');
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/log-in');
    }
}
