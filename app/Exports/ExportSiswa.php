<?php

namespace App\Exports;

use App\Models\Siswa;
use App\Models\Absen;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Http\Request;

class ExportSiswa implements FromView, ShouldAutoSize
{
    public function __construct(){
        $this->absen = Absen::first();
        $this->siswa = Siswa::first();
    }

    public function view(): View{
        return view('excel.absen', ['absen'=>$this->absen, 'siswa'=>$this->siswa]);
    }
}
