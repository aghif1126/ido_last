<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Nilai extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'nilais';

    protected $guarded = ['id'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logUnguarded()
        ->useLogName('Nilai');
    }

    public function pembimbing()
    {
        return $this->belongsTo(PembimbingPerusahaan::class, 'id_pembimbing', 'id');
    }


    public function kriteria()
    {
        return $this->belongsTo(Kriteria::class, 'foreign_key', 'other_key');
    }
}
