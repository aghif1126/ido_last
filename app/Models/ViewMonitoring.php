<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewMonitoring extends Model
{
    use HasFactory;
    protected $table = 'view_monitoring';
}
