<?php

namespace App\Models;

use App\Models\Kelas;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Siswa extends Model
{
    use HasFactory, LogsActivity;

    protected $table = "siswas";
    protected $primaryKey = 'nis';
    
    public $incrementing = false;
    
    // In Laravel 6.0+ make sure to also set $keyType
    protected $keyType = 'string';
    protected $fillable = ['nis', 'nama', 'id_kelas', 'telp', 'jenkel', 'alamat', 'tanggal_lahir',
    'semester', 'tahun_pelajaran', 'periode', 'program_keahlian', 'konsentrasi_keahlian', 'foto',
    'nama_ortu', 'alamat_ortu', 'telp_ortu', 'id_guru', 'id_perusahaan', 'id_user', 'id_tahun'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logFillable()
        ->useLogName('Siswa');
    }

/**
 * Get the user that owns the Siswa
 *
 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
 */
public function guru()
{
    return $this->belongsTo(Guru::class, 'id_guru', 'nis');
}
public function kelas()
{
    return $this->belongsTo(Kelas::class, 'id_kelas', 'id');
}
public function nilai()
{
    return $this->hasMany(Nilai::class, 'id_siswa', 'nis');
}

// public function perusahaan()
// {
//     return $this->belongsTo(Perusahaan::class, 'id_perusahaan', 'id');
// }
/**
 * Get all of the comments for the Siswa
 *
 * @return \Illuminate\Database\Eloquent\Relations\HasMany
 */
public function perusahaan()
{
    return $this->hasMany(SiswaPerusahaan::class, 'id_siswa', 'nis');
}
public function aktivitas()
{
    return $this->hasMany(Aktivitas::class, 'id_siswa', 'nis');
}

public function catatan()
{
    return $this->hasMany(Catatan::class, 'id_siswa', 'nis');
}

public function absen()
{
    return $this->hasMany(Absen::class, 'id_siswa', 'nis');
}
public function tahun_pelajaran()
{
    return $this->belongsTo(TahunPelajaran::class, 'id_tahun', 'id');
}

}
