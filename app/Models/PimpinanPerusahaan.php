<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class PimpinanPerusahaan extends Model
{
    use HasFactory, LogsActivity;

    protected $table = "pimpinan_perusahaans";
    protected $fillable = ['nama', 'nip', 'jenkel','pangkat','id_perusahaan', 'id_tahun'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logFillable()
        ->useLogName('PimpinanPerusahaan');
    }

    public function perusahaan()
    {
        return $this->belongsTo(Perusahaan::class, 'id_perusahaan', 'id');
    }
}
