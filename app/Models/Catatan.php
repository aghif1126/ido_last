<?php

namespace App\Models;

use App\Models\Siswa;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Catatan extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = ['catatan', 'tanggal', 'id_siswa', 'id_tahun', 'id_perusahaan'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logFillable()
        ->useLogName('Catatan');
    }

    /**
     * Get the catatan that owns the Catatan
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

     public function perusahaan()
{
    return $this->belongsTo(Perusahaan::class, 'id_perusahaan', 'id');
}

}
