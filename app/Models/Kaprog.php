<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kaprog extends Model
{
    use HasFactory;

    protected $table = "kaprog";
    protected $fillable = ["nama", "nip", "jenkel", "alamat", "telp", 'id_tahun'];

    public function user()
    {
        return $this->belogsTo(User::class, 'user_id', 'id');
    }
}
