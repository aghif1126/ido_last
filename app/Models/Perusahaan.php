<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Perusahaan extends Model
{
    use HasFactory, LogsActivity;

    protected $table = "perusahaans";
    protected $fillable = ['nama', 'alamat', 'lokasi', 'email', 'fax', 'telp', 'website', 'id_tahun', 'mou', 'id_tahun', 'kuota', 'id_guru'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logFillable()
        ->useLogName('Perusahaan');
    }

    public function siswa()
    {
        return $this->hasMany(SiswaPerusahaan::class, 'id_perusahaan', 'id');
    }
    public function pembimbing()
    {
        return $this->hasMany(PembimbingPerusahaan::class, 'id_perusahaan', 'id');
    }

    public function pimpinan()
    {
        return $this->belongsTo(PimpinanPerusahaan::class, 'id', 'id_perusahaan');
    }
    public function guru()
    {
        return $this->hasMany(GuruPerusahaan::class, 'id_perusahaan', 'id');
    }

    /**
     * The roles that belong to the Perusahaan
     *

     */
    public function jurusan()
    {
        return $this->hasMany(JurusanPerusahaan::class, 'id_perusahaan', 'id');
    }
}
