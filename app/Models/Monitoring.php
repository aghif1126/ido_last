<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Monitoring extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'monitorings';

    protected $fillable = ['kehadiran', 'sikap', 'kompetensi', 'tanggal', 'id_guru', 'id_jurusan','id_perusahaan', 'photo', 'id_tahun'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logFillable()
        ->useLogName('Monitoring');
    }

    /**
     * Get the user that owns the Monitoring
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function perusahaan()
    {
        return $this->belongsTo(Perusahaan::class, 'id_perusahaan', 'id');
    }

    public function guru(){
        return $this->belongsTo(Guru::class, 'id_guru', 'id');
    }
}
