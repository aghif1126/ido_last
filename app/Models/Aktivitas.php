<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Aktivitas extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 'aktivitas';
    protected $fillable = ['kegiatan', 'divisi', 'tanggal', 'mulai', 'selesai', 'id_siswa', 'id_perusahaan', 'id_tahun'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logFillable()
        ->useLogName('Aktivitas');
    }

    public function perusahaan()
    {
        return $this->belongsTo(Perusahaan::class, 'id_perusahaan', 'id');
    }

    public function absen(){
        return $this->hasMany(Absen::class, 'id_siswa', 'id');
    }

}
