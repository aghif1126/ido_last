<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class PembimbingPerusahaan extends Model
{
    use HasFactory, LogsActivity;

    protected $table='pembimbing_perusahaans';
    protected $fillable = ['nama', 'nip', 'alamat', 'telp', 'jenkel', 'id_perusahaan', 'id_tahun', 'id_user', 'id_jurusan'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logFillable()
        ->useLogName('PembimbingPerusahaan');
    }

    public function perusahaan(){
        return $this -> belongsTo(Perusahaan::class, 'id_perusahaan', 'id');
    }
    public function jurusan(){
        return $this -> belongsTo(Jurusan::class, 'id_jurusan', 'id');
    }
}
