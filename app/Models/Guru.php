<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Guru extends Model
{
    use HasFactory, LogsActivity;

    protected $table="gurus";
    protected $guarded = ['id'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logFillable()
        ->useLogName('Guru');
    }

    /**
     * Get all of the comments for the Guru
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function perusahaan()
    {
        return $this->hasMany(GuruPerusahaan::class, 'id_guru', 'id');
    }
}
