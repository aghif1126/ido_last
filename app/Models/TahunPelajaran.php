<?php

namespace App\Models;

use Illuminate\Contracts\Queue\Monitor;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class TahunPelajaran extends Model
{
    use HasFactory, LogsActivity;

    protected $table = "tahun_pelajaran";
    protected $guarded = ['id'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logUnguarded()
        ->useLogName('TahunPelajaran');
    }

    /**
     * Get all of the comments for the TahunPelajaran
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function siswa()
    {
        return $this->hasMany(Siswa::class, 'id_tahun', 'id');
    }
    public function absen()
    {
        return $this->hasMany(Absen::class, 'id_tahun', 'id');
    }
    public function aktivitas()
    {
        return $this->hasMany(Aktivitas::class, 'id_tahun', 'id');
    }
    public function catatan()
    {
        return $this->hasMany(Catatan::class, 'id_tahun', 'id');
    }
    public function guru()
    {
        return $this->hasMany(Guru::class, 'id_tahun', 'id');
    }
    public function jurusan()
    {
        return $this->hasMany(Jurusan::class, 'id_tahun', 'id');
    }
    public function jurusan_perusahaan()
    {
        return $this->hasMany(JurusanPerusahaan::class, 'id_tahun', 'id');
    }
    public function kelas()
    {
        return $this->hasMany(Kelas::class, 'id_tahun', 'id');
    }
    public function konsentrasi()
    {
        return $this->hasMany(KonsentrasiKeahlian::class, 'id_tahun', 'id');
    }
    public function kriteria()
    {
        return $this->hasMany(Kriteria::class, 'id_tahun', 'id');
    }
    public function monitoring()
    {
        return $this->hasMany(Monitoring::class, 'id_tahun', 'id');
    }
    public function nilai()
    {
        return $this->hasMany(Nilai::class, 'id_tahun', 'id');
    }
    public function pembimbing()
    {
        return $this->hasMany(PembimbingPerusahaan::class, 'id_tahun', 'id');
    }
    public function perusahaan()
    {
        return $this->hasMany(Perusahaan::class, 'id_tahun', 'id');
    }
    public function pimpinan_perusahaan()
    {
        return $this->hasMany(PimpinanPerusahaan::class, 'id_tahun', 'id');
    }
    public function siswa_perusahaan()
    {
        return $this->hasMany(SiswaPerusahaan::class, 'id_tahun', 'id');
    }
    public function users()
    {
        return $this->hasMany(User::class, 'id_tahun', 'id');
    }
}
