<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Kelas extends Model
{
    use HasFactory, LogsActivity;
    protected $table = 'kelas';
    protected $fillable = ['nama', 'id_konsentrasi_keahlian', 'id_tahun'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logFillable()
        ->useLogName('Kelas');
    }

    public function konsentrasi()
    {
        return $this->belongsTo(KonsentrasiKeahlian::class, 'id_konsentrasi_keahlian', 'id');
    }

    /**
     * Get the user that owns the Kelas
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

     /**
      * Get all of the comments for the Kelas
      *
      * @return \Illuminate\Database\Eloquent\Relations\HasMany
      */
     public function siswa()
     {
         return $this->hasMany(Siswa::class, 'id_kelas', 'id');
     }

}
