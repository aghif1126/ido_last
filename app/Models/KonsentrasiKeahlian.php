<?php

namespace App\Models;

use App\Models\Jurusan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class KonsentrasiKeahlian extends Model
{
    use HasFactory, LogsActivity;
    protected $table = 'konsentrasi_keahlian';
    protected $fillable = ['nama', 'id_jurusan', 'id_tahun'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logFillable()
        ->useLogName('KonsentrasiKeahlian');
    }

    public function jurusan(){
        return $this->belongsTo(Jurusan::class, 'id_jurusan', 'id');
    }
}
