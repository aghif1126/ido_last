<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Jurusan extends Model
{
    use HasFactory, LogsActivity;
    protected $table = 'jurusan';

    protected $guarded = ['id'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logUnguarded()
        ->useLogName('Jurusan');
    }

    public function kaprog()
    {
        return $this->belongsTo(Guru::class, 'id_guru', 'id');
    }

    public function jurusanperusahaan()
    {
        return $this->belongsTo(JurusanPerusahaan::class, 'id', 'id_perusahaan');
    }
}
