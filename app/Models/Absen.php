<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Absen extends Model
{
    use HasFactory, LogsActivity;

    protected $table = "absen";

    // protected $fillable = ["tanggal", "status", "id_siswa", 'photo'];
    protected $guarded = ['id'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        ->logUnguarded()
        ->useLogName('Absen');
    }

    /**
     * Get all of the comments for the Absen
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function aktivitas()
    {
        return $this->hasMany(Aktivitas::class, 'id_siswa', 'id_siswa');
    }

}
