<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JurusanPerusahaan extends Model
{
    use HasFactory;
    protected $timestamp = false;
    protected $guarded = ['id'];

    public function perusahaan(){
        return $this->belongsTo(Perusahaan::class, 'id_perusahaan', 'id');
    }
    public function detailjurusan(){
        return $this->belongsTo(Jurusan::class, 'id_jurusan', 'id');
    }
}
