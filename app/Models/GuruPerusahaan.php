<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GuruPerusahaan extends Model
{
    use HasFactory;
    protected $table = 'guru_perusahaan';
    protected $guarded = ['id'];

    public function detailperusahaan()
    {
        return $this->belongsto(Perusahaan::class, 'id_perusahaan', 'id');
    }
    public function detailguru()
    {
        return $this->belongsto(Guru::class, 'id_perusahaan', 'id');
    }
    public function jurusan()
    {
        return $this->belongsto(Jurusan::class, 'id_jurusan', 'id');
    }
}
