@extends('layout.gurupembimbing')

@section('title', 'Perusahaan')


@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <h1>Pilih Perusahaan</h1>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    {{-- @foreach ($pembimbing -> perusahaan as $pembimbing -> perusahaan )
                    --}}
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">No Telp</th>

                </tr>
                @foreach ($perusahaan as $a)
                <tr>

                    <td>{{ $loop -> iteration }}</td>
                    <td><a href="/guru/pembimbing/monitoring-form/{{ $a -> detailperusahaan  ->  id }}"><button>{{ $a -> detailperusahaan ->  nama }} ({{ $a -> jurusan -> nama }})</button></a></td>
                    <td>{{ $a -> detailperusahaan -> alamat }}</td>
                    <td>{{ $a -> detailperusahaan -> telp }}</td>



                </tr>
                @endforeach


            </thead>
        </table>
    </div>
</div>
</div>

@endsection
