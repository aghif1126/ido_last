@extends('layout.gurupembimbing')


@section('header')
    <div class="card-header text-center">
        <h3>Pengisian Laporan Monitoring</h3>
    </div>
@endsection



@section('content')
<style>
        .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 100px;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        position: relative;
        background-color: #fefefe;
        margin: auto;
        padding-left: 0.1%;
        padding-right: 0.1%;
        border: 1px solid #888;
        width: 40%;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        -webkit-animation-name: animatetop;
        -webkit-animation-duration: 0.4s;
        animation-name: animatetop;
        animation-duration: 0.4s
    }
    @media (max-width: 600px) {
        .modal-content {
            width: 90%;
        }

        #video {
            max-width: 240px;
            max-height: 240px;
            margin-left: -9%;
            margin-right: -20%;
        }
        #photo {
        /* border: 1px solid black;
        box-shadow: 2px 2px 3px black; */
        max-width: 240px;
            max-height: 200px;
        margin-left: -9%;
            margin-right: -20%;
      }

        /* #photo {
            max-width: 200px;
            max-height: 200px;
            margin-left: -9%;
            margin-right: -20%;
        } */

        #results img {
            max-width: 80%;
            max-height: 80%;
        }

        #my_camera1 video {
            max-width: 75%;
            max-height: 75%;
            margin-left: 29%;
            margin-right: -20%;
        }

        #results1 img {
            max-width: 80%;
            max-height: 80%;
        }
    }
    /* Add Animation */
    @-webkit-keyframes animatetop {
        from {
            top: -300px;
            opacity: 0
        }

        to {
            top: 0;
            opacity: 1
        }
    }

    @keyframes animatetop {
        from {
            top: -300px;
            opacity: 0
        }

        to {
            top: 0;
            opacity: 1
        }
    }

    /* The Close Button */
    .close {
        color: grey;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    .modal-header {
        padding: 2px 16px;
        /* background-color: red; */
        color: black;
    }

    .modal-body {
        padding: 2px 16px;
    }

    .modal-footer {
        padding: 2px 16px;
        background-color: #5cb85c;
        color: white;
    }

    #video {
        /* border: 1px solid black;
        box-shadow: 2px 2px 3px black; */
        width: 320px;
        height: 240px;
        transform: rotateY(3.142rad);
      }

      #photo {
        /* border: 1px solid black;
        box-shadow: 2px 2px 3px black; */
        width: 320px;
        height: 240px;
        display: none;
      }

      #canvas {
        display: none;
      }

      .camera {
        width: 340px;
        display: inline-block;
      }

      .output {
        width: 340px;
        display: inline-block;
        vertical-align: top;
      }

      /* #startbutton {
        display: block;
        position: relative;
        margin-left: auto;
        margin-right: auto;
        bottom: 32px;
        background-color: rgba(0, 150, 0, 0.5);
        border: 1px solid rgba(255, 255, 255, 0.7);
        box-shadow: 0px 0px 1px 2px rgba(0, 0, 0, 0.2);
        font-size: 14px;
        font-family: "Lucida Grande", "Arial", sans-serif;
        color: rgba(255, 255, 255, 1);
        position: absolute;
      } */

      .contentarea {
        font-size: 16px;
        font-family: "Lucida Grande", "Arial", sans-serif;
        width: 760px;
      }
</style>
    <div class="card-body">
        <form action="/guru/pembimbing/store-monitoring" method="post">
            @csrf
            <div class="form-group">
                <label for="input-1">Pilih Perusahaan</label>
                {{-- @dd($perusahaan) --}}
                <select name="id_perusahaan" class="form-control" aria-label="Default select example">
                    @foreach ($perusahaan as $item)
                        <option class="form-control" value="{{ $item -> detailperusahaan -> id }}-{{ $item -> id_jurusan }}">{{ $item -> detailperusahaan -> nama }} ({{ $item -> jurusan -> nama }})</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="input-2">Tanggal Monitoring</label>
                <input type="date" name="tanggal" id="" rows="3" class="form-control">
            </div>
            <div class="form-group">
             <label for="input-3">Kehadiran Siswa</label>
             <textarea name="kehadiran" id="" rows="3" placeholder="Kehadiran Siswa ..." class="form-control"></textarea>
            </div>
            <div class="form-group">
             <label for="input-4">Sikap Siswa</label>
             <textarea name="sikap" id="" rows="3" placeholder="Sikap Siswa ..." class="form-control"></textarea>
            </div>
            <div class="form-group">
             <label for="input-5">Kompetensi Siswa</label>
             <textarea name="kompetensi" id="" rows="3" placeholder="Kompetensai Siswa ..." class="form-control"></textarea>
            </div>
            <div class="form-group">
                <label for="input-5">Foto</label> <br>
                <button type="button" class="btn btn-secondary" id="myBtn">Ambil Foto</button>
            </div>
            <div class="form-group">
                <div class="d-flex flex-row-reverse">
                    <button type="submit" class="btn btn-secondary px-5" onClick="return confirm(`Yakin ingin melanjutkan?`)">Submit</button>
                </div>
            </div>
            <input type="hidden" name="photo" value="" id="photo1">
            <input type="hidden" name="id_guru" value="{{ $guru->id }}">
        </form>
        <!-- The Modal -->
        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close">&times;</span>
                    {{-- <h2>Modal Header</h2> --}}
                </div>
                <div class="modal-body mb-3">
                        <div class="row">
                            <div class="camera" style="margin-left: 18%;">
                                <video id="video">Video stream not available.</video>
                                <canvas id="canvas"> </canvas>
                                <div class="output" style="border-radius: 0px;">
                                  <img id="photo" style="border-radius: 0px; margin-bottom: 20px;" alt="The screen capture will appear in this box."/>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row bd-highlight mb-3">
                            <button class="p-2 btn btn-secondary ml-3" type="button" id="startbutton" onclick="replace('video', 'photo'); replace1('submit')">Take photo</button>
                            <button class="p-2 btn btn-secondary ml-3" onclick="replace('photo', 'video'); close1('submit')" type="button">Ulang</button>
                            <button class="p-2 btn btn-secondary ml-3" id="submit" type="button" style="display: none" onclick="close1('myModal')">Submit</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function replace(hide, show){
            document.getElementById(hide).style.display='none';
            document.getElementById(show).style.display='block';
        }
        function replace1(show){
            document.getElementById(show).style.display='block';
        }

        function close1(hide){
            document.getElementById(hide).style.display='none';
        }
    </script>

    <script type="text/javascript">
        (() => {
                var modal = document.getElementById("myModal");

                // Get the button that opens the modal
                var btn = document.getElementById("myBtn");
                var ambilpoto = document.getElementById("ambilpoto");
                // document.getElementById("submitpoto").style.display = "none";

                // Get the <span> element that closes the modal
                var span = document.getElementsByClassName("close")[0];

                // When the user clicks the button, open the modal
                btn.onclick = function() {
                modal.style.display = "block";
                }

                // When the user clicks on <span> (x), close the modal
                span.onclick = function() {
                modal.style.display = "none";
                }

                // When the user clicks anywhere outside of the modal, close it
                window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
                }
                // The width and height of the captured photo. We will set the
                // width to the value defined here, but the height will be
                // calculated based on the aspect ratio of the input stream.

                const width = 320; // We will scale the photo width to this
                let height = 0; // This will be computed based on the input stream

                // |streaming| indicates whether or not we're currently streaming
                // video from the camera. Obviously, we start at false.

                let streaming = false;

                // The various HTML elements we need to configure or control. These
                // will be set by the startup() function.

                let video = null;
                let canvas = null;
                let photo = null;
                let photo1 = null;
                let startbutton = null;

                function showViewLiveResultButton() {
                    if (window.self !== window.top) {
                    // Ensure that if our document is in a frame, we get the user
                    // to first open it in its own tab or window. Otherwise, it
                    // won't be able to request permission for camera access.
                    document.querySelector(".contentarea").remove();
                    const button = document.createElement("button");
                    button.textContent = "View live result of the example code above";
                    document.body.append(button);
                    button.addEventListener("click", () => window.open(location.href));
                    return true;
                    }
                    return false;
                }

                function startup() {
                    if (showViewLiveResultButton()) {
                    return;
                    }
                    video = document.getElementById("video");
                    canvas = document.getElementById("canvas");
                    photo = document.getElementById("photo");
                    photo1 = document.getElementById("photo1");
                    startbutton = document.getElementById("startbutton");

                    navigator.mediaDevices
                    .getUserMedia({ video: true, audio: false })
                    .then((stream) => {
                        video.srcObject = stream;
                        video.play();
                    })
                    .catch((err) => {
                        console.error(`An error occurred: ${err}`);
                    });

                    video.addEventListener(
                    "canplay",
                    (ev) => {
                        if (!streaming) {
                        height = video.videoHeight / (video.videoWidth / width);

                        // Firefox currently has a bug where the height can't be read from
                        // the video, so we will make assumptions if this happens.

                        if (isNaN(height)) {
                            height = width / (4 / 3);
                        }

                        video.setAttribute("width", width);
                        video.setAttribute("height", height);
                        canvas.setAttribute("width", width);
                        canvas.setAttribute("height", height);
                        streaming = true;
                        }
                    },
                    false
                    );

                    startbutton.addEventListener(
                    "click",
                    (ev) => {
                        takepicture();
                        ev.preventDefault();
                    },
                    false
                    );

                    clearphoto();
                }

                // Fill the photo with an indication that none has been
                // captured.

                function clearphoto() {
                    const context = canvas.getContext("2d");
                    context.fillStyle = "#AAA";
                    context.fillRect(0, 0, canvas.width, canvas.height);

                    const data = canvas.toDataURL("image/png");
                    photo.setAttribute("src", data);
                    photo1.value = data;
                }

                // Capture a photo by fetching the current contents of the video
                // and drawing it into a canvas, then converting that to a PNG
                // format data URL. By drawing it on an offscreen canvas and then
                // drawing that to the screen, we can change its size and/or apply
                // other changes before drawing it.

                function takepicture() {
                    const context = canvas.getContext("2d");
                    if (width && height) {
                    canvas.width = width;
                    canvas.height = height;
                    context.translate(width, 0);
                    context.scale(-1, 1);
                    context.font = "10px";
                    context.fillStyle = "#ffffff";
                    context.drawImage(video, 0, 0, width, height);
                    context.translate(width, 0);
                    context.scale(-1, 1);
                    context.fillText("{{ $wm }}", 200, 230, 140);

                    const data = canvas.toDataURL("image/png");
                    photo.setAttribute("src", data);
                    photo1.value = data;
                    // console.log(photo1.value)

                    } else {
                    clearphoto();
                    }
                }

                // Set up our event listener to run the startup process
                // once loading is complete.
                window.addEventListener("load", startup, false);
        })();
    </script>
@endsection
