@extends('layout.gurupembimbing')

@section('title', 'Perusahaan')

@section('header')

<h2>Welcome Pembimbing</h2>
<hr>
<h3>Hello {{ Auth::user()->name }} anda adalah {{ Auth::user()->role->name }}</h3>
<br>
{{-- <a href="./dashboard"><button type="button" class="align-self-end">Kembali</button></a> --}}
@endsection

@section('content')



<div class="container shadow p-3 mb-5 bg-body-tertiary rounded mt-4">
    <h1>Daftar Perusahaan</h1>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    {{-- @foreach ($pembimbing -> perusahaan as $pembimbing -> perusahaan )
                    --}}
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">No Telp</th>

                </tr>
                @foreach ($perusahaan as $a)
                <tr>

                    <td>{{ $loop -> iteration }}</td>
                    <td><a href="/guru/pembimbing/detail-perusahaan/{{ $a -> id }}"><button>{{ $a -> nama }}</button></a></td>
                    <td>{{ $a ->  alamat }}</td>
                    <td>{{ $a ->  telp }}</td>



                </tr>
                @endforeach


            </thead>
        </table>
    </div>
</div>


@endsection
