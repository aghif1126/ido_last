@extends('layout.gurupembimbing')


@section('header')
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.min.js" integrity="sha384-Rx+T1VzGupg4BHQYs2gCW9It+akI2MM/mndMCy36UVfodzcJcF0GGLxZIzObiEfa" crossorigin="anonymous"></script>
<div class="card-header">
    <h3>Profil Anda</h3>
    <div>
        <div class="d-flex flex-row-reverse">
            <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#editpassword">
                Edit Password
                </button>
                <div class="modal fade" id="editpassword" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title fs-5" id="exampleModalLabel">Ganti Password</h3>
        
                            <button type="button" class="btn zmdi zmdi-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="/guru/pembimbing/update-password" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-outline mb-3">
                                    <input type="text" class="form-control" name="id" placeholder="id" value="{{ $user -> id }}" hidden>
                                    {{-- <input type="text" name="email" value="{{$siswa -> id_user}}"> --}}
                                    <label class="form-label" for="form3Example4">Masukkan Password Baru</label>
                                    <input type="password" name="password" id="inputPassword"
                                            class="form-control form-control-sm" placeholder="Enter password"
                                            onfocus="focusevent1()" onblur="blurevent1()" required />
        
                                    <div class="mb-3 row ml-1">
                                        <div class="col-sm-12" style="margin-top: 6px; margin-left: 8px;">
                                            <input type="checkbox" class="form-check-input" id="show-password"
                                                onclick="myFunction()"> Show Password
                                        </div>
                                    </div>
                                    <select class="form-select" name="role_id" aria-label=".form-select-lg example" hidden>
                                        <option value="3" selected>Guru Pembimbing</option>                
                                    </select>
                                </div>
                        <div class="modal-footer">   
                                <button type="submit" class="btn btn-secondary" onClick="return confirm(`Anda Yakin?`)">Simpan</button>
                            </form>
                    </div>
                </div>
                    </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card-body">
    <div class="table-responsive">
        <table class="table">
            <tbody>
                <tr>
                    <th scope="col">NIP</th>
                    <td>:</td>
                    <td>
                        {{ $guru->nip }}
                    </td>
                </tr>
            <tr>
                <th scope="col">Nama</th>
                <td>:</td>
                <td>
                    {{ $guru->nama }}
                </td>
            </tr>
            <tr>
                <th scope="col">Jenis Kelamin</th>
                <td>:</td>
                <td>
                    {{ $guru->jenkel }}
                </td>
            </tr>
            <tr>
                <th scope="col">No. Telp</th>
                <td>:</td>
                <td>
                    {{ $guru->telp }}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<script>
    
    // Password mulai dari sini
    function myFunction() {
        var x = document.getElementById("inputPassword");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>
@endsection