@extends('layout.gurupembimbing')

@section('title', 'Dashboard')


@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Detail Pembimbing</h3></div>
    </div>
<hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">NIP</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> nip}}</td>
                </tr>
                <tr>
                <th scope="col">Alamat</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> alamat}}</td>
                </tr>
                <tr>
                <th scope="col">No. Telp</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> telp}}</td>
                </tr>
                <tr>
                    <th scope="col">Jenis Kelamin</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> jenkel}}</td>
                </tr>
                <tr>

                    <th scope="col">Nama Perusahaan</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> perusahaan -> nama}}</td>
                </tr>
                <tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
</div>

@endsection
