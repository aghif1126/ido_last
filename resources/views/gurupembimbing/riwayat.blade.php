@extends('layout.gurupembimbing')


@section('header')
    <div class="card-header">
        <h3>Riwayat Kegiatan Monitoring</h3>
        <hr>
    </div>
@endsection


@section('content')
    <div class="card-body">
       

        @if (!is_null($riwayat))
        @foreach ($riwayat as $r)
    
    
        <div class="accordion" id="accordionExample">
            <div class="accordion-item bg-light">
                <h2 class="accordion-header" id="heading{{ $loop -> iteration }}">
                    <button class="accordion-button collapsed btn btn-outline-secondary w-100" type="button" data-bs-toggle="collapse"
                        data-bs-target="#collapse{{ $loop -> iteration }}" aria-expanded="false"
                        aria-controls="collapse{{ $loop -> iteration }}">
                        Tanggal : {{ $r -> tanggal }}
                    </button>
                </h2>
                <div id="collapse{{ $loop -> iteration }}" class="accordion-collapse collapse"
                    aria-labelledby="heading{{ $loop -> iteration }}" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        {{-- <h3>{{ $r -> hari }}</h3> --}}
                        <div class="table-responsive">
                            <table class="table ">
                                <thead>
                                    {{-- <tr>
                                        <th scope="col" style="width: 20%">Tanggal</th>
                                        <td>:</td>
                                        <td>{{ $r -> tanggal }}</td>
                                    </tr> --}}
                                    <tr>
                                        <th scope="col" style="width: 20%">Kehadiran Siswa</th>
                                        <td>:</td>
                                        <td>{{ $r -> kehadiran }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col" style="width: 20%">Sikap Siswa</th>
                                        <td>:</td>
                                        <td>{{ $r -> sikap }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col" style="width: 20%">Kompetensi Siswa</th>
                                        <td>:</td>
                                        <td>{{ $r -> kompetensi }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col" style="width: 20%">Perusahaan</th>
                                        <td>:</td>
                                        <td>{{ $r -> perusahaan -> nama }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col" style="width: 20%">Foto</th>
                                        <td>:</td>
                                        <td><img src="{{ asset('uploads/monitoring/' . $r->photo) }}" style="width: 140px; height: 100px; border: 0px solid black; border-radius:10px" alt=""></td>
                                    </tr>
    
    
                                </thead>
                            </table>
                            <form action="/guru/pembimbing/download/monitoring" method="post">
                                @csrf
                                <input type="text" name="id_user" value="{{ Auth::user() -> id }}" hidden>
                                <input type="text" name="id_perusahaan" value="{{ $r -> perusahaan -> id }}" hidden>
                                <input type="text" name="loop" value="{{ $loop -> iteration }}" hidden>
                                <input type="text" name="tanggal" value="{{ $r -> tanggal }}" hidden>
                                <button class="btn btn-danger" role="button" style="margin-left:87%">Download PDF</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
    
    
        @endforeach
@else
@endif


    </div>

    
{{-- ini dropdownnya --}}
<script src="{{asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
@endsection