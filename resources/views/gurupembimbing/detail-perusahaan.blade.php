@extends('layout.gurupembimbing')
@section('header')
    <div class="card-header">
        <h3>Detail Perusahaan {{ $perusahaan -> nama }}</h3>
    </div>
@endsection

@section('content')
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <th scope="row">Nama</th>
                        <td>:</td>
                        <td>{{ $perusahaan ->  nama }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Alamat</th>
                        <td>:</td>
                        <td>{{ $perusahaan ->  alamat }}</td>
                    </tr>
                    <tr>
                        <th scope="row">No telepon</th>
                        <td>:</td>
                        <td>{{ $perusahaan ->  telp }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Jumlah Siswa</th>
                        <td>:</td>
                        <td>{{ $perusahaan -> siswa -> where('confirm', 'Sudah') -> count() }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Jurusan</th>
                        <td>:</td>
                        <td>
    
                                {{ $perusahaan -> jurusan -> where('id_jurusan', $idjur) -> first() -> detailjurusan -> nama}} <br>
    
    
                        </td>
                    </tr>
                    @if ($perusahaan -> pembimbing -> where('id_jurusan', $idjur) -> all())
                    @foreach ($perusahaan -> pembimbing -> where('id_jurusan', $idjur) -> all() as $pembimbing)
                    <tr>
                        <th scope="col">Pembimbing </th>
                        <td>:</td>
                        <td>
                            <a href="/guru/pembimbing/{{ $pembimbing -> id }}">
                                <button class="btn">
    
                                    {{ $pembimbing -> nama }}
                                </button>
                            </a>
                        </td>
                    </tr>
                      @endforeach
                      @else
                      <tr>
                        <th scope="col">Pembimbing </th>
                        <td>:</td>
                        <td>
    
                            Tidak Tersedia
    
    
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('content1')
    <div class="row">
        <div class="col-12 col-lg-12col-xl-12">
            <div class="card">
                <div class="card-header">
                    <h3>Daftar Siswa</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    {{-- @foreach ($pembimbing -> perusahaan as $pembimbing -> perusahaan )
                                    --}}
                                    <th scope="col">No</th>
                                    <th scope="col">NIS</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">L/P</th>
                                    <th scope="col">Kelas</th>
                
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($siswa as $a)
                
                                <tr>
                
                                    <td>{{ $loop -> iteration }}</td>
                                    <td>{{ $a -> detailsiswa -> nis }}</td>
                                    <form action="/guru/pembimbing/detail-siswa" method="post">
                                        @csrf
                                        <input type="text" name="id_siswa" value="{{ $a -> detailsiswa -> nis }}" hidden>
                                        <input type="text" name="id_perusahaan" value="{{ $perusahaan -> id}}" hidden>
                                    <td>
                                        <a href="/guru/pembimbing/detail-siswa" class="link-offset-2 link-offset-3-hover link-underline link-underline-opacity-0 link-underline-opacity-75-hover"><button class="btn">{{ $a -> detailsiswa ->  nama }}</button></a>
                                        </td>
                                    </form>
                                    <td>{{ $a -> detailsiswa ->  jenkel }}</td>
                                    <td>{{ $a -> detailsiswa ->  kelas -> nama }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
