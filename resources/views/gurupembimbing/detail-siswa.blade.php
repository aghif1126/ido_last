@extends('layout.gurupembimbing')

@section('header')
    <div class="card-header">
        <h3>Detail Siswa</h3>
    </div>
@endsection

@section('content')
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <th scope="row">NIS</th>
                        <td>:</td>
                        <td>{{ $siswa -> nis}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Nama</th>
                        <td>:</td>
                        <td>{{ $siswa -> nama}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Kelas</th>
                        <td>:</td>
                        <td>{{ $siswa -> kelas -> nama}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Jenis Kelamin</th>
                        <td>:</td>
                        <td>{{ $siswa -> jenkel}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Alamat</th>
                        <td>:</td>
                        <td>{{ $siswa -> alamat}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Tanggal Lahir</th>
                        <td>:</td>
                        <td>{{ $siswa -> tanggal_lahir}}</td>
                    </tr>
                    <tr>
                        <th scope="row">No telepon</th>
                        <td>:</td>
                        <td>{{ $siswa -> telp}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Tahun Pelajaran</th>
                        <td>:</td>
                        <td>{{ $siswa -> tahun_pelajaran -> tahun}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Periode</th>
                        <td>:</td>
                        <td>
                            {{ $bulan[intval(explode('-', $siswa -> perusahaan -> where('id_perusahaan', $perusahaan -> id) -> first() -> awal_periode)[1])]}}
                            ({{ explode('-',  $siswa -> perusahaan -> where('id_perusahaan', $perusahaan -> id) -> first() -> awal_periode)[0] }}) -
                            {{ $bulan[intval(explode('-', $siswa -> perusahaan -> where('id_perusahaan', $perusahaan -> id) -> first() -> akhir_periode)[1])]}}
                            ({{ explode('-',  $siswa -> perusahaan -> where('id_perusahaan', $perusahaan -> id) -> first() -> akhir_periode)[0] }})
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">Program Keahlian</th>
                        <td>:</td>
                        <td>{{ $siswa -> kelas -> konsentrasi -> jurusan -> nama}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Konsentrasi Keahlian</th>
                        <td>:</td>
                        <td>{{ $siswa -> kelas -> konsentrasi -> nama}}</td>
                    </tr><tr>
                        <th scope="col">Nama Orang Tua</th>
                        <td>:</td>
                        <td>{{ $siswa -> nama_ortu}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Alamat Orang Tua</th>
                        <td>:</td>
                        <td>{{ $siswa -> alamat_ortu}}</td>
                    </tr>
                    <tr>
                        <th scope="col">No. Telp Orang Tua</th>
                        <td>:</td>
                        <td>{{ $siswa -> telp_ortu}}</td>
                    </tr>


                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('content1')
    <div class="row">
        <div class="col-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header">
                    <h3>Riwayat Kehadiran</h3>
                </div>
                <div class="card-body">
                    @if (!is_null($absen))
                        @foreach ($absen as $r)
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item" style="margin-bottom: -50px">
                                    <h2 class="accordion-header" id="heading{{ $loop -> iteration + 800000 }}">
                                        <button class="accordion-button collapsed btn btn-outline-secondary w-100" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapse{{ $loop -> iteration + 800000 }}" aria-expanded="false"
                                            aria-controls="collapse{{ $loop -> iteration + 800000 }}">
                                            Tanggal : {{ $r -> tanggal }}
                                            {{-- {{ $r -> aktivitas }} --}}
                                        </button>
                                    </h2>
                                    <div id="collapse{{ $loop -> iteration + 800000 }}" class="accordion-collapse collapse"
                                    aria-labelledby="heading{{ $loop -> iteration + 800000 }}" data-bs-parent="#accordionExample">

                                        <div class="accordion-body bg-light">
                                            <div class="container">
                                                <h4>Detail Absen</h4>
                                                <div class="table-responsive">

                                                    <table class="table ">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col" style="width: 20%">Jam Masuk</th>
                                                                <td>:</td>
                                                                <td>{{ $r -> jam_masuk }}</td>
                                                                {{-- <th scope="col" style="width: 20%">Jam Pulang</th>
                                                                <td>:</td>
                                                                <td>{{ $r -> jam_masuk }}</td> --}}
                                                            </tr>
                                                            <tr>
                                                                <th scope="col" style="width: 20%">Status</th>
                                                                <td>:</td>
                                                                <td>{{$r -> status}}</td>
                                                            </tr>
                                                            <tr>
                                                                @if ($r->status == "Hadir")
                                                                <th scope="col" style="width: 20%">Poto</th>
                                                                <td>:</td>
                                                                <td>@if ($r -> poto_masuk)
                                                                        <img src="{{ asset('uploads/absen/masuk/uploads'.$r->poto_masuk) }}" style="border-radius:10px" alt=""
                                                                        height="240px" width="320px">
                                                                    @endif
                                                                </td>
                                                                @endif
                                                            </tr>
                                                            {{-- <tr>
                                                                <th scope="col" style="width: 20%">Tanggal</th>
                                                                <td>:</td>
                                                                <td>{{ $r -> tanggal }}</td>
                                                            </tr> --}}

                                                            @if ($r->jam_pulang != "Tidak Ada")
                                                            <tr>
                                                                <th scope="col" style="width: 20%">Pulang</th>
                                                                <td>:</td>
                                                                <td>{{ $r -> jam_pulang }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="col" style="width: 20%">Poto Pulang</th>
                                                                <td>:</td>
                                                                <td><img src="{{ asset('uploads/absen/pulang/uploads'.$r->poto_pulang) }}" alt="" height="100px" width="150px" style="border-radius: 10px;"></td>
                                                            </tr>
                                                            @endif


                                                        </thead>

                                                    </table>
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <br>
                                    <br>

                                    <div id="collapse{{ $loop -> iteration + 800000  }}" class="accordion-collapse collapse"
                                        aria-labelledby="heading{{ $loop -> iteration + 800000       }}" data-bs-parent="#accordionExample">
                                        @foreach ($r -> aktivitas  as $aktivitas)
                                        <div class="accordion-body bg-light">
                                            <div class="container">
                                                <h4>Kegiatan {{ $loop-> iteration }}</h4>
                                                <div class="table-responsive">

                                                    <table class="table ">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col" style="width: 20%">Kegiatan</th>
                                                                <td>:</td>
                                                                <td>{{ $aktivitas -> kegiatan }}</td>
                                                            </tr>

                                                            <tr>
                                                                <th scope="col" style="width: 20%">Mulai</th>
                                                                <td>:</td>
                                                                <td>{{ $aktivitas -> mulai }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="col" style="width: 20%">Selesai</th>
                                                                <td>:</td>
                                                                <td>{{ $aktivitas -> selesai }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="col" style="width: 20%">Perusahaan</th>
                                                                <td>:</td>
                                                                <td>{{ $aktivitas -> perusahaan -> nama }}</td>
                                                            </tr>


                                                        </thead>

                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <br> <br>
                                </div>
                            </div>
                        @endforeach
                    @else

                    @endif
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header">
                    <h3>Nilai Siswa di Perusahaan</h3>
                </div>
                <div class="card-body">
                    @if (!is_null($siswa -> nilai))
                        @foreach ($siswa -> perusahaan  as $perusahaan)
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="heading{{ $loop -> iteration }}">
                                        <button class="accordion-button collapsed btn btn-outline-secondary w-100" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapse{{ $loop -> iteration }}" aria-expanded="false"
                                            aria-controls="collapse{{ $loop -> iteration }}">
                                            {{ $perusahaan -> detail ->  nama }}
                                        </button>
                                    </h2>
                                    {{-- @dd($perusahaan -> detail ->  pembimbing -> count()) --}}
                                    @if ($perusahaan -> detail ->  pembimbing -> where('id_jurusan', $siswa -> kelas -> konsentrasi -> jurusan -> id) -> first())




                                    {{-- @foreach ($perusahaan -> detail ->  pembimbing -> all() as $key => $perusahaan -> detail ->  pembimbing[$i]) --}}
                                    {{-- @dd($key) --}}
                                    <div id="collapse{{ $loop -> iteration }}" class="accordion-collapse collapse"
                                        aria-labelledby="heading{{ $loop -> iteration }}" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <h3>{{ $perusahaan -> detail ->  pembimbing -> where('id_jurusan', $siswa -> kelas -> konsentrasi -> jurusan -> id) -> first()  -> nama }}</h3>
                                            <div class="table-responsive">
                                                <table class="table ">
                                                    <thead>
                                                        <tr>
                                                            <td scope="col" style="width: 5%;" class="table-secondary">
                                                                <p>No</p>
                                                            </td>
                                                            <td scope=" col" style="width: 20%" class="table-secondary">
                                                                <p>Komponen</p>
                                                            </td>
                                                            <td scope="col" style="width: 20%" class="table-secondary">Skor<br>(0-100)</td>
                                                            <td scope="col" style="width: 20%" class="table-secondary">
                                                                <p>Keterangan</p>
                                                            </td>

                                                        </tr>

                                                        @foreach ($kriteria -> where('id_pembimbing' , $perusahaan -> detail ->  pembimbing -> where('id_jurusan', $siswa -> kelas -> konsentrasi -> jurusan -> id) -> first()  -> id) as $item)
                                                        {{-- $nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa ->nis)-> first() --}}
                                                            <tr>
                                                                <td rowspan="1" style="background-color: rgb(236, 236, 236)">{{ $loop -> iteration
                                                                    }}</td>
                                                                <td style="background-color: rgb(236, 236, 236)">{{ $item -> nama_kriteria }}</td>
                                                                <td style="background-color: rgb(236, 236, 236)">
                                                                    @if ($nilai)
                                                                    {{ $nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa ->
                                                                    nis)-> first() ? $nilai -> where('id_kriteria', $item -> id)-> where('id_siswa',
                                                                    $siswa -> nis) -> first() -> point : ''}}
                                                                    @else
                                                                    @endif

                                                                </td>
                                                                <td style="background-color: rgb(236, 236, 236)">
                                                                    @if ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first())

                                                                    @if ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point >= 90)
                                                                        <div>A</div>
                                                                    @elseif ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point < 90)
                                                                        <div>B</div>
                                                                    @elseif ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point < 80)
                                                                        <div>C</div>
                                                                    @else
                                                                        <div>D</div>
                                                                    @endif
                                                                @else

                                                                @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </thead>
                                                </table>
                                                <br>
                                                <form action="/siswa/download/jurnalsiswa" method="post">
                                                    @csrf
                                                    <input type="text" name="id_siswa" value="{{ $siswa -> nis }}" hidden>
                                                    <input type="text" name="id_pembimbing" value="{{ $perusahaan -> detail ->  pembimbing -> where('id_jurusan', $siswa -> kelas -> konsentrasi -> jurusan -> id) -> first()  -> id }}" hidden>
                                                    <input type="text" name="id_perusahaan" value="{{ $perusahaan -> detail -> id }}" hidden>
                                                    <button class="btn btn-success" role="button">Download PDF</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- @endforeach --}}

                                    @endif
                                </div>

                            </div>
                        @endforeach
                    @else

                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header">
                    <h3>Catatan Siswa</h3>
                </div>
                <div class="card-body">
                    @if (!is_null($catatan))

                        @foreach ($catatan as $catatan)


                            <div class="accordion" id="catatanexample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headcatatan">
                                    <button class="accordion-button collapsed btn btn-outline-secondary w-100" type="button" data-bs-toggle="collapse" data-bs-target="#collaps{{ $loop -> iteration  }}" aria-expanded="false" aria-controls="collaps{{ $loop -> iteration}}">
                                    {{ $catatan -> perusahaan -> nama }}
                                    </button>
                                    </h2>
                                    <div id="collaps{{ $loop -> iteration  }}" class="accordion-collapse collapse" aria-labelledby="headcatatan" data-bs-parent="#catatanexample">
                                    <div class="accordion-body">

                                        <div class="table-responsive">
                                            <table class="table ">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" style="width: 20%">Catatan</th>
                                                        <td>:</td>
                                                        <td>{{ $catatan -> catatan }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="col" style="width: 20%">Tanggal Dibuat</th>
                                                        <td>:</td>
                                                        <td>{{ $catatan -> tanggal }}</td>
                                                    </tr>


                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>


                        @endforeach

                    @else

                    @endif
                </div>
            </div>
        </div>
    </div>

    {{-- ini dropdownnya --}}
    <script src="{{asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
@endsection
