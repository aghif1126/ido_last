@extends('layout.gurupembimbing')

@section('dashboard')
    <div style="background-color: rgb(10, 115, 235);margin-right: -25px; margin-left: -25px; margin-top:-10px; padding-right: 12.5px; padding-left: 12.5px; 12.5px; padding-top: 10.5px; 12.5px; padding-bottom: 10.5px;">
        <h3 class="text-white">Hi, {{ $guru -> nama }} </h3>
        <br>
        <div class="row">
            <div class="col-lg-12">
               <div class="card">
                <div class="card-body">
                    Ayo Lakukan Monitoring 
                    <br>
                    <a href="/guru/pembimbing/monitoring">    
                        <button class="btn btn-collapsed btn-info col-lg-12">Monitoring</button>
                    </a>
                </div>
               </div>
            </div>
        </div>
    </div>
    <br>
@endsection


@section('content')
    <div class="card-body">
        
        <h3>
            Daftar Perusahaan
        </h3>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Nama</th>
                        <th scope="col">Jumlah Siswa</th>
                        <th scope="col">Jurusan</th>
                      </tr>
                </thead>
               <tbody>
                {{-- @dd($perusahaan) --}}
                @foreach ($guru -> perusahaan -> all() as $perusahaan)
                {{-- @foreach ($perusahaan -> detailperusahaan -> jurusan as $jurusan) --}}
                <tr>
                    <td><a href="/guru/pembimbing/detail-perusahaan/{{ $perusahaan -> detailperusahaan -> id }}/{{ $perusahaan -> id_jurusan }}"><button class="btn">{{ $perusahaan -> detailperusahaan -> nama }}</button></a></td>
                    @if (($perusahaan -> detailperusahaan -> siswa -> where('confirm', "Sudah")) == "[]")
                    <td>Tidak Tersedia</td>
                    @else
                    <td>{{ $perusahaan -> detailperusahaan -> siswa -> where('confirm', "Sudah") -> where('id_jurusan', $perusahaan -> id_jurusan) -> count() }}</td>
                    @endif
                    <td>
                    {{ $perusahaan -> detailperusahaan -> jurusan -> where('id_jurusan', $perusahaan -> id_jurusan) -> first() -> detailjurusan -> nama }} <br>

                </td>
            </tr>
            {{-- @endforeach --}}
                @endforeach
               </tbody>
            </table>
        </div>
    </div>
@endsection
