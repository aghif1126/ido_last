

@extends('layout.gurupembimbing')

@section('title', 'Dashboard')


@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
    <h1>Daftar Perusahaan</h1></div>

    </div>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col" style="width: 20%">Nama</th>
                    <th scope="col" style="width: 20%">Jumlah Siswa</th>
                </tr>
                @foreach ($guru -> perusahaan as $perusahaan)
                <tr>
                    <td><a href="/guru/pembimbing/detail-perusahaan/{{ $perusahaan -> detailperusahaan -> id }}/{{ $jurusan -> find($perusahaan -> id_jurusan) -> id  }}"><button class="btn">{{ $perusahaan -> detailperusahaan -> nama }} ({{ $jurusan -> find($perusahaan -> id_jurusan) -> nama }})</button></a></td>
                    @if (($perusahaan  -> detailperusahaan  -> siswa -> where('id_jurusan', $jurusan -> find($perusahaan -> id_jurusan) -> id) -> where('confirm', "Sudah")) == "[]")
                    <td>Tidak Tersedia</td>
                    @else
                    <td>{{ $perusahaan  -> detailperusahaan  -> siswa ->  where('confirm', "Sudah") -> count() }}</td>
                    @endif
            </tr>

                @endforeach
            </thead>
        </table>
    </div>
</div>
</div>

@endsection

