<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Prakerin</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <style>
        #map {
    height: 500px;
    width: 100%;
    }
    </style>
    <!-- Favicon -->
    <link href="{{asset('logo/Logo.png')}}" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500&family=Jost:wght@500;600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{ asset('dboard/lib/animate/animate.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dboard/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('dboard/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="{{ asset('dboard/css/dashboard.css')}}" rel="stylesheet">
</head>

<body data-bs-spy="scroll" data-bs-target=".navbar" data-bs-offset="51">

    <div class="container-xxl bg-white p-0">
        <!-- Spinner Start -->
        {{-- <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-grow text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div> --}}
        <!-- Spinner End -->


        <!-- Navbar & Hero Start -->
        <div class="container-xxl position-relative p-0" id="home">
            <nav class="navbar navbar-expand-lg navbar-light px-4 px-lg-5 py-3 py-lg-0">
                <a href="" class="navbar-brand p-0">
                    <h1 class="m-0">Prakerin</h1>
                    <!-- <img src="img/logo.png" alt="Logo"> -->
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav mx-auto py-0">
                        <a href="#home" class="nav-item nav-link active">Home</a>
                        <a href="#feature" class="nav-item nav-link">Fitur</a>
                        <a href="#teamplay" class="nav-item nav-link">Teamplay</a>
                        {{-- <a href="#about" class="nav-item nav-link">About</a> --}}
                        <!-- <a href="#review" class="nav-item nav-link">About</a>
                        <a href="#contact" class="nav-item nav-link">Contact</a> -->
                    </div>
                    {{-- <a href="/logout" class="btn btn-primary-gradient rounded-pill py-2 px-4 ms-3 d-none d-lg-block" style="background-color: #899bbd;">Login</a> --}}
                </div>
            </nav>

            <div class="container-xxl bg-primary hero-header">
                <div class="container px-lg-5">
                    <div class="row g-5">
                        <div class="col-lg-8 text-center text-lg-start">
                            <h1 class="text-white mb-4 animated slideInDown">Selesaikan Semua Yang Telah Dilakukan Dengan Cara Yang Terbaik</h1>
                            <p class="text-white pb-3 animated slideInDown"></p>
                            <a href="logout" class="btn btn-primary-gradient py-sm-3 px-4 px-sm-5 rounded-pill me-3 animated slideInLeft" style="background-color: #899bbd;">Login</a>
                        </div>
                        <div class="col-lg-4 d-flex justify-content-center justify-content-lg-end wow fadeInUp" data-wow-delay="0.3s">
                            <div class="owl-carousel screenshot-carousel">
                                <img class="img-fluid" src="{{asset('bg/screenshot-1.png')}}" alt="">
                                <img class="img-fluid" src="{{asset('bg/screenshot-2.png')}}" alt="">
                                <img class="img-fluid" src="{{asset('bg/screenshot-3.png')}}" alt="">
                                <img class="img-fluid" src="{{asset('bg/screenshot-4.png')}}" alt="">
                                <img class="img-fluid" src="{{asset('bg/screenshot-5.png')}}" alt="">
                            </div>
                        </div>
                        {{-- <div class="col-lg-4 d-flex justify-content-center justify-content-lg-end wow fadeInUp" data-wow-delay="0.3s">
                            <div class="testt">
                                <div class="owl-carousel screenshot-carousel">
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>

        <!-- Features Start -->
        <div class="container-xxl py-5">
            <div class="container py-5 px-lg-5" id="feature">
                <div class="text-center wow fadeInUp" data-wow-delay="0.1s">
                    <h5 class="text-primary-gradient fw-medium">App Features</h5>
                    <h1 class="mb-5">Fitur yang tersedia untuk siswa</h1>
                </div>
                <div class="row g-4">
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                        <a href="fitur-absen">
                            <div class="feature-item bg-light rounded p-4">
                                <div class="d-inline-flex align-items-center justify-content-center bg-primary-gradient rounded-circle mb-4" style="width: 60px; height: 60px;">
                                    <i class="fa fa-eye text-white fs-4"></i>
                                </div>
                                <h5 class="mb-3">Absen</h5>
                                <h6 class="m-0">Absen datang dan pulang setiap hari pada saat pkl.</h6>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                        <a href="fitur-jurnal-kegiatan">
                            <div class="feature-item bg-light rounded p-4">
                                <div class="d-inline-flex align-items-center justify-content-center bg-secondary-gradient rounded-circle mb-4" style="width: 60px; height: 60px;">
                                    <i class="fa fa-layer-group text-white fs-4"></i>
                                </div>
                                <h5 class="mb-3">Jurnal Kegiatan</h5>
                                <h6 class="m-0">Setelah absen isilah jurnal kegiatan anda, apa yang anda lakukan saat pkl.</h6>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                        <a href="fitur-nilai">
                            <div class="feature-item bg-light rounded p-4">
                                <div class="d-inline-flex align-items-center justify-content-center bg-primary-gradient rounded-circle mb-4" style="width: 60px; height: 60px;">
                                    <i class="fa fa-edit text-white fs-4"></i>
                                </div>
                                <h5 class="mb-3">Nilai</h5>
                                <h6 class="m-0">Melihat nilai yang diberikan oleh pembimbing perusahaan dan anda bisa print nilai tersebut</h6>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Features End -->


        <!-- Pricing Start -->
        {{-- <div class="container-xxl py-5" id="teamplay">
            <div class="container py-5 px-lg-5">
                <div class="text-center wow fadeInUp" data-wow-delay="0.1s">
                    <h5 class="text-primary-gradient fw-medium">Daftar Perusahaan</h5>
                    <h1 class="mb-5">Yang Telah Bekerja Sama Dengan Kami</h1>

                </div>
                <div class="tab-class text-center pricing wow fadeInUp" data-wow-delay="0.1s">
                    <div class="tab-content text-start"> --}}

                        {{-- @foreach ($jurusan as $j) --}}
                        {{-- <div class="tab-pane fade show p-0 active">
                            
                            <div class="row g-4">
                                
                                @foreach ($perusahaan as $pe)
                                @if ($pe -> mou == 'ya')
                                <div class="col-lg-4">
                                    <div class="bg-light rounded">
                                        <div class="border-bottom p-4 mb-4">
                                            <h4 class="text-primary-gradient mb-1">{{$pe  -> nama}}</h4>
                                            <span>@foreach ($pe -> jurusan as $jurusan)
                                                {{ $jurusan -> nama }} <br>
                                                @endforeach</span>
                                        </div>
                                        <div class="p-4 pt-0">
                                            <h1 class="mb-3">
                                                <small class="align-top" style="font-size: 22px; line-height: 45px;">Google Maps</small>
                                            </h1>
                                            <div class="d-flex justify-content-between mb-3">
                                                <div>
                                                    <iframe src="{{$pe -> lokasi}}"  style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach

                            </div>
                        </div> --}}
                        {{-- @endforeach --}}
                        
                        
                    {{-- </div>
                </div>
            </div>
        </div> --}}
        <!-- Pricing End -->


        <!-- Testimonial Start -->
        <div class="container-xxl py-5">
            <div class="container px-lg-5" id="teamplay">
                <div class="text-center py-5 wow fadeInUp" data-wow-delay="0.1s">
                    <h1>Daftar Perusahaan</h1>
                    <h5 class="text-primary-gradient fw-medium">Yang Telah Bekerja Sama Dengan Kami</h5>
                </div>
                <div class="owl-carousel testimonial-carousel wow fadeInUp" data-wow-delay="0.1s">
                    @foreach ($perusahaan as $pe)
                        @if ($pe -> mou == 'ya')
                            <div class="testimonial-item rounded p-4 bg-light me-3">
                                <div class="d-flex align-items-center mb-4">
                                    {{-- <img class="img-fluid bg-white rounded flex-shrink-0 p-1" src="{{ asset('dboard/img/testimonial-2.jpg') }}" style="width: 85px; height: 85px;"> --}}
                                    <div class="ms-4">
                                        <h5 class="mb-1">{{$pe -> nama}}</h5>
                                        @foreach ($pe -> jurusan as $jurusan )
                                            <p class="mb-1">
                                                {{ $jurusan -> detailjurusan -> nama }} (Kuota {{ $jurusan -> kuota}} orang)  <br>
                                            </p>
                                        @endforeach
                                        {{-- @foreach ($pe -> jurusan as $jurusan)
                                            <p class="mb-1">
                                                {{$jurusan -> nama}}
                                                <br>
                                            </p>
                                        @endforeach --}}
                                    </div>
                                </div> <hr>
                                <p class="text-center">Google Maps</p>
                                <div class="mb-0">
                                    <iframe src="{{$pe -> lokasi}}" width="100%"  style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    {{-- <div class="testimonial-item rounded p-4">
                        <div class="d-flex align-items-center mb-4">
                            <img class="img-fluid bg-white rounded flex-shrink-0 p-1" src="{{ asset('dboard/img/testimonial-2.jpg') }}" style="width: 85px; height: 85px;">
                            <div class="ms-4">
                                <h5 class="mb-1">Rafli S.R.</h5>
                                <p class="mb-1">FrontEnd</p>
                                <div>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star"></small>
                                </div>
                            </div>
                        </div>
                        <p class="mb-0">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit diam amet diam et eos. Clita erat ipsum et lorem et sit.</p>
                    </div>
                    <div class="testimonial-item rounded p-4">
                        <div class="d-flex align-items-center mb-4">
                            <img class="img-fluid bg-white rounded flex-shrink-0 p-1" src="{{ asset('dboard/img/testimonial-2.jpg')}}" style="width: 85px; height: 85px;">
                            <div class="ms-4">
                                <h5 class="mb-1">M. Ridzki N.</h5>
                                <p class="mb-1">Frontend</p>
                                <div>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star"></small>
                                </div>
                            </div>
                        </div>
                        <p class="mb-0">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit diam amet diam et eos. Clita erat ipsum et lorem et sit.</p>
                    </div>
                    <div class="testimonial-item rounded p-4">
                        <div class="d-flex align-items-center mb-4">
                            <img class="img-fluid bg-white rounded flex-shrink-0 p-1" src="{{ asset('dboard/img/testimonial-3.jpg')}}" style="width: 85px; height: 85px;">
                            <div class="ms-4">
                                <h5 class="mb-1">Novan A.G.H.</h5>
                                <p class="mb-1">Backend</p>
                                <div>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                </div>
                            </div>
                        </div>
                        <p class="mb-0">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit diam amet diam et eos. Clita erat ipsum et lorem et sit.</p>
                    </div>
                    <div class="testimonial-item rounded p-4">
                        <div class="d-flex align-items-center mb-4">
                            <img class="img-fluid bg-white rounded flex-shrink-0 p-1" src="{{ asset('dboard/img/testimonial-3.jpg')}}" style="width: 85px; height: 85px;">
                            <div class="ms-4">
                                <h5 class="mb-1">M. Fathir N.A</h5>
                                <p class="mb-1">Backend</p>
                                <div>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                    <small class="fa fa-star text-warning"></small>
                                </div>
                            </div>
                        </div>
                        <p class="mb-0">Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit diam amet diam et eos. Clita erat ipsum et lorem et sit.</p>
                    </div> --}}
                </div>
            </div>
        </div>
        <!-- Testimonial End -->


        <!-- Footer Start -->
        <div class="container-fluid bg-primary text-light footer wow fadeIn" data-wow-delay="0.1s" id="about">
            <div class="container py-5 px-lg-5">
                <div class="row g-5">
                    <div class="col-md-6 col-lg-6">
                        <h4 class="text-white mb-4">Address</h4>
                        <p><i class="fa fa-map-marker-alt me-3"></i>Jl. Budi Jl. Raya Cilember, RT.01/RW.04, Sukaraja, Kec. Cicendo, Kota Bandung, Jawa Barat 40153</p>
                        <p><i class="fa fa-phone-alt me-3"></i>(022) 6652442</p>
                        <p><i class="fa fa-envelope me-3"></i>smkn11bdg@gmail.com</p>
                        <div class="d-flex pt-2">
                            <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-twitter"></i></a>
                            <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-instagram"></i></a>
                            <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-linkedin-in"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container px-lg-5">
                <div class="copyright">
                    <div class="row">
                        <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                            &copy; <a class="border-bottom" href="#">Prakerin</a>, All Right Reserved.

							<!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
							Designed By <a class="border-bottom" href="https://htmlcodex.com">I-Do</a>
                            </br>
                            Distributed By <a class="border-bottom" href="https://themewagon.com" target="_blank">RPL 2</a>
                        </div>
                        <div class="col-md-6 text-center text-md-end">
                            <div class="footer-menu">
                                <a href="#">Home</a>
                                <a href="#feature">Fitur</a>
                                <a href="#teamplay">Teamplay</a>
                                <a href="#about">About</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-lg-square back-to-top pt-2"><i class="bi bi-arrow-up text-white"></i></a>
    </div>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('dboard/lib/wow/wow.min.js')}}"></script>
    <script src="{{ asset('dboard/lib/easing/easing.min.js')}}"></script>
    <script src="{{ asset('dboard/lib/waypoints/waypoints.min.js')}}"></script>
    <script src="{{ asset('dboard/lib/counterup/counterup.min.js')}}"></script>
    <script src="{{ asset('dboard/lib/owlcarousel/owl.carousel.min.js')}}"></script>
    
    <!-- Template Javascript -->
    <script src="{{ asset('dboard/js/dashboard.js')}}"></script>
</body>

</html>
