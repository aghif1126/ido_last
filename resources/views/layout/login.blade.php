{{-- CREATE FORM LOGIN USING PHP AND BOOSTRAP ? --}}
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous">
    </script>
    <title>Login</title>
    <style>
        .footer {
            position: fixed;
            bottom: 0;
            center: 0;
            margin: 5px;
            padding: 5px;
            /* height: 18px;
        left: 10px;
        top: 610px;
        font-style: salsa;
        margin-top: 1vh; */
        }

        .logo {
            position: fixed;
            width: 40%;
            height: 40%;
            left: 1%;
            top: 1%;
        }

        html {
            height: 100%;
        }

        #content {
            min-height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            height: 100%;
            background-image: url('{{ asset('/bg/bglogin.png') }}');
            background-color: #dbe4f6;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: auto;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
        }

        .login {
            position: absolute;
            left: 7.5%;
            top: 6.5;
            font-size: 200%;
            font-family: Rum Raisin;
        }

        .tengah {
            position: absolute;
            width: 35%;
            height: 68%;
            left: 37%;
            top: 9%;
            background-color: #f6f9ff;
            border-radius: 20px;
            filter: drop-shadow(20px 15px 4px #bdbdbd);
        }

        /* @media screen and {max-width: 600px;} */

        .bgtengah {
            position: absolute;
            left: 49%;
            right: 0%;
            top: -0.1%;
            bottom: 0%;
            border-radius: 0px 13px 13px 13px;
        }
        @media (max-width: 600px) {
            .tengah{
                display: none;
            }
            .logo {
                display: none;
            }
            /* .footer{
                display: none;
            } */
        }
        @media (max-width: 600px){
            .mobil {
                width: 90%;
                height: 30%;
                margin: 0 auto;
                margin-top: 10%;
                background-color: #f6f9ff;
                border-radius: 20px;
                padding: 3% 3% 3% 3%;
            }
        }
        @media (min-width : 600px){
            .mobil {
                display: none;
            }
        }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href="{{ asset('/logo/Logo.png') }}" rel="icon">
</head>

<body bgcolor="#f6f9ff">

    {{-- <img class="background" src= alt="background"> --}}
    <div class="logo">
        <img src="{{asset('/logo/logo.png')}}" alt="Logo" width="75" height="75"
            class="d-inline-block align-text-top">
    </div>
    <div class="mobil">
        <div>

            <form action="/login/auth" method="post">
                @csrf

                <div class="form-outline mb-3">
                    <label class="form-label" for="form3Example3">Username</label>
            <input type="email" name="email" id="form3Example3" class="form-control form-control-sm"
            style="width:343px; border: 2px solid #899bbd; background: #f6f9ff;"
            placeholder="Enter a valid email address" onfocus="focusevent()" onblur="blurevent()"
                required />
            </div>


            <!-- Password input -->
            <div class="form-outline mb-3">
                <label class="form-label" for="form3Example4">Password</label>
                <input type="password" name="password" id="inputPassword"
                    style="width:343px; border: 2px solid #899bbd; background: #f6f9ff;"
                    class="form-control form-control-sm" placeholder="Enter     "
                    onfocus="focusevent1()" onblur="blurevent1()" required />

                    <div class="mb-3 row">
                        <div class="col-sm-12" style="margin-top: 6px; margin-left: 8px;">
                            <input type="checkbox" class="form-check-input" id="show-password"
                            onclick="myFunction()"> Show Password
                        </div>
                    </div>
                </div>

                <div class="d-flex justify-content-between align-items-center">
                    <!-- Checkbox -->


                    <div class="text-center text-lg-start mt-2 pt-3">
                    <button type="submit" class="btn btn-secondary btn-sm "
                        style="padding-left:25.5px; padding-right: 25.5px; margin-left: 280%; font-weight: bold; border: 2px solid #899bbd; background: #f6f9ff;">
                        <font color="#000000" style=" font-family: Rum Raisin;">Login</font>
                    </button>

                </div>

            </form>
        </div>

    </div>


    <style>
        .spinner {
   width: 100px;
   height: 100px;
   display: grid;
   border: 7px solid #0000;
   border-radius: 50%;
   border-color: #3b71ca #0000;
   animation: spinner-e04l1k 0.8s infinite linear;
}

.spinner::before,
.spinner::after {
   content: "";
   grid-area: 1/1;
   margin: 3.5px;
   border: inherit;
   border-radius: 50%;
}

.spinner::before {
   border-color: #9fa6b2 #0000;
   animation: inherit;
   animation-duration: 0.4s;
   animation-direction: reverse;
}

.spinner::after {
   margin: 14px;
}

@keyframes spinner-e04l1k {
   100% {
      transform: rotate(1turn);
   }
}
.preloader {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 9999;
    /* background-color: #fff; */
}
.preloader .loading {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%,-50%);
    font: 14px arial;
}
#spinner-overlay {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(255, 255, 255, 1); /* White overlay */
            z-index: 9999;
            opacity: 0.9;
        }

        #spinner-container {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 10000;
        }
       /* Add this CSS code to your main CSS file */
/* Add this CSS code to your main CSS file */
.pagination {
  display: inline-block;
}

.pagination a {
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
}
</style>

<div id="spinner-overlay">
    <div id="spinner-container">
        <div id="spinner" class="bg-white d-flex align-items-center justify-content-center">
            <div class="spinner"></div>
        </div>
    </div>
    </div>
{{--

<div class="preloader">

    <div class="loading">

      <div class="spinner"></div>


    </div>

  </div> --}}


<script type="text/javascript">

    // $(document).ready(function(){

    //  $(".preloader").fadeOut();

    // })

    // Fungsi untuk menampilkan spinner
function showSpinner() {
    document.getElementById('spinner-overlay').style.display = 'block';
    document.getElementById('spinner-container').style.display = 'flex';
}

// Fungsi untuk menyembunyikan spinner
function hideSpinner() {
    document.getElementById('spinner-overlay').style.display = 'none';
    document.getElementById('spinner-container').style.display = 'none';
}

// Event listener saat halaman sedang dimuat
window.addEventListener('beforeunload', function () {
    showSpinner();
});

// Event listener saat halaman sudah dimuat
window.addEventListener('unload', function () {
    hideSpinner();
});

   </script>


    @if (Session::has('status'))
        <div class="alert alert-danger align align-center" role="alert">
            {{ Session::get('message') }}
        </div>
    @endif

    <section class="">

        <div class="tengah" style="effect: dropshadow;">
            <div class="b">

                <p class="login">Sign Here!</p>

                <p style="font-size: 1.3rem; margin-top: 7.5%; margin-left: 83%">
                    <img src="{{asset('/logo/logo.png')}}" alt="Logo" width="35" height="35"
                        style="">{{-- I-Do --}}
                </p><br>
                <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">

                    <form action="/login/auth" method="post">
                        @csrf
                        {{-- <div class="bgtengah">
                  <img src="{{'/bg/login.png'}}" alt="Logo" width="100%" height="100%">
                </div> --}}

                        <!-- Email input -->
                        <div class="form-outline mb-3">
                            <div class="col-md-4">

                                <label class="form-label" for="form3Example3">Username</label>
                                <input type="text" name="email" id="form3Example3" class="form-control form-control-sm"
                                    style="width:343px; border: 2px solid #899bbd; background: #f6f9ff;"
                                    placeholder="Enter a valid email address" onfocus="focusevent()" onblur="blurevent()"
                                    required />
                            </div>
                        </div>


                        <!-- Password input -->
                        <div class="form-outline mb-3">
                            <label class="form-label" for="form3Example4">Password</label>
                            <input type="password" name="password" id="inputPassword1"
                                style="width:343px; border: 2px solid #899bbd; background: #f6f9ff;"
                                class="form-control" placeholder="Enter password"
                                onfocus="focusevent1()" onblur="blurevent1()" required />

                            <div class="mb-3 row">
                                <div class="col-sm-12" style="margin-top: 6px; margin-left: 8px;">
                                    <input type="checkbox" class="form-check-input" id="show-password"
                                        onclick="myFunction()"> Show Password
                                </div>
                            </div>
                        </div>

                        <div class="d-flex justify-content-between align-items-center">
                            <!-- Checkbox -->


                            <div class="text-center text-lg-start mt-2 pt-3">
                                <button type="submit" class="btn btn-secondary btn-sm "
                                    style="padding-left:25.5px; padding-right: 25.5px; margin-left: 280%; font-weight: bold; border: 2px solid #899bbd; background: #f6f9ff;">
                                    <font color="#000000" style=" font-family: Rum Raisin;">Login</font>
                                </button>

                            </div>

                    </form>
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>
    <div class="tengah" style="effect: dropshadow;">
        <div class="b">

            <p class="login">Sign Here!</p>

            <p style="font-size: 1.3rem; margin-top: 7.5%; margin-left: 83%">
                <img src="{{asset('/logo/logo.png')}}" alt="Logo" width="35" height="35"
                    style="">{{-- I-Do --}}
            </p><br>
            <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">

                <form action="/login/auth" method="post">
                    @csrf
                    {{-- <div class="bgtengah">
              <img src="{{'/bg/login.png'}}" alt="Logo" width="100%" height="100%">
            </div> --}}

                    <!-- Email input -->
                    <div class="form-outline mb-3">
                    <label class="form-label" for="form3Example3">Username</label>
                    <input type="text" name="email" id="form3Example3" class="form-control form-control-sm"
                        style="width:343px; border: 2px solid #899bbd; background: #f6f9ff;"
                        placeholder="Enter a valid email address" onfocus="focusevent()" onblur="blurevent()"
                        required />
                    </div>


                    <!-- Password input -->
                    <div class="form-outline mb-3">
                        <label class="form-label" for="form3Example4">Password</label>
                        <input type="password" name="password" id="inputPassword2"
                            style="width:343px; border: 2px solid #899bbd; background: #f6f9ff;"
                            class="form-control form-control-sm" placeholder="Enter password"
                            onfocus="focusevent1()" onblur="blurevent1()" required />

                        <div class="mb-3 row">
                            <div class="col-sm-12" style="margin-top: 6px; margin-left: 8px;">
                                <input type="checkbox" class="form-check-input" id="show-password"
                                    onclick="myFunction()"> Show Password
                            </div>
                        </div>
                    </div>

                    <div class="d-flex justify-content-between align-items-center">
                        <!-- Checkbox -->


                        <div class="text-center text-lg-start mt-2 pt-3">
                            <button type="submit" class="btn btn-secondary btn-sm "
                                style="padding-left:25.5px; padding-right: 25.5px; margin-left: 280%; font-weight: bold; border: 2px solid #899bbd; background: #f6f9ff;">
                                <font color="#000000" style=" font-family: Rum Raisin;">Login</font>
                            </button>

                        </div>

                </form>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class="footer">
        <font face="WildWest" size="3" color="#ffffff"> © 2023 I-Do web pkl untuk anda</font>
    </div>
    <script>
        function focusevent() {
            document.getElementById("form3Example3").style.background = "#dcdcdc";
        }

        function focusevent1() {
            document.getElementById("form3Example4").style.background = "#dcdcdc";
        }

        function blurevent() {
            document.getElementById("form3Example3").style.background = "white";
        }

        function blurevent1() {
            document.getElementById("form3Example4").style.background = "white";
        }
    </script>


    <script>
        function myFunction() {
            var x = document.getElementById("inputPassword");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
            var x = document.getElementById("inputPassword1");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
            var x = document.getElementById("inputPassword2");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
    </script>
</body>

</html>
