<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>i-Do @yield('title')</title>
  
  {{-- <!-- loader-->
  <link href="{{asset('selainad/assets/css/pace.min.css')}}" rel="stylesheet"/>
  <script src="{{asset('selainad/assets/js/pace.min.js')}}"></script> --}}
  <!--favicon-->
  <link rel="icon" href="{{asset('logo/Logo.png')}}" type="image/x-icon">
  <!-- Vector CSS -->
  <link href="{{asset('selainad/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet"/>
  <!-- simplebar CSS-->
  <link href="{{asset('selainad/assets/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="{{asset('selainad/assets/css/bootstrap.min.css')}}" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="{{asset('selainad/assets/css/animate.css')}}" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="{{asset('selainad/assets/css/icons.css')}}" rel="stylesheet" type="text/css"/>
  <!-- Sidebar CSS-->
  <link href="{{asset('selainad/assets/css/sidebar-menu.css')}}" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="{{asset('selainad/assets/css/app-style.css')}}" rel="stylesheet"/>
  
  <style>
    img{
        border-radius: 50%;
        object-fit: cover;
    }
</style>

</head>

<body class="bg-theme bg-theme1">
 
<!-- Start wrapper-->
 <div id="wrapper">
 
  <!--Start sidebar-wrapper-->
   <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
     <div class="brand-logo">
      <a href="/siswa/dashboard">
       <img src="{{asset('logo/Logo.png')}}" class="logo-icon" alt="logo icon">
       <h5 class="logo-text">i-Do</h5>
     </a>
     
   </div>
   <div class="profilsisi">
    <hr>

     <div class="media" style="margin-left: 10px; margin-top: 10px;">
      
      <div class="avatar">
        @if (is_null($siswa -> foto))
        <img class="align-self-start mr-3" src="{{asset('/bg/profil.png')}}" alt="profil" width="50px" height="50px">
        @elseif ($siswa -> foto != 'Tidak tersedia')
        <img class="align-self-start mr-3" src="{{asset('uploads/profil/' . $siswa->foto)}}" alt="{{$siswa->nama}}" width="50px" height="50px">
        @else
        <img class="align-self-start mr-3" src="{{asset('/bg/profil.png')}}" alt="profil" width="50px" height="50px">
        @endif
        {{-- <img class="align-self-start mr-3" src="https://via.placeholder.com/110x110" alt="user avatar"> --}}
      </div>
      <div class="media-body">
        <h6 class="mt-2 user-title">{{$siswa -> nama}}</h6>
        <p class="user-subtitle ">{{$siswa -> nis}}</p>
      </div>
    </div>
    <a href="/siswa/profile">
      <button class="btn btn-collapsed btn-info"><i class="zmdi zmdi-account mr-2"></i> Profile</button>
    </a>
    <a href="/logout">
      <button class="btn btn-collapsed btn-danger"><i class="icon-power mr-2"></i> Logout</button>
    </a>
  </div>
   <ul class="sidebar-menu do-nicescrol" style="margin-top: 1rem">
      <li class="sidebar-header">MAIN NAVIGATION</li>
      <li>
        <a href="/siswa/dashboard">
          <i class="zmdi zmdi-view-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>

      <li>
        <a href="/siswa/aktivitas">
          <i class="zmdi zmdi-edit"></i> <span>Aktivitas</span>
        </a>
      </li>

      <li>
        <a href="/siswa/nilai">
          <i class="zmdi zmdi-folder"></i> <span>Nilai</span>
        </a>
      </li>

      <li>
        <a href="/siswa/perusahaan">
          <i class="zmdi zmdi-city"></i> <span>Perusahaan</span>
        </a>
      </li>

      <li>
        <a href="/siswa/riwayat">
          <i class="zmdi zmdi-time-restore-setting"></i> <span>Riwayat Kehadiran</span>
          {{-- <small class="badge float-right badge-light">New</small> --}}
        </a>
      </li>

    </ul>
   
   </div>
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<header class="topbar-nav">
 <nav class="navbar navbar-expand fixed-top">
  <ul class="navbar-nav mr-auto align-items-center">
    <li class="nav-item">
      <a class="nav-link toggle-menu" href="javascript:void();">
       <i class="icon-menu menu-icon"></i>
     </a>
    </li>
  </ul>
     
  <ul class="navbar-nav align-items-center right-nav-link">
    
    <li class="nav-item">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
        <span class="user-profile">

            @if (is_null($siswa -> foto))
            <img class="align-self-start mr-3" src="{{asset('/bg/profil.png')}}" alt="profil" width="130px" height="130px">
        @elseif ($siswa -> foto != 'Tidak tersedia')
            <img class="align-self-start mr-3" src="{{asset('uploads/profil/' . $siswa->foto)}}" alt="{{$siswa->nama}}" width="130px" height="130px">
        @else
            <img class="align-self-start mr-3" src="{{asset('/bg/profil.png')}}" alt="profil" width="130px" height="130px">
        @endif
            
      </a>
      <ul class="dropdown-menu dropdown-menu-right">
       <li class="dropdown-item user-details">
        <a href="javaScript:void();">
           <div class="media">

            <div class="avatar">
                @if (is_null($siswa -> foto))
                    <img class="align-self-start mr-3" src="{{asset('/bg/profil.png')}}" alt="profil" width="130px" height="130px">
                @elseif ($siswa -> foto != 'Tidak tersedia')
                    <img class="align-self-start mr-3" src="{{asset('uploads/profil/' . $siswa->foto)}}" alt="{{$siswa->nama}}" width="130px" height="130px">
                @else
                    <img class="align-self-start mr-3" src="{{asset('/bg/profil.png')}}" alt="profil" width="130px" height="130px">
                @endif
                {{-- <img class="align-self-start mr-3" src="https://via.placeholder.com/110x110" alt="user avatar"> --}}
            </div>
            <div class="media-body">
                <h6 class="mt-2 user-title">{{$siswa -> nama}}</h6>
                <p class="user-subtitle">{{$siswa -> nis}}</p>
            </div>   
             
           </div>
          </a>
        </li>
        <a href="/siswa/profile">
            <li class="dropdown-divider"></li>
            <li class="dropdown-item"><i class="zmdi zmdi-account mr-2"></i> Profile</li>
        </a>
        <a href="/logout">
            <li class="dropdown-divider"></li>
            <li class="dropdown-item"><i class="icon-power mr-2"></i> Logout</li>
        </a>
      </ul>
    </li>
  </ul>
</nav>
</header>
<!--End topbar header-->

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">

  <!--Start Dashboard Content--> 
    {{-- Loading Screen --}}
        <style>
            .loding {
                width: 100px;
                height: 100px;
                display: grid;
                border: 7px solid #0000;
                border-radius: 50%;
                border-color: #3b71ca #0000;
                animation: spinner-e04l1k 0.8s linear infinite;
            }

            .loding::before,
            .loding::after {
                content: "";
                grid-area: 1/1;
                margin: 3.5px;
                border: inherit;
                border-radius: 50%;
            }

            .loding::before {
                border-color: #9fa6b2 #0000;
                animation: inherit;
                animation-duration: 0.4s;
                animation-direction: reverse;
            }

            .loding::after {
                margin: 14px;
            }

            @keyframes spinner-e04l1k {
                100% {
                    transform: rotate(1turn);
                }
            }

            #spinner-overlay {
                display: none;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(255, 255, 255, 1);
                /* White overlay */
                z-index: 9999;
                opacity: 0.9;
            }

            #spinner-container {
                display: none;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                display: flex;
                align-items: center;
                justify-content: center;
                z-index: 10000;
            }

            /* Add this CSS code to your main CSS file */
            /* Add this CSS code to your main CSS file */
            .pagination {
                display: inline-block;
            }

            .pagination a {
                color: black;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }
        </style>

        <div id="spinner-overlay">
            <div id="spinner-container">
                <div id="spinner" class="bg-white d-flex align-items-center justify-content-center">
                    <div class="loding"></div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            // Fungsi untuk menampilkan spinner
        function showSpinner() {
            document.getElementById('spinner-overlay').style.display = 'block';
            document.getElementById('spinner-container').style.display = 'flex';
            document.getElementById('loding').style.display = 'flex';
        }

        // Fungsi untuk menyembunyikan spinner
        function hideSpinner() {
            document.getElementById('spinner-overlay').style.display = 'none';
            document.getElementById('spinner-container').style.display = 'none';
            document.getElementById('loding').style.display = 'block';
        }

        // Event listener saat halaman sedang dimuat
        window.addEventListener('beforeunload', function () {
            showSpinner();
        });

        // Event listener saat halaman sudah dimuat
        window.addEventListener('unload', function () {
            hideSpinner();
        });

        //ini menu untuk mobile
        var mobileMenuBtn = document.querySelector("#mobile-menu-btn");
        var mobileMenu = document.querySelector(".mobile-menu");
        mobileMenuBtn.addEventListener("click", () => {
        if (mobileMenu.style.display === "none") {
            mobileMenu.style.display = "flex";
        } 
        else {
            mobileMenu.style.display = "none";
        }
        });

        </script>

    {{-- end loading --}}
	  @yield('dashboard')

	<div class="row">
     <div class="col-12 col-lg-12 col-xl-12">
	    <div class="card">
            
            @yield('header')

         @yield('content')
		 
         
		 
		</div>
	 </div>
	</div><!--End Row-->

    @yield('content1')

      <!--End Dashboard Content-->
	  
	<!--start overlay-->
		  <div class="overlay toggle-menu"></div>
		<!--end overlay-->
		
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->
	<footer class="footer">
      <div class="container">
        <div class="text-center">
          Copyright © 2023 i-Do! Prakerin
        </div>
      </div>
    </footer>
	<!--End footer-->
   
  </div><!--End wrapper-->

  <!-- Bootstrap core JavaScript-->
  <script src="{{asset('selainad/assets/js/jquery.min.js')}}"></script>
  <script src="{{asset('selainad/assets/js/popper.min.js')}}"></script>
  <script src="{{asset('selainad/assets/js/bootstrap.min.js')}}"></script>
	
 <!-- simplebar js -->
  <script src="{{asset('selainad/assets/plugins/simplebar/js/simplebar.js')}}"></script>
  <!-- sidebar-menu js -->
  <script src="{{asset('selainad/assets/js/sidebar-menu.js')}}"></script>
  {{-- <!-- loader scripts -->
  <script src="{{asset('selainad/assets/js/jquery.loading-indicator.js')}}"></script> --}}
  <!-- Custom scripts -->
  <script src="{{asset('selainad/assets/js/app-script.js')}}"></script>
  <!-- Chart js -->
  
  <script src="{{asset('selainad/assets/plugins/Chart.js/Chart.min.js')}}"></script>
 
  <!-- Index js -->
  <script src="{{asset('selainad/assets/js/index.js')}}"></script>

  
</body>
</html>
