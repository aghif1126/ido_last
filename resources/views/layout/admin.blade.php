<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous">
    </script>
    <title>@yield('Title')</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Rum+Raisin&display=swap');

        * {
            font-family: 'Ubuntu';
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        :root {
            /*color*/
            --body-color: #e4e9f7;
            --sidebar-color: #4a5257;
            --primary-color: #c79a7a;
            --primary-color-light: #f6f5ff;
            --toogle-color: #ddd;
            --text-color: #ffffff;
        }

        body {
            height: 100vh;
            background: var(--body-color);
        }

        /*sidebar*/
        .sidebar {
            position: fixed;
            top: 0px;
            left: 0px;
            height: 100%;
            width: 250px;
            padding: 10px 14px;
            background: var(--sidebar-color);
            transition: var(--tran-02);
            z-index: 100;
        }

        /*resuable css*/
        .sidebar .text {
            font-size: 15px;
            font-weight: 500;
            color: var(--text-color);
            transition: var(--tran-03);
            white-space: nowrap;
            opacity: 1;
        }

        .sidebar .image {
            min-width: 60px;
            display: flex;
            align-items: center;
        }

        .sidebar li {
            height: 40px;
            list-style: none;
            display: flex;
            align-items: center;
            margin-top: 2px;
        }

        .sidebar li .icon {
            display: flex;
            align-items: center;
            min-width: 45px;
            font-size: 30px;
        }

        .sidebar li .icon,
        .sidebar li .text {
            color: var(--text-color);
            transition: var(--tran-02);
            margin-left: 7px;
        }

        .sidebar header {
            position: relative;
        }

        .sidebar .image-text img {
            width: 40px;
            border-radius: 6px;
        }

        .sidebar header .image-text {
            display: flex;
            align-items: center;
        }

        header .image-text .header-text {
            display: flex;
            flex-direction: column;
        }

        .header-text .name {
            font-size: 25px;
            font-weight: 600;
        }

        .sidebar .menu {
            margin-top: 30px;
        }

        .sidebar .menu-bar {
            height: calc(100% - 50px);
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }

        .sidebar li a {
            height: 100%;
            width: 100%;
            display: flex;
            align-items: center;
            text-decoration: none;
            border-radius: 5px;
            transition: var(--tran-04);
        }

        .sidebar li a .text {
            height: 100%;
            width: 100%;
            display: flex;
            align-items: center;
            text-decoration: none;
            border-radius: 5px;
            transition: var(--tran-04);
        }

        .sidebar li a:hover {
            background: var(--primary-color);
        }

        .sidebar li a:hover .icon,
        .sidebar li a:hover .text {
            color: var(--sidebar-color);
        }

        .navbar-color {
            background-color: #4A5257;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
        }
    </style>
</head>

<body>
    <div class="navbar navbar-color navbar-expand-lg" style="height: 60px">
        <a href="#" style="margin-left: 95%;">
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor"
                class="bi bi-person-circle" viewBox="0 0 16 16" style="color:white;">
                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                <path fill-rule="evenodd"
                    d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
            </svg>
        </a>
    </div>
    <nav class="sidebar">
        <header>
            <div class="image-text">
                <span class="image">
                    <img src="{{asset('logo/Logo.png') }}" alt="Logo">
                </span>

                <div class="text header-text">
                    <span class="name">I-Do</span>
                </div>
            </div>
        </header>

        <div class="menu-bar">
            <div class="menu">

                <ul class="menu-links" style="padding-left: 0px;">
                    <li class="nav-link">
                        <a href="/pembimbing/dashboard">
                            <i class='bx bx-home-alt icon'></i>
                            <span class="text nav-text">Home</span>
                        </a>
                    </li>
                    <li class="nav-link">
                        <a href="/guru/show-siswa">
                            <i class='bx bxs-edit-alt icon'></i>
                            <span class="text nav-text">Siswa</span>
                        </a>
                    </li>
                    <li class="nav-link">
                        <a href="/guru/show-user">
                            <i class='bx bx-library icon'></i>
                            <span class="text nav-text">User</span>
                        </a>
                    </li>
                    <li class="nav-link">
                        <a href="/guru/show-perusahaan">
                            <i class='bx bxs-business icon'></i>
                            <span class="text nav-text">Company</span>
                        </a>
                    </li>
                    <li class="nav-link">
                        <a href="/guru/show-jurusan">
                            <i class='bx bx-user icon'></i>
                            <span class="text nav-text">Jurusan</span>
                        </a>
                    </li>
                    <li class="nav-link">
                        <a href="/guru/show-konsentrasi">
                            <i class='bx bx-user icon'></i>
                            <span class="text nav-text">Konsentrasi</span>
                        </a>
                    </li>
                    <li class="nav-link">
                        <a href="/guru/show-kelas">
                            <i class='bx bx-user icon'></i>
                            <span class="text nav-text">Kelas</span>
                        </a>
                    </li>
                    <li class="nav-link">
                        <a href="/guru/pemetaan-siswa">
                            <i class='bx bx-user icon'></i>
                            <span class="text nav-text">Pemetaan</span>
                        </a>
                    </li>
                    <li class="nav-link">
                        <a href="/guru/show-kaprog">
                            <i class='bx bx-user icon'></i>
                            <span class="text nav-text">Kaprog</span>
                        </a>
                    </li>
                    <li class="nav-link">
                        <a href="/guru/show-pimpinan">
                            <i class='bx bx-user icon'></i>
                            <span class="text nav-text">Pimpinan</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="bottom-content">
                <li class="nav-link">
                    <a href="/logout">
                        <i class='bx bx-log-out icon'></i>
                        <span class="text nav-text">Logout</span>
                    </a>
                </li>
            </div>
        </div>
    </nav>

    <div class="container shadow p-3 mb-3 my-3 bg-white rounded" style="width: 76%; margin-left: 280px">

        @yield('header')
    </div>

    <div class="container shadow p-3 mb-3 my-3 bg-white rounded" style="width: 76%; margin-left: 280px">
        @yield('content')
    </div>

    <footer class="bg-light text-center text-lg-start" style="margin-top: auto; width: 76%; margin-left: 280px">
        <!-- Copyright -->
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
            © 2023 Copyright:
            <a class="text-dark" href="#">I-Do!</a>
        </div>
        <!-- Copyright -->
    </footer>

</body>

</html>
