<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>i-Do @yield('judul')</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

        <!-- Favicons -->
    <link href="{{ asset('/logo/Logo.png')}}" rel="icon">
    <link href="{{ asset('/logo/Logo.png')}}" rel="">

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{ asset('/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{ asset('/vendor/quill/quill.snow.css')}}" rel="stylesheet">
    <link href="{{ asset('/vendor/quill/quill.bubble.css')}}" rel="stylesheet">
    <link href="{{ asset('/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{ asset('/vendor/simple-datatables/style.css')}}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link type="text/css" href="{{ asset('/css/style.css') }}" rel="stylesheet">

    <style>
      table.dataTable thead .sorting_asc {
    content: "\e150";
}

table.dataTable thead .sorting_desc {
    content: "\e155";
}

table.dataTable thead .sorting {
    content: "\e156";
}

      .dataTables_wrapper .dataTables_paginate {
  float: right;
  text-align: right;
  padding-top: 0.25em;
}
.dataTables_wrapper .dataTables_paginate .paginate_button {
  box-sizing: border-box;
  display: inline-block;
  min-width: 1.5em;
  padding: 0.5em 1em;
  margin-left: 2px;
  text-align: center;
  text-decoration: none !important;
  cursor: pointer;
  color: inherit !important;
  border: 1px solid transparent;
  border-radius: 2px;
  background: transparent;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
  color: inherit !important;
  border: 1px solid rgba(0, 0, 0, 0.3);
  background-color: rgba(0, 0, 0, 0.05);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(230, 230, 230, 0.05)), color-stop(100%, rgba(0, 0, 0, 0.05)));
  /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top, rgba(230, 230, 230, 0.05) 0%, rgba(0, 0, 0, 0.05) 100%);
  /* Chrome10+,Safari5.1+ */
  background: -moz-linear-gradient(top, rgba(230, 230, 230, 0.05) 0%, rgba(0, 0, 0, 0.05) 100%);
  /* FF3.6+ */
  background: -ms-linear-gradient(top, rgba(230, 230, 230, 0.05) 0%, rgba(0, 0, 0, 0.05) 100%);
  /* IE10+ */
  background: -o-linear-gradient(top, rgba(230, 230, 230, 0.05) 0%, rgba(0, 0, 0, 0.05) 100%);
  /* Opera 11.10+ */
  background: linear-gradient(to bottom, rgba(230, 230, 230, 0.05) 0%, rgba(0, 0, 0, 0.05) 100%);
  /* W3C */
}
.dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
  cursor: default;
  color: #666 !important;
  border: 1px solid transparent;
  background: transparent;
  box-shadow: none;
}
.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
  color: white !important;
  border: 1px solid #949494;
  background-color: #949494;
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #dbdbdb), color-stop(100%, #949494));
  /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top, #dbdbdb 0%, #949494 100%);
  /* Chrome10+,Safari5.1+ */
  background: -moz-linear-gradient(top, #dbdbdb 0%, #949494 100%);
  /* FF3.6+ */
  background: -ms-linear-gradient(top, #dbdbdb 0%, #949494 100%);
  /* IE10+ */
  background: -o-linear-gradient(top, #dbdbdb 0%, #949494 100%);
  /* Opera 11.10+ */
  background: linear-gradient(to bottom, #dbdbdb 0%, #949494 100%);
  /* W3C */
}
.dataTables_wrapper .dataTables_paginate .paginate_button:active {
  outline: none;
  background-color: #8f8f8f;
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #aeaeae), color-stop(100%, #8f8f8f));
  /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top, #aeaeae 0%, #8f8f8f 100%);
  /* Chrome10+,Safari5.1+ */
  background: -moz-linear-gradient(top, #aeaeae 0%, #8f8f8f 100%);
  /* FF3.6+ */
  background: -ms-linear-gradient(top, #aeaeae 0%, #8f8f8f 100%);
  /* IE10+ */
  background: -o-linear-gradient(top, #aeaeae 0%, #8f8f8f 100%);
  /* Opera 11.10+ */
  background: linear-gradient(to bottom, #aeaeae 0%, #8f8f8f 100%);
  /* W3C */
  box-shadow: inset 0 0 3px #111;
}

.dataTables_filter {
    display: none;
}
    </style>

</head>
<body class="toggle-sidebar">

    <header id="header" class="header fixed-top d-flex align-items-center">

        <div class="d-flex align-items-center justify-content-between">
          <a href="/guru/dashboard" class="logo d-flex align-items-center">
            <img src="{{ asset('/logo/Logo.png')}}" alt="">
            <span class="d-none d-lg-block">i-Do</span>
          </a>
          <i class="bi bi-list toggle-sidebar-btn"></i>
        </div><!-- End Logo -->

        {{-- <div class="search-bar">
          <form class="search-form d-flex align-items-center" method="get" action="#">
            <input type="text" name="query" placeholder="Search" title="Enter search keyword">
            <button type="submit" title="Search"><i class="bi bi-search"></i></button>
          </form>
        </div><!-- End Search Bar --> --}}

        <nav class="header-nav ms-auto">
          <ul class="d-flex align-items-center">

            {{-- <li class="nav-item d-block d-lg-none">
              <a class="nav-link nav-icon search-bar-toggle " href="#">
                <i class="bi bi-search"></i>
              </a>
            </li><!-- End Search Icon--> --}}

            <li class="nav-item dropdown pe-3">
                <li>
                  <a class="dropdown-item d-flex align-items-center" href="/logout">
                    <i class="bi bi-box-arrow-right"></i>
                    <span>Log Out</span>
                  </a>
                </li>
            </li><!-- End Profile Nav -->
          </ul>
        </nav><!-- End Icons Navigation -->
      </header><!-- End Header -->



      <!-- ======= Sidebar ======= -->
  <aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="{{ Request::is('guru/dashboard') ? 'nav-link' : 'nav-link collapsed'  }}"
            href="{{ url('/guru/dashboard') }}">
          <i class="{{ Request::is('guru/dashboard') ? 'bi bi-grid-fill' : 'bi bi-grid'  }}"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-item">
        <a class="{{ Request::is('guru/show-perusahaan' , 'guru/add-perusahaan' , 'guru/delete-perusahaan' , 'guru/detail-perusahaan/' , 'guru/edit-perusahaan/')
                    ? 'nav-link' : 'nav-link collapsed' }} "
            href="{{ url('/guru/show-perusahaan') }}">
            <i class=" {{ Request::is('guru/show-perusahaan' , 'guru/add-perusahaan' , 'guru/delete-perusahaan' , 'guru/detail-perusahaan/' , 'guru/edit-perusahaan/')
                    ? 'ri-building-4-fill' : 'ri-building-4-line'  }}"></i>
            <span>Perusahaan</span>
        </a>
      </li><!-- End Company Nav -->

      <li class="nav-item">
        <a class="{{ Request::is('guru/pemetaan-siswa' , 'guru/pemetaan-siswa/pilih-perusahaan/')
                    ? 'nav-link' : 'nav-link collapsed' }}" href="/guru/pemetaan-siswa">
          <i class="{{ Request::is('guru/pemetaan-siswa' , 'guru/pemetaan-siswa/pilih-perusahaan/')
          ? 'bi bi-map-fill' : 'bi bi-map' }}"></i>
          <span>Pemetaan</span>
        </a>
      </li><!-- End Pemetaan Nav -->

      <li class="nav-item">
        <a class="{{ Request::is('guru/show-kelas' , 'guru/add-kelas' , 'guru/delete-kelas' , 'guru/detail-kelas/' , 'guru/edit-kelas/')
        ? 'nav-link' : 'nav-link collapsed' }}" href="/guru/show-kelas">
          <i class="{{ Request::is('guru/show-kelas' , 'guru/add-kelas' , 'guru/delete-kelas' , 'guru/detail-kelas/' , 'guru/edit-kelas/')
          ? 'ri-book-read-fill' : 'ri-book-read-line' }}"></i>
          <span>Kelas</span>
        </a>
      </li><!-- End Kelas Nav -->

      <li class="nav-item">
        <a class="{{ Request::is('guru/show-jurusan' , 'guru/add-jurusan' , 'guru/delete-jurusan' , 'guru/detail-jurusan/' , 'guru/edit-jurusan/')
        ? 'nav-link' : 'nav-link collapsed' }}" href="/guru/show-jurusan">
          <i class=" {{ Request::is('guru/show-jurusan' , 'guru/add-jurusan' , 'guru/delete-jurusan' , 'guru/detail-jurusan/' , 'guru/edit-jurusan/')
           ? 'bi bi-briefcase-fill' : 'bi bi-briefcase'  }}"></i>
          <span>Jurusan</span>
        </a>
      </li><!-- End Jurusan Nav -->

      <li class="nav-item">
        <a class="{{ Request::is('guru/show-konsentrasi' , 'guru/add-konsentrasi' , 'guru/delete-konsentrasi' , 'guru/detail-konsentrasi/' , 'guru/edit-konsentrasi/')
        ? 'nav-link' : 'nav-link collapsed' }}" href="/guru/show-konsentrasi">
          <i class="{{ Request::is('guru/show-konsentrasi' , 'guru/add-konsentrasi' , 'guru/delete-konsentrasi' , 'guru/detail-konsentrasi/' , 'guru/edit-konsentrasi/')
          ? 'ri-computer-fill' : 'ri-computer-line'  }}"></i>
          <span>Konsentrasi</span>
        </a>
      </li><!-- End Konsentrasi Nav -->

      <li class="nav-item">
        <a class="{{ Request::is('guru/show-monitoring')
        ? 'nav-link' : 'nav-link collapsed' }}" href="/guru/show-monitoring">
          <i class="{{ Request::is('guru/show-monitoring')
          ? 'bi bi-stopwatch-fill' : 'bi bi-stopwatch'  }}"></i>
          <span>Monitoring</span>
        </a>
      </li><!-- End Monitoring Nav -->
      <li class="nav-item">
        <a class="{{ Request::is('guru/show-tahun-pelajaran')
        ? 'nav-link' : 'nav-link collapsed' }}" href="/guru/show-tahun-pelajaran">
          <i class="{{ Request::is('guru/show-tahun-pelajaran')
          ? 'bi bi-calendar-fill' : 'bi bi-calendar'  }}"></i>
          <span>Tahun Pelajaran</span>
        </a>
      </li><!-- End Monitoring   Nav -->

      <li class="nav-item">
        <a class="{{ Request::is(
            'guru/show-user', 'guru/pilih-user', 'guru/pilih-user/guru', 'guru/pilih-user/siswa', 'guru/pilih-user/pembimbing', 'guru/pilih-user/kaprog', 'guru/delete-user',
            'guru/show-siswa', 'guru/add-siswa', 'guru/delete-siswa',
            'guru/show-guru', 'guru/add-guru', 'guru/delete-guru',
            'guru/show-pembimbing', 'guru/add-pembimbing', 'guru/delete-pembimbing',
            'guru/show-pimpinan', 'guru/add-pimpinan', 'guru/delete-pimpinan',
            'guru/show-kaprog', 'guru/add-kaprog', 'guru/delete-kaprog'
            ) ? 'nav-link' : 'nav-link collapsed' }}" data-bs-target="#user-nav" data-bs-toggle="collapse" href="guru/show-user">
          <i class="{{ Request::is(
            'guru/show-user', 'guru/pilih-user', 'guru/pilih-user/guru', 'guru/pilih-user/siswa', 'guru/pilih-user/pembimbing', 'guru/pilih-user/kaprog', 'guru/delete-user',
            'guru/show-siswa', 'guru/add-siswa', 'guru/delete-siswa',
            'guru/show-guru', 'guru/add-guru', 'guru/delete-guru',
            'guru/show-pembimbing', 'guru/add-pembimbing', 'guru/delete-pembimbing',
            'guru/show-pimpinan', 'guru/add-pimpinan', 'guru/delete-pimpinan',
            'guru/show-kaprog', 'guru/add-kaprog', 'guru/delete-kaprog'
            ) ? 'bi bi-person-fill' : 'bi bi-person' }}"></i>
          <span>User</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="user-nav" class="nav-content collapse" data-bs-parent="#sidebar-nav">
            <li>
                <a href="/guru/show-user" class="{{ Request::is('guru/show-user', 'guru/pilih-user', 'guru/pilih-user/guru', 'guru/pilih-user/siswa', 'guru/pilih-user/pembimbing', 'guru/pilih-user/kaprog', 'guru/delete-user')
                 ? 'active' : '' }}">
                  <i class="bi bi-circle"></i><span>Daftar Akun</span>
                </a>
              </li>
          <li>
            <a href="/guru/show-siswa" class="{{ Request::is('guru/show-siswa', 'guru/add-siswa', 'guru/delete-siswa') ? 'active' : '' }}">
              <i class="bi bi-circle"></i><span>Siswa</span>
            </a>
          </li>
          <li>
            <a href="/guru/show-guru" class="{{ Request::is('guru/show-guru', 'guru/add-guru', 'guru/delete-guru') ? 'active' : '' }}">
              <i class="bi bi-circle"></i><span>Guru Pembimbing</span>
            </a>
          </li>
          <li>
            <a href="/guru/show-pembimbing" class="{{ Request::is('guru/show-pembimbing', 'guru/add-pembimbing', 'guru/delete-pembimbing') ? 'active' : '' }}">
              <i class="bi bi-circle"></i><span>Pembimbing Perusahaan</span>
            </a>
          </li>
          <li>
            <a href="/guru/show-pimpinan" class="{{ Request::is('guru/show-pimpinan', 'guru/add-pimpinan', 'guru/delete-pimpinan') ? 'active' : '' }}">
              <i class="bi bi-circle"></i><span>Pimpinan Perusahaan</span>
            </a>
          </li>
          {{-- <li>
            <a href="/guru/show-kaprog" class="{{ Request::is('guru/show-kaprog', 'guru/add-kaprog', 'guru/delete-kaprog') ? 'active' : '' }}">
              <i class="bi bi-circle"></i><span>Kepala Program</span>
            </a>
          </li> --}}
        </ul>
      </li><!-- End Users Nav -->
    </ul>
  </aside><!-- End Sidebar-->




  <main id="main" class="main">

    <div class="pagetitle">
        @yield('title')
    </div><!-- End Page Title -->

    <section class="section dashboard">
      <div class="row">

        <!-- body -->
        <div class="col-lg-15">
          <div class="row">

            @yield('header')
                    @yield('content')

          </div>
        </div><!-- End Body -->
      </div>
    </section>

  </main><!-- End #main -->


  {{-- Loading Screen --}}
  <style>

    .loding {
       width: 100px;
       height: 100px;
       display: grid;
       border: 7px solid #0000;
       border-radius: 50%;
       border-color: #3b71ca #0000;
       animation: spinner-e04l1k 0.8s linear infinite;
    }

    .loding::before,
    .loding::after {
       content: "";
       grid-area: 1/1;
       margin: 3.5px;
       border: inherit;
       border-radius: 50%;
    }

    .loding::before {
       border-color: #9fa6b2 #0000;
       animation: inherit;
       animation-duration: 0.4s;
       animation-direction: reverse;
    }

    .loding::after {
       margin: 14px;
    }

    @keyframes spinner-e04l1k {
       100% {
          transform: rotate(1turn);
       }
    }
    #spinner-overlay {
                display: none;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(255, 255, 255, 1); /* White overlay */
                z-index: 9999;
                opacity: 0.9;
            }

            #spinner-container {
                display: none;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                display: flex;
                align-items: center;
                justify-content: center;
                z-index: 10000;
            }
           /* Add this CSS code to your main CSS file */
    /* Add this CSS code to your main CSS file */
    .pagination {
      display: inline-block;
    }

    .pagination a {
      color: black;
      float: left;
      padding: 8px 16px;
      text-decoration: none;
    }
    </style>

    <div id="spinner-overlay">
        <div id="spinner-container">
            <div id="spinner" class="bg-white d-flex align-items-center justify-content-center">
                <div class="loding"></div>
            </div>
        </div>
        </div>


    <script type="text/javascript">


        // Fungsi untuk menampilkan spinner
    function showSpinner() {
        document.querySelector('#spinner-overlay').style.display = 'flex';
        document.querySelector('#spinner-container').style.display = 'flex';
        document.querySelector('#loding').style.display = 'flex';
        document.querySelector('.spinner').style.display = 'flex';
    }

    // Fungsi untuk menyembunyikan spinner
    function hideSpinner() {
        document.querySelector('#spinner-overlay').style.visibility = 'none';
        document.querySelector('#spinner-container').style.visibility = 'none';
        document.querySelector('#loding').style.visibility = 'none';
        document.querySelector('.spinner').style.visibility = 'none';
    }

    // Event listener saat halaman sedang dimuat
    window.addEventListener('beforeunload', function () {
        showSpinner();
    });

    // Event listener saat halaman sudah dimuat
    window.addEventListener('unload', function () {
        hideSpinner();
    });

       </script>

       {{-- end loading --}}


  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright <strong><span>i-Do </span></strong>2023
    </div>
    <div class="credits">
       Do it Your Work</a>
    </div>
  </footer><!-- End Footer -->


    <!-- Vendor JS Files -->
    <script src="{{asset('/vendor/apexcharts/apexcharts.min.js')}}"></script>
    <script src="{{asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('/vendor/chart.js/chart.umd.js')}}"></script>
    <script src="{{asset('/vendor/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('/vendor/quill/quill.min.js')}}"></script>
    <script src="{{asset('/vendor/simple-datatables/simple-datatables.js')}}"></script>
    {{-- <script src="{{asset('/vendor/tinymce/tinymce.min.js')}}"></script> --}}
    <script src="{{asset('/vendor/php-email-form/validate.js')}}"></script>

    <!-- Template Main JS File -->
    <script src="{{asset('/js/main.js')}}"></script>

</body>
</html>
