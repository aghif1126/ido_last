@extends('layout.pembimbing')

@section('header')
    <div class="card-header text-center">
        <h3>Tambah Kriteria Penilaian</h3></div>
@endsection

@section('content')
    <div class="card-body">
        
        <form action="{{ $data ?  '/pembimbing/update-kriteria/'.$data -> id  :  '/pembimbing/store-kriteria'}}" method="post">
            @csrf
        <input type="hidden" name="id_pembimbing" value="{{ $pembimbing -> id }}">
            <div class="form-group">
                <label for="input-1">Nama Kriteria</label>
                <input type="text" name="nama_kriteria" id="" rows="3" placeholder="Nama" class="form-control" required value="{{ $data ? $data -> nama_kriteria : '' }}">
            </div>
            <div class="d-flex flex-row-reverse">
                <button class="btn btn-secondary col-lg-6" onClick="Anda Yakin?`)">
                    Submit
                </button>
            </div>
        </form>
    </div>
@endsection