@extends('layout.pembimbing')

@section('header')
    <div class="card-header text-center">
        <h3>Detail Nilai {{$siswa -> nama}}</h3></div>
@endsection

@section('content')
    <div class="card-body">
        <div class="d-flex flex-row-reverse">
            <a href="/pembimbing/edit-nilai/{{ $siswa->nis }}">
                <button class="btn btn-info col-lg-12">
                    Ubah Nilai
                </button>
            </a>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-striped">
               <thead>
                 <tr>
                   <th scope="col">No</th>
                   <th scope="col">Komponen</th>
                   <th scope="col">Skor <br> (0-100)</th>
                   <th scope="col">Keterangan</th>
                 </tr>
               </thead>
               <tbody>
                @foreach ($kriteria as $item)
                <tr>
                    <td rowspan="1" style="background-color: rgb(236, 236, 236)">{{ $loop -> iteration }}</td>
                    <td style="background-color: rgb(236, 236, 236)">{{ $item -> nama_kriteria }}</td>
                    <td style="background-color: rgb(236, 236, 236)">
                        @if ($nilai)
                        {{ $nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() ? $nilai -> where('id_kriteria', $item -> id)-> where('id_siswa', $siswa -> nis) -> first() -> point : ''}}
                        @else
                        @endif

                    </td>
                    <td style="background-color: rgb(236, 236, 236)">
                        @if ($nilai)

                        @if ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first())

                            @if ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point >= 90)
                                <div>A</div>
                            @elseif ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point < 90)
                                <div>B</div>
                            @elseif ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point < 80)
                                <div>C</div>
                            @else
                                <div>D</div>
                            @endif
                        @else

                        @endif

                        @else

                        @endif

                    </td>
                </tr>

                @endforeach
               </tbody>
             </table>
         </div>
    </div>
@endsection