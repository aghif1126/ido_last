@extends('layout.pembimbing')


@section('header')
    <div class="card-header text-center">
        <h3>Tulis Catatan ke {{$siswa -> nama}}</h3></div>
        <hr>
@endsection

@section('content')
    <div class="card-body">
        
        <form action="../store-catatan " method="post">
            @csrf
            <input type="hidden" name="id_siswa" value="{{ $siswa->nis }}">
            <input type="hidden" name="id_perusahaan" value="{{ $id_perusahaan }}">
            <div class="form-group">
                <label for="input-1">Tanggal</label>
                <input type="date" name="tanggal" id="" rows="3" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="input-2">Catatan</label>
                <textarea name="catatan" id="" rows="3" placeholder="Catatan siswa ..." class="form-control" required></textarea>
               </div>
            <div class="d-flex flex-row-reverse">
                <button class="btn btn-secondary col-lg-4" onClick="return confirm(`Yakin ingin Menambah Catatan?`)">
                    Submit
                </button>
            </div>
        </form>
    </div>
@endsection