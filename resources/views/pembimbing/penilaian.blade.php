@extends('layout.pembimbing')

@section('content')

<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="table-responsive">
        <form action="../store-nilai" method="post" >
        @csrf
        <input type="text" name="nis" value="{{ $siswa->nis}}" hidden>
        <table class="table ">
            <thead>
                <tr>
                    <td scope="col" style="width: 5%;" class="table-secondary"><p>No</p></td>
                    <td scope=" col" style="width: 20%" class="table-secondary"><p>Komponen</p></td>
                    <td scope="col" style="width: 20%" class="table-secondary">Skor<br>(0-100)</td>
                    <td scope="col" style="width: 20%" class="table-secondary"><p>&nbsp;</p></td>

                </tr>

                <tr>
                    <td rowspan="1" style="background-color: rgb(236, 236, 236)">1</td>
                    <td style="background-color: rgb(236, 236, 236)">Aspek Sikap</td>
                    <td style="background-color: rgb(236, 236, 236)">&nbsp;</td>
                    <td style="background-color: rgb(236, 236, 236)">&nbsp;</td>
                </tr>
                <tr>
                    <td rowspan="6"></td>
                    <td>a. Penampilan dan kerapihan pakaian</td>
                    <td><input type="text" required style="width:10%" name="penampilan"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>b. Komitmen dan integritas</td>
                    <td><input type="text" required style="width:10%" name="komitmen"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>c. Menghargai dan menghormati (kesopanan)</td>
                    <td><input type="text" required style="width:10%" name="kesopanan"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>d. Kreativitas</td>
                    <td><input type="text" required style="width:10%" name="kreativitas"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>e. Kerja sama tim</td>
                    <td><input type="text" required style="width:10%" name="kerjasamatim"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>f. Disiplin dan tanggung jawab</td>
                    <td><input type="text" required style="width:10%" name="disiplin"></td>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td rowspan="1" style="background-color: rgb(236, 236, 236)">2</td>
                    <td style="background-color: rgb(236, 236, 236)">Aspek Pengetahuan</td>
                    <td style="background-color: rgb(236, 236, 236)">&nbsp;</td>
                    <td style="background-color: rgb(236, 236, 236)">&nbsp;</td>
                </tr>

                <tr>
                    <td rowspan="3"></td>
                    <td>a. Penguasaan keilmuan</td>
                    <td><input type="text" required style="width:10%" name="penguasaan_keilmuan"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>b. Kemampuan mengidentifikasi masalah</td>
                    <td><input type="text" required style="width:10%" name="kemampuan_identifikasi"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>c. Kemampuan menemukan alternatif solusi secara kreatif</td>
                    <td><input type="text" required style="width:10%" name="kemampuan_alternatif"></td>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td rowspan="1" style="background-color: rgb(236, 236, 236)">3</td>
                    <td style="background-color: rgb(236, 236, 236)">Aspek Pengetahuan</td>
                    <td style="background-color: rgb(236, 236, 236)">&nbsp;</td>
                    <td style="background-color: rgb(236, 236, 236)">&nbsp;</td>
                </tr>

                <tr>
                    <td rowspan="4"></td>
                    <td>a. Keahlian dan keterampilan</td>
                    <td><input type="text" required style="width:10%" name="keterampilan"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>b. Inovasi dan kreatifitas</td>
                    <td><input type="text" required style="width:10%" name="inovasi"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>c. Produktifitas dan penyelesaian tugas</td>
                    <td><input type="text" required style="width:10%" name="produktivitas"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>d. Penguasaan alat kerja</td>
                    <td><input type="text" required style="width:10%" name="penguasaan_alatkerja"></td>
                    <td>&nbsp;</td>
                </tr>

            </thead>
        </table>
        <br>
        <button type="submit" class="btn btn-secondary" style="margin-left:87%">Submit</button>
        </form>
    </div>
</div>
</div>

@endsection
