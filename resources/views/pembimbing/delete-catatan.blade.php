@extends('layout.pembimbing')
@section('title', 'Dashboard')

@section('title')
<h1>Hapus Catatan</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/pembimbing/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/pembimbing/catatan">Daftar Catatan</a></li>
          <li class="breadcrumb-item active">hapus Catatan</li>
        </ol>
      </nav>
@endsection

@section('content')
    @csrf
    <div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
        <div class="d-flex flex-row">
            <div class="me-auto p-2">
                <h3>Pilih catatan yang akan di Hapus</h3>
            </div>
        </div>
        <hr>
    <form action="/pembimbing/destroy-catatan" method="POST">
        @csrf
        <input type="text" value="{{$siswa->nis}}" name="id_siswa" id="id-siswa" hidden>
        <div class="table-responsive">
            <table class="table" id="mainTable">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">tanggal</th>
                        <th scope="col">catatan</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($catatan as $a)

                    <tr>
                        <td id="number">{{ $loop->iteration }}</td>
                        <td>{{ $a -> tanggal}}</td>
                        <td>{{ $a -> catatan}}</td>
                        <td><button value="{{ $a -> id }}" name='name' type="submit" class="btn btn-secondary" onClick="return confirm(`Yakin ingin menghapus {{ $a -> nama }}?`)">{{ $a -> nama}} Hapus</button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
</form>
</div>
</div>

@endsection