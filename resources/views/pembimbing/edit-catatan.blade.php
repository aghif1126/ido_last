@extends('layout.pembimbing')

@section('header')
    <div class="card-header text-center">
        <h3>Edit Catatan</h3></div>
        <hr>
@endsection

@section('content')
    <div class="card-body">
        
        <form action="../update-catatan " method="post">
            @csrf
        <input type="hidden" name="id" value="{{ $catatan->id }}">
            <div class="form-group">
                <label for="input-1">Tanggal</label>
                <input type="date" name="tanggal" id="" rows="3" class="form-control"  value="{{ $catatan->tanggal }}" required>
            </div>
            <div class="form-group">
                <label for="input-1">Catatan</label>
                <textarea name="catatan" id="" rows="3" placeholder="Catatan siswa ..." class="form-control" required>{{ $catatan->catatan }}</textarea>
               </div>
            <div class="d-flex flex-row-reverse">
                <button class="btn btn-secondary col-lg-4" onClick="return confirm(`Yakin ingin mengedit?`)">
                    Submit
                </button>
            </div>
        </form>
    </div>
@endsection