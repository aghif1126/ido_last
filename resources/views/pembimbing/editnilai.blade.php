@extends('layout.pembimbing')

@section('header')
    <div class="card-header text-center">
        <h3>Berikan Nilai {{$siswa -> nama}}</h3></div>
@endsection

@section('content')
    <div class="card-body">
        <br>
        <form action="../update-nilai" method="post">
            @csrf
        <input type="text" name="id_siswa" value="{{ $siswa->nis }}" hidden>
        <div class="table-responsive">
            <table class="table table-striped">
               <thead>
                 <tr>
                   <th scope="col">No</th>
                   <th scope="col">Komponen</th>
                   <th scope="col">Skor <br> (0-100)</th>
                   <th scope="col">Keterangan</th>
                </tr>
               </thead>
               <tbody>
                @foreach ($kriteria as $item)
                <input type="text" name="kriteria[]" value="{{ $item -> id }}" hidden>
                <tr>
                    <td rowspan="1" style="background-color: rgb(236, 236, 236)">{{ $loop -> iteration }}</td>
                    <td style="background-color: rgb(236, 236, 236)">{{ $item -> nama_kriteria }}</td>
                    <td style="background-color: rgb(236, 236, 236)">
                        @if ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis) -> first() )
                        <input name="nilai[]" type="number" onKeyUp="if(this.value>100){this.value='';}else if(this.value<0){this.value='';}" class="form-control col-lg-4" value="{{$nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis) -> first() -> point }}" required>
                        @else
                        <input name="nilai[]" type="number" onKeyUp="if(this.value>100){this.value='';}else if(this.value<0){this.value='';}" class="form-control col-lg-4" value="" required>
                        @endif


                    </td>
                    <td style="background-color: rgb(236, 236, 236)">&nbsp;</td>
                </tr>

                @endforeach
                </tbody>
             </table>
            </div><br>
            <div class="d-flex flex-row-reverse">
                <a href="">
                    <button class="btn btn-info col-lg-12">
                        Submit
                    </button>
                </a>
            </div>
        </form>
    </div>
@endsection