@extends('layout.pembimbing')

@section('header')
    <div class="card-header text-center">
        <h3>List Kriteria</h3></div>
@endsection

@section('content')
    <div class="card-body">
        <div class="d-flex flex-row-reverse">
            <a href="add-kriteria">
                <button class="btn btn-info col-lg-12">
                    Tambah
                </button>
            </a>
        </div>
        <br>

        @if (!is_null($kriteria))
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Kriteria</th>
                        <th scope="col">Action</th>
                      </tr>
                </thead>
               <tbody>
                 @foreach ($kriteria as $a)

                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $a ->  nama_kriteria }}</td>
                    <td>
                        <a class="btn btn-dark" href="/pembimbing/edit-kriteria/{{ $a->id }}" role="button" >Edit Kriteria</a>
                        <a class="btn btn-danger" href="/pembimbing/delete-kriteria/{{ $a->id }}" role="button" onClick="return confirm(`Anda Yakin?`)">Hapus Kriteria</a>

                    </td>
                </tr>
                @endforeach
               </tbody>
            </table>
        </div>
        @else

        @endif
    </div>
@endsection