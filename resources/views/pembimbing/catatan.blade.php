@extends('layout.pembimbing')

@section('header')
    <div class="card-header text-center">
        <h3>List Catatan {{ $siswa -> nama }}</h3>
    <hr>
    </div>
@endsection

@section('content')
    <div class="card-body">
        <div class="d-flex flex-row-reverse">
            <a href="../add-catatan/{{ $siswa->nis}}">
                <button type="button" class="btn btn-info">Tambah</button>
            </a>
        </div>
        <br>
        @if (!is_null($catatan))
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col" style="width: 10%">No</th>
                    <th scope="col" style="width: 10%">Tanggal</th>
                    <th scope="col" >Catatan</th>
                    <th scope="col" >Action</th>

                </tr>
            </thead>
            <tbody>
                @foreach ($catatan as $a)


                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $a ->  tanggal }}</td>
                    <td>{{ $a ->  catatan }}</td>
                    <td>
                        <a class="btn btn-dark" href="../edit-catatan/{{ $a->id }}" role="button">Edit</a>
                        <form action="/pembimbing/destroy-catatan" method="POST">
                            @csrf
                        <button class="btn btn-danger" type="submit" name="id_catatan" value="{{ $a -> id }}" role="button" onClick="return confirm(`Anda Yakin?`)">Hapus</button>

                        </form>
                    </td>


                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    @else

    @endif
    </div>
@endsection
