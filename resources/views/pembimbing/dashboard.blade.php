@extends('layout.pembimbing')

@section('dashboard')
<div style="background-color: rgb(10, 115, 235);margin-right: -25px; margin-left: -25px; margin-top:-10px; padding-right: 12.5px; padding-left: 12.5px; 12.5px; padding-top: 10.5px; 12.5px; padding-bottom: 10.5px;">
    <h3 class="text-white">Hi, {{ $pembimbing -> nama }} </h3>
    <p class="text-white">Anda adalah pembimbing dari perusahaan {{$pembimbing -> perusahaan -> nama}}</p>
    <br>
    <div class="row">
        <div class="col-lg-12">
           <div class="card">
            
            <div class="card-body">
                <h3>Detail Perusahaan</h3>
                <div class="table-responsive">
                    <table class="table">
                       <tbody>
                        <tr>
                            <th scope="row">Nama Perusahaan</th>
                            <td>:</td>
                            <td>{{ $pembimbing -> perusahaan -> nama }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Alamat</th>
                            <td>:</td>
                            <td>{{ $pembimbing -> perusahaan -> alamat }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Email</th>
                            <td>:</td>
                            <td>{{ $pembimbing -> perusahaan -> email }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Fax</th>
                            <td>:</td>
                            <td>{{ $pembimbing -> perusahaan -> fax }}</td>
                        </tr>
                        <tr>
                            <th scope="row">No Telepon</th>
                            <td>:</td>
                            <td>{{ $pembimbing -> perusahaan -> telp }}</td>
                        </tr>
                       </tbody>
                    </table>
                </div>
            </div>
           </div>
        </div>
    </div>
</div>
<br>
@endsection


@section('header')

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.min.js" integrity="sha384-Rx+T1VzGupg4BHQYs2gCW9It+akI2MM/mndMCy36UVfodzcJcF0GGLxZIzObiEfa" crossorigin="anonymous"></script>

    <div class="card-header">
        <div>
            <div class="d-flex flex-row-reverse">
                <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#editpassword">
                    Ganti Password
                    </button>
                    <div class="modal fade" id="editpassword" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title fs-5" id="exampleModalLabel">Ganti Password</h3>
            
                                <button type="button" class="btn zmdi zmdi-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form action="/pembimbing/update-password" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-outline mb-3">
                                        <input type="text" class="form-control" name="id" placeholder="id" value="{{ $user -> id }}" hidden>
                                        {{-- <input type="text" name="email" value="{{$siswa -> id_user}}"> --}}
                                        <label class="form-label" for="form3Example4">Masukkan Password Baru</label>
                                        <input type="password" name="password" id="inputPassword"
                                                class="form-control form-control-sm" placeholder="Enter password"
                                                onfocus="focusevent1()" onblur="blurevent1()" required />
            
                                        <div class="mb-3 row ml-1">
                                            <div class="col-sm-12" style="margin-top: 6px; margin-left: 8px;">
                                                <input type="checkbox" class="form-check-input" id="show-password"
                                                    onclick="myFunction()"> Show Password
                                            </div>
                                        </div>
                                        <select class="form-select" name="role_id" aria-label=".form-select-lg example" hidden>
                                            <option value="2" selected>Pembimbing</option>                
                                        </select>
                                    </div>
                            <div class="modal-footer">   
                                    <button type="submit" class="btn btn-secondary" onClick="return confirm(`Anda Yakin?`)">Simpan</button>
                                </form>
                        </div>
                    </div>
                        </div>
            </div>
        </div>
    </div>
@endsection


@section('content')

<div class="card-body">
    <h3>Daftar Siswa</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">NIS</th>
                    <th scope="col">Nama</th>
                    <th scope="col">L/P</th>
                    <th scope="col">Kelas</th>
                    <th scope="col">Konsentrasi</th>
                    <th scope="col">Catatan</th>
                    <th scope="col">Nilai</th>
                  </tr>
            </thead>
           <tbody>
            @foreach ($siswa as $a)


        <tr>
            {{-- <td>{{ $siswa -> firstItem() + $loop->index }}</td> --}}
            <td> {{ $loop->iteration }}</td>
            <td>{{ $a -> detailsiswa ->  nis }}</td>
            <td><a href="/pembimbing/detail-siswa/{{ $a -> detailsiswa -> nis }}"
                    class=""><button class="btn">
                    {{ $a -> detailsiswa -> nama}}</button></a></td>
            <td>{{ $a -> detailsiswa -> jenkel}}</td>
            <td>{{ $a -> detailsiswa ->  kelas -> nama }}</td>
            <td>{{ $a -> detailsiswa -> kelas -> konsentrasi -> nama}}</td>
            <td><a href="/pembimbing/catatan/{{ $a -> detailsiswa -> nis }}" class=""><button class="btn btn collapsed btn-info"> Catatan</button></a></td>
            <td><a href="/pembimbing/nilai/{{ $a -> detailsiswa -> nis }}" class=""><button class="btn btn collapsed btn-info"> Lihat Nilai</button></a></td>

        </tr>
        @endforeach
           </tbody>
        </table>
    </div>
</div>
    <script>
    
        // Password mulai dari sini
        function myFunction() {
            var x = document.getElementById("inputPassword");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
    </script>
@endsection