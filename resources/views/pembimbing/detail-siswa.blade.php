@extends('layout.pembimbing')

@section('foto')
    <img src="https://via.placeholder.com/110x110" class="img-circle" alt="user avatar"></span>
@endsection

@section('profil')
    <div class="avatar"><img class="align-self-start mr-3" src="https://via.placeholder.com/110x110" alt="user avatar"></div>
    <div class="media-body">
        <h6 class="mt-2 user-title">nama</h6>
        <p class="user-subtitle">nip</p>
    </div>    
@endsection

@section('header')
    <div class="card-header">
        <h3>Detail dari {{ explode(" ", $siswa -> nama)[0] }}</h3>
    </div>
@endsection


@section('content')
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
               <tbody>
                <tr>
                    <th scope="row">Nama</th>
                    <td>:</td>
                    <td>{{ $siswa -> nama }}</td>
                </tr>
                <tr>
                    <th scope="row">Kelas</th>
                    <td>:</td>
                    <td>{{ $siswa -> kelas -> nama }}</td>
                </tr>
                <tr>
                    <th scope="row">Jenis Kelamin</th>
                    <td>:</td>
                    <td>{{ $siswa -> jenkel }}</td>
                </tr>
                <tr>
                    <th scope="row">Alamat</th>
                    <td>:</td>
                    <td>{{ $siswa -> alamat }}</td>
                </tr>
                <tr>
                    <th scope="row">Tanggal Lahir</th>
                    <td>:</td>
                    <td>{{ $siswa -> tanggal_lahir }}</td>
                </tr>
                <tr>
                    <th scope="col">Konsentrasi Keahlian</th>
                    <td>:</td>
                    <td>{{ $siswa -> kelas -> konsentrasi -> nama }}</td>
                </tr>
                <tr>
                    <th scope="col">Program Keahlian</th>
                    <td>:</td>
                    <td>{{ $siswa -> kelas -> konsentrasi -> jurusan -> nama }}</td>
                </tr>
               </tbody>
            </table>
        </div>
    </div>
@endsection

@section('content1')
<div class="row">
    <div class="col-12 col-lg-12 col-xl-12">
       <div class="card">
        <div class="card-header">
            <h3>Riwayat Kehadiran</h3>
            <hr>
        </div>
        <div class="card-body">
            @foreach ($absen as $r)
    <div class="accordion" id="accordionExample">
        <div class="accordion-item bg-light">
            <h2 class="accordion-header" id="heading{{ $loop -> iteration }}">
                <button class="accordion-button collapsed btn btn-outline-secondary w-100" type="button" data-bs-toggle="collapse"
                    data-bs-target="#collapse{{ $loop -> iteration }}" aria-expanded="false"
                    aria-controls="collapse{{ $loop -> iteration }}">
                    Tanggal : {{ $r -> tanggal }}
                    {{-- {{ $r -> aktivitas }} --}}
                </button>
            </h2>
            <div id="collapse{{ $loop -> iteration }}" class="accordion-collapse collapse"
                aria-labelledby="heading{{ $loop -> iteration }}" data-bs-parent="#accordionExample">

                <div class="accordion-body">
                    <h4>Detail Absen</h4>
                    <div class="table-responsive">

                        <table class="table ">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 20%">Jam Masuk</th>
                                    <td>:</td>
                                    <td>{{ $r -> jam_masuk }}</td>
                                    {{-- <th scope="col" style="width: 20%">Jam Pulang</th>
                                    <td>:</td>
                                    <td>{{ $r -> jam_masuk }}</td> --}}
                                </tr>
                                <tr>
                                    <th scope="col" style="width: 20%">Status</th>
                                    <td>:</td>
                                    <td>{{$r -> status}}</td>
                                </tr>
                                <tr>
                                    @if ($r->status == "Hadir")
                                    <th scope="col" style="width: 20%">Poto</th>
                                    <td>:</td>
                                    <td>@if ($r -> poto_masuk)
                                            <img src="{{ asset('uploads/absen/masuk/uploads'.$r->poto_masuk) }}" style="border-radius:10px" alt=""
                                            height="240px" width="320px">
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                {{-- <tr>
                                    <th scope="col" style="width: 20%">Tanggal</th>
                                    <td>:</td>
                                    <td>{{ $r -> tanggal }}</td>
                                </tr> --}}

                                @if ($r->jam_pulang != "Tidak Ada")
                                <tr>
                                    <th scope="col" style="width: 20%">Pulang</th>
                                    <td>:</td>
                                    <td>{{ $r -> jam_pulang }}</td>
                                </tr>
                                <tr>
                                    <th scope="col" style="width: 20%">Poto Pulang</th>
                                    <td>:</td>
                                    <td><img src="{{ asset('uploads/absen/pulang/uploads'.$r->poto_pulang) }}" alt="" height="100px" width="150px" style="border-radius: 10px;"></td>
                                </tr>
                                @endif


                            </thead>

                        </table>
                    </div>
                </div>

            </div>


            <br>
            <br>

            <div id="collapse{{ $loop -> iteration }}" class="accordion-collapse collapse"
                aria-labelledby="heading{{ $loop -> iteration }}" data-bs-parent="#accordionExample">
                @foreach ($r -> aktivitas  as $aktivitas)
                <div class="accordion-body">
                    <h4>Kegiatan {{ $loop-> iteration }}</h4>
                    <div class="table-responsive">

                        <table class="table ">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 20%">Kegiatan</th>
                                    <td>:</td>
                                    <td>{{ $aktivitas -> kegiatan }}</td>
                                </tr>
                                <tr>
                                    <th scope="col" style="width: 20%">Divisi</th>
                                    <td>:</td>
                                    <td>{{ $aktivitas -> divisi }}</td>
                                </tr>
                                {{-- <tr>
                                    <th scope="col" style="width: 20%">Tanggal</th>
                                    <td>:</td>
                                    <td>{{ $aktivitas -> tanggal }}</td>
                                </tr> --}}

                                <tr>
                                    <th scope="col" style="width: 20%">Mulai</th>
                                    <td>:</td>
                                    <td>{{ $aktivitas -> mulai }}</td>
                                </tr>
                                <tr>
                                    <th scope="col" style="width: 20%">Selesai</th>
                                    <td>:</td>
                                    <td>{{ $aktivitas -> selesai }}</td>
                                </tr>
                                <tr>
                                    <th scope="col" style="width: 20%">Perusahaan</th>
                                    <td>:</td>
                                    <td>{{ $aktivitas -> perusahaan -> nama }}</td>
                                </tr>


                            </thead>

                        </table>
                    </div>
                </div>
                @endforeach
            </div>






        </div>
    </div>

    @endforeach
        </div>
       </div>
    </div>
</div>

{{-- ini dropdownnya --}}
<script src="{{asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
@endsection