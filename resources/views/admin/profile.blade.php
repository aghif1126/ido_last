@extends('layout.admin')
@section('content')


<style>
    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    .awal {
        display: block;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
</style>
<div class="container shadow p-3 mb-5 my-3 bg-body-tertiary rounded bg-dark text-dark"
    style="width: 76%; margin-left: 280px">
    <h1 align="center">Profile</h1>

    <div class="table-responsive-md">
        <table class="table table-bordered">
            <tbody>
                <tr class="">
                    <td scope="row" height="400px" width="300px">
                        <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor"
                            class="bi bi-person-circle" viewBox="0 0 16 16"
                            style="margin-left: 100px; margin-top: 100px">
                            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                            <path fill-rule="evenodd"
                                d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                        </svg>
                        <h3 align="center">&nbsp&nbsp&nbspNama</h3>
                    </td>
                    <td>
                        <div class="tab">
                            <button class="tablinks" onclick="openCity(event, 'PersonalInfo')">PersonalInfo</button>
                            <button class="tablinks" onclick="openCity(event, 'Contact')">Contact</button>
                            <button class="tablinks" onclick="openCity(event, 'About')">About</button>
                        </div>

                        <div id="awal" class="awal">
                            <h3>Hai</h3>
                            <p>This is your personal information.</p>
                        </div>

                        <div id="PersonalInfo" class="tabcontent">
                            <h3>Hai</h3>
                            <p>This is your personal information.</p>
                        </div>

                        <div id="Contact" class="tabcontent">
                            <h3>My Contact</h3>
                            <p>0823XXXXXX</p>
                        </div>

                        <div id="About" class="tabcontent">
                            <h3>About</h3>
                            <p>About Me.</p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <script>
        function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        // awal = document.getElementsByClassName("awal");
        // for (i = 0; i < awal.length; i++) {
        //     tabcontent[i].style.display = "block";
        // }
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        document.getElementById('awal').style.display = "none";
        evt.currentTarget.className += " active";
        }
    </script>
</div>
@endsection