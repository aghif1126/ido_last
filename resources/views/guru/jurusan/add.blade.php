{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Jurusan
@endsection
@section('title')
<h1>Tambah Jurusan</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-jurusan">Daftar Jurusan</a></li>
          <li class="breadcrumb-item active">Tambah Jurusan</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <form action="store-jurusan" method="post">
        @csrf

        <div class="mb-3">
            {{-- <label for="nis" class="form-label fs-4">NIS</label> --}}
            <input type="text" class="form-control" name="nama" onkeydown="return /[a-z A-Z .]/i.test(event.key)" placeholder="Name" required>
        </div>
        <div class="d-flex justify-content-end">
            <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin menambahkan jurusan?`)">Submit</button></div>
    </form>
</div>
</div>

@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
