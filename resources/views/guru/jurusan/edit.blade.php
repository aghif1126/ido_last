{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Jurusan
@endsection
@section('title')
<h1>Edit Jurusan</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-jurusan">Daftar Jurusan</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-jurusan/{{$jurusan -> id}}">Detail Jurusan</a></li>
          <li class="breadcrumb-item active">Edit Jurusan</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Detail {{ $jurusan -> nama }} </h3>
        </div>

    </div>
    <hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td>{{ $jurusan -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Kaprog</th>
                    <td>:</td>
                    @if (is_null($jurusan -> kaprog))
                    <td>Tidak Tersedia</td>
                    @else
                    <td>{{ $jurusan -> kaprog -> nama}}</td>
                    @endif
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
</div>

<div clas="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Edit jurusan </h3>
        </div>

    </div>
    <hr>
    <form action="/guru/update-jurusan" method="post">
        @csrf
        <div class="table-responsive">
            <table class="table ">
                <thead>
                    <input type="text" class="form" name="id" placeholder="Website" value="{{ $jurusan->id }}" hidden>
                    <tr>
                        <th scope="col">Nama</th>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="nama" onkeydown="return /[a-z A-Z .]/i.test(event.key)" placeholder="Website"
                                value="{{  $jurusan -> nama }}" required></td>
                    </tr>
                    <tr>
                        <th scope="col">Kaprog</th>
                        <td>:</td>
                        <td>
                            <select name="id_kaprog" class="form-select"
                                aria-label=".form-select-lg example" required>
                                @if (is_null($jurusan -> id_guru))
                                <option selected hidden>Pilih Kaprog</option>
                                @foreach ($kaprog as $g )
                                <option value="{{ $g->id }}">{{ $g->nama }}
                                </option>

                                @endforeach
                                @else
                                <option selected value="{{ $jurusan -> id_guru }}">
                                    {{ $jurusan -> kaprog -> nama }}</option>
                                @foreach ($kaprog as $g )
                                <option value="{{ $g->id }}">{{ $g->nama }}</option>

                                @endforeach
                                @endif

                            </select>
                        </td>
                    </tr>
                </thead>
            </table>
            <div class="d-flex justify-content-end">
                <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin mengedit {{ $jurusan -> nama }}?`)">Simpan</button></div>
    </form>
</div>
</div>

@endsection
