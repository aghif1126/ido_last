{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Jurusan
@endsection
@section('title')
<h1>Detail Jurusan</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-jurusan">Daftar Jurusan</a></li>
          <li class="breadcrumb-item active">Detail Jurusan</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Detail {{ $jurusan -> nama }} </h3>
        </div>
        {{-- <a href="/guru/delete-jurusan"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Hapus</button></a> --}}
        <a href="/guru/edit-jurusan/{{ $id }}"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Edit</button></a>

    </div>
    <hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td>{{ $jurusan -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Kaprog</th>
                    <td>:</td>
                    @if (is_null($jurusan -> kaprog))
                    <td>Tidak Tersedia</td>
                    @else
                    <td><a href="/guru/detail-guru/{{ $jurusan -> kaprog -> id }}"><button>{{ $jurusan -> kaprog ->
                                nama}}</button></a></td>
                    @endif
                </tr>
                <tr>
                    <th scope="col">Konsentrasi </th>
                    <td>:</td>
                    @if (is_null($konsentrasi))
                    <td>Data Tidak Ada</td>
                    @else
                    <td>
                        @foreach ($konsentrasi as $a)
                        {{ $a -> nama }} <br>
                        @endforeach
                    </td>
                    @endif
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

@endsection
