{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>Tambah Akun Siswa</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-user">Daftar Akun</a></li>
          <li class="breadcrumb-item"><a href="/guru/pilih-user">Tambah Akun</a></li>
          <li class="breadcrumb-item"><a href="/guru/pilih-user/siswa">Pilih Akun Siswa</a></li>
          <li class="breadcrumb-item active">Tambah Akun Siswa</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <form action="../../siswa/store-user" method="post">
        @csrf
        <input type="text" class="form-control" name="id" placeholder="id" value="{{ $id }}" hidden>
        <div class="table-responsive">
            <table class="table ">
                <thead>
                    <tr>
                        <th scope="col">Nama</th>
                        <td>:</td>
                        <td>
                            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $user->nama }}">
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">Username</th>
                        <td>:</td>
                        <td>
                            <input type="text" class="form-control" name="email" placeholder="Email">
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">Password</th>
                        <td>:</td>
                        <td>
                            <div class="input-group">
                            <input type="password" id="pass" class="form-control" name="pass" placeholder="Password">
                            <div class="input-group-append">

                                <!-- kita pasang onclick untuk merubah icon buka/tutup mata setiap diklik  -->
                                <span id="mybutton" onclick="change()" class="input-group-text">

                                    <!-- icon mata bawaan bootstrap  -->
                                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-eye-fill">
                                        <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
                                        <path fill-rule="evenodd"
                                            d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
                                    </svg>
                                </span>
                            </div>
                            </div>
                        </td>
                    </tr>
                </thead>
            </table>
        </div>



        <div class="mb-3">
            <select class="form-select" name="role_id" aria-label=".form-select-lg example" hidden>
                <option value="4" selected>Siswa</option>

            </select>
        </div>

        <div class="d-flex justify-content-end">
            <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin melanjutkan?`)">Submit</button></div>
    </form>
</div>
</div>

@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
<script>
    // membuat fungsi change
    function change() {

        // membuat variabel berisi tipe input dari id='pass', id='pass' adalah form input password
        var x = document.getElementById('pass').type;

        //membuat if kondisi, jika tipe x adalah password maka jalankan perintah di bawahnya
        if (x == 'password') {

            //ubah form input password menjadi text
            document.getElementById('pass').type = 'text';

            //ubah icon mata terbuka menjadi tertutup
            document.getElementById('mybutton').innerHTML = `<svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-eye-slash-fill" >
                                                            <path d="M10.79 12.912l-1.614-1.615a3.5 3.5 0 0 1-4.474-4.474l-2.06-2.06C.938 6.278 0 8 0 8s3 5.5 8 5.5a7.029 7.029 0 0 0 2.79-.588zM5.21 3.088A7.028 7.028 0 0 1 8 2.5c5 0 8 5.5 8 5.5s-.939 1.721-2.641 3.238l-2.062-2.062a3.5 3.5 0 0 0-4.474-4.474L5.21 3.089z"/>
                                                            <path d="M5.525 7.646a2.5 2.5 0 0 0 2.829 2.829l-2.83-2.829zm4.95.708l-2.829-2.83a2.5 2.5 0 0 1 2.829 2.829z"/>
                                                            <path fill-rule="evenodd" d="M13.646 14.354l-12-12 .708-.708 12 12-.708.708z"/>
                                                            </svg>`;
        }
        else {

            //ubah form input password menjadi text
            document.getElementById('pass').type = 'password';

            //ubah icon mata terbuka menjadi tertutup
            document.getElementById('mybutton').innerHTML = `<svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-eye-fill">
                                                            <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                                            <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                                            </svg>`;
        }
    }
</script>
