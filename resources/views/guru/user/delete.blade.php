{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - User
@endsection
@section('title')
<h1>Hapus Akun</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-user">Daftar Akun</a></li>
          <li class="breadcrumb-item active">Hapus Akun</li>
        </ol>
      </nav>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">

@section('content')


<form action="destroy-user" method="POST">
    @csrf
    <div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">

        <div class="d-flex flex-row">
            <div class="me-auto p-2">
                <h3>Pilih daftar pengguna yang akan dihapus</h3>
            </div>
            <div class="col-12 col-sm-8 col-md-6">
                <form action="">
                    <div class="input-group mb-3 ">
                        <input type="text" class="form-control" placeholder="Search Data" name="keyword" id="myInput">
                        {{-- <button class="btn btn-outline-secondary" type="submit" id="button-addon1">Search</button> --}}
                    </div>
                </form>
            </div>
        </div>
        <hr>

        <div class="table-responsive">
            <table class="table" id="mainTable">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Username</th>
                        <th scope="col">Password</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user as $a)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td><button name='id' value="{{  $a -> id }}"
                                class="link-dark link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover" onClick="return confirm(`Yakin ingin menghapus user {{ $a -> name }}?`)">
                                {{ $a -> name}}</a></td>
                        <td>{{ $a -> email}}</td>
                        <td>{{ $a -> pass}}</td>
                        @if (is_null($a -> role -> name))
                        <td> Tidak Tersedia </td>
                        @else
                        <td> {{ $a -> role -> name }} </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- <div class="container">
                {{ $user->links() }}
            </div> --}}
        </div>
    </div>
    </div>
</form>
@endsection

<script>
    // $(document).on('submit', '[id^=tableDelete_]' , function() {
    //     return callAjax( $(this).serialize() );
    //     });

    //     function callAjax( data ) {
    //         $.ajax({
    //         type : 'POST',
    //         url  : 'call/page.php',
    //         data : data,
    //         success :  function(data) { ... },
    //         error: function(data) { ... }
    //     });
    //     return false;
    //     };

    $(document).ready(function(){
        var table = $("#mainTable").DataTable({
            "pageLength": 15,
            // "searching": false,
            "lengthChange": false,
            "pagingType": "simple_numbers",

            columnDefs: [
            {
                searchable: false,
                orderable: false,
                targets: 0
            }],

            order: [[1, 'asc']]
        });

        table
        .on('order.dt search.dt', function () {
            let i = 1;
    
            table
                .cells(null, 0, { search: 'applied', order: 'applied' })
                .every(function (cell) {
                    this.data(i++);
                });
        })
        .draw();

        $("#myInput").on("keyup", function() {
            table.search( this.value ).draw();
        });
    });
</script>
