{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - User
@endsection
@section('title')
<h1>Detail Akun</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-user">Daftar Akun</a></li>
          <li class="breadcrumb-item active">Detail Akun</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Detail Pengguna </h3>
        </div>
        <a href="/guru/edit-user/{{ $id }}"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Edit</button></a>

    </div>
    <hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    <td>:</td>
                    <td>{{ $user -> name}}</td>
                </tr>
                <tr>
                    <th scope="col">Email</th>
                    <td>:</td>
                    <td>{{ $user -> email}}</td>
                </tr>
                <tr>
                    <th scope="col">Password</th>
                    <td>:</td>
                    <td>{{ $user -> pass}}</td>
                </tr>
                <tr>
                    <th scope="col">Status</th>
                    <td>:</td>
                    <td>{{ $user -> role -> name}}</td>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

@endsection
