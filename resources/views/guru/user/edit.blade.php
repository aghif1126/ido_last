{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - User
@endsection
@section('title')
<h1>Edit Akun</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-user">Daftar Akun</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-user/{{$user -> id}}">Detai Akun</a></li>
          <li class="breadcrumb-item active">Edit Aku </li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Edit User </h3></div>
        {{-- <a href="/guru/add-user"><button type="button" class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
        <a href="/guru/delete-user"><button type="button" class="btn btn-secondary mx-3 align-self-end">Hapus</button></a> --}}

    </div>
<hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    <td>:</td>
                    <td>    {{ $user -> name}}</td>
                </tr>
                <tr>
                    <th scope="col">Email</th>
                    <td>:</td>
                    <td>{{ $user -> email}}</td>
                </tr>
                <tr>
                    <th scope="col">Password</th>
                    <td>:</td>
                    <td>{{ $user -> pass}}</td>
                </tr>
                <tr>
                <th scope="col">Status</th>
                    <td>:</td>
                    <td>{{ $user -> role -> name}}</td>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Edit user </h3></div>
        {{-- <a href="/guru/add-user"><button type="button" class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
        <a href="/guru/delete-user"><button type="button" class="btn btn-secondary mx-3 align-self-end">Hapus</button></a> --}}

    </div>
<hr>
<form action="/guru/update-user" method="post">
    @csrf
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <input type="text" class="form" name="id" placeholder="Website" value="{{ $user->id }}" hidden>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td><input type="text" class="form-control" name="name" placeholder="Website" value="{{  $user -> name }}"></td>
                </tr>
                <tr>
                    <th scope="col">Username</th>
                    <td>:</td>
                    <td><input type="text" class="form-control" name="email" placeholder="Website" value="{{  $user -> email }}"></td>
                </tr>
                <tr>
                    <th scope="col">Password</th>
                    <td>:</td>
                    <td><input type="text" class="form-control" name="pass" placeholder="Website" value="{{  $user -> pass }}"></td>
                </tr>
                <tr>
                <th scope="col">Status</th>
                    <td>:</td>
                    <td>
                    <select name="role_id" id="" class="form-select form-select" aria-label=".form-select">
                        <option value="{{ $user -> role -> id }}">{{ $user -> role -> name }}</option>
                        {{-- @foreach ($role as $item)
                        <option value="{{ $item -> id }}">{{ $item -> name }}</option>
                        @endforeach --}}
                    </select>
                    </td>
                </tr>
            </thead>
        </table>
        <div class="d-flex justify-content-end">
            <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin mengedit {{ $user -> name }}?`)">Simpan</button>
    </div>
</form>
</div>
</div>

@endsection
