@extends('layout.guru')
@section('judul')
    - User
@endsection
@section('title')
<h1>Tambah Akun</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-user">Daftar User</a></li>
          <li class="breadcrumb-item active">Tambah Akun</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
<h1>Pilih Akun yang akan ditambahkan</h1>

<div class="row">
    <div class="col-4">
            <a href="pilih-user/guru" class="border border-5 border-secondary "><button class="btn btn-secondary">
                <div class="card-body">
                    <h5 class="card-text" align="center">Guru</h5>
                    <p class="card-text" align="center">Buatkan akun untuk Guru</p>
                </div></button>
            </a>
    </div>
    <div class="col-4">
            <a href="pilih-user/siswa" class="border border-5 border-secondary "><button class="btn btn-secondary">
                <div class="card-body">
                    <h5 class="card-text" >Siswa</h5>
                    <p class="card-text" >Buatkan akun untuk Siswa</p>
                </div></button>
            </a>
    </div>
    <div class="col-4 ">
            <a href="pilih-user/pembimbing" class="border border-5 border-secondary"><button class="btn btn-secondary">
                <div class="card-body">
                <h5 class="card-text" >Pembimbing</h5>
                <p class="card-text" >Buatkan akun untuk Pembimbing</p>
                </div></button>
            </a>

    </div>
    {{-- <div class="col-sm-6">
        <div class="card">
            <a href="pilih-user/kaprog" class="border border-5 border-secondary r"><button class="btn btn-secondary">
                <div class="card-body">
                <h5 class="card-title" align="center">Kepala Program</h5>
                <p class="card-text" align="center">Buatkan akun untuk Kepala Program</p>
                </div></button>
            </a>
        </div>
    </div> --}}
  </div>
</div>
</div>
@endsection
