@extends('layout.guru')
@section('title')
<div class="d-flex flex-row">
    <div class="me-auto p-2">
        <h1>Pilih Akun Guru</h1>
        <nav>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
            <li class="breadcrumb-item"><a href="/guru/show-user">Daftar Akun</a></li>
            <li class="breadcrumb-item"><a href="/guru/pilih-user">Tambah Akun</a></li>
            <li class="breadcrumb-item active">Pilih Akun Guru</li>
            </ol>
        </nav>
    </div>
    <div class="col-sm-12 col-md-5 mb-md-0 me-2">
        <form action="">
            {{-- @csrf --}}
            <select class="form-control custom-select select2 ng-pristine ng-valid ng-not-empty ng-touched" ng-model="" name="tahun" id="" ng-change="">
                @foreach ($tahun as $item)
                <option value="{{ $item -> id }}" class="ng-binding ng-scope" {{ $item -> status == "aktif" ? 'selected' : '' }}>Tahun Pelajaran {{ $item -> tahun }} - ({{ $item -> status }})</option>
                @endforeach
            </select>
        </div>
            <div>
                <input type="submit" value="Terapkan" class="btn border border-info" >
            </div>
        </form>
</div>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">

@section('content')

<div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
<div class="col-12 col-sm-8 col-md-6">
    <form action="">
        <div class="input-group mb-3 ">
            <input type="text" class="form-control" placeholder="Search Data" name="keyword" id="myInput">
            {{-- <button class="btn btn-outline-secondary" type="submit" id="button-addon1">Search</button> --}}
        </div>
    </form>
</div>
<hr>
    <div class="table-responsive">
        <table class="table" id="mainTable">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">NIP</th>
                    <th scope="col">Jenkel</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Telepon</th>
                    {{-- <th scope="col">Tahun Pelajaran</th> --}}
                </tr>
            </thead>
            <tbody>
                @foreach ($guru as $a)
                @if (is_null($a -> id_user))
                <tr>
                    <td class="number">{{ $loop->iteration }}</td>
                    <td><a href="../add-user/guru/{{ $a -> id }}" >{{ $a -> nama}}</a></td>
                    <td>{{ $a -> nip}}</td>
                    <td>{{ $a -> jenkel}}</td>
                    <td>{{ $a -> alamat}}</td>
                    <td>{{ $a -> telp}}</td>
                    {{-- <td>{{ $a -> tahun_pelajaran}}</td> --}}

                </tr>
                @endif


                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection

<script>
    $(document).ready(function(){
        var table = $("#mainTable").DataTable({
            "pageLength": 15,
            // "searching": false,
            "lengthChange": false,
            "pagingType": "simple_numbers",

            columnDefs: [
            {
                searchable: false,
                orderable: false,
                targets: 0
            }],

            order: [[1, 'asc']]
        });

        table
        .on('order.dt search.dt', function () {
            let i = 1;

            table
                .cells(null, 0, { search: 'applied', order: 'applied' })
                .every(function (cell) {
                    this.data(i++);
                });
        })
        .draw();

        $("#myInput").on("keyup", function() {
            table.search( this.value ).draw();
        });
    });
</script>
