{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - User
@endsection
@section('title')
<h1>Daftar Akun</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item active">Daftar Akun</li>
        </ol>
      </nav>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">

@section('content')
<div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Daftar Pengguna</h3>
        </div>
        <div class="col-12 col-sm-8 col-md-6">
            <form action="">
                <div class="input-group mb-3 ">
                    <input type="text" class="form-control" placeholder="Search Data" name="keyword" id="myInput">
                    {{-- <button class="btn btn-outline-secondary" type="submit" id="button-addon1">Search</button> --}}
                </div>
            </form>
        </div>
        {{-- <a href="pilih-user"><button type="button" class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
        <a href="delete-user"><button type="button" class="btn btn-secondary mx-3 align-self-end">Hapus</button></a> --}}
    </div>
    <hr>
    <div class="table-responsive">
        {{-- {{ dd($user) }} --}}
        <table class="table" id="mainTable">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Username</th>
                    <th scope="col">Password</th>
                    <th scope="col">Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($user as $a)

                <tr>
                    <td id="number">{{ $loop->iteration }}</td>
                    <td><a href="detail-user/{{ $a -> id }}"
                            class=""><button class="btn">
                            {{ $a -> name}}</button></a></td>
                    <td>{{ $a -> email}}</td>
                    <td>{{ $a -> pass}}</td>
                    @if (is_null($a -> role -> name))
                    <td> Tidak Tersedia </td>
                    @else

                    <td> {{ $a -> role -> name }} </td>
                    @endif

                </tr>

                @endforeach
            </tbody>
        </table>
        {{-- <div class="container">
            {{ $user->withQueryString()->links() }}
        </div> --}}
    </div>
</div>
</div>
@endsection

<script>
    $(document).ready(function(){
        var table = $("#mainTable").DataTable({
            "pageLength": 15,
            // "searching": false,
            "lengthChange": false,
            "pagingType": "simple_numbers",

            columnDefs: [
            {
                searchable: false,
                orderable: false,
                targets: 0
            }],

            order: [[4, 'desc']]
        });

        // table
        // .order( [ 5, 'desc' ] )
        // .draw();

        table
        .on('order.dt search.dt', function () {
            let i = 1;

            table
                .cells(null, 0, { search: 'applied', order: 'applied' })
                .every(function (cell) {
                    this.data(i++);
                });
        })
        .draw();

        $("#myInput").on("keyup", function() {
            table.search( this.value ).draw();
        });
    });
</script>

