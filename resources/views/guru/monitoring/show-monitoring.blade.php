@extends('layout.guru')
@section('title')
<div class="d-flex flex-row">
    <div class="me-auto p-2">
        <h1>Riwayat Monitoring</h1>
        <nav>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
            <li class="breadcrumb-item active">Riwayat Kegiatan Monitoring</li>
            </ol>
        </nav>
    </div>
    <div class="col-sm-12 col-md-5 mb-md-0 me-2">
        <form action="">
            {{-- @csrf --}}
            <select class="form-control custom-select select2 ng-pristine ng-valid ng-not-empty ng-touched" ng-model="" name="tahun" id="" ng-change="">
                @foreach ($tahun as $item)
                <option value="{{ $item -> id }}" class="ng-binding ng-scope" {{ $item -> id == $tahun_pelajaran -> id ? 'selected' : '' }}>Tahun Pelajaran {{ $item -> tahun }} - ({{ $item -> status }})</option>
                @endforeach
            </select>
    </div>
        <div>
            <input type="submit" value="Terapkan" class="btn border border-info" >
        </div>
        </form>
</div>
@endsection

@section('content')

@if (!is_null($monitoring))

<div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
        <h1>Riwayat Kegiatan Monitoring</h1>
        <hr>
        @foreach ($monitoring as $r)


    <div class="accordion" id="accordionExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="heading{{ $loop -> iteration }}">
                <button class="accordion-button collapsed" style="height: 50px" type="button" data-bs-toggle="collapse"
                    data-bs-target="#collapse{{ $loop -> iteration }}" aria-expanded="false"
                    aria-controls="collapse{{ $loop -> iteration }}">
                     <h4>Tanggal : {{ $r -> tanggal }}</h4>
                </button>
            </h2>
            <div id="collapse{{ $loop -> iteration }}" class="accordion-collapse collapse"
                aria-labelledby="heading{{ $loop -> iteration }}" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <div class="table-responsive">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 20%">Perusahaan</th>
                                    <td>:</td>
                                    <td>{{ $r -> perusahaans  }}</td>
                                </tr>
                                <tr>
                                    <th scope="col" style="width: 20%">Nama</th>
                                    <td>:</td>
                                    <td>{{ $r -> guru  }}</td>
                                </tr>
                            </table>
                            <br>
                            <table class="table">
                                <tr>
                                    <th scope="col" style="width: 20%">Kehadiran Siswa</th>
                                    <td>:</td>
                                    <td>{{ $r -> kehadiran }}</td>
                                </tr>
                                <tr>
                                    <th scope="col" style="width: 20%">Sikap Siswa</th>
                                    <td>:</td>
                                    <td>{{ $r -> sikap }}</td>
                                </tr>
                                <tr>
                                    <th scope="col" style="width: 20%">Kompetensi Siswa</th>
                                    <td>:</td>
                                    <td>{{ $r -> kompetensi }}</td>
                                </tr>
                                <tr>
                                    <th scope="col" style="width: 20%">Foto</th>
                                    <td>:</td>
                                    <td><img src="{{ asset('uploads/monitoring/' . $r->photo) }}" style="width: 140px; height: 100px; border: 0px solid black; border-radius:10px" alt=""></td>
                                </tr>


                            </thead>
                        </table>
                        <form action="/guru/download-monitoring/{{ $r->id }}" method="post">
                            @csrf
                            <input type="text" name="id_user" value="{{ Auth::user() -> id }}" hidden>
                            <input type="text" name="id_guru" value="{{ $r -> id_guru }}" hidden>
                            <input type="text" name="id_perusahaan" value="{{ $r -> id_perusahaan }}" hidden>
                            <input type="text" name="tanggal" value="{{ $r -> tanggal }}" hidden>
                            <button class="btn btn-danger" role="button" style="margin-left:87%">Download PDF</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>


    @endforeach

</div>
@else

<div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
        <h1>Riwayat Kegiatan Monitoring</h1>
        <hr>
    </div>
</div>
@endif
</div>
</div>

@endsection
