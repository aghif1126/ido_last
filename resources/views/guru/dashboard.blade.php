{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
@section('title')
<style>
        .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 100px;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        position: relative;
        background-color: #fefefe;
        margin: auto;
        padding-left: 0.1%;
        padding-right: 0.1%;
        border: 1px solid #888;
        width: 40%;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        -webkit-animation-name: animatetop;
        -webkit-animation-duration: 0.4s;
        animation-name: animatetop;
        animation-duration: 0.4s
    }

    @media (max-width: 600px) {
        .modal-content {
            width: 90%;
        }

        #my_camera video {
            max-width: 75%;
            max-height: 75%;
            margin-left: 29%;
            margin-right: -20%;
        }

        #results img {
            max-width: 80%;
            max-height: 80%;
        }

        #my_camera1 video {
            max-width: 75%;
            max-height: 75%;
            margin-left: 29%;
            margin-right: -20%;
        }

        #results1 img {
            max-width: 80%;
            max-height: 80%;
        }
    }

    /* Add Animation */
    @-webkit-keyframes animatetop {
        from {
            top: -300px;
            opacity: 0
        }

        to {
            top: 0;
            opacity: 1
        }
    }

    @keyframes animatetop {
        from {
            top: -300px;
            opacity: 0
        }

        to {
            top: 0;
            opacity: 1
        }
    }

    /* The Close Button */
    .close {
        color: grey;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    .modal-header {
        padding: 2px 16px;
        /* background-color: red; */
        color: black;
    }

    .modal-body {
        padding: 2px 16px;
    }

    .modal-footer {
        padding: 2px 16px;
        background-color: #5cb85c;
        color: white;
    }
</style>
<div class="d-flex">
    <div class="me-auto p-2"><h1>Dashboard</h1></div>
    <div class="col-sm-12 col-md-5 mb-md-0 me-2">
        <form action="">
            {{-- @csrf --}}
            <select class="form-control custom-select select2 ng-pristine ng-valid ng-not-empty ng-touched" ng-model="" name="tahun" id="" ng-change="">
                @foreach ($tahun as $item)
                <option value="{{ $item -> id }}" class="ng-binding ng-scope" {{ $item -> id == $tahun_pelajaran -> id ? 'selected' : '' }}>Tahun Pelajaran {{ $item -> tahun }} - ({{ $item -> status }})</option>
                @endforeach
            </select>
        </div>
            <div>
                <input type="submit" value="Terapkan" class="btn border border-info" >
            </div>
        </form>
        <form action="backup-db" method="POST">
            @csrf
            <div>
                <input type="submit" value="Backup" class="btn border border-info" >
            </div>
        </form>
        <button type="button" class="btn border border-info" name="status" id="myBtn" style="height: 38px">Restore</button>
        <!-- The Modal -->
 <div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            {{-- <h2>Modal Header</h2> --}}
        </div>
        <div class="modal-body mb-3">
            <form method="POST" action="restore-db" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12 text-center">
                        <input class="form-control" type="file" name="file" id="formFile">
                        <br>
                        <button class="btn btn-success" type="submit">Restore Data</button>
                    </div>
                </div>
            </form>
        </div>
        {{-- <div class="modal-footer">
            <h3>Modal Footer</h3> --}}
        </div>
    </div>
</div>
@endsection

@section('header')
<div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">

      <div class="card-body">
<h2>Welcome Guru</h2>
<hr>
<h3>Hello {{ Auth::user()->name }} anda adalah {{ Auth::user()->role->name }}</h3>
<br>
<br>
      </div>
    </div>
</div>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">

@section('content')


{{-- grafik siswa --}}
<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <a href="/guru/pemetaan-siswa">
                <h5 class="card-title" align="center">Pemetaan</h5>
            </a>
            <canvas id="tablepemetaan" style="max-height: 400px;"></canvas>
            <script>
                const ctx4 = document.getElementById('tablepemetaan');

                new Chart(ctx4, {
                type: 'bar',
                data: {
                    labels: ['Belum Dipetakan', 'Belum dikonfirmasi', 'Sudah dikonfirmasi',
                    ],
                    datasets: [{
                    label: 'Siswa',
                    data: [
                        "{{$belum}}", "{{$tunggu}}", "{{$confirm}}"
                    ],
                    backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 205, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(201, 203, 207, 0.2)'
                    ],
                    borderColor: [
                    'rgb(255, 99, 132)',
                    'rgb(255, 159, 64)',
                    'rgb(255, 205, 86)',
                    'rgb(75, 192, 192)',
                    'rgb(54, 162, 235)',
                    'rgb(153, 102, 255)',
                    'rgb(201, 203, 207)'
                    ],
                    borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                    y: {
                        beginAtZero: true,
                        // min: 0,
                        // max: 800,
                        // ticks: {
                        //     // forces step size to be 50 units
                        //     stepSize: 160
                        // }
                    }
                    }
                }
                });
            </script>
        </div>
    </div>
</div>

{{-- grafik guru --}}
<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <a href="/guru/show-guru">
                <h5 class="card-title" align="center">Pembimbing Sekolah</h5>
            </a>
            <canvas id="tablepembimbing" style="max-height: 400px;"></canvas>
            <script>
                const ctx2 = document.getElementById('tablepembimbing');

                new Chart(ctx2, {
                type: 'bar',
                data: {
                    labels: ['Sudah Dipetakan', 'Belum Dipetakan',
                    ],
                    datasets: [{
                    label: 'Pembimbing Sekolah',
                    data: [
                        "{{$atos}}", "{{$acan}}"
                    ],
                    backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 205, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(201, 203, 207, 0.2)'
                    ],
                    borderColor: [
                    'rgb(255, 99, 132)',
                    'rgb(255, 159, 64)',
                    'rgb(255, 205, 86)',
                    'rgb(75, 192, 192)',
                    'rgb(54, 162, 235)',
                    'rgb(153, 102, 255)',
                    'rgb(201, 203, 207)'
                    ],
                    borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                    y: {
                        beginAtZero: true,
                        // min: 0,
                        // max: 800,
                        // ticks: {
                        //     // forces step size to be 50 units
                        //     stepSize: 160
                        // }
                    }
                    }
                }
                });
            </script>
        </div>
    </div>
</div>
{{-- <div class="col">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
        <div>p</div>
    </div>
</div> --}}
<div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
      <div class="card-body">
        <div>
            <h3>Log</h3>
            <div class="col-12 col-sm-8 col-md-4">
                <form action="">
                    <div class="input-group mb-3 ">
                        <input type="text" class="form-control" placeholder="Search Data" name="keyword" id="myInput">
                        {{-- <button class="btn btn-outline-secondary" type="submit" id="button-addon1">Search</button> --}}
                    </div>
                </form>
            </div>
        </div>
        <hr>
        <div>
            <table class="table" id="mainTable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Event</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($log as $logs)
                    <tr>
                        <td scope="row">{{ $loop->iteration }}</td>
                        <td>
                            @if ($logs->description == "created")
                            {{-- {{ $logs->properties['attributes']['nama'] ? $logs->properties['attributes']['nama'] : ''}} Telah ditambahkan[{{ $logs->ip }}] --}}
                            {{ $logs->log_name }} Telah ditambahkan[{{ $logs->ip }}]

                            @elseif ($logs->description == "updated")
                            {{-- {{ $logs->properties['attributes']['nama'] ? $logs->properties['attributes']['nama'] : ''}} Telah diperbarui[{{ $logs->ip }}] --}}
                            {{ $logs->log_name }} Telah diperbarui[{{ $logs->ip }}]

                            @else
                            {{-- {{ $logs->properties['attributes']['nama'] ? $logs->properties['attributes']['nama'] : ''}} Telah dihapus[{{ $logs->ip }}] --}}
                            Telah dihapus[{{ $logs->ip }}]
                            @endif
                            {{-- {{ dd($logs->properties['attributes']) }} --}}
                        </td>
                        <td>{{ $logs->created_at }}</td>
                        <td>{{ $logs->updated_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>

<script>
    $(document).ready(function(){
    var table = $("#mainTable").DataTable({
        "pageLength": 15,
        // "searching": false,
        "lengthChange": false,
        "pagingType": "simple_numbers",

        columnDefs: [
        {
            searchable: false,
            orderable: false,
            targets: 0
        }],

        order: [[2, 'desc']]
    });

    table
    .on('order.dt search.dt', function () {
        let i = 1;

        table
            .cells(null, 0, { search: 'applied', order: 'applied' })
            .every(function (cell) {
                this.data(i++);
            });
    })
    .draw();

    $("#myInput").on("keyup", function() {
        table.search( this.value ).draw();
    });

    var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");
var ambilpoto = document.getElementById("ambilpoto");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
if (event.target == modal) {
    modal.style.display = "none";
}
}

});
</script>



@endsection
