{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Perusahaan
@endsection
@section('title')
<h1>Hapus Jurusan Perusahaan</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-perusahaan">Daftar Perusahaan</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-perusahaan/{{$perusahaan -> id}}">Detail Perusahaan</a></li>
          {{-- <li class="breadcrumb-item"><a href="/guru/edit-perusahaan/{{$perusahaan -> id}}">Edit Perusahaan</a></li> --}}
          <li class="breadcrumb-item active">Edit Perusahaan</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Pilih Jurusan yang akan dihapus</h3></div>

    </div>
<hr>
<form action="/guru/edit-perusahaan/destroy-jurusan" method="POST">
    @csrf
    <input type="text" name="id_perusahaan" value="{{ $perusahaan -> id }}" hidden>
    <div class="table-responsive">
        <table class="table" >

            <tbody>
                @foreach ($perusahaan -> jurusan as $jurusan)
                <tr>
                    <td><button type="submit" name="id_jurusan" value="{{ $jurusan -> detailjurusan -> id }}" onClick="return confirm(`Yakin ingin menghapus {{ $jurusan -> detailjurusan -> nama }}?`)"> {{ $jurusan -> detailjurusan -> nama }}</button></td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </form>
    </div>
</div>

@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
