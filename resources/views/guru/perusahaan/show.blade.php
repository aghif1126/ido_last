{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
- Perusahaan
@endsection
@section('title')
<div class="d-flex flex-row">
    <div class="me-auto p-2">
        <h1>Daftar Perusahaan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></td>
                <li class="breadcrumb-item active">Daftar Perusahaan</li>
            </ol>
        </nav>
    </div>
    <div class="col-sm-12 col-md-5 mb-md-0 me-2">
        <form action="">
            {{-- @csrf --}}
            <select class="form-control custom-select select2 ng-pristine ng-valid ng-not-empty ng-touched" ng-model=""
                name="tahun" id="" ng-change="">
                @foreach ($tahun as $item)
                <option value="{{ $item -> id }}" class="ng-binding ng-scope" {{ $item -> id == $tahun_pelajaran -> id ?
                    'selected' : '' }}>Tahun Pelajaran {{ $item -> tahun }} - ({{ $item -> status }})</option>
                @endforeach
            </select>
    </div>
    <div>
        <input type="submit" value="Terapkan" class="btn border border-info">
    </div>
    </form>
</div>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
    integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js"
    integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">

@section('content')
<style>
    .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 100px;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        position: relative;
        background-color: #fefefe;
        margin: auto;
        padding-left: 0.1%;
        padding-right: 0.1%;
        border: 1px solid #888;
        width: 40%;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        -webkit-animation-name: animatetop;
        -webkit-animation-duration: 0.4s;
        animation-name: animatetop;
        animation-duration: 0.4s
    }

    @media (max-width: 600px) {
        .modal-content {
            width: 90%;
        }

        #my_camera video {
            max-width: 75%;
            max-height: 75%;
            margin-left: 29%;
            margin-right: -20%;
        }

        #results img {
            max-width: 80%;
            max-height: 80%;
        }

        #my_camera1 video {
            max-width: 75%;
            max-height: 75%;
            margin-left: 29%;
            margin-right: -20%;
        }

        #results1 img {
            max-width: 80%;
            max-height: 80%;
        }
    }

    /* Add Animation */
    @-webkit-keyframes animatetop {
        from {
            top: -300px;
            opacity: 0
        }

        to {
            top: 0;
            opacity: 1
        }
    }

    @keyframes animatetop {
        from {
            top: -300px;
            opacity: 0
        }

        to {
            top: 0;
            opacity: 1
        }
    }

    /* The Close Button */
    .close {
        color: grey;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    .modal-header {
        padding: 2px 16px;
        /* background-color: red; */
        color: black;
    }

    .modal-body {
        padding: 2px 16px;
    }

    .modal-footer {
        padding: 2px 16px;
        background-color: #5cb85c;
        color: white;
    }
</style>

<div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
        <div class="d-flex flex-row">
            <div class="me-auto p-2">
                <h3>Daftar Perusahaan</h3>
            </div>
            <div class="col-12 col-sm-8 col-md-4">
                <form action="">
                    <div class="input-group mb-3 ">
                        <input type="serach" class="form-control" placeholder="Search Data" name="keyword" id="myInput">
                        {{-- <button class="btn btn-outline-secondary" type="submit" id="button-addon1">Search</button>
                        --}}
                    </div>
                </form>
            </div>
            <a href="add-perusahaan"><button type="button"
                    class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
            <a href="delete-perusahaan"><button type="button"
                    class="btn btn-secondary mx-3 align-self-end">Hapus</button></a>
            <button type="button" class="btn btn-secondary mx-3 align-self-end" name="status" id="myBtn"
                style="width: 200px">Import</button>


        </div>

        <hr>
 <!-- The Modal -->
 <div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            {{-- <h2>Modal Header</h2> --}}
        </div>
        <div class="modal-body mb-3">
            <form method="POST" action="import-perusahaan" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-3">


                    </div>
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-12 text-center">
                        <a  class="btn btn-secondary form-control" href="/guru/template-perusahaan" >Download Format</a> <br> <br>
                        <input class="form-control" type="file" name="file" id="formFile">
                        <button id="submitpoto" class="btn btn-success">Import Perusahaan</button>
                        <br>
                        <select name="id_jurusan" class="form-select mb-3" aria-label="Default select example">
                            <option selecte hidden >Pilih Jurusan</option>
                            @foreach ($jurusan as $a)

                            <option value="{{ $a -> id }}">{{ $a -> nama }}</option>
                            @endforeach

                          </select>
                        <button id="submitpoto" class="btn btn-success">Import Perusahaan</button>
                    </div>
                </div>
            </form>
        </div>
        {{-- <div class="modal-footer">
            <h3>Modal Footer</h3> --}}
        </div>
    </div>


<script type="text/javascript">

var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");
var ambilpoto = document.getElementById("ambilpoto");
document.getElementById("submitpoto").style.display = "none";

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
if (event.target == modal) {
    modal.style.display = "none";
}
}
</script>
        <div class="table-responsive">
            <table class="table" id="mainTable">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Kuota</th>
                        {{-- <th scope="col">Fax</th> --}}
                        {{-- <th scope="col">Telp</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($perusahaan as $a)
                    @foreach ($a -> jurusan as $jurusan)
                    <tr>
                        <td scope="row" id="number">{{ $loop->iteration }}</td>
                        <td><a href="detail-perusahaan/{{ $a -> id }}"
                                class="link-offset-2 link-offset-3-hover link-underline link-underline-opacity-0 link-underline-opacity-75-hover"><button
                                    class="btn">{{ $a -> nama}} ({{ $jurusan -> detailjurusan ?  $jurusan -> detailjurusan -> nama : ''}})</button></a></td>
                        <td>{{ $a -> alamat}}</td>
                        {{-- @dd($a -> jurusan) --}}
                        <td>{{ $jurusan -> kuota}}</td>
                        {{-- <td>{{ $a -> fax}}</td> --}}



                    </tr>
                    @endforeach
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>

@endsection

<script>
    $(document).ready(function(){
        var table = $("#mainTable").DataTable({
            "pageLength": 15,
            // "searching": false,
            "lengthChange": false,
            "pagingType": "simple_numbers",

            columnDefs: [
            {
                searchable: false,
                orderable: false,
                targets: 0
            }],

            order: [[1, 'asc']]
        });

        table
        .on('order.dt search.dt', function () {
            let i = 1;

            table
                .cells(null, 0, { search: 'applied', order: 'applied' })
                .every(function (cell) {
                    this.data(i++);
                });
        })
        .draw();

        $("#myInput").on("keyup", function() {
            table.search( this.value ).draw();
        });
    });
</script>
