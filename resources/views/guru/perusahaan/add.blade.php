{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Perusahaan
@endsection
@section('title')
<h1>Tambah Perusahaan</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-perusahaan">Daftar Perusahaan</a></li>
          <li class="breadcrumb-item active">Tambah Perusahaan</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
<form action="store-perusahaan" method="post">
    @csrf

    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td><input type="text" class="form-control" name="nama" placeholder="Nama" required></td>
                </tr>
                <tr>
                    <th scope="col">Status MOU</th>
                    <td>:</td>
                    <td>
                        <select name="mou" class="form-select" aria-label=".form-select" required>
                            <option value="ya">Ya</option>
                            <option value="tidak" selected>Tidak</option>
                        </select>
                    </td>
                </tr>
                <tr>
                <tr>
                    <th scope="col">Alamat</th>
                    <td>:</td>
                    <td><input type="text" class="form-control" name="alamat" placeholder="Alamat" required></td>
                </tr>
                <tr>
                    <th scope="col">Lokasi</th>
                    <td>:</td>
                    <td><input type="text" class="form-control" name="lokasi" placeholder="Masukan Link Maps Perusahaan"></td>
                </tr>
                <tr>
                    <th scope="col">Email</th>
                    <td>:</td>
                    <td>
                        <input type="email" class="form-control" name="email" placeholder="Email" required>
                    </td>
                </tr>
                <tr>
                    <th scope="col">Fax</th>
                    <td>:</td>
                    <td>
                        <input type="number" class="form-control" name="fax" placeholder="Fax" required>
                    </td>
                </tr>
                <tr>
                    <th scope="col">No. Telp</th>
                    <td>:</td>
                    <td><input type="number" class="form-control" name="telp" placeholder="No. Telp" required></td>
                </tr>
                <tr>
                    <th scope="col">Website</th>
                    <td>:</td>
                    <td>
                        <input type="text" class="form-control" name="website" placeholder="Website" required>
                    </td>
                </tr>
                <tr>
                    <th scope="col">Kuota</th>
                    <td>:</td>
                    <td>
                        <input type="number" class="form-control" name="kuota" placeholder="Kuota" required>
                    </td>
                </tr>
                <tr>
                    <th scope="col">Jurusan</th>
                    <td>:</td>
                    <td>
                        <select class="form-select" name="id_jurusan1" aria-label=".form-select-lg example" required>
                            <option selected hidden>Jurusan</option>
                            @foreach ($jurusan as $g )
                            <option value="{{ $g->id }}">{{ $g->nama }}</option>

                            @endforeach
                        </select>
                    </td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="d-flex justify-content-end">
        <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin menambahkan perusahaan?`)">Submit</button></div>
</form>
</div>
</div>

@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>

