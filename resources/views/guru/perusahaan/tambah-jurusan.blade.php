{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>Tambah Jurusan Perusahaan</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-perusahaan">Daftar Perusahaan</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-perusahaan/{{$perusahaan -> id}}">Detail Perusahaan</a></li>
          {{-- <li class="breadcrumb-item"><a href="/guru/edit-perusahaan/{{$perusahaan -> id}}">Edit Perusahaan</a></li> --}}
          <li class="breadcrumb-item active">Edit Perusahaan</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Pilih Jurusan</h3></div>

    </div>
<hr>
<form action="/guru/edit-perusahaan/store-jurusan" method="POST">
    @csrf
    <div class="table-responsive">
        <table class="table">
            <thead>

                <tr>
                    <th scope="col">Pilih Jurusan</th>
                    <td>:</td>
                    <td >
                    <input type="text" name="id_perusahaan" value="{{ $perusahaan -> id }}" class="m" hidden>
                       <select class="form-control" name="id_jurusan">
                        <option selected hidden>Pilih</option>
                        @foreach ($jurusan as $jurusan)
                        <option  value="{{ $jurusan -> id }}">{{ $jurusan -> nama }}</option>
                        @endforeach
                    </td>


                </tr>
                <tr>
                    <th scope="col">Kuota yang di sediakan</th>
                    <td>:</td>
                    <td>
                   <input type="number" name="kuota" class="form-control" required>
                    </td>


                </tr>

            </thead>
            <tbody>

            </tbody>
        </table>
        <div class="d-flex justify-content-end">
            <button type="submit" class="btn btn-secondary my-3 align-self-end" onClick="return confirm(`Yakin ingin menambahkan jurusan?`)">Simpan</button>
        </div>
    </form>
    </div>
</div>

@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
