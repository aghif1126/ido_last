{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Perusahaan
@endsection
@section('title')
<h1>Edit Perusahaan</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-perusahaan">Daftar Perusahaan</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-perusahaan/{{$perusahaan -> id}}">Detail Perusahaan</a></li>
          <li class="breadcrumb-item active">Edit Perusahaan</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Detail Perusahaan </h3></div>
        {{-- <a href="/guru/add-perusahaan"><button type="button" class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
        <a href="/guru/delete-perusahaan"><button type="button" class="btn btn-secondary mx-3 align-self-end">Hapus</button></a> --}}

    </div>
<hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td>    {{ $perusahaan -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Status MOU</th>
                    <td>:</td>
                    <td>{{ $perusahaan -> mou}}</td>
                </tr>
                <tr>
                    <th scope="col">Alamat</th>
                    <td>:</td>
                    <td>{{ $perusahaan -> alamat}}</td>
                </tr>
                <tr>
                    <th scope="col">Lokasi</th>
                    <td>:</td>
                    <td><div style="
                        overflow: hidden;
                       -webkit-box-orient: vertical;
                      -webkit-line-clamp: 1;
                      display: -webkit-box;">
                        {{ $perusahaan -> lokasi}}</div></td>
                </tr>
                <tr>
                    <th scope="col">Email</th>
                    <td>:</td>
                    <td>{{ $perusahaan -> email}}</td>
                </tr>
                <tr>
                <th scope="col">Fax</th>
                    <td>:</td>
                    <td>{{ $perusahaan -> fax}}</td>
                </tr>
                <tr>
                    <th scope="col">Telepon</th>
                    <td>:</td>
                    <td>{{ $perusahaan -> telp}}</td>
                </tr>
                <tr>
                    <th scope="col">Website</th>
                    <td>:</td>
                    @if (is_null($perusahaan -> website))
                    <td>Tidak Tersedia</td>

                    @endif
                    <td>{{ $perusahaan -> website}}</td>
                </tr>
                <tr>
                    <th scope="col">Kuota</th>
                    <td>:</td>
                    <td> @foreach ($perusahaan -> jurusan as $jurusan )
                        {{-- @dd($perusahaan -> jurusan[1] -> kuota) --}}
                        {{ $jurusan -> detailjurusan -> nama }} : ({{ $perusahaan -> siswa -> where('id_jurusan',
                        $jurusan -> id_jurusan) -> count() + $jurusan -> kuota }} orang) Sisa : ( {{
                        $jurusan -> kuota}} orang ) <br>
                        @endforeach</td>
                </tr>
                <tr>
                    <th scope="col">Jurusan</th>
                    <td>:</td>
                    <td>
                        @foreach ($perusahaan -> jurusan as $jurusan)
                        {{ $jurusan -> detailjurusan -> nama }} <br>
                        @endforeach
                </td>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Edit Perusahaan </h3></div>

    </div>
<hr>
<form action="/guru/update-perusahaan" method="post">
    @csrf
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <input type="text" class="form-control" name="id" placeholder="Website" value="{{ $perusahaan->id }}" hidden>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td colspan="3"><input type="text" class="form-control" name="name" placeholder="Nama" value="{{  $perusahaan -> nama }}" required></td>
                </tr>
                <tr>
                    <th scope="col">Staus MOU</th>
                    <td>:</td>
                    <td colspan="3">
                        <select name="mou" class="form-select form-select" aria-label=".form-select" required>
                            <option selected hidden>{{ $perusahaan -> mou }}</option>
                            <option value="ya">Ya</option>
                            <option value="tidak">Tidak</option>
                    </select>
                    </td>
                </tr>
                <tr>
                    <th scope="col">Alamat</th>
                    <td>:</td>
                    <td colspan="3"><input type="text" class="form-control" name="alamat" placeholder="Website" value="{{ $perusahaan -> alamat}}" required></td>
                </tr>
                <tr>
                    <th scope="col">Lokasi</th>
                    <td>:</td>
                    <td colspan="3"><input type="text" class="form-control" name="lokasi" placeholder="Website" value="{{  $perusahaan -> lokasi }}" required></td>
                </tr>
                <tr>
                    <th scope="col">Email</th>
                    <td>:</td>
                    <td colspan="3"><input type="text" class="form-control" name="email" placeholder="Website" value="{{ $perusahaan -> email}}" required></td>
                </tr>
                <tr>
                <th scope="col">Fax</th>
                    <td>:</td>
                    <td colspan="3"><input type="text" class="form-control" name="fax" placeholder="Website" value="{{ $perusahaan -> fax}}" required></td>
                </tr>
                <tr>
                    <th scope="col">Telepon</th>
                    <td>:</td>
                    <td colspan="3"><input type="text" class="form-control" name="telp" placeholder="Website" value="{{ $perusahaan -> telp}}" required></td>
                </tr>
                <tr>
                    <th scope="col">Website</th>
                    <td>:</td>
                    @if (is_null($perusahaan -> website))
                    <td colspan="3"><input type="text" class="form-control" name="website" placeholder="Website" value="Tidak Tersedia"></td>
                    @else
                    <td colspan="3"><input type="text" class="form-control" name="website" placeholder="Website" value="{{ $perusahaan -> website}}" required></td>

                    @endif
                </tr>
                <tr>
                    <th scope="col">Kuota</th>
                    <td>:</td>

                    <td>
                        @foreach ($perusahaan -> jurusan as $jurusan )
                        {{-- @dd($perusahaan -> jurusan[1] -> kuota) --}}
                        {{ $jurusan -> detailjurusan -> nama }} : ({{ $perusahaan -> siswa -> where('id_jurusan',
                        $jurusan -> id_jurusan) -> count() + $jurusan -> kuota }} orang) Sisa : ( {{
                        $jurusan -> kuota}} orang ) <br>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th scope="col">Jurusan</th>
                    <td>:</td>
                    <td>
                        @foreach ($perusahaan -> jurusan as $jurusan)
                        {{ $jurusan -> detailjurusan -> nama }} <br>
                        @endforeach
                    </td>
                </tr>
                <td> <a href="{{$id}}/add-jurusan" ><button type="button" class="btn btn-secondary mx-3 align-self-end">Tambah</button></a></td>
                <td><a href="{{ $id }}/delete-jurusan"><button type="button" class="btn btn-secondary mx-3 align-self-end">Hapus</button></a></td>

                </tr>
            </thead>
        </table>
        <div class="d-flex justify-content-end">
            <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin mengedit {{ $perusahaan -> nama }}?`)">Simpan</button></div>
    </form>
    </div>
</div>
</div>

@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
