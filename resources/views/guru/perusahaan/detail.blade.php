{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
- Perusahaan
@endsection
@section('title')
<h1>Detail Perusahaan</h1>
<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
        <li class="breadcrumb-item"><a href="/guru/show-perusahaan">Daftar Perusahaan</a></li>
        <li class="breadcrumb-item active">Detail Perusahaan</li>
    </ol>
</nav>
@endsection

@section('content')

<div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
        <div class="d-flex flex-row">
            <div class="me-auto p-2">
                <h3>Detail Perusahaan </h3>
            </div>
            <a href="/guru/edit-perusahaan/{{ $id }}"><button type="button"
                    class="btn btn-secondary mx-3 align-self-end">Edit</button></a>

        </div>
        <hr>
        <div class="table-responsive">
            <table class="table ">
                <thead>
                    <tr>
                        <th scope="col">Nama</th>
                        <td>:</td>
                        <td>{{ $perusahaan -> nama}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Status MOU</th>
                        <td>:</td>
                        <td>{{ $perusahaan -> mou}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Alamat</th>
                        <td>:</td>
                        <td>{{ $perusahaan -> alamat}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Lokasi</th>
                        <td>:</td>
                        <td>
                            <div style="
                            overflow: hidden;
                           -webkit-box-orient: vertical;
                          -webkit-line-clamp: 1;
                          display: -webkit-box;">
                                {{ $perusahaan -> lokasi}}</div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">Email</th>
                        <td>:</td>
                        <td>{{ $perusahaan -> email}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Fax</th>
                        <td>:</td>
                        <td>{{ $perusahaan -> fax}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Telepon</th>
                        <td>:</td>
                        <td>{{ $perusahaan -> telp}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Website</th>
                        <td>:</td>
                        @if (is_null($perusahaan -> website))
                        <td>Tidak Tersedia</td>

                        @endif
                        <td>{{ $perusahaan -> website}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Kuota</th>
                        <td>:</td>

                        <td>
                            @foreach ($perusahaan -> jurusan as $jurusan )
                            {{ $jurusan -> detailjurusan -> nama }} : ({{ $perusahaan -> siswa -> where('id_jurusan',
                            $jurusan -> id_jurusan) -> count() + $jurusan -> kuota }} orang) Sisa : ( {{
                            $jurusan -> kuota}} orang ) <br>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">Jurusan</th>
                        <td>:</td>
                        <td>
                            @foreach ($perusahaan -> jurusan as $jurusan)
                            {{ $jurusan -> detailjurusan -> nama }} <br>
                            @endforeach
                        </td>
                    </tr>
                    @if (!is_null($perusahaan -> pembimbing))
                    @foreach ($perusahaan -> pembimbing as $pembimbing)
                    <tr>
                        {{-- @dd( ) --}}
                        <th scope="col">Pembimbing </th>
                        <td>:</td>
                        <td>
                            <a href="/guru/detail-pembimbing/{{ $pembimbing -> id }}"> {{ $pembimbing -> nama }} ({{
                                $pembimbing -> jurusan -> nama }})</a>


                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <th scope="col">Pembimbing </th>
                        <td>:</td>
                        <td>

                            Tidak Tersedia


                        </td>
                    </tr>
                    @endif
                    <tr>
                        <th scope="col">Jumlah Siswa</th>
                        <td>:</td>
                        <td>{{ $total}}</td>
                    </tr>



                </thead>
                <tbody>

                </tbody>
            </table>
        </div>


    </div>
</div>



@foreach ($perusahaan -> jurusan as $jurusan)
<div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
        <div class="d-flex flex-row">
            <div class="me-auto p-2">
                <h3>{{ $jurusan -> detailjurusan -> nama }}</h3>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table ">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Jenkel</th>
                        <th scope="col">Confirm</th>
                        <th scope="col">Action</th>
                        {{-- <th scope="col">Semester</th> --}}
                        {{-- <th scope="col">Terpetakan</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($siswa -> where('id_jurusan', $jurusan -> id_jurusan) as $a)
                    <tr>
                        <td>{{ $loop -> iteration }}</td>
                        <td style="width: 20%"><a href="/guru/detail-siswa/{{ $a -> detailsiswa ->  nis }}"
                                class="link-dark link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover">
                                {{ $a -> detailsiswa -> nama}}</a></td>
                        <td>{{ $a -> detailsiswa -> kelas -> nama}}</td>
                        <td>{{ $a -> detailsiswa -> jenkel}}</td>
                        @if ($a -> confirm == "Belum")
                        <td>Menunggu</td>
                        <td style="width: 30%">
                            <div class="d-flex">
                                <form action="/guru/pemetaan-siswa/pilih-perusahaan/konfirmasi" method="post">
                                    @csrf
                                    <input type="text" name="id_perusahaan" value="{{ $perusahaan -> id }}" hidden>
                                    <button name='id_siswa' value="{{ $a -> detailsiswa ->  nis }}" type="confirm"
                                        class="mx-3 btn btn-success"
                                        onClick="return confirm(`Apakah anda yakin mengkonfirmasi {{ $a -> detailsiswa ->  nama }}?`)">Konfirmasi</button>
                                </form>
                                <br>
                                <form action="/guru/pemetaan-siswa/pilih-perusahaan/batalkan" method="post">
                                    @csrf
                                    <input type="text" name="id_perusahaan" value="{{ $perusahaan -> id }}" hidden>
                                    <button name='id_siswa' value="{{ $a -> detailsiswa ->  nis }}" type="confirm"
                                        class="mx-3 btn btn-danger"
                                        onClick="return confirm(`Apakah anda yakin membatalkan {{ $a -> detailsiswa ->  nama }}?`)">Batalkan</button>
                                </form>
                            </div>
                        </td>
                        @else
                        <td>{{ $a -> where('id_perusahaan', $perusahaan -> id) -> first() -> confirm}}</td>
                        <td>
                            <div class="d-flex">
                                <form action="/guru/pemetaan-siswa/pilih-perusahaan/konfirmasi" method="post">
                                    @csrf
                                    <input type="text" name="id_perusahaan" value="{{ $perusahaan -> id }}" hidden>
                                    <button name='id_siswa' value="{{ $a -> detailsiswa ->  nis }}" type="confirm"
                                        class="mx-3 btn btn-success"
                                        onClick="return confirm(`Apakah anda yakin mengkonfirmasi {{ $a -> detailsiswa ->  nama }}?`)"
                                        disabled>Konfirmasi</button>
                                </form>
                                <br>
                                <form action="/guru/pemetaan-siswa/pilih-perusahaan/batalkan" method="post">
                                    @csrf
                                    <input type="text" name="id_perusahaan" value="{{ $perusahaan -> id }}" hidden>
                                    <button name='id_siswa' value="{{ $a -> detailsiswa ->  nis }}" type="confirm"
                                        class="mx-3 btn btn-danger"
                                        onClick="return confirm(`Apakah anda yakin membatalkan {{ $a -> detailsiswa ->  nama }}?`)">Batalkan</button>
                                </form>
                            </div>
                        </td>

                        @endif
                        {{-- <td>{{ $a -> detailsiswa -> semester}}</td> --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endforeach




@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
