{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Kelas
@endsection
@section('title')
<h1>Detail Kelas</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/pemetaan-siswa">pemetaan</a></li>
          <li class="breadcrumb-item active">Detail Kelas</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Detail Kelas </h3>
        </div>
        <a href="/guru/edit-kelas/{{ $id }}"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Edit</button></a>

    </div>
    <hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td>{{ $kelas -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Konsentrasi</th>
                    <td>:</td>
                    <td>{{ $kelas -> konsentrasi -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Jurusan</th>
                    <td>:</td>
                    <td>{{ $kelas -> konsentrasi -> jurusan -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Jumlah Siswa</th>
                    <td>:</td>
                    <td>{{ $siswa -> count()}} -> <a href="{{ $id }}/siswa"> Lihat </a></td>
                </tr>
                <tr>
                    <th scope="col">Terpetakan</th>
                    <td>:</td>
                    <td>{{ $sudah}}</td>
                </tr>
                <tr>
                    <th scope="col">Belum Terpetakan</th>
                    <td>:</td>
                    <td>{{ $belum}} </td>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

@endsection
