{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Kelas
@endsection
@section('title')
<h1>Edit Kelas</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-kelas">Daftar Kelas</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-kelas/{{$kelas -> id}}">Detail Kelas</a></li>
          <li class="breadcrumb-item active">Edit Kelas</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Detail Kelas {{ $kelas->nama }} </h3>
        </div>
        {{-- <a href="/guru/add-perusahaan"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
        <a href="/guru/delete-perusahaan"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Hapus</button></a> --}}

    </div>
    <hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td> {{ $kelas -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Konsentrasi Keahlian</th>
                    <td>:</td>
                    <td>{{ $kelas -> konsentrasi -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Jurusan</th>
                    <td>:</td>
                    <td>{{ $kelas -> konsentrasi -> jurusan -> nama}}</td>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Edit Kelas </h3>
        </div>
        {{-- <a href="/guru/add-perusahaan"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
        <a href="/guru/delete-perusahaan"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Hapus</button></a> --}}

    </div>
    <hr>
    <form action="/guru/update-kelas" method="post">
        @csrf
        <div class="table-responsive">
            <table class="table ">
                <thead>
                    <input type="text" class="form" name="id" placeholder="Website" value="{{ $kelas->id }}" hidden>
                    <tr>
                        <th scope="col">Nama</th>
                        <td>:</td>
                        <td>
                            <input type="text" class="form-control" name="nama" onkeydown="return /[a-z A-Z0-9 .]/i.test(event.key)" placeholder="Nama"
                                value="{{  $kelas -> nama }}" required>
                            {{-- <input type="text" class="form" name="nama" placeholder="Website"
                                value="{{  $kelas -> nama }}" required> --}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Konsentrasi Keahlian</th>
                        <td>:</td>
                        <td>
                            <select name="id_konsentrasi_keahlian" class="form-select"
                                aria-label=".form-select example" required>
                                <option selected hidden value="{{ $kelas -> konsentrasi -> id }}">
                                    {{ $kelas -> konsentrasi -> nama }}</option>
                                @foreach ($konsentrasi as $k )
                                <option value="{{ $k -> id }}">
                                    {{ $k -> nama }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">Jurusan</th>
                        <td>:</td>
                        <td>
                            <select name="jurusan" class="form-select"
                                aria-label=".form-select example" required>
                                <option>{{ $kelas-> konsentrasi -> jurusan -> nama }}</option>

                            </select>
                        </td>
                    </tr>
                </thead>
            </table><div class="d-flex justify-content-end">
                <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin mengedit {{ $kelas -> nama }}?`)">Simpan</button></div>
    </form>
</div>
</div>

@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
