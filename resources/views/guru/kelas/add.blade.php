{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Kelas
@endsection
@section('title')
<h1>Tambah Kelas</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-kelas">Daftar Kelas</a></li>
          <li class="breadcrumb-item active">Tambah Kelas</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <form action="store-kelas" method="post">
        @csrf

        <div class="mb-3">
            <input type="text" class="form-control" name="nama" onkeydown="return /[a-z A-Z0-9 .]/i.test(event.key)" placeholder="Name" required>
        </div>
        <div class="mb-3">
            <select class="form-select" name="id_konsentrasi_keahlian"
                aria-label=".form-select-lg example" required>
                <option selected hidden>Jurusan</option>
                @foreach ($jurusan as $g )
                <option value="{{ $g->id }}">{{ $g->nama }}</option>
                @endforeach
            </select>
        </div>
        <div class="d-flex justify-content-end">
            <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin menambahkan kelas?`)">Submit</button></div>
    </form>
</div>
</div>

@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
