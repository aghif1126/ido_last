{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<div class="d-flex flex-row">
    <div class="me-auto p-2">
    <h1>Daftar Siswa Di Kelas</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
            <li class="breadcrumb-item"><a href="/guru/pemetaan-siswa">pemetaan</a></li>
            <li class="breadcrumb-item"><a href="/guru/detail-kelas/{{$kelas ? $kelas->id : 'Tidak Tersedia'}}"> Detail Kelas</a></li>
            <li class="breadcrumb-item active">Daftar Siswa di Kelas</li>
        </ol>
    </nav>
    </div>
    <div class="col-sm-12 col-md-5 mb-md-0 me-2">
        <form action="">
            {{-- @csrf --}}
            <select class="form-control custom-select select2 ng-pristine ng-valid ng-not-empty ng-touched" ng-model="" name="tahun" id="" ng-change="">
                @foreach ($tahun as $item)
                <option value="{{ $item -> id }}" class="ng-binding ng-scope" {{ $item -> id == $tahun_pelajaran -> id ? 'selected' : '' }}>Tahun Pelajaran {{ $item -> tahun }} - ({{ $item -> status }})</option>
                @endforeach
            </select>
        </div>
            <div>
                <input type="submit" value="Terapkan" class="btn border border-info" >
            </div>
        </form>
</div>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Daftar Siswa di {{ $kelas ? $kelas->nama : 'Tidak Tersedia'}} </h3>
        </div>
        <div class="col-12 col-sm-8 col-md-5">
            <form action="">
                <div class="input-group mb-3 ">
                    <input type="text" class="form-control" placeholder="Search Data" name="keyword" id="myInput">
                </div>
            </form>
        </div>
        {{-- <a href="/guru/add-kelas"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
        <a href="/guru/delete-kelas"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Hapus</button></a>
        <a href="/guru/edit-kelas/{{ $id }}"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Edit</button></a> --}}
    </div>
    <form action="/guru/pemetaan-siswa/pilih-perusahaan" method="post">
    @csrf
    <div class="d-flex justify-content-end">
        <button type="submit" class="btn btn-secondary" type="submit">Pilih Perusahaan</button>
    </div>
    <hr>
    <div class="table-responsive">
        <table class="table" id="mainTable">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">L/P</th>
                    {{-- <th scope="col">Alamat</th> --}}
                    <th scope="col">Terpetakan</th>
                    <th scope="col">Periode</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($siswa as $a)
                <tr>
                    <td><input style="margin-top: 10px" type="checkbox" name="siswa[]" id="" value="{{ $a -> nis }}"></td>
                    <td><a href="../../detail-siswa/{{ $a -> nis }}"
                            class="">
                            {{ $a -> nama}}</a></td>
                    <td>{{ $a -> jenkel}}</td>

                    <td>
                    @foreach ($a -> perusahaan -> all() as $item)

                    <a href="/guru/detail-perusahaan/{{ $item  -> id_perusahaan }}">{{ $item  -> detail -> nama}}</a>
                        @if ($item  -> confirm == "Belum")
                        ( Menunggu)<br>
                        @else
                        ( Diterima)  <br>
                        @endif
                    @endforeach
                </td>
                <td>
                    @foreach ($a -> perusahaan -> all() as $item)

                    {{ $bulan[intval(explode('-', $item -> awal_periode)[1])]}} ({{ explode('-', $item -> awal_periode)[0] }}) -
                   {{ $bulan[intval(explode('-', $item -> akhir_periode)[1])]}} ({{ explode('-', $item -> akhir_periode)[0] }})     <br>

                    @endforeach
                </td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </form>
        {{-- <div class="container">
            {{ $siswa->withQueryString()->links() }}
        </div> --}}
    </div>
</div>
</div>

@endsection

<script>
    $(document).ready(function(){
        var table = $("#mainTable").DataTable({
            // "pageLength": 15,
            // "searching": false,
            "lengthChange": false,
            "pagingType": "simple_numbers",
            "pagination": false

            columnDefs: [
            {
                searchable: false,
                orderable: false,
                targets: 0
            }],

            order: [[1, 'asc']]
        });

        table
        .on('order.dt search.dt', function () {
            let i = 1;

            table
                .cells(null, 0, { search: 'applied', order: 'applied' })
                .every(function (cell) {
                    this.data(i++);
                });
        })
        .draw();

        $("#myInput").on("keyup", function() {
            table.search( this.value ).draw();
        });
    });
</script>
