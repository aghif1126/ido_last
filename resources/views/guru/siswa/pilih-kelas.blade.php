{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>Tambah Siswa</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-siswa">Daftar Siswa</a></li>
          <li class="breadcrumb-item active">Pilih Kelas</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <form action="import-siswa" method="post" enctype="multipart/form-data" >
        @csrf

        <div class="table-responsive">
            <table class="table ">
                <thead>
                    <tr>
                        <p>Format Import</p>
                        <a name="export" class="btn btn-secondary" href="download-template" role="button">Download Format</a>
                    </tr>
                    <br><br><br>
                    <tr>
                        <input type="file" name="file">
                    </tr>
                    <tr>
                        <th scope="col">Kelas</th>
                        <td>:</td>
                        <td><select name="id_kelas" class="form-select" aria-label=".form-select-lg example" required>
                            <option value="" selected hidden>Pilih Kelas</option>
                            @foreach ($kelas as $k )
                            <option value="{{ $k -> id }}">{{ $k -> nama }}</option>
                            @endforeach</td>
                    </tr>

                </thead>
                <tbody>
                </tbody>
            </table>
    </div>

        <div class="d-flex justify-content-end">
            <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin mengimport siswa?`)">Submit</button></div>
    </form>
</div>
</div>

@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
