{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Siswa
@endsection
@section('title')
<h1>Tambah Siswa</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-siswa">Daftar Siswa</a></li>
          <li class="breadcrumb-item active">Tambah Siswa</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <form action="store-siswa" method="post">
        @csrf

        <div class="table-responsive">
            <table class="table ">
                <thead>
                    <tr>
                        <th scope="col">NIS</th>
                        <td>:</td>
                        <td><input type="number" class="form-control" name="nis" placeholder="NIS" required></td>
                    </tr>
                    <tr>
                        <th scope="col">Nama</th>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="nama" onkeydown="return /[a-z A-Z '.]/i.test(event.key)" placeholder="Nama" required></td>
                    </tr>
                    <tr>
                    <tr>
                        <th scope="col">Kelas</th>
                        <td>:</td>
                        <td><select name="id_kelas" class="form-select" aria-label=".form-select-lg example" required>
                            <option value="" selected hidden>Pilih Kelas</option>
                            @foreach ($kelas as $k )
                            <option value="{{ $k -> id }}">{{ $k -> nama }}</option>
                            @endforeach</td>
                    </tr>
                    <tr>
                        <th scope="col">Jenis Kelamin</th>
                        <td>:</td>
                        <td><select name="jenkel" class="form-select" aria-label=".form-select" required>
                                    <option selected hidden>L/P</option>
                                    <option value="L">L</option>
                                    <option value="P">P</option>
                            </select>
                        </td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
    </div>

        <div class="d-flex justify-content-end">
            <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin menambahkan siswa?`)">Submit</button></div>
        </form>
        <a href="pilih-kelas-siswa"><button class="btn btn-secondary" style="width: 25%;">Import</button></a>
</div>
</div>

@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
