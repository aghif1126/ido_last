{{-- create double stdder registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Siswa
@endsection
@section('title')
<div class="d-flex flex-row">
    <div class="me-auto p-2">
        <h1>Daftar Siswa</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></td>
                <li class="breadcrumb-item active">Daftar Siswa</td>
            </ol>
        </nav>
    </div>
    <div class="col-sm-12 col-md-5 mb-md-0 me-2">
        <form action="">
            {{-- @csrf --}}
            <select class="form-control custom-select select2 ng-pristine ng-valid ng-not-empty ng-touched" ng-model="" name="tahun" id="" ng-change="">
                @foreach ($tahun as $item)
                <option value="{{ $item -> id }}" class="ng-binding ng-scope" {{ $item -> status == "aktif" ? 'selected' : '' }}>Tahun Pelajaran {{ $item -> tahun }} - ({{ $item -> status }})</option>
                @endforeach
            </select>
        </div>
            <div>
                <input type="submit" value="Terapkan" class="btn border border-info" >
            </div>
        </form>
</div>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">

@section('content')

<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Daftar Siswa</h3>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Jumlah Siswa</th>
                    <td>:</td>
                    <td>{{ $jumlah }}</td>
                </tr>
                <tr>
                    <th scope="col">Terpetakan</th>
                    <td>:</td>
                    <td>{{ $sudah}}</td>
                </tr>
                <tr>
                    <th scope="col">Belum Terpetakan</th>
                    <td>:</td>
                    <td>{{ $belum}}</td>
                </tr>
                {{-- <tr>
                    <th scope="col">Siswa yang sudah dikonfirmasi</th>
                    <td>:</td>
                    <td>{{ $confirm}}</td>
                </tr> --}}
                <tr>
                    <th scope="col">Siswa Sedang Menunggu Konfirmasi</th>
                    <td>:</td>
                    <td>{{ $tunggu}}</td>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Daftar Siswa</h3>
        </div>
        <div class="col-12 col-sm-8 col-md-6">
            <div class="input-group mb-3 ">
                <input type="text" class="form-control" id="myInput" placeholder="Search Data" name="keyword">
                {{-- <button class="btn btn-outtdne-secondary" type="submit" id="button-addon1">Search</button> --}}
            </div>
        </div>

        <a href="add-siswa"><button type="button" class="btn btn-secondary mx-3 atdgn-self-end">Tambah</button></a>
        <a href="delete-siswa"><button type="button" class="btn btn-secondary mx-3 atdgn-self-end">Hapus</button></a>

    </div>

    <hr>
    <div class="table-responsive">
        <table class="table" id="mainTable">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">L/P</th>
                    <th scope="col">Kelas</th>
                    <th scope="col">Konsentrasi</th>
                    <th scope="col">Terpetakan</th>
                    {{-- <th scope="col">Status</th> --}}
                </tr>
            </thead>
            <tbody id="myTable">
                @foreach ($siswa as $a)


                <tr>
                    {{-- <td>{{ $siswa -> firstItem() + $loop->index }}</td> --}}
                    <td class="number">{{ $loop->iteration }}</td>
                    <td><a href="detail-siswa/{{ $a -> nis }}" class="nothing"><button class="btn">{{ $a -> nama}}</button></a></td>
                    <td>{{ $a -> jenkel}}</td>
                    @if (is_null($a -> kelas))
                    <td>Tidak tersedia</td>
                    <td>Tidak tersedia</td>
                    @else
                    <td>{{ $a -> kelas -> nama }}</td>
                    <td>{{ $a -> kelas -> konsentrasi -> nama}}</td>
                    @endif

                <td>
                    @if (!is_null($a -> perusahaan))
                    @foreach ($a -> perusahaan as $perusahaan)
                    <a href="/guru/detail-perusahaan/{{ $perusahaan -> id_perusahaan }}" class="nothing">
                        {{ $perusahaan -> detail -> nama}}</a>
                        @if ($perusahaan  -> confirm == "Belum")
                        ( Menunggu)<br>
                        @else
                        ( Diterima)  <br>
                        @endif

                    @endforeach

                    @else
                       Belum Terpetakan
                    @endif

                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{-- <div class="container secondary">
            {{ $siswa->withQueryString()->links() }}
        </div> --}}
    </div>
</div>
</div>

@endsection

<script>
    $(document).ready(function(){
        var table = $("#mainTable").DataTable({
            "pageLength": 15,
            // "searching": true,
            "lengthChange": false,
            "pagingType": "simple_numbers",

            columnDefs: [
            {
                searchable: false,
                orderable: false,
                targets: 0
            }],

            order: [[3, 'asc']]
        });

        table
        .on('order.dt search.dt', function () {
            let i = 1;

            table
                .cells(null, 0, { search: 'applied', order: 'applied' })
                .every(function (cell) {
                    this.data(i++);
                });
        })
        .draw();

        $("#myInput").on("keyup", function() {
            table.search( this.value ).draw();
        });
    });

</script>
