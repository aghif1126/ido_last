{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Siswa
@endsection
@section('title')
<h1>Edit Siswa</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-siswa">Daftar Siswa</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-siswa/{{$siswa -> nis}}">Detail Siswa</a></li>
          <li class="breadcrumb-item active">Edit Siswa</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Detail {{ $siswa -> nama }} </h3>
        </div>
    </div>
    <hr>
<div class="table-responsive">
    <table class="table ">
        <thead>
            <tr>
                <th scope="col">NIS</th>
                <td>:</td>
                <td>{{ $siswa -> nis}}</td>
            </tr>
            <tr>
                <th scope="col">Nama</th>
                <td>:</td>
                <td>{{ $siswa -> nama}}</td>
            </tr>
            <tr>
                <th scope="col">Kelas</th>
                <td>:</td>
                <td>{{ $siswa -> kelas -> nama}}</td>
            </tr>
            <tr>
                <th scope="col">Jenis Kelamin</th>
                <td>:</td>
                <td>{{ $siswa -> jenkel}}</td>
            </tr>
            <tr>
                <th scope="col">Alamat</th>
                <td>:</td>
                <td>{{ $siswa -> alamat}}</td>
            </tr>
            <tr>
                <th scope="col">Tanggal Lahir</th>
                <td>:</td>
                <td>{{ $siswa -> tanggal_lahir}}</td>
            </tr>
            {{-- <tr>
                <th scope="col">Semester</th>
                <td>:</td>
                <td>{{ $siswa -> semester}}</td>
            </tr>
            <tr>
                <th scope="col">Tahun Pelajaran</th>
                <td>:</td>
                <td>{{ $siswa -> tahun_pelajaran}}</td>
            </tr> --}}
            <tr>
                <th>Periode</th>
                <td>:</td>
                <td>{{ $siswa -> periode}}</td>
            </tr>
            <tr>
                <th>Program Keahlian</th>
                <td>:</td>
                <td>{{ $siswa -> kelas -> konsentrasi -> jurusan -> nama}}</td>
            </tr>
            <tr>
                <th>Konsentrasi Keahlian</th>
                <td>:</td>
                <td>{{ $siswa -> kelas -> konsentrasi -> nama}}</td>
            </tr>
            <tr>
                <th>Nama Orang Tua / Wali</th>
                <td>:</td>
                <td>{{ $siswa -> nama_ortu}}</td>
            </tr>
            <tr>
                <th>Alamat Orang tua</th>
                <td>:</td>
                <td>{{ $siswa -> alamat_ortu}}</td>
            </tr>
            <tr>
                <th>Telepon Orang Tua / Wali</th>
                <td>:</td>
                <td>{{ $siswa -> telp_ortu}}</td>
            </tr>

        </thead>
    </table>
</div>
</div>
</div>

<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Edit Siswa</h3>
        </div>
    </div>
    <hr>
    <form action="/guru/ubah-siswa" method="post">

        @csrf

        <div class="table-responsive">
            <table class="table ">
                <thead>
                    <input type="text" class="form" name="nis" placeholder="Website" value="{{ $siswa->id }}" hidden>
                    <tr>
                        <th scope="col">NIS</th>
                        <td>:</td>
                        <td><input type="number" class="form-control" name="nis" placeholder="NIS"
                                value="{{  $siswa -> nis }}" required></td>
                    </tr>
                    <tr>
                        <th scope="col">Nama</th>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="nama" onkeydown="return /[a-z A-Z '.]/i.test(event.key)" placeholder="Nama"
                                value="{{  $siswa -> nama }}" required></td>
                    </tr>
                    <tr>
                        <th scope="col">Kelas</th>
                        <td>:</td>
                        <td>
                            <select name="kelas" class="form-select" aria-label=".form-select-lg example" required>
                                <option selected value="{{ $siswa -> id_kelas }}" hidden>{{ $siswa -> kelas -> nama }}</option>
                                @foreach ($kelas as $g )
                                <option value="{{ $g->id }}">{{ $g->nama }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">No. Telp</th>
                        <td>:</td>
                        <td><input type="number" class="form-control" name="telp" placeholder="No. Telp"
                                value="{{  $siswa -> telp }}" ></td>
                    </tr>
                    <tr>
                        <th scope="col">Jenis Kelamin</th>
                        <td>:</td>
                        <td><select name="jenkel" class="form-select form-select" aria-label=".form-select" required>
                                    <option selected hidden>{{ $siswa-> jenkel }}</option>
                                    <option value="L">L</option>
                                    <option value="P">P</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        </select>
                        <th scope="col">Alamat</th>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="alamat" placeholder="Alamat"
                                value="{{  $siswa -> alamat }}" ></td>
                    </tr>
                    <tr>
                        </select>
                        <th scope="col">Tanggal Lahir</th>
                        <td>:</td>
                        <td><input type="date" class="form-control" name="tanggal_lahir" value="{{ $siswa -> tanggal_lahir }}" ></td>
                    </tr>



                </thead>
                <tbody>
                </tbody>
            </table>
    </div>
    <div class="d-flex justify-content-end">
        <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin mengedit {{ $siswa -> nama }}?`)">Simpan</button>
    </div>
    </form>
</div>
</div>

@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
