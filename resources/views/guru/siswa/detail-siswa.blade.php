{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Siswa
@endsection
@section('title')
<h1>Detail Siswa</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/pemetaan-siswa">pemetaan</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-kelas/{{$siswa -> kelas -> id}}"> Detail Kelas</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-kelas/{{$siswa -> kelas -> id}}/siswa">Daftar Siswa di Kelas</a></li>
          <li class="breadcrumb-item active">Detail Siswa</li>
        </ol>
      </nav>
@endsection

@section('content')
<style>
    .frame{
        background-color: grey;
        width: 142px;
        height: 102px;
    }
</style>



<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Detail Siswa</h3>
        </div>

        <a href="../edit-siswa/{{ $siswa -> nis }}"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Edit</button></a>

    </div>
    <hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">NIS</th>
                    <td>:</td>
                    <td>{{ $siswa -> nis}}</td>
                </tr>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td>{{ $siswa -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Kelas</th>
                    <td>:</td>
                    <td>{{ $siswa -> kelas -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Jenis Kelamin</th>
                    <td>:</td>
                    <td>{{ $siswa -> jenkel}}</td>
                </tr>
                <tr>
                    <th scope="col">Alamat</th>
                    <td>:</td>
                    <td>{{ $siswa -> alamat}}</td>
                </tr>
                <tr>
                    <th scope="col">Tanggal Lahir</th>
                    <td>:</td>
                    <td>{{ $siswa -> tanggal_lahir}}</td>
                </tr>
                <tr>
                    <th scope="col">Tahun Pelajaran</th>
                    <td>:</td>
                    <td>{{ $siswa -> tahun_pelajaran -> tahun}}</td>
                </tr>
                <tr>
                    <th>Periode</th>
                    <td>:</td>
                    <td>
                        @foreach ($siswa -> perusahaan as $perusahaan)
                        <a href="/guru/detail-perusahaan/{{ $perusahaan ->  id_perusahaan }}">
                            {{ $perusahaan -> detail -> nama}}</a> :
                                @if (in_array($sekarang, explode('-', $perusahaan -> periode)) &&
                                (intval(explode('-', $perusahaan -> awal_periode)[0]) <= $tahun &&
                                $tahun <= (intval(explode('-', $perusahaan -> akhir_periode)[0]))))

                                 {{ $bulan[intval(explode('-', $perusahaan -> awal_periode)[1])]}} ({{ explode('-', $perusahaan -> awal_periode)[0] }}) -
                                 {{ $bulan[intval(explode('-', $perusahaan -> akhir_periode)[1])]}} ({{ explode('-', $perusahaan -> akhir_periode)[0] }})
                                sedang dilaksanakan
                                @else
                                    @if ((intval(explode('-', $perusahaan -> awal_periode)[0]) > $tahun ))
                                    {{ $bulan[intval(explode('-', $perusahaan -> awal_periode)[1])]}} ({{ explode('-', $perusahaan -> awal_periode)[0] }}) -
                                    {{ $bulan[intval(explode('-', $perusahaan -> akhir_periode)[1])]}} ({{ explode('-', $perusahaan -> akhir_periode)[0] }})
                                   Belum Dimulai
                                    @else
                                    {{ $bulan[intval(explode('-', $perusahaan -> awal_periode)[1])]}} ({{ explode('-', $perusahaan -> awal_periode)[0] }}) -
                                    {{ $bulan[intval(explode('-', $perusahaan -> akhir_periode)[1])]}} ({{ explode('-', $perusahaan -> akhir_periode)[0] }})
                                   selesai
                                    @endif

                                @endif

                           <br>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>Program Keahlian</th>
                    <td>:</td>
                    <td>{{ $siswa -> kelas -> konsentrasi -> jurusan -> nama}}</td>
                </tr>
                <tr>
                    <th>Konsentrasi Keahlian</th>
                    <td>:</td>
                    <td>{{ $siswa -> kelas -> konsentrasi -> nama}}</td>
                </tr>
                <tr>

                    <th>Nama Orang Tua / Wali</th>
                    <td>:</td>
                    <td>{{ $siswa -> nama_ortu}}</td>
                </tr>
                <tr>
                    <th>Alamat Orang tua</th>
                    <td>:</td>
                    <td>{{ $siswa -> alamat_ortu}}</td>
                </tr>
                <tr>
                    <th>Telepon Orang Tua / Wali</th>
                    <td>:</td>
                    <td>{{ $siswa -> telp_ortu}}</td>
                </tr>
                <tr>
                    <th>Perusahaan</th>
                    <td>:</td>
                    <td>
                    @foreach ($siswa->perusahaan as $item)
                    <div class="d-flex flex-column">
                        <div>
                            {{ $item->detail->nama }}
                        </div>
                    </div>
                    @endforeach
                </td>
                </tr>
                    <tr>
                        <th>Action</th>
                        <td>:</td>
                        <td>
                            <a href="/guru/pemetaan-siswa/{{$siswa -> nis}}/pilih-perusahaan">Tambah Perusahaan</a>
                        </td>
                    </tr>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

@if (!is_null($riwayat))

<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Absen Siswa</h3>
        </div>
    </div>

@foreach ($absen as $a)



<div class="accordion" id="riwayat">
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingriwayat{{ $loop -> iteration + 14}}">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $loop -> iteration + 14}}" aria-expanded="false" aria-controls="collapse{{ $loop -> iteration + 14}}">
            Tanggal : {{ $a  -> tanggal }}
          </button>
        </h2>
        <div id="collapse{{ $loop -> iteration + 14}}" class="accordion-collapse collapse" aria-labelledby="headingriwayat{{ $loop -> iteration + 14}}" data-bs-parent="#riwayat">
          <div class="accordion-body">
            <h3>{{ $a -> hari }}</h3>
            <div class="table-responsive">
                <table class="table ">
                    <thead>
                        <tr>
                            <th scope="col" style="width: 20%">Keterangan</th>
                            <td>:</td>
                            <td>{{ $a -> status }}</td>
                        </tr>
                        <tr>
                            @if ($a->status == "Hadir")
                            <th scope="col" style="width: 20%">Poto</th>
                            <td><p style="margin-top: -60px">: </td>
                            <td>@if ($a -> poto_masuk)
                                    <img src="{{ asset('uploads/absen/masuk/uploads'.$a->poto_masuk) }}" style="width: 140px; height: 100px; border: 0px solid black; border-radius:10px" alt=""
                                    height="240px" width="320px">
                                @endif
                            </td>
                            @endif
                        </tr>
                        @if ($a->jam_pulang != "Tidak Ada")
                        <tr>
                            <th scope="col" style="width: 20%">Pulang</th>
                            <td>:</td>
                            <td>{{ $a -> jam_pulang }}</td>
                        </tr>
                        <tr>
                            <th scope="col" style="width: 20%">Poto Pulang</th>
                            <td>:</td>
                            <td><img src="{{ asset('uploads/absen/pulang/uploads'.$a->poto_pulang) }}" alt="" height="100px" width="150px" style="border-radius: 10px;"></td>
                        </tr>
                        @endif

                        <tr>
                            <th scope="col" style="width: 20%"></th>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>                        <tr>
                            <th scope="col" style="width: 20%">Kegiatan</th>
                            <td>:</td>
                            <td>
                            @foreach ($riwayat -> where('tanggal', $a -> tanggal) -> all() as $item)
                                {{$item -> kegiatan}} <br>
                            @endforeach
                            </td>
                        </tr>



                    </thead>
                </table>
            </div>
          </div>
        </div>
      </div>
  </div>
{{-- <div class="card top-selling overflow-auto shadow p-3 mb-3 my-3 bg-white accordion-collapse collapse show" id="kotak">

</div> --}}

@endforeach
</div>

@else

@endif

@if (!is_null($siswa -> nilai ))

@if (!is_null($siswa -> nilai ))

<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Nilai Siswa di Perusahaan</h3>
        </div>
    </div>
    @foreach ($siswa -> perusahaan  as $perusahaan)


    <div class="accordion" id="accordionExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="heading{{ $loop -> iteration }}">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#collapse{{ $loop -> iteration }}" aria-expanded="false"
                    aria-controls="collapse{{ $loop -> iteration }}">
                    {{ $perusahaan -> detail ->  nama }}
                </button>
            </h2>
            {{-- @dd($perusahaan -> detail ->  pembimbing -> count()) --}}
            @if ($perusahaan -> detail ->  pembimbing -> where('id_jurusan', $siswa -> kelas -> konsentrasi -> jurusan -> id) != null)

            {{-- @for ($i = 0; $i < $perusahaan -> detail ->  pembimbing -> count(); $i++) --}}



            @foreach ($perusahaan -> detail ->  pembimbing -> where('id_jurusan', $siswa -> kelas -> konsentrasi -> jurusan -> id) as  $pembimbing)
            {{-- @dd($key) --}}
            <div id="collapse{{ $loop -> iteration }}" class="accordion-collapse collapse"
                aria-labelledby="heading{{ $loop -> iteration }}" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <h3>{{ $pembimbing -> nama }}</h3>
                    <div class="table-responsive">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <td scope="col" style="width: 5%;" class="table-secondary">
                                        <p>No</p>
                                    </td>
                                    <td scope=" col" style="width: 20%" class="table-secondary">
                                        <p>Komponen</p>
                                    </td>
                                    <td scope="col" style="width: 20%" class="table-secondary">Skor<br>(0-100)</td>
                                    <td scope="col" style="width: 20%" class="table-secondary">
                                        <p>Keterangan</p>
                                    </td>

                                </tr>

                               @foreach ($kriteria -> where('id_pembimbing' , $pembimbing -> id) as $item)
                               {{-- $nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa ->nis)-> first() --}}
                               <tr>
                                   <td rowspan="1" style="background-color: rgb(236, 236, 236)">{{ $loop -> iteration
                                       }}</td>
                                   <td style="background-color: rgb(236, 236, 236)">{{ $item -> nama_kriteria }}</td>
                                   <td style="background-color: rgb(236, 236, 236)">
                                       @if ($nilai)
                                       {{ $nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa ->
                                       nis)-> first() ? $nilai -> where('id_kriteria', $item -> id)-> where('id_siswa',
                                       $siswa -> nis) -> first() -> point : ''}}
                                       @else
                                       @endif

                                   </td>
                                   <td style="background-color: rgb(236, 236, 236)">
                                       @if ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first())

                                       @if ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point >= 90)
                                           <div>A</div>
                                       @elseif ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point < 90)
                                           <div>B</div>
                                       @elseif ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point < 80)
                                           <div>C</div>
                                       @else
                                           <div>D</div>
                                       @endif
                                   @else

                                   @endif
                                   </td>
                               </tr>

                               @endforeach


                            </thead>
                        </table>
                        <form action="/siswa/download/jurnalsiswa" method="post">
                            @csrf
                            <input type="text" name="id_siswa" value="{{ $siswa -> nis }}" hidden>
                            <input type="text" name="id_pembimbing" value="{{ $pembimbing ->id }}" hidden>
                            <input type="text" name="id_perusahaan" value="{{ $perusahaan -> detail -> id }}" hidden>
                            <button class="btn btn-danger" role="button" style="margin-left:87%">Download PDF</button>
                        </form>
                    </div>
                </div>
            </div>

            @endforeach
            {{-- @endfor --}}
            @endif
        </div>

    </div>


    @endforeach

</div>
@else
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Nilai Siswa</h3>
        </div>
    </div>
</div>
@endif
@else
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Nilai Siswa</h3>
        </div>
    </div>
</div>
@endif


{{-- ini bagian Catatan siswa --}}

@if (!is_null($catatan))

<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Catatan Siswa</h3>
        </div>
    </div>
    @foreach ($catatan as $catatan)


<div class="accordion" id="catatanexample">
    <div class="accordion-item">
        <h2 class="accordion-header" id="headcatatan">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collaps{{ $loop -> iteration  }}" aria-expanded="false" aria-controls="collaps{{ $loop -> iteration}}">
           {{ $catatan -> perusahaan -> nama }} :  {{$catatan -> tanggal}}
          </button>
        </h2>
        <div id="collaps{{ $loop -> iteration  }}" class="accordion-collapse collapse" aria-labelledby="headcatatan" data-bs-parent="#catatanexample">
          <div class="accordion-body">

            <div class="table-responsive">
                <table class="table ">
                    <thead>
                        <tr>
                            <th scope="col" style="width: 20%">Catatan</th>
                            <td>:</td>
                            <td>{{ $catatan -> catatan }}</td>
                        </tr>
                        <tr>
                            <th scope="col" style="width: 20%">Tanggal Dibuat</th>
                            <td>:</td>
                            <td>{{ $catatan -> tanggal }}</td>
                        </tr>


                    </thead>
                </table>
            </div>
          </div>
        </div>
      </div>
  </div>


  @endforeach

</div>
@else

@endif




{{-- <!-- Vendor JS Files -->
<script src="{{asset('/vendor/apexcharts/apexcharts.min.js')}}"></script>
<script src="{{asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('/vendor/chart.js/chart.umd.js')}}"></script>
<script src="{{asset('/vendor/echarts/echarts.min.js')}}"></script>
<script src="{{asset('/vendor/quill/quill.min.js')}}"></script>
<script src="{{asset('/vendor/simple-datatables/simple-datatables.js')}}"></script>
<script src="{{asset('/vendor/tinymce/tinymce.min.js')}}"></script>
<script src="{{asset('/vendor/php-email-form/validate.js')}}"></script>

<!-- Template Main JS File -->
<script src="{{asset('/js/main.js')}}"></script> --}}


@endsection
