{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>Detail Guru</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-guru">Daftar Guru</a></li>
          <li class="breadcrumb-item active">Detail Guru</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Detail Guru</h3></div>
        <a href="../edit-guru/{{ $guru -> id }}"><button type="button" class="btn btn-secondary mx-3 align-self-end">Edit</button></a>
    </div>
<hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td>{{ $guru -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">NIP</th>
                    <td>:</td>
                    <td>{{ $guru -> nip}}</td>
                </tr>
                <tr>
                    <th scope="col">Status</th>
                    <td>:</td>
                    <td>
                        @if ($guru -> status == "Pilih Status")

                        @else
                            {{ $guru -> status}}
                        @endif
                    </td>
                </tr>
                <tr>
                <th scope="col">Alamat</th>
                    <td>:</td>
                    <td>{{ $guru -> alamat}}</td>
                </tr>
                <tr>
                    <th scope="col">Jenis Kelamin</th>
                    <td>:</td>
                    <td>{{ $guru -> jenkel}}</td>
                </tr>
                <tr>
                <th scope="col">No. Telp</th>
                    <td>:</td>
                    <td>{{ $guru -> telp}}</td>
                </tr>
                <th scope="col">Perusahaan</th>
                    <td>:</td>
                    <td>
                        @if (is_null($guru -> perusahaan -> all()))
                        Tidak Tersedia
                       @else
                       @foreach ($guru -> perusahaan as $item)
                       {{ $item -> detailperusahaan -> nama }} ({{ $item -> jurusan -> nama }})<br>
                   @endforeach
                       @endif
                </td>
                </tr>


            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
</div>

@endsection
