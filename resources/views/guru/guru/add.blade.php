{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>Tambah Guru</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-guru">Daftar Guru</a></li>
          <li class="breadcrumb-item active">Tambah Guru</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
<form action="store-guru" method="post" >
    @csrf

    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">NIP</th>
                    <td>:</td>
                    <td><input type="number" class="form-control" name="nip" placeholder="NIP" required></td>
                </tr>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td><input type="text" class="form-control" name="nama" onkeydown="return /[a-z A-Z .]/i.test(event.key)" placeholder="Nama" required></td>
                </tr>
                <tr>
                <tr>
                    <th scope="col">Alamat</th>
                    <td>:</td>
                    <td><input type="text" class="form-control" name="alamat" placeholder="Alamat" required></td>
                </tr>
                <tr>
                    <th scope="col">No. Telp</th>
                    <td>:</td>
                    <td><input type="number" class="form-control" name="telp" placeholder="No. Telp" required></td>
                </tr>
                <tr>
                    <th scope="col">Jenis Kelamin</th>
                    <td>:</td>
                    <td><select name="jenkel" class="form-select form-select" aria-label=".form-select" required>
                        <option selected hidden>L/P</option>
                                <option value="L">L</option>
                                <option value="P">P</option>
                        </select>
                    </td>
                </tr>
            </thead>
        </table>
    </div>
    <div class="d-flex justify-content-end">
        <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin menambahkan Guru Pembimbing?`)">Submit</button>
    </div>
</form>
</div>
</div>

@endsection
