{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<div class="d-flex flex-row">
    <div class="me-auto p-2">

        <h1>Tambah Perusahaan Guru</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
                <li class="breadcrumb-item"><a href="/guru/show-guru">Daftar Guru</a></li>
                <li class="breadcrumb-item"><a href="/guru/detail-guru/{{$guru ? $guru->id : 'Tidak Tersedia'}}">Detail Guru</a></li>
                <li class="breadcrumb-item"><a href="/guru/edit-guru/{{$guru ? $guru->id : 'Tidak Tersedia'}}">Edit Guru</a></li>
                <li class="breadcrumb-item active">Tambah Perusahaan Guru</li>
            </ol>
        </nav>
    </div>
        <div class="col-sm-12 col-md-5 mb-md-0 me-2">
            <form action="">
                {{-- @csrf --}}
                <select class="form-control custom-select select2 ng-pristine ng-valid ng-not-empty ng-touched" ng-model="" name="tahun" id="" ng-change="">
                    @foreach ($tahun as $item)
                    <option value="{{ $item -> id }}" class="ng-binding ng-scope" {{ $item -> id == $tahun_pelajaran -> id ? 'selected' : '' }}>Tahun Pelajaran {{ $item -> tahun }} - ({{ $item -> status }})</option>
                    @endforeach
                </select>
            </div>
                <div>
                    <input type="submit" value="Terapkan" class="btn border border-info" >
                </div>
            </form>
</div>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">
@section('content')


<div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
        <div class="d-flex flex-row">
            <div class="me-auto p-2">
                <h3>Daftar Perusahaan</h3>
            </div>
            <div class="col-12 col-sm-8 col-md-6">
                <form action="/guru/edit-guru/{{ $guru ? $guru->id : 'Tidak Tersedia'}}/add-perusahaan" method="get">

                    <div class="input-group mb-3 ">
                        {{-- <button class="btn btn-outline-secondary" type="submit" id="button-addon1">Search</button> --}}
                        <input type="text" class="form-control" placeholder="Search Data" name="keyword" id="myInput">

                    </div>
                </form>
            </div>

            {{-- <a href="add-perusahaan"><button type="button"
                    class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
            <a href="delete-perusahaan"><button type="button"
                    class="btn btn-secondary mx-3 align-self-end">Hapus</button></a> --}}

        </div>
        <form action="/guru/edit-guru/store-perusahaan" method="POST">
            @csrf
        <div class="d-flex justify-content-end">
            <button type="submit" class="btn btn-secondary" style="width: 25%;"
                onClick="return confirm(`Yakin ingin menambah perusahaan?`)">Simpan</button>
        </div>
        <hr>

            <input type="text" name="id_guru" value="{{ $guru ? $guru->id : 'Tidak Tersedia'}}" hidden>
            <div class="table-responsive">
                <table class="table" id="mainTable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama</th>
                            <th scope="col" style="width: 40%">Alamat</th>
                            {{-- <th scope="col" style="width: 30%">Jurusan</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($perusahaan  as $a)
                        {{-- @dd($perusahaan[1] -> guru) --}}

                       @foreach ($a -> jurusan as $jurusan)
                       {{-- @dd($perusahaan[5]-> guru -> where('id_jurusan', $jurusan -> id)) --}}
                       @if ($a -> guru -> where('id_jurusan', $jurusan -> id) != "[]")
                       @else
                       <tr>
                           <td>
                               <input type="checkbox" name="perusahaan[]" id="" value="{{ $a->id.'-'.$jurusan -> detailjurusan -> id}}">
                           </td>
                           <td>{{ $a -> nama}} - ({{ $jurusan -> detailjurusan -> nama }})</td>
                           <td>{{ $a -> alamat}}</td>
                       </tr>
                       @endif
                       @endforeach

                        @endforeach
                    </tbody>
                </table>
                <div class="container">
                    {{-- {{ $perusahaan->links() }} --}}
                </div>


            </div>
    </div>
</div>
</form>

@endsection
<script>
    $(document).ready(function(){
        $(document).ready(function(){
        var table = $("#mainTable").DataTable({
            // "pageLength": 15,
            // "searching": false,
            "lengthChange": false,
            "pagingType": "simple_numbers",
            "paging": false,

            columnDefs: [
            {
                searchable: false,
                orderable: false,
                targets: 0
            }],

            order: [[2, 'asc']]
        });

        $("#myInput").on("keyup", function() {
            table.search( this.value ).draw();
        });
    });
    });
</script>
