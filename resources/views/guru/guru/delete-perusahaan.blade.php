{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>Hapus Perusaan Guru</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-guru">Daftar Guru</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-guru/{{$guru -> id}}">Detail Guru</a></li>
          <li class="breadcrumb-item"><a href="/guru/edit-guru/{{$guru -> id}}">Edit Guru</a></li>
          <li class="breadcrumb-item active">Hapus Perusahaan Guru</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Pilih perusahaan yang akan dihapus</h3></div>

    </div>
<hr>
<form action="/guru/edit-guru/destroy-perusahaan" method="POST">
    @csrf
    <input type="text" name="id_guru" value="{{ $guru -> id }}" hidden>
    <div class="table-responsive">
        <table class="table" >

            <tbody>
                @foreach ($guru -> perusahaan as $perusahaan)
                <tr>
                    <td><button type="submit" name="id_perusahaan" value="{{ $perusahaan -> detailperusahaan -> id }}" onClick="return confirm(`Yakin ingin menghapus {{ $perusahaan -> detailperusahaan -> nama }}?`)"> {{ $perusahaan -> detailperusahaan -> nama }}</button></td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
    </form>
</div>
</div>

@endsection
