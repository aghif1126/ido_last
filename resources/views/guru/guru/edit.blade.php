{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>Edit Guru</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-guru">Daftar Guru</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-guru/{{$guru -> id}}">Detail Guru</a></li>
          <li class="breadcrumb-item active">Edit Guru</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Detail {{ $guru -> nama }} </h3>
        </div>
    </div>
    <hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td>{{ $guru -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">NIP</th>
                    <td>:</td>
                    <td>{{ $guru -> nip}}</td>
                </tr>
                <tr>
                    <th scope="col">Alamat</th>
                    <td>:</td>
                    <td>{{ $guru -> alamat}}</td>
                </tr>
                <tr>
                    <th scope="col">Jenis Kelamin</th>
                    <td>:</td>
                    <td>{{ $guru -> jenkel}}</td>
                </tr>
                <tr>
                    <th scope="col">No. Telp</th>
                    <td>:</td>
                    <td>{{ $guru -> telp}}</td>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Edit guru </h3>
        </div>
    </div>
    <hr>

<form action="/guru/update-guru" method="post">
    @csrf


    <div class="table-responsive">
            <table class="table ">
                <thead>
                    <input type="text" class="form" name="id" placeholder="Website" value="{{ $guru->id }}" hidden>
                    <tr>
                        <th scope="col">Nama</th>
                        <td>:</td>
                        <td colspan="3"><input type="text" class="form-control" name="nama" onkeydown="return /[a-z A-Z .]/i.test(event.key)" placeholder="Nama"
                                value="{{  $guru -> nama }}" required></td>
                    </tr>
                    <tr>
                        <th scope="col">NIP</th>
                        <td>:</td>
                        <td colspan="3"><input type="number" class="form-control" name="nip" placeholder="NIP"
                                value="{{  $guru -> nip }}" required></td>
                    </tr>
                    <tr>
                        <th scope="col">Status</th>
                        <td>:</td>
                        <td colspan="3">
                            <select name="status" class="form-select form-select" aria-label=".form-select" required>
                                @if (is_null($guru -> status))
                                    <option selected hidden>Pilih Status</option>
                                @else
                                    <option selected hidden>{{ $guru-> status }}</option>
                                @endif
                                <option value="Kaprog">Kaprog</option>
                                <option value="Guru">Guru</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">Alamat</th>
                        <td>:</td>
                        <td colspan="3"><input type="text" class="form-control" name="alamat" placeholder="Alamat"
                                value="{{  $guru -> alamat }}" required></td>
                    </tr>
                    <tr>
                        <th scope="col">Jenis Kelamin</th>
                        <td>:</td>
                        <td colspan="3"><select name="jenkel" class="form-select form-select" aria-label=".form-select" required>
                                    <option selected hidden>{{ $guru-> jenkel }}</option>
                                    <option value="L">L</option>
                                    <option value="P">P</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">No. Telp</th>
                        <td>:</td>
                        <td colspan="3"><input type="number" class="form-control" name="telp" placeholder="No. Telp"
                                value="{{  $guru -> telp }}" required></td>
                    </tr>
                    <tr>
                        <th scope="col">Perusahaan</th>
                        <td>:</td>
                        <td>
                            {{-- @dd($guru -> perusahaan) --}}
                            @if (is_null($guru -> perusahaan))
                            Tidak Tersedia
                           @else
                           @foreach ($guru -> perusahaan  as $item)
                           {{ $item -> detailperusahaan -> nama }} ({{ $jurusan -> find($item  -> id_jurusan) -> nama }})<br>
                       @endforeach
                           @endif
                            <td> <a href="{{ $guru -> id }}/add-perusahaan" ><button type="button" class="btn btn-secondary mx-3 align-self-end">Tambah</button></a></td>
                            <td><a href="{{ $guru -> id }}/delete-perusahaan"><button type="button" class="btn btn-secondary mx-3 align-self-end">Hapus</button></a></td>
                            </td>
                    </tr>
                </thead>
            </table>
    </div>
    <div class="d-flex justify-content-end">
        <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin mengedit {{ $guru -> nama }}?`)">Simpan</button></div>
    </form>
</div>
</div>

@endsection


