{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Pemetaan
@endsection
@section('title')
<div class="d-flex flex-row">
<div class="me-auto p-2">
        <h1>Pemetaan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
                <li class="breadcrumb-item active">Pemetaan</li>
            </ol>
        </nav>
    </div>
    <div class="col-sm-12 col-md-5 mb-md-0 me-2">
        <form action="">
            {{-- @csrf --}}
            <select class="form-control custom-select select2 ng-pristine ng-valid ng-not-empty ng-touched" ng-model="" name="tahun" id="" ng-change="">
                @foreach ($tahun as $item)
                <option value="{{ $item -> id }}" class="ng-binding ng-scope" {{ $item -> id == $tahun_pelajaran -> id ? 'selected' : '' }}>Tahun Pelajaran {{ $item -> tahun }} - ({{ $item -> status }})</option>
                @endforeach
            </select>
        </div>
            <div>
                <input type="submit" value="Terapkan" class="btn border border-info" >
            </div>
        </form>
</div>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Data Pemetaan</h3>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Jumlah Siswa</th>
                    <td>:</td>
                    <td>{{ $jumlah_siswa}}</td>
                </tr>
                <tr>
                    <th scope="col">Terpetakan</th>
                    <td>:</td>
                    <td>{{ $sudah}}</td>
                </tr>
                <tr>
                    <th scope="col">Belum Terpetakan</th>
                    <td>:</td>
                    <td>{{ $belum}}</td>
                </tr>
                <tr>
                    <th scope="col">Sudah Di Konfirmasi</th>
                    <td>:</td>
                    <td>{{ $confirm}}</td>
                </tr>
                <tr>
                    <th scope="col">Belum Di Konfirmasi</th>
                    <td>:</td>
                    <td>{{ $tunggu}}</td>
                </tr>

            </thead>
        </table>
        <div>
            <form action="/admin/download/rekappemetaan" method="post">
                @csrf
                <input type="hidden" name="tahun_pelajaran" value="{{ $tahun_pelajaran->tahun }}">
                <input type="hidden" name="id_tahun" value="{{ $tahun_pelajaran->id }}">
                <button type="submit" class="btn btn-secondary">Rekap Pemetaan</button>
            </form>
        </div>
    </div>
</div>
</div>
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Data Kelas</h3>
        </div>
        {{-- <div class="col-12 col-sm-8 col-md-6">
            <form action="">
                <div class="input-group mb-3 ">
                    <input type="text" class="form-control" placeholder="Search Data" name="keyword">
                    <button class="btn btn-outline-secondary" type="submit" id="button-addon1">Search</button>
                </div>
            </form>
        </div> --}}

    </div>

    <hr>
    <div class="table-responsive">
        <table class="table " id="mainTable">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Dipetakan</th>
                    <th scope="col">Belum Dipetakan</th>
                    <th scope="col">Konfirmasi</th>
                    <th scope="col">Menunggu</th>
                    <th scope="col">Jumlah Siswa</th>

                    {{-- <th scope="col">Terpetakan</th> --}}
                </tr>
            </thead>
            <tbody>

                @foreach ($kelas as $a)


                <tr>

                    <td class="number"></td>
                    <td><a href="detail-kelas/{{ $a -> id }}/siswa"
                            class=""><button class="btn">
                            {{ $a -> nama}}</button></a></td>
                    <td>
                        <center>{{ $a -> sudah }}</center>
                    </td>
                    <td>
                        <center>{{ $a -> belum }}
                    </td>
                    <td>
                        <center>{{ $a -> confirm }}
                    </td>
                    <td>
                        <center>{{ $a -> tunggu }}
                    </td>
                    <td>
                        <center>{{ $a -> jumlah_siswa}}
                    </td>


                </tr>
                @endforeach
            </tbody>
        </table>
        {{-- <div class="container">
            {{ $kelas->withQueryString()->links() }}
        </div> --}}
    </div>
</div>
</div>

<script>
        $(document).ready(function(){
        var table = $("#mainTable").DataTable({
            // "pageLength": 15,
            // "searching": false,
            "lengthChange": false,
            // "pagingType": "simple_numbers",
            "paging": false,

            columnDefs: [
            {
                searchable: false,
                orderable: false,
                targets: 0
            }],

            order: [[1, 'asc']]
        });

        table
        .on('order.dt search.dt', function () {
            let i = 1;

            table
                .cells(null, 0, { search: 'applied', order: 'applied' })
                .every(function (cell) {
                    this.data(i++);
                });
        })
        .draw();
    });
</script>

@endsection
