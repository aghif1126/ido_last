{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')

@section('judul')
- Pemetaan
@endsection
@section('title')
<div class="d-flex flex-row">
    <div class="me-auto p-2">
        <h1>Pilih Perusahaan</h1>

    </div>
    <div class="col-sm-12 col-md-5 mb-md-0 me-2">
        <form action="">

            <select class="form-control custom-select select2 ng-pristine ng-valid ng-not-empty ng-touched"
                ng-model="" name="tahun" id="" ng-change="">
                @foreach ($tahun as $item)
                <option value="{{ $item -> id }}" class="ng-binding ng-scope" {{ $item -> id == $tahun_pelajaran
                    -> id ? 'selected' : '' }}>Tahun Pelajaran {{ $item -> tahun }} - ({{ $item -> status }})
                </option>
                @endforeach
            </select>
    </div>
    <div>
        <input type="submit" value="Terapkan" class="btn border border-info">
    </div>
</form>
</div>




@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
    integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js"
    integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>

@section('content')
<form action="/guru/pemetaan-siswa/pilih-perusahaan/periode" method="post" id="form">
    @csrf
    @foreach($id_siswa as $id)
    <input type="text" name="id_siswa[]" value="{{ $id }}" hidden>
    @endforeach

<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white" id="periode">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Pilih Periode</h3>
        </div>
    </div>


        <div class="table-responsive">
            <table class="table ">
                <thead>
                    <tr>
                        <th scope="col">Bulan Awal Periode</th>
                        <td>:</td>
                        <td><input type="month" name="awal_periode" id="data_awal_periode" required></td>
                    </tr>
                    <tr>
                        <th scope="col">Bulan Akhir Periode</th>
                        <td>:</td>
                        <td><input type="month" name="akhir_periode" id="data_akhir_periode" required></td>
                    </tr>
                </thead>
            </table>
        </div>


</div>

<div class="col-12" >
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
        <div class="d-flex flex-row">
            <div class="me-auto p-2">
                <h3>Daftar Perusahaan</h3>
            </div>
            <div class="col-12 col-sm-8 col-md-6">
                <form action="">
                    <div class="input-group mb-3 ">
                        <input type="text" class="form-control" placeholder="Search Data" name="keyword" id="myInput">

                    </div>
                </form>
            </div>
        </div>

        <hr>
        <div class="table-responsive">
            <table class="table" id="mainTable">
                <thead>
                    <tr>
                        <th scope="col" style="width: 5%">No</th>
                        <th scope="col" style="width: 25%">Nama</th>
                        <th scope="col" style="width: 25%">Alamat</th>
                        <th scope="col" style="width: 25%">Jurusan</th>
                        <th scope="col" style="width: 20%">Kuota</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($perusahaan as $a)

                    @foreach ($a -> jurusan as $jurusan)
                        @if ($jurusan -> id_jurusan == $id_jurusan)
                        <tr>
                            <td class="number">{{ $loop->iteration }}</td>

                                <td>
                                    {{-- <input type="text" name="id_perusahaan" value="{{ $a -> id }}" hidden> --}}

                                    @if ($a -> jurusan -> where('id_jurusan', $id_jurusan) -> first() ->  kuota)
                                    <button type="submit"
                                    class="link-dark link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover"
                                    onClick="return confirm(`Yakin ingin melanjutkan?`)" name="id_perusahaan" value="{{ $a -> id }}">
                                    {{$a -> nama}}
                                </button>
                                    @else
                                    {{ $a -> nama }}
                                    @endif
                                </td>


                            <td>
                               {{ $a -> alamat }}

                            </td>
                            <td>
                                @foreach ($a -> jurusan as $jurusan)
                                {{ $jurusan -> detailjurusan -> nama }} <br>
                                @endforeach

                            </td>
                            <td>
                                {{ $a -> jurusan -> where('id_jurusan', $id_jurusan) -> first() ->  kuota }}

                            </td>

                        </tr>
                        @endif
                    @endforeach


                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
</form>
@endsection


<script>
    // $(document).on('submit', '[id^=tableDelete_]' , function() {
    //     return callAjax( $(this).serialize() );
    //     });

    //     function callAjax( data ) {
    //         $.ajax({
    //         type : 'POST',
    //         url  : 'call/page.php',
    //         data : data,
    //         success :  function(data) { ... },
    //         error: function(data) { ... }
    //     });
    //     return false;
    //     };

    $(document).ready(function(){
        var table = $("#mainTable").DataTable({
            "pageLength": 15,
            // "searching": false,
            "lengthChange": false,
            "pagingType": "simple_numbers",

            columnDefs: [
            {
                searchable: false,
                orderable: false,
                targets: 0
            }],

            order: [[1, 'asc']]
        });

        table
        .on('order.dt search.dt', function () {
            let i = 1;

            table
                .cells(null, 0, { search: 'applied', order: 'applied' })
                .every(function (cell) {
                    this.data(i++);
                });
        })
        .draw();

        $("#myInput").on("keyup", function() {
            table.search( this.value ).draw();
        });
    });
</script>
