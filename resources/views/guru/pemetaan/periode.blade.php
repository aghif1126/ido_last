{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Pemetaan
@endsection
@section('title')
<h1>Atur Periode</h1>
      {{-- <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/pemetaan-siswa">pemetaan</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-kelas/{{$siswa -> kelas -> id}}"> Detail Kelas</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-kelas/{{$siswa -> kelas -> id}}/siswa">Daftar Siswa</a></li>
          <li class="breadcrumb-item active">Detail Siswa</li>
        </ol>
      </nav> --}}
@endsection

@section('content')

<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Pilih Periode</h3>
        </div>
    </div>
    <form action="/guru/pemetaan-siswa/pilih-perusahaan/periode" method="post">
        @csrf
        @foreach ($id_siswa as $item)
            
        <input type="text" name="id_siswa[]" value="{{ $item }}" hidden>
        <input type="text" name="id_perusahaan" value="{{ $id_perusahaan }}" hidden>
        @endforeach
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Bulan Awal Periode</th>
                    <td>:</td>
                    <td><input type="month" name="awal_periode"></td>
                </tr>
                <tr>
                    <th scope="col">Bulan Akhir Periode</th>
                    <td>:</td>
                    <td><input type="month" name="akhir_periode"></td>
                </tr>
            </thead>
        </table>
    </div>
    <div class="d-flex justify-content-end">
        <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin melanjutkan?`)">Submit</button></div>
</form>
</div>



@endsection
