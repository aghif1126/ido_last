{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Pemetaan
@endsection
@section('header')
<script>
    function goBack() {
      window.history.back()
    }
</script>
<h2>Welcome Guru</h2>
<hr>
<h3>Edit Data Siswa</h3>
<br>
<button>
<img src="{{asset('bg/back.png')}}" style="width:50%;" onclick="goBack()"></button>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <form action="/guru/ubah-siswa" method="post">
        @csrf

        <input type="text" class="form-control" id="nis" name="id" value="{{ $siswa -> id }}" hidden>
        <div class="mb-3">
            <label for="nis" class="form-label fs-4">NIS</label>
            <input type="text" class="form-control" id="nis" name="nis" value="{{ $siswa -> nis }}">
        </div>

        <div class="mb-3">
            <label for="nis" class="form-label fs-4">Nama</label>
            <input type="text" class="form-control" id="nis" name="nama" value="{{ $siswa -> nama }}">
        </div>

        <div class="mb-3">
            <label for="nis" class="form-label fs-4">Kelas</label>
            <select name="kelas" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
                <option selected value="{{ $siswa -> id_kelas }}">{{ $siswa -> kelas -> nama }}</option>
                @foreach ($kelas as $g )
                <option value="{{ $g->id }}">{{ $g->nama }}</option>

                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <label for="nis" class="form-label fs-4">Telp </label>
            <input type="text" class="form-control" id="nis" name="telp" value="{{ $siswa -> telp }}">
        </div>

        <div class="mb-3">
            <label for="nis" class="form-label fs-4">Jenkel</label>
            <select name="jenkel" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
                <option selected>{{ $siswa -> jenkel }}</option>
                <option value="P">P</option>
                <option value="L">L</option>
                {{-- <option value="3">Three</option> --}}
            </select>
        </div>

        <div class="mb-3">
            <label for="nis" class="form-label fs-4">Alamat </label>
            <input type="text" class="form-control" id="nis" name="alamat" value="{{ $siswa -> alamat }}">
        </div>

        <div class="mb-3">
            <label for="nis" class="form-label fs-4">Tanggal Lahir </label>
            <input type="date" class="form-control" id="nis" name="tanggal_lahir" value="{{ $siswa -> tanggal_lahir }}">
        </div>

        <div class="mb-3">
            <label for="nis" class="form-label fs-4">Semester </label>
            <input type="text" class="form-control" id="nis" name="semester" value="{{ $siswa -> semester }}">
        </div>

        <div class="mb-3">
            <label for="nis" class="form-label fs-4">Tahun Pelajaran </label>
            <input type="text" class="form-control" id="nis" name="tahun_pelajaran"
                value="{{ $siswa -> tahun_pelajaran }}">
        </div>

        {{-- <div class="mb-3">
            <label for="nis" class="form-label fs-4">Periode </label>
            <select class="form-select form-select-lg mb-3" name="periode" aria-label=".form-select-lg example">
                <option selected value="{{ $siswa -> periode }}">{{ $siswa -> periode }}</option>
                <option value="3 bulan">3 bulan</option>
                <option value="6 bulan">6 bulan</option>

            </select>
        </div> --}}



        <div class="mb-3">
            <label for="nis" class="form-label fs-4">Guru Pembimbing</label>
            <select class="form-select form-select-lg mb-3" name="id_guru" aria-label=".form-select-lg example">
                <option selected value="{{ $siswa -> guru -> id }}">{{ $siswa -> guru -> nama }}</option>
                @foreach ($guru as $g )
                <option value="{{ $g->id }}">{{ $g->nama }}</option>

                @endforeach

                {{-- <option value="3">Three</option> --}}
            </select>
        </div>

        <div class="mb-3">
            <label for="nis" class="form-label fs-4">Perusahaan</label>
            <select class="form-select form-select-lg mb-3" name="id_perusahaan" aria-label=".form-select-lg example">
                <option selected value="{{ $siswa -> perusahaan -> id }}">{{ $siswa -> perusahaan -> nama }}</option>
                @foreach ($perusahaan as $g )
                <option value="{{ $g->id }}">{{ $g->nama }}</option>

                @endforeach

                {{-- <option value="3">Three</option> --}}
            </select>
        </div>






        <button type="submit" class="btn btn-primary" onClick="return confirm(`Yakin ingin melanjutkan?`)">Simpan</button>
    </form>
</div>
</div>

@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
