{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>Edit Pimpinan Perusahaan</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-pimpinan">Daftar Pimpinan</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-pimpinan/{{$pimpinanPerusahaan -> id}}">Detail Pimpinan</a></li>
          <li class="breadcrumb-item active">Detail Pimpinan</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Detail Pimmpinan Perusahaan </h3></div>

    </div>
<hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td>    {{ $pimpinanPerusahaan -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">NIP</th>
                    <td>:</td>
                    <td>{{ $pimpinanPerusahaan -> nip}}</td>
                </tr>
                <tr>
                    <th scope="col">Jenis Kelamin</th>
                    <td>:</td>
                    <td>{{ $pimpinanPerusahaan -> jenkel}}</td>
                </tr>
                <tr>
                <th scope="col">Pangkat</th>
                    <td>:</td>
                    <td>{{ $pimpinanPerusahaan -> pangkat}}</td>
                </tr>
                <tr>
                    <th scope="col">Perusahaan</th>
                    <td>:</td>
                    <td>{{ $pimpinanPerusahaan -> perusahaan -> nama}}</td>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Edit Pimpinan Perusahaan </h3></div>

    </div>
<hr>
<form action="/guru/update-pimpinan" method="post">
    @csrf
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <input type="text" class="form-control" name="id" placeholder="Website" value="{{ $pimpinanPerusahaan->id }}" hidden>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td><input type="text" class="form-control" name="nama" onkeydown="return /[a-z A-Z .]/i.test(event.key)" placeholder="Website" value="{{  $pimpinanPerusahaan -> nama }}"></td>
                </tr>
                <tr>
                    <th scope="col">NIP</th>
                    <td>:</td>
                    <td><input type="number" class="form-control" name="nip" placeholder="Website" value="{{ $pimpinanPerusahaan -> nip}}"></td>
                </tr>
                <tr>
                    <th scope="col">Jenis Kelamin</th>
                    <td>:</td>
                    <td>
                        <select name="jenkel" class="form-select form-select" aria-label=".form-select" required>
                            <option selected hidden>{{ $pimpinanPerusahaan-> jenkel }}</option>
                            <option value="L">L</option>
                            <option value="P">P</option>
                        </select>
                    </td>
                </tr>
                <th scope="col">Pangkat</th>
                    <td>:</td>
                    <td><input type="text" class="form-control" name="pangkat" placeholder="pangkat" value="{{ $pimpinanPerusahaan -> pangkat}}"></td>
                </tr>
                <tr>
                    <th scope="col">Perusahaan</th>
                    <td>:</td>
                    <td><select name="id_perusahaan" class="form-select form-selepimpinanPeruusahaanct" aria-label=".form-select" required>
                        <option selected hidden>{{ $pimpinanPerusahaan -> perusahaan -> nama }}</option>
                        <option selected value="{{$pimpinanPerusahaan -> perusahaan -> id}}">{{$pimpinanPerusahaan -> perusahaan -> nama}}</option>
                        @foreach ($role as $g )
                            <option value="{{ $g->id }}">{{ $g -> nama }}</option>
                        @endforeach
                      </select></td>
                </tr>
            </thead>
        </table>
        <div class="d-flex justify-content-end">
            <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin mengedit {{ $pimpinanPerusahaan -> nama }}?`)">Submit</button></div>
    </form>
    </div>
</div>

@endsection
