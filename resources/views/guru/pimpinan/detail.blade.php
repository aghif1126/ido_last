{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>Detail Pimpinan Perusahaan</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-pimpinan">Daftar Pimpinan</a></li>
          <li class="breadcrumb-item active">Detail Pimpinan</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Detail Pimpinan Perusahaan</h3></div>
        <a href="/guru/edit-pimpinan/{{ $pimpinanPerusahaan -> id }}"><button type="button" class="btn btn-secondary mx-3 align-self-end">Edit</button></a>
    </div>
<hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td>{{ $pimpinanPerusahaan -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">NIP</th>
                    <td>:</td>
                    <td>{{ $pimpinanPerusahaan -> nip}}</td>
                </tr>
                <tr>
                    <th scope="col">Jenis Kelamin</th>
                    <td>:</td>
                    <td>{{ $pimpinanPerusahaan -> jenkel}}</td>
                </tr>
                <tr>
                <th scope="col">Pangkat</th>
                    <td>:</td>
                    <td>{{ $pimpinanPerusahaan -> pangkat}}</td>
                </tr>
                <tr>
                    <th scope="col">Perusahaan</th>
                    <td>:</td>
                    <td>{{ $pimpinanPerusahaan -> perusahaan -> nama}}</td>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

@endsection
