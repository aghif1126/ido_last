{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>Detail Konsentrasi Keahlian</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-konsentrasi">Daftar Konsentrasi</a></li>
          <li class="breadcrumb-item active">Detail Konsentrasi</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Detail Konsentrasi Keahlian </h3>
        </div>
        <a href="/guru/edit-konsentrasi/{{ $id }}"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Edit</button></a>

    </div>
    <hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td>{{ $konsentrasi -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Jurusan</th>
                    <td>:</td>
                    <td>{{ $konsentrasi -> jurusan -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Kepala Progam</th>
                    <td>:</td>
                        @if (is_null($konsentrasi ->jurusan -> kaprog))
                    <td>Tidak Tersedia</td>
                        @else
                    <td><a href="/guru/detail-guru/{{ $konsentrasi -> jurusan -> kaprog -> id }}"><button>{{ $konsentrasi -> jurusan -> kaprog -> nama}}</button></a></td>
                    @endif
                </tr>
                <tr>
                    <th scope="col">Jumlah Kelas</th>
                    <td>:</td>
                    <td>{{ $kelas -> count()}}</td>
                </tr>
                <tr>
                    <th scope="col">Kelas Terkait</th>
                    <td>:</td>
                    <td>
                        @foreach ($kelas as $a)

                        <a href="/guru/detail-kelas/{{ $a -> id }}"><button>{{ $a -> nama}}</button></a>
                        @endforeach
                    </td>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

@endsection
