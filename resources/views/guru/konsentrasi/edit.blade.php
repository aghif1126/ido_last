{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>Edit Konsentrasi Keahlian</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-konsentrasi">Daftar Konsentrasi</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-konsentrasi/{{$konsentrasi -> id}}">Detail Konsentrasi</a></li>
          <li class="breadcrumb-item active">Edit Konsentrasi</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Detail Konsentrasi Keahlian </h3>
        </div>
        {{-- <a href="/guru/add-perusahaan"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
        <a href="/guru/delete-perusahaan"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Hapus</button></a> --}}

    </div>
    <hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td> {{ $konsentrasi -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Alamat</th>
                    <td>:</td>
                    <td>{{ $konsentrasi -> jurusan -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">Email</th>
                    <td>:</td>
                    <td>{{ $konsentrasi -> jurusan -> kaprog -> nama}}</td>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Edit Konsentrasi </h3>
        </div>
        {{-- <a href="/guru/add-perusahaan"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
        <a href="/guru/delete-perusahaan"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Hapus</button></a> --}}

    </div>
    <hr>
    <form action="/guru/update-konsentrasi" method="post">
        @csrf
        <div class="table-responsive">
            <table class="table ">
                <thead>
                    <input type="text" class="form" name="id" placeholder="Website" value="{{ $konsentrasi->id }}"
                        hidden>
                    <tr>
                        <th scope="col">Nama</th>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="nama" onkeydown="return /[a-z A-Z .]/i.test(event.key)" placeholder="Website"
                                value="{{  $konsentrasi -> nama }}" required></td>
                    </tr>
                    <tr>
                        <th scope="col">Jurusan</th>
                        <td>:</td>
                        <td>
                            <select name="id_jurusan" class="form-select "
                                aria-label=".form-select example" required>
                                <option selected value="{{ $konsentrasi -> id_jurusan }}">
                                    {{ $konsentrasi -> jurusan -> nama }}</option>
                                @foreach ($jurusan as $g )
                                <option value="{{ $g->id }}">{{ $g->nama }}</option>

                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">Kaprog</th>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="" placeholder="Otomatis"
                                value="{{ $konsentrasi -> jurusan -> kaprog -> nama}}" readonly></td>
                    </tr>
                </thead>
            </table>
            <div class="d-flex justify-content-end">
                <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin mengedit {{ $konsentrasi -> nama }}?`)">Simpan</button></div>
    </form>
</div>
</div>
@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
