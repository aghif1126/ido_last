{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>hapus Konsentrasi Keahlian</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-konsentrasi">Daftar Konsentrasi</a></li>
          <li class="breadcrumb-item active">Hapus Konsentrasi</li>
        </ol>
      </nav>
@endsection

@section('content')


<form action="destroy-konsentrasi" method="POST">
    @csrf
<div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white" id="siswa">
        <div class="d-flex flex-row">
            <div class="me-auto p-2">
                <h3>Pilih konsentrasi keahlian yang akan dihapus</h3>
            </div>

        </div>
        <hr>
        <div class="table-responsive">
            <table class="table ">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        {{-- <th scope="col">Action</th> --}}
                        {{-- <th scope="col">Jurusan</th> --}}
                    </tr>
                </thead>
                <tbody>

                    @foreach ($konsentrasi as $a)


                    <tr>
                        <td>{{ $konsentrasi -> firstItem() + $loop->index }}</td>
                        <td><button value="{{  $a -> id }}" name="id" type="submit" onClick="return confirm(`Yakin ingin menghapus {{ $a -> nama }}?`)">{{ $a -> nama}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="container">
                {{ $konsentrasi->links() }}
            </div>
        </div>
    </div>
</div>
</form>
@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
