{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<div class="d-flex flex-row">
    <div class="me-auto p-2">
    <h1>Daftar Konsentrasi Keahlian</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
            <li class="breadcrumb-item active">Daftar Konsentrasi</li>
        </ol>
    </nav>
    </div>
    <div class="col-sm-12 col-md-5 mb-md-0 me-2">
        <form action="">
            {{-- @csrf --}}
            <select class="form-control custom-select select2 ng-pristine ng-valid ng-not-empty ng-touched" ng-model="" name="tahun" id="" ng-change="">
                @foreach ($tahun as $item)
                <option value="{{ $item -> id }}" class="ng-binding ng-scope" {{ $item -> id == $tahun_pelajaran -> id ? 'selected' : '' }}>Tahun Pelajaran {{ $item -> tahun }} - ({{ $item -> status }})</option>
                @endforeach
            </select>
        </div>
            <div>
                <input type="submit" value="Terapkan" class="btn border border-info" >
            </div>
        </form>
</div>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white" id="siswa">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Konsentrasi Keahlian</h3>
        </div>
        <a href="add-konsentrasi"><button type="button"
                class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
        {{-- <a href="delete-konsentrasi"><button type="button" class="btn btn-secondary mx-3 align-self-end">Hapus</button></a> --}}

    </div>
    <hr>
    <div class="table-responsive">
        <table class="table " id="mainTable">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    {{-- <th scope="col">Action</th> --}}
                    {{-- <th scope="col">Jurusan</th> --}}
                </tr>
            </thead>
            <tbody>

                @foreach ($konsentrasi as $a)


                <tr>
                    <td class="number"></td>
                    <td><a href="detail-konsentrasi/{{ $a -> id }}"><button class="btn">{{ $a -> nama}}</button></td>
                    {{-- <td><a href="">Edit</a></td> --}}

                    {{-- <td>{{ $a -> jurusan -> nama}}</td> --}}

                </tr>
                {{-- <tr>
                    <td scope="row">Item</td>
                    <td>Item</td>
                    <td>Item</td>
                </tr> --}}
                @endforeach

            </tbody>



        </table>
        {{-- <div class="container">
            {{ $konsentrasi->links() }}
        </div> --}}
    </div>
</div>
</div>

<script>
    $(document).ready(function(){
    var table = $("#mainTable").DataTable({
        // "pageLength": 15,
        // "searching": false,
        "lengthChange": false,
        // "pagingType": "simple_numbers",
        "paging": false,

        columnDefs: [
        {
            searchable: false,
            orderable: false,
            targets: 0
        }],

        order: [[1, 'asc']]
    });

    table
    .on('order.dt search.dt', function () {
        let i = 1;

        table
            .cells(null, 0, { search: 'applied', order: 'applied' })
            .every(function (cell) {
                this.data(i++);
            });
    })
    .draw();
});
</script>

@endsection
