{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Perusahaan
@endsection
@section('title')
<div class="d-flex flex-row">
    <div class="me-auto p-2">
        <h1>Pilih Perusahaan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></td>
                <li class="breadcrumb-item active">Pilih Perusahaan</li>
            </ol>
      </nav>
    </div>

@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Daftar Perusahaan</h3></div>
        <div class="col-12 col-sm-8 col-md-4">
            <form action="">
                <div class="input-group mb-3 ">
                    <input type="serach" class="form-control" placeholder="Search Data" name="keyword" id="myInput">
                    {{-- <button class="btn btn-outline-secondary" type="submit" id="button-addon1">Search</button> --}}
                </div>
            </form>
        </div>
        {{-- <a href="add-perusahaan"><button type="button" class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
        <a href="delete-perusahaan"><button type="button" class="btn btn-secondary mx-3 align-self-end">Hapus</button></a> --}}

    </div>
<hr>
    <div class="table-responsive">
        <table class="table" id="mainTable">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Jurusan</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($perusahaan as $a)


                <tr>
                    <form action="add-pembimbing" method="POST">

                        @csrf
                    <td scope="row" id="number">{{ $loop->iteration }}</td>
                    <td>        <input type="text" name="id_perusahaan" value="{{ $a -> id }}" hidden>
                        <input type="submit" class="btn" value="{{ $a -> nama}}" hidden >
                        {{-- {{ $a -> nama}} --}}
                        <button type="submit" class="link-dark link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover" onClick="return confirm(`Yakin ingin melanjutkan?`)">
                        {{$a -> nama}}
                    </button>
                    </td>
                    <td>{{ $a -> alamat}}</td>

                    <td>
                        @foreach ($a -> jurusan as $jurusan)
                        {{ $jurusan -> detailjurusan -> nama }} <br>
                        @endforeach

                    </td>
                </form>
                </tr>

                @endforeach
            </tbody>
        </table>

    </div>
</div>
</div>

@endsection

<script>
    $(document).ready(function(){
        var table = $("#mainTable").DataTable({
            "pageLength": 15,
            // "searching": false,
            "lengthChange": false,
            "pagingType": "simple_numbers",

            columnDefs: [
            {
                searchable: true,
                orderable: false,
                targets: 0
            }],

            order: [[1, 'asc']]
        });

        table
        .on('order.dt search.dt', function () {
            let i = 1;

            table
                .cells(null, 0, { search: 'applied', order: 'applied' })
                .every(function (cell) {
                    this.data(i++);
                });
        })
        .draw();

        $("#myInput").on("keyup", function() {
            table.search( this.value ).draw();
        });
    });
</script>
