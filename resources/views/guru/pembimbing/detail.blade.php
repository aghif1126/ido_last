{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>Detail Pembimbing Perusahaan</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-pembimbing">Daftar pembimbing</a></li>
          <li class="breadcrumb-item active">Detail Pembimbing</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2"> <h3>Detail Pembimbing</h3></div>
        <a href="../edit-pembimbing/{{ $pembimbing -> id }}"><button type="button" class="btn btn-secondary mx-3 align-self-end">Edit</button></a>
    </div>
<hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">NIP</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> nip}}</td>
                </tr>
                <tr>
                <th scope="col">Alamat</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> alamat}}</td>
                </tr>
                <tr>
                <th scope="col">No. Telp</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> telp}}</td>
                </tr>
                <tr>
                    <th scope="col">Jenis Kelamin</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> jenkel}}</td>
                </tr>
                <tr>

                    <th scope="col">Nama Perusahaan</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> perusahaan -> nama}}</td>
                </tr>
                <tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
</div>


@endsection
