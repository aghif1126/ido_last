{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('title')
<h1>Edit Pembimbing Perusahaan</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-pembimbing">Daftar pembimbing</a></li>
          <li class="breadcrumb-item"><a href="/guru/detail-pembimbing/{{$pembimbing -> id}}">Detail pembimbing</a></li>
          <li class="breadcrumb-item active">Edit Pembimbing</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Detail {{ $pembimbing -> nama }} </h3>
        </div>
    </div>
    <hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> nama}}</td>
                </tr>
                <tr>
                    <th scope="col">NIP</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> nip}}</td>
                </tr>
                <tr>
                    <th scope="col">Alamat</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> alamat}}</td>
                </tr>
                <tr>
                    <th scope="col">No. Telp</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> telp}}</td>
                </tr>
                <tr>
                    <th scope="col">Jenis Kelamin</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> jenkel}}</td>
                </tr>
                <tr>
                    <th scope="col">Perusahaan</th>
                    <td>:</td>
                    <td>{{ $pembimbing -> perusahaan -> nama}}</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
</div>

<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Edit Pembimbing </h3>
        </div>
    </div>
    <hr>

<form action="/guru/update-pembimbing" method="post">
    @csrf


    <div class="table-responsive">
            <table class="table ">
                <thead>
                    <input type="text" class="form" name="id" placeholder="Website" value="{{ $pembimbing->id }}" hidden>
                    <tr>
                        <th scope="col">Nama</th>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="nama" onkeydown="return /[a-z A-Z .]/i.test(event.key)" placeholder="Nama"
                                value="{{  $pembimbing -> nama }}" required></td>
                    </tr>
                    <tr>
                        <th scope="col">NIP</th>
                        <td>:</td>
                        <td><input type="number" class="form-control" name="nip" placeholder="NIP"
                                value="{{  $pembimbing -> nip }}" required></td>
                    </tr>
                    <tr>
                        <th scope="col">Alamat</th>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="alamat" placeholder="Alamat"
                                value="{{  $pembimbing -> alamat }}" required></td>
                    </tr>
                    <tr>
                        <th scope="col">No. Telp</th>
                        <td>:</td>
                        <td><input type="number" class="form-control" name="telp" placeholder="No. Telp"
                                value="{{  $pembimbing -> telp }}" required></td>
                    </tr>
                    <tr>
                        <th scope="col">Jenis Kelamin</th>
                        <td>:</td>
                        <td><select name="jenkel" class="form-select form-select" aria-label=".form-select" required>
                                    <option selected hidden>{{ $pembimbing-> jenkel }}</option>
                                    <option value="L">L</option>
                                    <option value="P">P</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">Perusahaan</th>
                        <td>:</td>
                        <td><select name="id_perusahaan" class="form-select form-select" aria-label=".form-select" required>
                            <option selected hidden>{{ $pembimbing-> perusahaan -> nama }}</option>
                            <option selected value="{{$pembimbing -> perusahaan -> id}}" hidden>{{$pembimbing -> perusahaan -> nama}}</option>
                            @foreach ($role as $g )
                                <option value="{{ $g->id }}">{{ $g -> nama }}</option>
                            @endforeach
                          </select></td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
    </div>
    <div class="d-flex justify-content-end">
        <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin mengedit {{ $pembimbing -> nama }}?`)">Simpan</button></div>
    </form>
</div>
</div>

@endsection
