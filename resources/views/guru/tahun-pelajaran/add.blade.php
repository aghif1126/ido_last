{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Tahun Pelajaran
@endsection
@section('title')
<h1>Tambah Tahun Pelajaran</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-tahun-pelajaran">Daftar Tahun Pelajaran</a></li>
          <li class="breadcrumb-item active">Tambah Tahun Pelajaran</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <form action="store-tahun-pelajaran" method="post">
        @csrf
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Tahun Pelajaran</th>
                    <td>:</td>
                    <td><input type="text" class="form-control" name="tahun" placeholder="Tahun Pelajaran" required></td>
                </tr>
            </thead>
        </table>
        {{-- <div class="mb-3 d-flex">
            <label for="nis" class="form-label fs-4">Tahun Pelajaran : </label>
            <input type="text" class="form-control" name="tahun"  placeholder="Name" required>
        </div> --}}
        <div class="d-flex justify-content-end">
            <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin menambahkan Tahun Pelajaran?`)">Submit</button></div>
    </form>
</div>
</div>

@endsection

<script>
    $(document).on('submit', '[id^=tableDelete_]' , function() {
        return callAjax( $(this).serialize() );
        });

        function callAjax( data ) {
            $.ajax({
            type : 'POST',
            url  : 'call/page.php',
            data : data,
            success :  function(data) { ... },
            error: function(data) { ... }
        });
        return false;
        };
</script>
