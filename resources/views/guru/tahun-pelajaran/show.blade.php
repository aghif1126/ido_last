{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
- Tahun Pelajaran
@endsection
@section('title')
<h1>Daftar Tahun Pelajaran</h1>
<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Daftar Tahun Pelajaran</li>
    </ol>
</nav>
</div>
@endsection

@section('content')
<div class="col-12">
    <div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white" id="siswa">
        <div class="d-flex flex-row">
            <div class="me-auto p-2">
                <h3>Jurusan</h3>
            </div>
            <a href="add-tahun-pelajaran"><button type="button"
                    class="btn btn-secondary mx-3 align-self-end">Tambah</button></a>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table ">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Tahun Pelajaran</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tahun as $a)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>
                            {{-- <a href="detail-tahun-pelajaran/{{ $a -> id }}"> --}}
                                <button class="btn">{{ $a -> tahun}}</button>
                            {{-- </a> --}}
                        </td>
                        <td>{{ $a -> status }}</td>
                        <td>
                            <a href="ganti-tahun-pelajaran/{{ $a -> id }}"><button class="btn">Ganti Status</button></a>
                            <a href="edit-tahun-pelajaran/{{ $a -> id }}"><button class="btn">Edit</button></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="container">
                {{-- {{ $tahun->links() }} --}}
            </div>
        </div>
    </div>
</div>

@endsection
