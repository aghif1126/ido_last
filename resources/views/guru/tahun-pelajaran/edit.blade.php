{{-- create double slider registration and login page using php and bootstrap? --}}
@extends('layout.guru')
@section('judul')
    - Tahun Pelajaran
@endsection
@section('title')
<h1>Edit Tahun Pelajaran</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/guru/dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="/guru/show-tahun-pelajaran">Daftar Tahun Pelajaran</a></li>
          {{-- <li class="breadcrumb-item"><a href="/guru/detail-tahun-pelajaran/{{$tahun -> id}}">Detail Tahun Pelajaran</a></li> --}}
          <li class="breadcrumb-item active">Edit Tahun Pelajaran</li>
        </ol>
      </nav>
@endsection

@section('content')
<div class="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Detail Tahun Pelajaran </h3>
        </div>

    </div>
    <hr>
    <div class="table-responsive">
        <table class="table ">
            <thead>
                <tr>
                    <th scope="col">Tahun Pelajaran</th>
                    <td>:</td>
                    <td>{{ $tahun -> tahun}}</td>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
</div>

<div clas="col-12">
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Edit Tahun Pelajaran </h3>
        </div>

    </div>
    <hr>
    <form action="/guru/update-tahun-pelajaran" method="post">
        @csrf
        <div class="table-responsive">
            <table class="table ">
                <thead>
                    <input type="text" class="form" name="id" placeholder="Website" value="{{ $tahun->id }}" hidden>
                    <tr>
                        <th scope="col">Tahun Pelajaran</th>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="tahun" placeholder="Website"
                                value="{{  $tahun -> tahun }}" required></td>
                    </tr>
                </thead>
            </table>
            <div class="d-flex justify-content-end">
                <button type="submit" class="btn btn-secondary" style="width: 25%;" onClick="return confirm(`Yakin ingin mengedit {{ $tahun -> nama }}?`)">Simpan</button></div>
    </form>
</div>
</div>

@endsection
