@extends('layout.siswa')


@section('header')
    <div class="card-header">
        <h3>Riwayat Kehadiran</h3>
    </div>
@endsection

@section('content')
<style>
    @media (max-width: 600px) {
        img {
            width: 150%; 
            height: 150%;
        }
    }
</style>
    <div class="card-body">
        @foreach ($absen as $r)
    <div class="accordion" id="accordionExample">
        <div class="accordion-item bg-light">
            <h2 class="accordion-header" id="heading{{ $loop -> iteration }}">
                <button class="accordion-button collapsed btn btn-outline-secondary w-100" type="button" data-bs-toggle="collapse"
                    data-bs-target="#collapse{{ $loop -> iteration }}" aria-expanded="false"
                    aria-controls="collapse{{ $loop -> iteration }}">
                    Tanggal : {{ $r -> tanggal }}
                    {{-- {{ $r -> aktivitas }} --}}
                </button>
            </h2>
            <div id="collapse{{ $loop -> iteration }}" class="accordion-collapse collapse"
                aria-labelledby="heading{{ $loop -> iteration }}" data-bs-parent="#accordionExample">

                <div class="accordion-body">
                    <h4>Detail Absen</h4>
                    <div class="table-responsive">

                        <table class="table ">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 20%">Jam Masuk</th>
                                    <td>:</td>
                                    <td>{{ $r -> jam_masuk }}</td>
                                    {{-- <th scope="col" style="width: 20%">Jam Pulang</th>
                                    <td>:</td>
                                    <td>{{ $r -> jam_masuk }}</td> --}}
                                </tr>
                                <tr>
                                    @if ($r->status == "Hadir")
                                    <th scope="col" style="width: 20%">Poto</th>
                                    <td>:</td>
                                    <td>@if ($r -> poto_masuk)
                                            <img src="{{ asset('uploads/absen/masuk/uploads'.$r->poto_masuk) }}" style="border-radius:10px" alt=""
                                            height="240px" width="320px">
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                <tr>
                                    <th scope="col" style="width: 20%">Status</th>
                                    <td>:</td>
                                    <td >{{ $r -> status }}</td>
                                </tr>

                                @if ($r->jam_pulang != "Tidak Ada")
                                <tr>
                                    <th scope="col" style="width: 20%">Pulang</th>
                                    <td>:</td>
                                    <td>{{ $r -> jam_pulang }}</td>
                                </tr>
                                <tr>
                                    <th scope="col" style="width: 20%">Poto Pulang</th>
                                    <td>:</td>
                                    <td>
                                        <img src="{{ asset('uploads/absen/pulang/uploads'.$r->poto_pulang) }}" style=" border-radius:10px" alt=""
                                            height="240px" width="320px">
                                    </td>
                                </tr>
                                @endif


                            </thead>

                        </table>
                    </div>
                </div>

            </div>




            <div id="collapse{{ $loop -> iteration }}" class="accordion-collapse collapse"
                aria-labelledby="heading{{ $loop -> iteration }}" data-bs-parent="#accordionExample">
                @foreach ($r -> aktivitas -> where('tanggal', $r -> tanggal) as $aktivitas)
                <div class="accordion-body">
                    <br>
                    <h4>Kegiatan {{ $loop-> iteration }}</h4>
                    <div class="table-responsive">

                        <table class="table ">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 20%">Kegiatan</th>
                                    <td>:</td>
                                    <td>{{ $aktivitas -> kegiatan }}</td>
                                </tr>
                                {{-- <tr>
                                    <th scope="col" style="width: 20%">Divisi</th>
                                    <td>:</td>
                                    <td>{{ $aktivitas -> divisi }}</td>
                                </tr> --}}
                                {{-- <tr>
                                    <th scope="col" style="width: 20%">Tanggal</th>
                                    <td>:</td>
                                    <td>{{ $aktivitas -> tanggal }}</td>
                                </tr> --}}

                                <tr>
                                    <th scope="col" style="width: 20%">Mulai</th>
                                    <td>:</td>
                                    <td>{{ $aktivitas -> mulai }}</td>
                                </tr>
                                <tr>
                                    <th scope="col" style="width: 20%">Selesai</th>
                                    <td>:</td>
                                    <td>{{ $aktivitas -> selesai }}</td>
                                </tr>
                                <tr>
                                    <th scope="col" style="width: 20%">Perusahaan</th>
                                    <td>:</td>
                                    <td>{{ $aktivitas -> perusahaan -> nama }}</td>
                                </tr>


                            </thead>

                        </table>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @endforeach
    </div>

{{-- ini dropdownnya --}}
<script src="{{asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

@endsection