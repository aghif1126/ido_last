@extends('layout.siswa')


@section('header')
    <div class="card-header">
        <h3>Tempat PKL</h3>
    </div>
@endsection


@section('content')
@if (!is_null($perusahaan))
@foreach ($siswa -> perusahaan -> where('confirm', 'Sudah') -> all() as $perusahaan)

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                       <tbody>
                        <tr>
                            <th scope="row">Perusahaan</th>
                            <td>:</td>
                            <td>{{ $perusahaan -> detail -> nama}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Periode</th>
                            <td>:</td>
                            <td>
                                {{ $bulan[intval(explode('-', $perusahaan -> awal_periode)[1])]}} ({{ explode('-', $perusahaan -> awal_periode)[0] }}) -
                                {{ $bulan[intval(explode('-', $perusahaan -> akhir_periode)[1])]}} ({{ explode('-', $perusahaan -> akhir_periode)[0] }})
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Alamat</th>
                            <td>:</td>
                            <td>{{ $perusahaan -> detail -> alamat }}</td>
                        </tr>
                        <tr>
                            <th scope="row">No Telepon</th>
                            <td>:</td>
                            <td>{{ $perusahaan -> detail -> telp }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Fax</th>
                            <td>:</td>
                            <td>{{ $perusahaan -> detail ->fax }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Email</th>
                            <td>:</td>
                            <td>{{ $perusahaan -> detail -> email }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Website</th>
                            <td>:</td>
                            <td>{{ $perusahaan -> detail -> website }}</td>
                        </tr>
                        @if ($perusahaan -> detail -> pembimbing -> where('id_jurusan', $siswa -> kelas -> konsentrasi -> jurusan -> id))
                        @foreach ($perusahaan -> detail -> pembimbing -> where('id_jurusan', $siswa -> kelas -> konsentrasi -> jurusan -> id) as $pembimbing )

                                <tr>
                                    <th scope="row">Pembimbing</th>
                                    <td>:</td>
                                    <td>{{ $pembimbing -> nama }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">No Telepon Pembimbing</th>
                                    <td>:</td>
                                    <td>{{ $pembimbing -> telp }}</td>
                                </tr>

                        @endforeach
                        @endif
                       </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endforeach
@endif
@endsection