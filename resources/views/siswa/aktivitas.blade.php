@extends('layout.siswa')

@section('dashboard')

<style>
    .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 100px;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        position: relative;
        background-color: #fefefe;
        margin: auto;
        padding-left: 0.1%;
        padding-right: 0.1%;
        border: 1px solid #888;
        width: 40%;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        -webkit-animation-name: animatetop;
        -webkit-animation-duration: 0.4s;
        animation-name: animatetop;
        animation-duration: 0.4s
    }

    @media (max-width: 600px) {
        .modal-content {
            width: 90%;
        }

        #video {
            max-width: 240px;
            max-height: 240px;
            margin-left: -9%;
            margin-right: -20%;
        }

        #photo {
            /* border: 1px solid black;
        box-shadow: 2px 2px 3px black; */
            max-width: 240px;
            max-height: 200px;
            margin-left: -9%;
            margin-right: -20%;
        }

        /* #photo {
            max-width: 200px;
            max-height: 200px;
            margin-left: -9%;
            margin-right: -20%;
        } */

        #photo {
            /* border: 1px solid black;
        box-shadow: 2px 2px 3px black; */
            max-width: 240px;
            max-height: 200px;
            margin-left: -9%;
            margin-right: -20%;
        }

        /* #photo {
            max-width: 200px;
            max-height: 200px;
            margin-left: -9%;
            margin-right: -20%;
        } */

        #results img {
            max-width: 80%;
            max-height: 80%;
        }

        #my_camera1 video {
            max-width: 75%;
            max-height: 75%;
            margin-left: 29%;
            margin-right: -20%;
        }

        #results1 img {
            max-width: 80%;
            max-height: 80%;
        }
    }

    /* Add Animation */
    @-webkit-keyframes animatetop {
        from {
            top: -300px;
            opacity: 0
        }

        to {
            top: 0;
            opacity: 1
        }
    }

    @keyframes animatetop {
        from {
            top: -300px;
            opacity: 0
        }

        to {
            top: 0;
            opacity: 1
        }
    }

    /* The Close Button */
    .close {
        color: grey;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    .modal-header {
        padding: 2px 16px;
        /* background-color: red; */
        color: black;
    }

    .modal-body {
        padding: 2px 16px;
    }

    .modal-footer {
        padding: 2px 16px;
        background-color: #5cb85c;
        color: white;
    }

    #video {
        /* border: 1px solid black;
        box-shadow: 2px 2px 3px black; */
        width: 320px;
        height: 240px;
        transform: rotateY(3.142rad);
    }

    #photo {
        /* border: 1px solid black;
        box-shadow: 2px 2px 3px black; */
        width: 320px;
        height: 240px;
        display: none;
    }

    #canvas {
        display: none;
    }

    .camera {
        width: 340px;
        display: inline-block;
    }

    .output {
        width: 340px;
        display: inline-block;
        vertical-align: top;
    }

    /* #startbutton {
        display: block;
        position: relative;
        margin-left: auto;
        margin-right: auto;
        bottom: 32px;
        background-color: rgba(0, 150, 0, 0.5);
        border: 1px solid rgba(255, 255, 255, 0.7);
        box-shadow: 0px 0px 1px 2px rgba(0, 0, 0, 0.2);
        font-size: 14px;
        font-family: "Lucida Grande", "Arial", sans-serif;
        color: rgba(255, 255, 255, 1);
        position: absolute;
      } */

    .contentarea {
        font-size: 16px;
        font-family: "Lucida Grande", "Arial", sans-serif;
        width: 760px;
    }
</style>

    @if ($perusahaan)


@if ($siswa -> perusahaan == "[]")
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h3>Anda Belum Dipetakan</h3>
            </div>
        </div>
    </div>
</div>
@else
<div
    style="background-color: rgb(10, 115, 235);margin-right: -25px; margin-left: -25px; margin-top:-10px; padding-right: 12.5px; padding-left: 12.5px; 12.5px; padding-top: 10.5px; 12.5px; padding-bottom: 10.5px;">
    {{-- @dd($absen) --}}
    @if ($absen)
    <h3 class="text-white">Kegiatan Harian </h3>
    <i class="text-white">Anda sudah absen hari ini.</i>
    @else
    <h3 class="text-white">Ayo Absen Untuk Hari ini</h3>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    @if ($absen && $absen->poto_pulang == "Tidak Ada" && $absen -> status == "Hadir")
                    <h5>Presensi pulang</h5>
                    <button class="btn btn-info" id="myBtn">Absen</button>
                    <!-- The Modal -->
                    <div id="myModal" class="modal">

                        <!-- Modal content -->
                        <div class="modal-content">
                            <div class="modal-header">
                                <span class="close">&times;</span>
                                {{-- <h2>Modal Header</h2> --}}
                            </div>
                            <div class="modal-body mb-3">
                                <form method="POST" action="/siswa/absenpulang">
                                    @csrf
                                    <div class="row">
                                        <div class="camera" style="margin-left: 18%;">
                                            <video id="video">Video stream not available.</video>
                                            <canvas id="canvas"> </canvas>
                                            <div class="output" style="border-radius: 0px;">
                                                <img id="photo" style="border-radius: 0px; margin-bottom: 20px;"
                                                    alt="The screen capture will appear in this box." />
                                            </div>
                                        </div>
                                        <input type="hidden" name="photo" value="" id="photo1">
                                        <input type="hidden" name="id_siswa" value="{{ $siswa->nis }}">
                                    </div>
                                    <div class="d-flex flex-row bd-highlight mb-3">
                                        <button class="p-2 btn btn-secondary ml-3" type="button" id="startbutton"
                                            onclick="replace('video', 'photo'); replace1('submit')">Take photo</button>
                                        <button class="p-2 btn btn-secondary ml-3"
                                            onclick="replace('photo', 'video'); close1('submit')"
                                            type="button">Ulang</button>
                                        <button class="p-2 btn btn-secondary ml-3" id="submit" type="submit"
                                            style="display: none">Submit</button>
                                    </div>
                                </form>
                            </div>
                            {{-- <div class="modal-footer">
                                <h3>Modal Footer</h3> --}}
                            </div>
                        </div>
                        <hr>
                        @else

                        @endif
                        @if (is_null($absen))

                        <div class="d-flex flex-row">
                            <button class="btn btn-success p-2 ml-3" id="myBtn">Hadir</button>

                            <form action="/siswa/absen" method="post">
                                @csrf
                                <input type="hidden" name="id_siswa" value="{{ $siswa->nis }}">
                                <input type="hidden" name="status" value="Sakit">
                                <input type="hidden" name="id_perusahaan" value="{{$perusahaan ->detail -> id}}">
                                <button class="btn btn-success p-2 ml-3" type="submit">Sakit</button>
                            </form>

                            <form action="/siswa/absen" method="post">
                                @csrf
                                <input type="hidden" name="id_siswa" value="{{ $siswa->nis }}">
                                <input type="hidden" name="status" value="Izin">
                                <input type="hidden" name="id_perusahaan" value="{{$perusahaan ->detail -> id}}">
                                <button class="btn btn-success p-2 ml-3" type="submit">Izin</button>
                            </form>
                        </div>
                        @endif

                        @endif


                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>


    @endif


  @endsection

    {{-- @section('header')
    <div class="card-header">
        <h3>Kegiatan Harian</h3>
        @if ($absen)
        <i>Anda sudah absen hari ini.</i>
        @endif
    </div>
    @endsection --}}

    @section('content')


    @if ($perusahaan)
    <div class="card-body">
        {{-- @if ($absen)
        <h5>Presensi</h5>
        Anda sudah absen hari ini.
        <hr>
        <br>
        @endif --}}

        @if (is_null($absen))

        {{-- <div class="d-flex flex-row">
            <button class="btn btn-success p-2 ml-3" id="myBtn">Hadir</button>

            <form action="/siswa/absen" method="post">
                @csrf
                <input type="hidden" name="id_siswa" value="{{ $siswa->nis }}">
                <input type="hidden" name="status" value="Sakit">
                <button class="btn btn-success p-2 ml-3" type="submit">Sakit</button>
            </form>

            <form action="/siswa/absen" method="post">
                @csrf
                <input type="hidden" name="id_siswa" value="{{ $siswa->nis }}">
                <input type="hidden" name="status" value="Izin">
                <button class="btn btn-success p-2 ml-3" type="submit">Izin</button>
            </form>
        </div> --}}

        <br>

        @elseif($absen->status == "Hadir" && $absen->poto_pulang == "Tidak Ada")

        <h5>Pengisian Laporan Kegiatan Harian</h5>
        <form method="post" action="/siswa/aktivitas/isi">
            @csrf
            <input type="hidden" name="id_siswa" value="{{ $siswa->nis }}">
            <input type="hidden" name="id_perusahaan" value="{{ $perusahaan -> detail -> id }}">
            <input type="hidden" name="tanggal" value="{{ $tanggal }}">
            <div class="form-group">
                <label for="input-1">Kegiatan</label>
                <textarea name="kegiatan" id="" rows="3" placeholder="Kegiatan yang saya lakukan hari ini ..."
                    class="form-control" required></textarea>
                {{-- <input type="textarea" class="form-control" id="input-1" placeholder="Enter Your Name"> --}}
            </div>
            <div class="col-md-4">

                <div class="form-group">
                    <label for="input-4">Jam Datang</label>
                    <input type="time" name="mulai" class="form-control" required>
                    {{-- <input type="text" class="form-control" id="input-4" placeholder="Enter Password"> --}}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="input-4">Jam Kembali</label>
                    <input type="time" name="selesai" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <div class="d-flex flex-row-reverse">
                    <button type="submit" class="btn btn-secondary px-5">Submit</button>
                </div>
            </div>
        </form>

        @else

        @endif
        @if ($siswa -> perusahaan == "[]")
        @else

        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close">&times;</span>
                    {{-- <h2>Modal Header</h2> --}}
                </div>
                <div class="modal-body mb-3">
                    <form method="POST" action="/siswa/absen">
                        @csrf
                        <div class="row">
                            <div class="camera" style="margin-left: 18%;">
                                <video id="video">Video stream not available.</video>
                                <canvas id="canvas"> </canvas>
                                <div class="output" style="border-radius: 0px;">
                                    <img id="photo" style="border-radius: 0px; margin-bottom: 20px;"
                                        alt="The screen capture will appear in this box." />
                                </div>
                            </div>
                            <input type="hidden" name="photo" value="" id="photo1">
                            <input type="hidden" name="status" value="Hadir">
                            <input type="hidden" name="id_perusahaan" value="{{ $perusahaan -> detail -> id }}">
                            <input type="hidden" name="id_siswa" value="{{ $siswa->nis }}">
                        </div>
                        <div class="d-flex flex-row bd-highlight mb-3">
                            <button class="p-2 btn btn-secondary ml-3" type="button" id="startbutton"
                                onclick="replace('video', 'photo'); replace1('submit')">Take photo</button>
                            <button class="p-2 btn btn-secondary ml-3"
                                onclick="replace('photo', 'video'); close1('submit')" type="button">Ulang</button>
                            <button class="p-2 btn btn-secondary ml-3" id="submit" type="submit"
                                style="display: none">Submit</button>
                        </div>
                    </form>
                </div>
                {{-- <div class="modal-footer">
                    <h3>Modal Footer</h3> --}}
                </div>
            </div>
        </div>
        @endif
        <!-- The Modal -->

        <script>
            function replace(hide, show){
            document.getElementById(hide).style.display='none';
            document.getElementById(show).style.display='block';
        }
        function replace1(show){
            document.getElementById(show).style.display='block';
        }

        function close1(hide){
            document.getElementById(hide).style.display='none';
        }
        </script>

        <script type="text/javascript">
            (() => {
                var modal = document.getElementById("myModal");

                // Get the button that opens the modal
                var btn = document.getElementById("myBtn");
                var ambilpoto = document.getElementById("ambilpoto");
                // document.getElementById("submitpoto").style.display = "none";

                // Get the <span> element that closes the modal
                var span = document.getElementsByClassName("close")[0];

                // When the user clicks the button, open the modal
                btn.onclick = function() {
                modal.style.display = "block";
                }

                // When the user clicks on <span> (x), close the modal
                span.onclick = function() {
                modal.style.display = "none";
                }

                // When the user clicks anywhere outside of the modal, close it
                window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
                }
                // The width and height of the captured photo. We will set the
                // width to the value defined here, but the height will be
                // calculated based on the aspect ratio of the input stream.

                const width = 320; // We will scale the photo width to this
                let height = 0; // This will be computed based on the input stream

                // |streaming| indicates whether or not we're currently streaming
                // video from the camera. Obviously, we start at false.

                let streaming = false;

                // The various HTML elements we need to configure or control. These
                // will be set by the startup() function.

                let video = null;
                let canvas = null;
                let photo = null;
                let photo1 = null;
                let startbutton = null;

                function showViewLiveResultButton() {
                    if (window.self !== window.top) {
                    // Ensure that if our document is in a frame, we get the user
                    // to first open it in its own tab or window. Otherwise, it
                    // won't be able to request permission for camera access.
                    document.querySelector(".contentarea").remove();
                    const button = document.createElement("button");
                    button.textContent = "View live result of the example code above";
                    document.body.append(button);
                    button.addEventListener("click", () => window.open(location.href));
                    return true;
                    }
                    return false;
                }

                function startup() {
                    if (showViewLiveResultButton()) {
                    return;
                    }
                    video = document.getElementById("video");
                    canvas = document.getElementById("canvas");
                    photo = document.getElementById("photo");
                    photo1 = document.getElementById("photo1");
                    startbutton = document.getElementById("startbutton");

                    navigator.mediaDevices
                    .getUserMedia({ video: true, audio: false })
                    .then((stream) => {
                        video.srcObject = stream;
                        video.play();
                    })
                    .catch((err) => {
                        console.error(`An error occurred: ${err}`);
                    });

                    video.addEventListener(
                    "canplay",
                    (ev) => {
                        if (!streaming) {
                        height = video.videoHeight / (video.videoWidth / width);

                        // Firefox currently has a bug where the height can't be read from
                        // the video, so we will make assumptions if this happens.

                        if (isNaN(height)) {
                            height = width / (4 / 3);
                        }

                        video.setAttribute("width", width);
                        video.setAttribute("height", height);
                        canvas.setAttribute("width", width);
                        canvas.setAttribute("height", height);
                        streaming = true;
                        }
                    },
                    false
                    );

                    startbutton.addEventListener(
                    "click",
                    (ev) => {
                        takepicture();
                        ev.preventDefault();
                    },
                    false
                    );

                    clearphoto();
                }

                // Fill the photo with an indication that none has been
                // captured.

                function clearphoto() {
                    const context = canvas.getContext("2d");
                    context.fillStyle = "#AAA";
                    context.fillRect(0, 0, canvas.width, canvas.height);

                    const data = canvas.toDataURL("image/png");
                    photo.setAttribute("src", data);
                    photo1.value = data;
                }

                // Capture a photo by fetching the current contents of the video
                // and drawing it into a canvas, then converting that to a PNG
                // format data URL. By drawing it on an offscreen canvas and then
                // drawing that to the screen, we can change its size and/or apply
                // other changes before drawing it.

                function takepicture() {
                    const context = canvas.getContext("2d");
                    if (width && height) {
                    canvas.width = width;
                    canvas.height = height;
                    context.translate(width, 0);
                    context.scale(-1, 1);
                    context.font = "10px";
                    context.fillStyle = "#ffffff";
                    context.drawImage(video, 0, 0, width, height);
                    context.translate(width, 0);
                    context.scale(-1, 1);
                    context.fillText("{{ $wm }}", 200, 230, 140);

                    const data = canvas.toDataURL("image/png");
                    photo.setAttribute("src", data);
                    photo1.value = data;
                    // console.log(photo1.value)

                    } else {
                    clearphoto();
                    }
                }

                // Set up our event listener to run the startup process
                // once loading is complete.
                window.addEventListener("load", startup, false);
        })();
        </script>

    @endif


        @endsection
