@extends('layout.siswa')


@section('header')
<div class="card-header">
    <h3>Nilai Anda</h3>
</div>
@endsection


@section('content')

@if (!is_null($siswa -> nilai ))

@if (!is_null($siswa -> nilai ))

<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Nilai Siswa di Perusahaan</h3>
        </div>
    </div>
    @foreach ($siswa -> perusahaan  as $perusahaan)

    <div class="accordion" id="accordionExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="heading{{ $loop -> iteration }}">
                <button class="accordion-button collapsed btn btn-outline-secondary w-100" type="button" data-bs-toggle="collapse"
                    data-bs-target="#collapse{{ $loop -> iteration }}" aria-expanded="false"
                    aria-controls="collapse{{ $loop -> iteration }}">
                    {{ $perusahaan -> detail ->  nama }}
                </button>
            </h2>
            @if ($perusahaan -> detail ->  pembimbing -> where('id_jurusan', $siswa -> kelas -> konsentrasi -> jurusan -> id) != null)


            @foreach ($perusahaan -> detail ->  pembimbing -> where('id_jurusan', $siswa -> kelas -> konsentrasi -> jurusan -> id) as  $pembimbing)
            {{-- @dd($key) --}}
            <div id="collapse{{ $loop -> iteration }}" class="accordion-collapse collapse"
                aria-labelledby="heading{{ $loop -> iteration }}" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <h3>{{ $pembimbing -> nama }}</h3>
                    <div class="table-responsive">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <td scope="col" style="width: 5%;" class="table-secondary">
                                        <p>No</p>
                                    </td>
                                    <td scope=" col" style="width: 20%" class="table-secondary">
                                        <p>Komponen</p>
                                    </td>
                                    <td scope="col" style="width: 20%" class="table-secondary">Skor<br>(0-100)</td>
                                    <td scope="col" style="width: 20%" class="table-secondary">
                                        <p>Keterangan</p>
                                    </td>

                                </tr>

                               @foreach ($kriteria -> where('id_pembimbing' , $pembimbing -> id) as $item)
                               {{-- $nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa ->nis)-> first() --}}
                               <tr>
                                   <td rowspan="1" style="background-color: rgb(236, 236, 236)">{{ $loop -> iteration
                                       }}</td>
                                   <td style="background-color: rgb(236, 236, 236)">{{ $item -> nama_kriteria }}</td>
                                   <td style="background-color: rgb(236, 236, 236)">
                                       @if ($nilai)
                                       {{ $nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa ->
                                       nis)-> first() ? $nilai -> where('id_kriteria', $item -> id)-> where('id_siswa',
                                       $siswa -> nis) -> first() -> point : ''}}
                                       @else
                                       @endif

                                   </td>
                                   <td style="background-color: rgb(236, 236, 236)">
                                       @if ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first())

                                       @if ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point >= 90)
                                           <div>A</div>
                                       @elseif ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point < 90)
                                           <div>B</div>
                                       @elseif ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point < 80)
                                           <div>C</div>
                                       @else
                                           <div>D</div>
                                       @endif
                                   @else

                                   @endif
                                   </td>
                               </tr>

                               @endforeach


                            </thead>
                        </table>
                        <form action="/siswa/download/jurnalsiswa" method="post">
                            @csrf
                            <input type="text" name="id_siswa" value="{{ $siswa -> nis }}" hidden>
                            <input type="text" name="id_pembimbing" value="{{ $pembimbing ->id }}" hidden>
                            <input type="text" name="id_perusahaan" value="{{ $perusahaan -> detail -> id }}" hidden>
                            <button class="btn btn-danger" role="button" style="margin-left:87%">Download PDF</button>
                        </form>
                    </div>
                </div>
            </div>

            @endforeach
            {{-- @endfor --}}
            @endif
        </div>

    </div>


    @endforeach

</div>
@else
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Nilai Siswa</h3>
        </div>
    </div>
</div>
@endif
@else
<div class="card recent-sales overflow-auto shadow p-3 mb-3 my-3 bg-white">
    <div class="d-flex flex-row">
        <div class="me-auto p-2">
            <h3>Nilai Siswa</h3>
        </div>
    </div>
</div>
@endif



{{-- ini dropdownnya --}}
<script src="{{asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
@endsection
