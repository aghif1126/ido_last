<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>i-Do</title>
  
  {{-- <!-- loader-->
  <link href="{{asset('selainad/assets/css/pace.min.css')}}" rel="stylesheet"/>
  <script src="{{asset('selainad/assets/js/pace.min.js')}}"></script> --}}
  <!--favicon-->
  <link rel="icon" href="{{asset('logo/Logo.png')}}" type="image/x-icon">
  <!-- Vector CSS -->
  <link href="{{asset('selainad/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet"/>
  <!-- simplebar CSS-->
  <link href="{{asset('selainad/assets/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="{{asset('selainad/assets/css/bootstrap.min.css')}}" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="{{asset('selainad/assets/css/animate.css')}}" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="{{asset('selainad/assets/css/icons.css')}}" rel="stylesheet" type="text/css"/>
  <!-- Sidebar CSS-->
  <link href="{{asset('selainad/assets/css/sidebar-menu.css')}}" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="{{asset('selainad/assets/css/app-style.css')}}" rel="stylesheet"/>
  
  <style>
    img{
        border-radius: 50%;
        object-fit: cover;
    }
</style>

</head>

<body class="bg-theme bg-theme1">
 
<!-- Start wrapper-->
 {{-- <div id="wrapper"> --}}
 
  <!--Start sidebar-wrapper-->
     {{-- <div class="brand-logo"> --}}
   
   {{-- </div> --}}
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<header class="topbar-nav1">
        <nav class="navbar navbar-expand fixed-top">
         <ul class="navbar-nav mr-auto align-items-center">
           <li class="nav-item">
             <a class="nav-link toggle-menu" href="javascript:void();">
              {{-- <i class="icon-menu menu-icon"></i> --}}
            </a>
           </li>
         </ul>
  {{-- <ul class="navbar-nav mr-auto align-items-center">
    <li class="nav-item">
      <a class="nav-link toggle-menu" href="javascript:void();">
       <i class="icon-menu menu-icon"></i>
     </a>
    </li>
  </ul> --}}
     
  <ul class="navbar-nav align-items-center right-nav-link">
      
      <li class="nav-item">
          <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
          <div class="d-flex flex-row-reverse">
        <span class="user-profile">
            @if (is_null($siswa -> foto))
            <img class="align-self-start mr-3" src="{{asset('/bg/profil.png')}}" alt="profil" width="130px" height="130px">
        @elseif ($siswa -> foto != 'Tidak tersedia')
            <img class="align-self-start mr-3" src="{{asset('uploads/profil/' . $siswa->foto)}}" alt="{{$siswa->nama}}" width="130px" height="130px">
        @else
            <img class="align-self-start mr-3" src="{{asset('/bg/profil.png')}}" alt="profil" width="130px" height="130px">
        @endif
        </span>
        
    </div>
    </a>
      <ul class="dropdown-menu dropdown-menu-right">
       <li class="dropdown-item user-details">
        <a href="javaScript:void();">
           <div class="media">
            <div class="avatar">
                @if (is_null($siswa -> foto))
                    <img class="align-self-start mr-3" src="{{asset('/bg/profil.png')}}" alt="profil" width="130px" height="130px">
                @elseif ($siswa -> foto != 'Tidak tersedia')
                    <img class="align-self-start mr-3" src="{{asset('uploads/profil/' . $siswa->foto)}}" alt="{{$siswa->nama}}" width="130px" height="130px">
                @else
                    <img class="align-self-start mr-3" src="{{asset('/bg/profil.png')}}" alt="profil" width="130px" height="130px">
                @endif
                {{-- <img class="align-self-start mr-3" src="https://via.placeholder.com/110x110" alt="user avatar"> --}}
            </div>
            <div class="media-body">
                <h6 class="mt-2 user-title">{{$siswa -> nama}}</h6>
                <p class="user-subtitle">{{$siswa -> nis}}</p>
            </div>
           </div>
          </a>
        </li>
        <a href="/logout">
            <li class="dropdown-divider"></li>
            <li class="dropdown-item"><i class="icon-power mr-2"></i> Logout</li>
        </a>
      </ul>
    </li>
  </ul>
</nav>
</header>
<!--End topbar header-->

{{-- <div class="clearfix"></div> --}}
	
  <div class="content-wrapper1">
    <div class="container-fluid">

  <!--Start Dashboard Content--> 
    {{-- Loading Screen --}}
        <style>
            .loding {
                width: 100px;
                height: 100px;
                display: grid;
                border: 7px solid #0000;
                border-radius: 50%;
                border-color: #3b71ca #0000;
                animation: spinner-e04l1k 0.8s linear infinite;
            }

            .loding::before,
            .loding::after {
                content: "";
                grid-area: 1/1;
                margin: 3.5px;
                border: inherit;
                border-radius: 50%;
            }

            .loding::before {
                border-color: #9fa6b2 #0000;
                animation: inherit;
                animation-duration: 0.4s;
                animation-direction: reverse;
            }

            .loding::after {
                margin: 14px;
            }

            @keyframes spinner-e04l1k {
                100% {
                    transform: rotate(1turn);
                }
            }

            #spinner-overlay {
                display: none;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(255, 255, 255, 1);
                /* White overlay */
                z-index: 9999;
                opacity: 0.9;
            }

            #spinner-container {
                display: none;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                display: flex;
                align-items: center;
                justify-content: center;
                z-index: 10000;
            }

            /* Add this CSS code to your main CSS file */
            /* Add this CSS code to your main CSS file */
            .pagination {
                display: inline-block;
            }

            .pagination a {
                color: black;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }
        </style>

        <div id="spinner-overlay">
            <div id="spinner-container">
                <div id="spinner" class="bg-white d-flex align-items-center justify-content-center">
                    <div class="loding"></div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            // Fungsi untuk menampilkan spinner
        function showSpinner() {
            document.getElementById('spinner-overlay').style.display = 'block';
            document.getElementById('spinner-container').style.display = 'flex';
            document.getElementById('loding').style.display = 'flex';
        }

        // Fungsi untuk menyembunyikan spinner
        function hideSpinner() {
            document.getElementById('spinner-overlay').style.display = 'none';
            document.getElementById('spinner-container').style.display = 'none';
            document.getElementById('loding').style.display = 'block';
        }

        // Event listener saat halaman sedang dimuat
        window.addEventListener('beforeunload', function () {
            showSpinner();
        });

        // Event listener saat halaman sudah dimuat
        window.addEventListener('unload', function () {
            hideSpinner();
        });

        //ini menu untuk mobile
        var mobileMenuBtn = document.querySelector("#mobile-menu-btn");
        var mobileMenu = document.querySelector(".mobile-menu");
        mobileMenuBtn.addEventListener("click", () => {
        if (mobileMenu.style.display === "none") {
            mobileMenu.style.display = "flex";
        } 
        else {
            mobileMenu.style.display = "none";
        }
        });

        </script>

    {{-- end loading --}}
	  
	<div class="row">
     <div class="col-12 col-lg-12 col-xl-9">
	    <div class="card">
            {{-- <div class="card-header">
                <h3>Lengkapi Profil anda</h3>
                <hr>
            </div> --}}
            <div class="card-body">
                <h3>Lengkap Profil Anda</h3>
                <hr>
                <form action="/siswa/update-data" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="text" name="nis" value="{{ $siswa -> nis }}" hidden>
                    <div class="form-group">
                     <label for="input-1">NIS</label>
                     <input type="text" rows="3" class="form-control" id="nis" name="nis" required value="{{ $siswa -> nis }}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="input-2">Nama</label>
                        <input type="text" id="nama" name="nama" onkeydown="return /[a-z A-Z .']/i.test(event.key)" required value="{{ $siswa -> nama }}" class="form-control">
                    </div>
                    
                    <div class="form-group">
                        <label for="input-3">Kelas</label>
                        <select class="form-control" name="id_jurusan1" aria-label="Default select example" required>
                            <option selected hidden>{{$siswa -> kelas -> nama}}</option>
                            @foreach ($kelas as $g )
                            <option value="{{ $g->id }}" class="form-control">{{ $g->nama }}</option>

                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="input41">Jenis Kelamin</label>
                        <select name="jenkel" class="form-control" aria-label="Default select example" required>
                            <?php
                            if(is_null($siswa -> jenkel)){
                            ?>
                            <option selected hidden class="form-control">L/P</option>
                            <?php
                            } else{
                            ?>
                            <option selected hidden class="form-control">{{ $siswa-> jenkel }}</option>
                            <?php }?>
                            <option value="L" class="form-control">L</option>
                            <option value="P" class="form-control">P</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="input-5">No Telepon</label>
                        <input type="number" id="telp" name="telp" required value="{{ $siswa -> telp }}" placeholder="No.Telp" class="form-control">
                    </div>
                    @if ($siswa -> nama_ortu == 'Tidak tersedia')
                    <div class="form-group">
                        <label for="input-6">Alamat</label>
                        <input type="text" id="alamat" name="alamat" required placeholder="Alamat" class="form-control">
                    </div>
                    @else
                    <div class="form-group">
                        <label for="input-6">Alamat</label>
                        <input type="text" id="alamat" name="alamat" required value="{{ $siswa -> alamat }}" placeholder="Alamat" class="form-control">
                    </div>
                    @endif
                    <div class="form-group">
                        <label for="input-7">Tangal Lahir</label>
                        <input type="date" id="tanggal_lahir" name="tanggal_lahir" required value="{{ $siswa -> tanggal_lahir }}" class="form-control">
                    </div>

                    @if ($siswa -> nama_ortu == 'Tidak tersedia')
                        <div class="form-group">
                            <label for="input-8">Nama Orang Tua</label>
                            <input type="text" id="nama_ortu" name="nama_ortu" required onkeydown="return /[a-z A-Z .']/i.test(event.key)" placeholder="Nama Orang Tua" class="form-control">
                        </div>
                    @else
                        <div class="form-group">
                            <label for="input-8">Nama Orang Tua</label>
                            <input type="text" id="nama_ortu" name="nama_ortu" value="{{$siswa->nama_ortu}}" required onkeydown="return /[a-z A-Z .']/i.test(event.key)" placeholder="Nama Orang Tua" class="form-control">
                        </div>
                    @endif

                    @if ($siswa -> alamat_ortu == 'Tidak tersedia')
                        <div class="form-group">
                            <label for="input-9">Alamat Orang Tua</label>
                            <input type="text" id="alamat_ortu" name="alamat_ortu" required placeholder="No.Telp Orang Tua" class="form-control">
                        </div>
                    @else
                        <div class="form-group">
                            <label for="input-1">Alamat Orang Tua</label>
                            <input type="text" id="alamat_ortu" name="alamat_ortu" value="{{$siswa->alamat_ortu}}" required placeholder="No.Telp Orang Tua" class="form-control">
                        </div>
                    @endif

                    @if ($siswa->telp_ortu =='Tidak tersedia')
                        <div class="form-group">
                            <label for="input-1">Telp Orang Tua</label>
                            <input type="number" id="telp_ortu" name="telp_ortu" required placeholder="No.Telp Orang Tua" class="form-control">
                        </div>
                    @else
                        <div class="form-group">
                            <label for="input-1">Telp Orang Tua</label>
                            <input type="number" value="{{$siswa->telp_ortu}}" id="telp_ortu" name="telp_ortu" required placeholder="No.Telp Orang Tua" class="form-control">
                        </div>
                    @endif
                    <div class="form-group">
                        <div class="d-flex flex-row-reverse">
                            <button type="submit" class="btn btn-secondary px-5" onClick="return confirm(`Anda Yakin?`)">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
		</div>
	 </div>
	</div><!--End Row-->

      <!--End Dashboard Content-->
	  
	<!--start overlay-->
		  <div class="overlay toggle-menu"></div>
		<!--end overlay-->
		
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->
	<footer style="text-align: center;">
        <hr>
          Copyright © 2023 i-Do! Prakerin
        
    </footer>
	<!--End footer-->
   
  </div><!--End wrapper-->

  <!-- Bootstrap core JavaScript-->
  <script src="{{asset('selainad/assets/js/jquery.min.js')}}"></script>
  <script src="{{asset('selainad/assets/js/popper.min.js')}}"></script>
  <script src="{{asset('selainad/assets/js/bootstrap.min.js')}}"></script>
	
 <!-- simplebar js -->
  <script src="{{asset('selainad/assets/plugins/simplebar/js/simplebar.js')}}"></script>
  <!-- sidebar-menu js -->
  <script src="{{asset('selainad/assets/js/sidebar-menu.js')}}"></script>
  {{-- <!-- loader scripts -->
  <script src="{{asset('selainad/assets/js/jquery.loading-indicator.js')}}"></script> --}}
  <!-- Custom scripts -->
  <script src="{{asset('selainad/assets/js/app-script.js')}}"></script>
  <!-- Chart js -->
  
  <script src="{{asset('selainad/assets/plugins/Chart.js/Chart.min.js')}}"></script>
 
  <!-- Index js -->
  <script src="{{asset('selainad/assets/js/index.js')}}"></script>

  
</body>
</html>
