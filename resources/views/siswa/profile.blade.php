@extends('layout.siswa')


@section('header')
    <div class="card-header">Profil anda
        <br>
        <a href="form">
            <button class="btn btn-info">Edit Profil</button>
        </a>
        <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Edit Foto</button>
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
              <div class="modal-header">
                  <h3 id="exampleModalLabel">Edit Foto Profil</h3>
                  <button type="button" class="btn zmdi zmdi-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <form action="/siswa/update-poto" method="post" enctype="multipart/form-data">
                  @csrf
                <div class="modal-body">
                    <div class="mb-3">
                      {{-- <label for="recipient-name" class="col-form-label">Recipient:</label> --}}
                      <input type="file" class="form-control" name="foto" id="foto">
                      <input type="text" name="nis" value="{{$siswa->nis}}" hidden>
                    </div>
                  
                </div>
                <div class="modal-footer">
                  {{-- <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Kembali</button> --}}
                  <button type="submit" class="btn btn-secondary" onClick="return confirm(`Anda Yakin?`)">Submit</button>
                </div>
              </form>
              </div>
            </div>
          </div>

        {{-- <button class="btn btn-secondary">Edit Foto</button> --}}
    </div>
@endsection

@section('content')

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.min.js" integrity="sha384-Rx+T1VzGupg4BHQYs2gCW9It+akI2MM/mndMCy36UVfodzcJcF0GGLxZIzObiEfa" crossorigin="anonymous"></script>

<div class="card-body">
    <div class="table-responsive">
        <table class="table">
            <tbody>
            <tr>
                <th scope="col">Nama</th>
                <td>:</td>
                <td>{{ $siswa -> nama }}</td>
            </tr>
            <tr>
                <th scope="col">NIS</th>
                <td>:</td>
                <td>{{ $siswa -> nis }}</td>
            </tr>
            <tr>
                <th scope="col">Jenis Kelamin</th>
                <td>:</td>
                <td>{{ $siswa -> jenkel }}</td>
            </tr>
            <tr>
                <th scope="col">Tanggal Lahir</th>
                <td>:</td>
                <td>{{ $siswa -> tanggal_lahir }}</td>
            </tr>
            <tr>
                <th scope="col">Konsentrasi Keahlian</th>
                <td>:</td>
                <td>{{ $siswa -> kelas -> konsentrasi -> nama }}</td>
            </tr>
            <tr>
                <th scope="col">Program Keahlian</th>
                <td>:</td>
                <td>{{ $siswa -> kelas -> konsentrasi -> jurusan -> nama }}</td>
            </tr>
            <tr>
                <th>Periode</th>
                <td>:</td>
                <td>
                    @foreach ($siswa -> perusahaan as $perusahaan)

                    {{ $perusahaan -> detail -> nama}} 
                        @if (in_array($sekarang, explode('-', $perusahaan -> periode)) &&
                        (intval(explode('-', $perusahaan -> awal_periode)[0]) <= $tahun &&
                        $tahun <= (intval(explode('-', $perusahaan -> akhir_periode)[0]))))

                        {{ $bulan[intval(explode('-', $perusahaan -> awal_periode)[1])]}} ({{ explode('-', $perusahaan -> awal_periode)[0] }}) -
                        {{ $bulan[intval(explode('-', $perusahaan -> akhir_periode)[1])]}} ({{ explode('-', $perusahaan -> akhir_periode)[0] }})
                        sedang dilaksanakan
                        @else
                            @if ((intval(explode('-', $perusahaan -> awal_periode)[0]) > $tahun ))
                            {{ $bulan[intval(explode('-', $perusahaan -> awal_periode)[1])]}} ({{ explode('-', $perusahaan -> awal_periode)[0] }}) -
                            {{ $bulan[intval(explode('-', $perusahaan -> akhir_periode)[1])]}} ({{ explode('-', $perusahaan -> akhir_periode)[0] }})
                        Belum Dimulai
                            @else
                            {{ $bulan[intval(explode('-', $perusahaan -> awal_periode)[1])]}} ({{ explode('-', $perusahaan -> awal_periode)[0] }}) -
                            {{ $bulan[intval(explode('-', $perusahaan -> akhir_periode)[1])]}} ({{ explode('-', $perusahaan -> akhir_periode)[0] }})
                        selesai
                            @endif

                        @endif

                <br>
                @endforeach
                </td>
            </tr>
            <tr>
                <th scope="col">Alamat</th>
                <td>:</td>
                <td>{{ $siswa -> alamat }}</td>
            </tr>
            <tr>
                <th scope="col">No. Telp</th>
                <td>:</td>
                <td>{{ $siswa -> telp }}</td>
            </tr>
            <tr>
                <th scope="col">Nama Orang Tua</th>
                <td>:</td>
                <td>{{ $siswa -> nama_ortu }}</td>
            </tr>
            <tr>
                <th scope="col">Alamat Orang Tua</th>
                <td>:</td>
                <td>{{ $siswa -> alamat_ortu }}</td>
            </tr>
            <tr>
                <th scope="col">No. Telp Orang Tua</th>
                <td>:</td>
                <td>{{ $siswa -> telp_ortu }}</td>
            </tr>
            <tr>
                <th scope="col">Sekolah</th>
                <td>:</td>
                <td>SMK Negeri 11 Bandung</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="d-flex flex-row-reverse">

    <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#editpassword">
        Ganti Password
        </button>
        <div class="modal fade" id="editpassword" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title fs-5" id="exampleModalLabel">Ganti Password</h3>

                    <button type="button" class="btn zmdi zmdi-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/siswa/update-password" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-outline mb-3">
                            <input type="text" class="form-control" name="id" placeholder="id" value="{{ $user -> id }}" hidden>
                            {{-- <input type="text" name="email" value="{{$siswa -> id_user}}"> --}}
                            <label class="form-label" for="form3Example4">Masukkan Password Baru</label>
                            <input type="password" name="password" id="inputPassword"
                                    class="form-control form-control-sm" placeholder="Enter password"
                                    onfocus="focusevent1()" onblur="blurevent1()" required />

                            <div class="mb-3 row ml-1">
                                <div class="col-sm-12" style="margin-top: 6px; margin-left: 8px;">
                                    <input type="checkbox" class="form-check-input" id="show-password"
                                        onclick="myFunction()"> Show Password
                                </div>
                            </div>
                            <select class="form-select" name="role_id" aria-label=".form-select-lg example" hidden>
                                <option value="4" selected>Siswa</option>                
                            </select>
                        </div>
                <div class="modal-footer">   
                        <button type="submit" class="btn btn-secondary">Simpan</button>
                    </form>
            </div>
        </div>
        {{-- <button class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#editpassword">Edit Password</button> --}}
    </div>
</div>

@endsection

<script>

    const exampleModal = document.getElementById('exampleModal')
        if (exampleModal) {
        exampleModal.addEventListener('show.bs.modal', event => {
            const button = event.relatedTarget
            const recipient = button.getAttribute('data-bs-whatever')

            const modalTitle = exampleModal.querySelector('.modal-title')
            const modalBodyInput = exampleModal.querySelector('.modal-body input')

            modalBodyInput.value = recipient
        })
    }

    // Password mulai dari sini
    function myFunction() {
        var x = document.getElementById("inputPassword");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>