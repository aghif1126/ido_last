@extends('layout.siswa')

{{-- @section('header')
    <div class="card-header">
        <h3>Hi, {{ $siswa -> nama }} </h3>
        <p>Jangan lupa untuk absen hari ini</p>
    </div>
@endsection --}}

@section('dashboard')
    <div style="background-color: rgb(10, 115, 235);margin-right: -25px; margin-left: -25px; margin-top:-10px; padding-right: 12.5px; padding-left: 12.5px; 12.5px; padding-top: 10.5px; 12.5px; padding-bottom: 10.5px;">
        <h3 class="text-white">Hi, {{ $siswa -> nama }} </h3>
        <p class="text-white">Jangan lupa untuk absen hari ini</p>
        <center>
            <a href="aktivitas">
                <button class="btn btn-collapsed btn-info col-12">Absen</button>
            </a>
        </center>
        <br>
        <div class="row">
            <div class="col-lg-12">
               <div class="card">
                <div class="card-body">
                    {{$perusahaan ? ' Anda melaksanakan PKL di ' . $perusahaan -> detail -> nama : 'Anda Sedang Tidak Melaksanakan PKL'}}
                    <br>
                    <a href="perusahaan">
                        <button class="btn btn-collapsed btn-info">Lihat Data Perusahaan</button>
                    </a>
                </div>
               </div>
            </div>
        </div>
    </div>
    <br>
@endsection

@section('content')

{{-- <div class="card rounded-lg shadow text-dark border-0 overflow-hidden mb-3 lift ng-scope">
    <h5 class="card-header">Absensi Kehadiran</h5>
    <div class="d-flex flex-row justify-content-center">
        <div class="card ml-1 panel-block-progress panel-block-progress-2 panel-block-no-hover">
            <div class="panel-block-progress-item rounded-lg" style="height:auto;">
                <div class="d-flex flex-column h-100 py-1 px-md-3 px-1">
                    <div class="flex-1 text-center">
                        <h3>0</h3>
                        <div class="d-block text-sm mb-1 ng-binding">
                            Hadir
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="card ml-1 panel-block-progress panel-block-progress-2 panel-block-no-hover">
            <div class="panel-block-progress-item rounded-lg" style="height:auto;">
                <div class="d-flex flex-column h-100 py-1 px-md-3 px-1">
                    <div class="flex-1 text-center">
                        <h3>0</h3>
                        <div class="d-block text-sm mb-1 ng-binding">
                            Izin
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="card ml-1 panel-block-progress panel-block-progress-2 panel-block-no-hover">
            <div class="panel-block-progress-item rounded-lg" style="height:auto;">
                <div class="d-flex flex-column h-100 py-1 px-md-3 px-1">
                    <div class="flex-1 text-center">
                        <h3>0</h3>
                        <div class="d-block text-sm mb-1 ng-binding">
                            Sakit
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div> --}}

<div class="card-body">

        <hr>


    <br>
    <h4>Nilai
    </h4>
    <h6>Lihat nilai yang telah anda dapatkan</h6>
    <a href="nilai">
        <button class="btn btn-collapsed btn-info">Lihat Nilai</button>
    </a>
 </div>
@endsection
