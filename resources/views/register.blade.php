

<!DOCTYPE html>
<html>
<head>
  <title>Register Form</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

  <div class="container">

    <div class="row">

      <div class="col-md-6 offset-md-3">

        <h1 class="text-center">Register Form</h1>

        <form action="" method="post">

          <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" name="name" id="" class="form-control" placeholder="" aria-describedby="" required />
          </div>

          <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" name="email" id="" class="form-control" placeholder="" aria-describedby="" required />
          </div>

          <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" name = "password" id="" class = "form-control " placeholder = "" aria - describedby = "" required />
          </div >

          <button type = "submit " name = "register_btn "class = "btn btn-primary btn-block " > Register </button >

        </form >

      </div >

    </div >

  </ div >

</body>
</html
