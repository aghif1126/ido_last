<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fitur Jurnal Kegiatan</title>

    <!-- Favicon -->
    <link href="{{asset('logo/Logo.png')}}" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500&family=Jost:wght@500;600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{ asset('dboard/lib/animate/animate.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dboard/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('dboard/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="{{ asset('dboard/css/dashboard.css')}}" rel="stylesheet">
</head>
<body data-bs-spy="scroll" data-bs-target=".navbar" data-bs-offset="51">
    <div class="container-xxl bg-white p-0">
        <div class="container-xxl position-relative p-0" id="home">
            <nav class="navbar navbar-expand-lg navbar-light px-4 px-lg-5 py-3 py-lg-0">
                <a href="/" class="navbar-brand p-0">
                    <h1 class="m-0">Prakerin</h1>
                    <!-- <img src="img/logo.png" alt="Logo"> -->
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav mx-auto py-0">
                        <a href="/" class="nav-item nav-link active">Home</a>
                        <a href="#feature" class="nav-item nav-link">Fitur</a>
                        {{-- <a href="#teamplay" class="nav-item nav-link">Teamplay</a> --}}
                        {{-- <a href="#about" class="nav-item nav-link">About</a> --}}
                        <!-- <a href="#review" class="nav-item nav-link">About</a>
                        <a href="#contact" class="nav-item nav-link">Contact</a> -->
                    </div>
                    {{-- <a href="/logout" class="btn btn-primary-gradient rounded-pill py-2 px-4 ms-3 d-none d-lg-block" style="background-color: #899bbd;">Login</a> --}}
                </div>
            </nav>
            <div class="container-xxl bg-primary hero-header2">
                <div class="container px-lg-2"  id="feature">
                    {{-- <div class="row g-5"> --}}
                        {{-- <div class="col-lg-8 text-center text-lg-start">
                            <h1 class="text-white mb-4 animated slideInDown">Selesaikan Semua Yang Telah Dilakukan Dengan Cara Yang Terbaik</h1>
                            <p class="text-white pb-3 animated slideInDown"></p>
                            <a href="logout" class="btn btn-primary-gradient py-sm-3 px-4 px-sm-5 rounded-pill me-3 animated slideInLeft" style="background-color: #899bbd;">Login</a>
                        </div> --}}
                        {{-- <div class="col-lg-4 d-flex justify-content-center justify-content-lg-end wow fadeInUp" data-wow-delay="0.3s">
                            <div class="owl-carousel screenshot-carousel">
                                <img class="img-fluid" src="img/screenshot-1.png" alt="">
                                <img class="img-fluid" src="img/screenshot-2.png" alt="">
                                <img class="img-fluid" src="img/screenshot-3.png" alt="">
                                <img class="img-fluid" src="img/screenshot-4.png" alt="">
                                <img class="img-fluid" src="img/screenshot-5.png" alt="">
                            </div>
                        </div> --}}
                        {{-- <div class="col-lg-4 d-flex justify-content-center justify-content-lg-end wow fadeInUp" data-wow-delay="0.3s">
                            <div class="testt">
                                <div class="owl-carousel screenshot-carousel">
                                </div>
                            </div>
                        </div> --}}
                    {{-- </div> --}}
                </div>
            </div>
        </div>

        <div class="container-xxl py-2" id="feature">
            <div class="container py-2 px-lg-5">
                <div class="row mb-5">
                    <div class="col-lg-12 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="feature-item bg-light rounded p-4">
                            <h5>
                                Setelah melakukan absen akan muncul tampilan seperti dibawah untuk mengisi kegiatan
                            </h5>
                            <br>
                            <img src="{{asset('bg/isi_kegiatan.png')}}" alt="" width="100%">
                            <br> <br>
                            <h6>
                                Isi kegiatan-kegiatan yang telah anda lakukan saat pkl jangan lupa isi JAM DATANG dan JAM KEMBALI <br> <br>
                                Jika sudah mengisi kegiatan maka akan ada riwayat yang telah anda isi seperti gambar dibawah
                            </h6>
                            <br>
                            <img src="{{asset('bg/riwayat.png')}}" alt="" width="100%">
                            
                        </div>
                    </div>
                </div>
                {{-- <div class="row mb-5">
                    <div class="col-lg-12 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="feature-item bg-light rounded p-4">
                            <p>oasok</p>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('dboard/lib/wow/wow.min.js')}}"></script>
    <script src="{{ asset('dboard/lib/easing/easing.min.js')}}"></script>
    <script src="{{ asset('dboard/lib/waypoints/waypoints.min.js')}}"></script>
    <script src="{{ asset('dboard/lib/counterup/counterup.min.js')}}"></script>
    <script src="{{ asset('dboard/lib/owlcarousel/owl.carousel.min.js')}}"></script>
    
    <!-- Template Javascript -->
    <script src="{{ asset('dboard/js/dashboard.js')}}"></script>
</body>
</html>