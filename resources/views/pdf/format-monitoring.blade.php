<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>
        <div>
            <div><img src="./logo/Coat_of_arms_of_West_Java.png" alt="" style="width: 100px; position:absolute; margin-left: 40px; margin-top: 50px"></div>
            <div style="margin-left: 100px; text-align: center">
                <h4>PEMERINTAH DAERAH PROVINSI JAWA BARAT <br> DINAS PENDIDIKAN <br> CABANG DINAS PENDIDIKAN WILAYAH VII <br></h4><h2 style="line-height: 0px">SMK NEGERI 11 BANDUNG</h2>
                <p style="line-height: 0px; font-size: 14px">Bisinis Manajemen - Teknologi Informasi - Seni dan Ekonomi Kreatif </p>
                <p style="line-height: 0px">Jl Budhi Cilember (022) 6652442 Fax. (022) 6613508 Bandung 40175</p>
                <p style="line-height: 0px">NPSN: 20219175 NSS: 34.1.02.60.03.001</p>
                <p style="line-height: 0px">http://smkn11bdg.net &nbsp;&nbsp; E-mail:smkn11bdg@gmail.com</p>
            </div>
            <hr style="width: 700px; height: 3px; background-color: black; margin-top:1px; position:absolute">
            <hr style="width: 700px; background-color: black; position:absolute">
        </div>
        <br><br>
    </div>
    <div style="text-align: center">
        <h3>BUKTI KUNJUNGAN/MONITORING</h3>
        <br>
    </div>
    <div style="margin-left: 40px">
        <p>Pada hari ini ...... Tanggal ....... Bulan ........ Tahun ........ Pukul</p>
        <p>Menerangkan bahwa:</p>
        <table style="margin-left: 40px">
            <tr>
                <td style="width: 100px">Nama</td>
                <td>:</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>NIP</td>
                <td>:</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <p>Telah melakukan monitoring dan evaluasi dalam rangka pelaksanaan Praktek Kerja Lapangan (PKL)</p>
        <table>
            <tr>
                <td style="width: 200px">Pada Perusahaan/Instansi</td>
                <td>:</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <br><br><br><br>
        <table border="1">
            <tr style="line-height: 10px">
                <td colspan="3" style="text-align:right">.......,.......20.....</td>
            </tr>
            <tr style="line-height: 1px">
                <td><p>Pihak Perushaan</p></td>
                <td style="width: 400px"></td>
                <td style="text-align: center"><p>Pihak Sekolah</p></td>
            </tr>
            <tr>
                <td><p>...........................</p></td>
                <td></td>
                <td><p>.........................</p></td>
            </tr>
        </table>
    </div>
</body>
</html>