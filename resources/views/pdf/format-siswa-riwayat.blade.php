<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .page_break { page-break-before: always; }
    </style>
</head>
<body>
    <div class="container shadow p-3 mb-5 bg-body-tertiary rounded mt-4">
        <h2 style="text-align:center;">Laporan Kegiatan Harian Peserta Didik Pada Pelaksanaan PKL</h2>
        <br>
        <br>
        <div class="table-responsive">
            <table class="table" border="0">
                <thead>
                    <tr>
                        {{-- @foreach ($siswa as $siswa ) --}}
                       
                        <th scope="col" style="width: 20%; text-align:left; font-weight:normal">Nama Siswa</th>
                        <td>:</td>
                        <td>{{ $siswa -> nama }}</td>
                    </tr>
                    {{-- <tr>
                        <th scope="col" style="width: 20%">NIS</th>
                        <td>:</td>
                        <td>{{ $siswa -> nis }}</td>
                    </tr> --}}
                    <tr>
                        <th scope="col" style="width: 20%; text-align:left; font-weight:normal;">Konsentrasi Keahlian</th>
                        <td>:</td>
                        <td>{{ $siswa -> kelas -> konsentrasi -> nama }}</td>
                    </tr>
                    <tr>
                        <th scope="col" style="width: 20%; text-align:left; font-weight:normal;">Tahun Pelajaran</th>
                        <td>:</td>
                        <td>{{ $siswa -> tahun_pelajaran }}</td>
                    </tr>
                    <tr>
                        <th scope="col" style="width: 20%; text-align:left; font-weight:normal;">Tempat Prakerin</th>
                        <td>:</td>
                        <td>{{ $siswa -> perusahaan -> nama }}</td>
                    </tr>
                    <tr>
                        <th scope="col" style="width: 30%; text-align:left; font-weight:normal;">Nama Instruktur(pembimbing PKL)</th>
                        <td>:</td>
                        {{-- <td>{{ $siswa -> guru -> nama }}</td> --}}
                    </tr>
    
                </thead>
            </table>
            <br>
            <br>
            <div class="table-responsive">
                <table border="1px" style="border-collapse: collapse">
                    <thead>
                        <tr>
                            <th scope="col" style="width: 1%;">No.</th>
                            <th scope="col" style="width: 50%;">AktivitasPKL</th>
                            <th scope="col" style="width: 1%;">Hari/Tgl pelaksanaan</th>
                            <th scope="col" style="width: 1%;">Divisi/Dept</th>
                            <th scope="col" style="width: 5%;">Mulai Pukul</th>
                            <th scope="col" style="width: 5%;">Selesai Pukul</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($riwayat as $r)
                        <tr class="">
                                
                            <td scope="row">{{ $loop->iteration }}</td>
                            <td>{{ $r->kegiatan }}</td>
                            <td>{{ $r->tanggal }}</td>
                            <td>{{ $r->divisi }}</td>
                            <td>{{ $r->mulai }}</td>
                            <td>{{ $r->selesai }}</td>
                            @endforeach
                        </tr>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
</body>
</html>