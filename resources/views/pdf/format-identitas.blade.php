<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .a{
            width: 120px;
            height: 160px;
            border: 2px solid;
            border-color: black;
            background-color: white;
        }
    </style>
</head>
<body style="margin-left: 10%">
    <h2 style="text-align: center">DATA/IDENTITAS</h2>
    <br>

    <div class="table-responsive">
        <table class="table table-primary">
            <tbody>
                <tr class="">
                    <td style="width: 230px">NAMA LENGKAP</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>NIS</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>JENIS KELAMIN</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>TEMPAT/TANGGAL LAHIR</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>PROGRAM KEAHLIAN</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>KOMPETENSI KEAHLIAN</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>ALAMAT TANGGAL</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>NO. TELEPON/HP</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>NAMA ORANG TUA</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>ALAMAT ORANG TUA</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>NO. TELEPON/HP</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td><p style="margin-bottom: 80px">SEKOLAH</p></td>
                    <td><p style="margin-bottom: 80px">:</p></td>
                    <td><p>SMK NEGERI 11 BANDUNG <br> JL. BUDHI CILEMBER <br> Tlp.(022) 6652442 <br> BANDUNG 40175</p></td>
                </tr>
                
            </tbody>
        </table>
    </div>
    <p>PEMBIMBING SEKOLAH</p>
    <div class="table-responsive">
        <table class="table table-primary">
            <tbody>
                <tr class="">
                    <td style="width: 200px">NAMA</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>NIP</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
    <br>
    <div class="a" style="margin-left:30%; margin-top: 10px"><p style="text-align: center; margin-top:40px;">PHOTO <br> ( 4 x 3 )</p></div>
    
</body>
</html>