<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>JURNAL KEGIATAN SISWA PRAKTEK KERJA LAPANGAN (PKL)</title>
    <style>
        .page_break { page-break-before: always; }

        .a{
            width: 120px;
            height: 160px;
            border: 2px solid;
            border-color: black;
            background-color: white;
        }

        .b{
            width: 140px;
            height: 140px;
            background-color: transparent;
            border: 2px solid;
            border-radius: 100%;
        }

        .rotate{
            transform: rotate(90deg);
            margin-top: 100px;
            margin-left: 300px
        }

        .hadir{
            width: 9px;
            height: 9px;
            background-color: lime;
            position: absolute;
            margin-top: 5px;
            margin-left: 5px;
        }

        .sakit{
            width: 9px;
            height: 9px;
            background-color: yellow;
            position: absolute;
            margin-top: 5px;
            margin-left: 5px;
        }

        .izin{
            width: 9px;
            height: 9px;
            background-color: orange;
            position: absolute;
            margin-top: 5px;
            margin-left: 5px;
        }
        .kethadir{
            width: 9px;
            height: 9px;
            background-color: lime;
            position: absolute;
            margin-top: 7px;
        }

        .ketsakit{
            width: 9px;
            height: 9px;
            background-color: yellow;
            position: absolute;
            margin-top: 7px;
        }

        .ketizin{
            width: 9px;
            height: 9px;
            background-color: orange;
            position: absolute;
            margin-top: 7px;
        }
    </style>
</head>
<body>
<div style="text-align: center">
    <div class="cover-frame"><img src="./logo/cover-frame.png" style="width: 700px; height: 1050px; position: absolute;" alt=""></div>
    <h2 style="margin-top: 70px"> JURNAL KEGIATAN SISWA <br> PRAKTEK KERJA LAPANGAN <br> (PKL)</h2>
    <br><br>

    <div><img src="./logo/11.png" alt="" style="width: 130px; margin-top: 70px;"></div>
    <br>
    <br>
    <br>
    <br>
    <br>

    <div class="table-responsive" style="margin-left: 50px; margin-top: 50px;">
        <table class="table table-primary">
            <tbody>
                <tr class="">
                    <td style="width: 200px">Nama</td>
                    <td>:</td>
                    <td>{{ $siswa->nama }}</td>
                </tr>
                <tr class="">
                    <td>No.Induk Siswa</td>
                    <td>:</td>
                    <td>{{ $siswa->nis }}</td>
                </tr>
                <tr class="">
                    <td>Kelas</td>
                    <td>:</td>
                    <td>{{ $siswa->kelas->nama }}</td>
                </tr>
                <tr class="">
                    <td>Periode PKL</td>
                    <td>:</td>
                    <td>
                        {{ $bulan[intval(explode('-', $siswa_perusahaan->awal_periode)[1])]}} {{ explode('-', $siswa_perusahaan->awal_periode)[0] }}
                        s/d
                        {{ $bulan[intval(explode('-', $siswa_perusahaan->akhir_periode)[1])]}} {{ explode('-', $siswa_perusahaan->akhir_periode)[0] }}
                    </td>
                </tr>
                <tr class="">
                    <td>Program Keahlian</td>
                    <td>:</td>
                    <td>{{ $siswa->kelas->konsentrasi->jurusan->nama }}</td>
                </tr>
                <tr class="">
                    <td>Kompetensi Keahlian</td>
                    <td>:</td>
                    <td>{{ $siswa->kelas->konsentrasi->nama }}</td>
                </tr>
            </tbody>
        </table>
    </div>

    <div style="margin-top: 90px;">
        <h3>PEMERINTAH DAERAH PROVINSI JAWA BARAT <br> DINAS PENDIDIKAN <br> CABANG DINAS PENDIDIKAN WILAYAH VII <br> SMK NEGERI 11 BANDUNG</h3>
        <p style="line-height: 0px">Bisinis Manajemen - Teknologi Informatika dan Komunikasi </p>
        <p style="line-height: 0px">Jl Budhi Cilember (022) 6652442 Bandung 40175 </p>
        <p style="line-height: 0px">http://smkn11bdg.net E-mail:smkn11bdg@gmail.com</p>
    </div>
</div>
    <div class="page_break"></div>

    <h4 style="text-align: center">DATA/IDENTITAS</h4>
    <br>

    <div class="table-responsive">
        <table class="table table-primary">
            <tbody>
                <tr class="">
                    <td style="width: 230px">NAMA LENGKAP</td>
                    <td>:</td>
                    <td>{{ $siswa->nama }}</td>
                </tr>
                <tr class="">
                    <td>NIS</td>
                    <td>:</td>
                    <td>{{ $siswa->nis }}</td>
                </tr>
                <tr class="">
                    <td>JENIS KELAMIN</td>
                    <td>:</td>
                    <td>{{ $siswa->jenkel }}</td>
                </tr>
                <tr class="">
                    <td>TEMPAT/TANGGAL LAHIR</td>
                    <td>:</td>
                    <td>{{ $siswa->tanggal_lahir }}</td>
                </tr>
                <tr class="">
                    <td>PROGRAM KEAHLIAN</td>
                    <td>:</td>
                    <td>{{ $siswa->kelas->konsentrasi->jurusan->nama }}</td>
                </tr>
                <tr class="">
                    <td>KOMPETENSI KEAHLIAN</td>
                    <td>:</td>
                    <td>{{ $siswa->kelas->konsentrasi->nama }}</td>
                </tr>
                <tr class="">
                    <td>ALAMAT TINGGAL</td>
                    <td>:</td>
                    <td>{{ $siswa->alamat }}</td>
                </tr>
                <tr class="">
                    <td>NO. TELEPON/HP</td>
                    <td>:</td>
                    <td>{{ $siswa->telp }}</td>
                </tr>
                <tr class="">
                    <td>NAMA ORANG TUA</td>
                    <td>:</td>
                    <td>{{ $siswa->nama_ortu }}</td>
                </tr>
                <tr class="">
                    <td>ALAMAT ORANG TUA</td>
                    <td>:</td>
                    <td>{{ $siswa->alamat_ortu }}</td>
                </tr>
                <tr class="">
                    <td>NO. TELEPON/HP</td>
                    <td>:</td>
                    <td>{{ $siswa->telp_ortu }}</td>
                </tr>
                <tr class="">
                    <td><p style="margin-bottom: 80px">SEKOLAH</p></td>
                    <td><p style="margin-bottom: 80px">:</p></td>
                    <td><p>SMK NEGERI 11 BANDUNG <br> JL. BUDHI CILEMBER <br> Tlp.(022) 6652442 <br> BANDUNG 40175</p></td>
                </tr>

            </tbody>
        </table>
    </div>
    <p>PEMBIMBING SEKOLAH</p>
    <div class="table-responsive">
        <table class="table table-primary">
            <tbody>
                <tr class="">
                    <td style="width: 200px">NAMA</td>
                    <td>:</td>
                    <td>

                            {{ $pembimbing->nama }}
                    </td>
                </tr>
                <tr class="">
                    <td>NIP</td>
                    <td>:</td>
                    <td>

                            {{ $pembimbing->nip }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br>
    <div class="a" style="margin-left:30%; margin-top: 10px"><p style="text-align: center; margin-top:40px;">PHOTO <br> ( 4 x 3 )</p></div>
    <div class="page_break"></div>

    <h4 style="text-align: center;">TEMPAT PRAKTEK KERJA LAPANGAN (PKL)</h4>
    <br>

    <div class="table-responsive" style="margin-left: 40px">
        <table class="table table-primary">
            <tbody>
                <tr class="">
                    <td style="width: 200px">TEMPAT PKL</td>
                    <td>:</td>
                    <td>

                            {{ $perusahaan->nama }}

                    </td>
                </tr>
                <tr class="">
                    <td>ALAMAT</td>
                    <td>:</td>
                    <td>
                            {{ $perusahaan->alamat }}
                    </td>
                </tr>
                <tr class="">
                    <td>TELEPON</td>
                    <td>:</td>
                    <td>
                            {{ $perusahaan->telp }}
                    </td>
                </tr>
                <tr class="">
                    <td>FAX</td>
                    <td>:</td>
                    <td>
                            {{ $perusahaan->fax }}
                    </td>
                </tr>
                <tr class="">
                    <td>E-MAIL</td>
                    <td>:</td>
                    <td>
                            {{ $perusahaan->email }}
                    </td>
                </tr>
                <tr class="">
                    <td>WEBSITE</td>
                    <td>:</td>
                    <td>
                            {{ $perusahaan->website }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="margin-left: 43px"><p>PIMPINAN INDUSTRI &nbsp;&nbsp;:</p></div>

    <div class="table-responsive" style="margin-left: 70px">
        <table class="table table-primary">
            <tbody>
                {{-- <tr class="">
                    <td style="width: 170px">NAMA JABATAN</td>
                    <td>:</td>
                    <td>
                        @foreach ($perusahaan as $item)
                            {{ $item->pembimbing->nama }}
                        @endforeach
                    </td>
                </tr> --}}
                <tr class="">
                    <td>NAMA</td>
                    <td>:</td>
                    <td>
                        {{ $perusahaan->pimpinan ? $perusahaan->pimpinan->nama : 'Tidak tersedia'}}
                    </td>
                </tr>
                <tr class="">
                    <td>Jabatan</td>
                    <td>:</td>
                    <td>
                        PIMPINAN
                    </td>
                </tr>
                <tr class="">
                    <td>NIP</td>
                    <td>:</td>
                    <td>
                        {{  $perusahaan->pimpinan ?  $perusahaan->pimpinan->nip : 'Tidak tersedia'}}
                    </td>
                </tr>
                <tr class="">
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td>
                        {{  $perusahaan->pimpinan ? $perusahaan->pimpinan->jenkel : 'Tidak tersedia'}}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="margin-left: 43px"><p>PEMBIMBING INDUSTRI &nbsp;&nbsp;:</p></div>

    <div class="table-responsive" style="margin-left: 70px">
        <table class="table table-primary">
            <tbody>
                <tr class="">
                    <td style="width: 170px">NAMA</td>
                    <td>:</td>
                    <td>
                        {{ $pembimbing->nama }}
                    </td>
                </tr>
                <tr class="">
                    <td>JABATAN</td>
                    <td>:</td>
                    <td>Pembimbing</td>
                </tr>
                <tr class="">
                    <td>NIP</td>
                    <td>:</td>
                    <td>
                        {{ $pembimbing ?  $pembimbing->nip : 'Tidak tersedia'}}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br>

    <p style="margin-left: 255px">Bandung, ....................., 20.....<br>Siswa<br><br><br><br><br></p>
    <div style="margin-left: 255px"><p>..................................................<br>NIS.{{$siswa -> nis}}</p></div>
    <div class="page_break"></div>

<div>
    <div class="container shadow p-3 mb-5 bg-body-tertiary rounded mt-4">
        <h4 style="text-align:center">LAPORAN KEGIATAN HARIAN PESERTA DIDIK PADA PELAKSANAAN PKL</h4>
        <br>
        <br>
        <div class="table-responsive">
            <table class="table" border="0">
                <thead>
                    <tr>
                        {{-- @foreach ($siswa as $siswa ) --}}

                        <th scope="col" style="width: 200px; text-align:left; font-weight:normal">Nama Siswa</th>
                        <td>:</td>
                        <td scope="col" style="width: 500px; ">{{ $siswa -> nama }}</td>
                    </tr>
                    {{-- <tr>
                        <th scope="col" style="width: 200px">NIS</th>
                        <td>:</td>
                        <td>{{ $siswa -> nis }}</td>
                    </tr> --}}
                    <tr>
                        <th scope="col" style="width: 200px; text-align:left; font-weight:normal;">Konsentrasi Keahlian</th>
                        <td>:</td>
                        <td>{{ $siswa -> kelas -> konsentrasi -> nama }}</td>
                    </tr>
                    <tr>
                        <th scope="col" style="width: 200px; text-align:left; font-weight:normal;">Tahun Pelajaran</th>
                        <td>:</td>
                        <td>{{ $siswa -> tahun_pelajaran -> tahun}}</td>
                    </tr>
                    <tr>
                        <th scope="col" style="width: 200px; text-align:left; font-weight:normal;">Tempat Prakerin</th>
                        <td>:</td>
                        <td>

                            {{ $perusahaan->nama }}

                        </td>
                    </tr>
                    <tr>
                        <th scope="col" style="width: 200px; text-align:left; font-weight:normal;">Nama Instruktur(pembimbing PKL)</th>
                        <td>:</td>
                        <td>{{ $pembimbing -> nama }}</td>
                    </tr>

                </thead>
            </table>
            <br>
            <br>
            <div>
                <table border="1" style="border-collapse: collapse">
                    <thead>
                        <tr>
                            <th scope="col" style="width: 1%;">No.</th>
                            <th scope="col" style="width: 300px;">AktivitasPKL</th>
                            <th scope="col" style="width: 1%;">Hari/Tgl pelaksanaan</th>
                            <th scope="col" style="width: 5%;">Mulai Pukul</th>
                            <th scope="col" style="width: 5%;">Selesai Pukul</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($riwayat as $r)
                        <tr class="">
                            <td scope="row">{{ $loop->iteration }}</td>
                            <td>{{ $r->kegiatan }}</td>
                            <td>{{ $r->tanggal }}</td>
                            <td>{{ $r->mulai }}</td>
                            <td>{{ $r->selesai }}</td>
                            @endforeach
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
    <div class="page_break"></div>

    <h4 style="text-align: center">PENILAIAN PRAKTIK KERJA LAPANGAN</h4>
    <br>

    <div class="table-responsive">
        <table class="table table-primary">
            <tbody>
                <tr>
                    <td>Nama Peserta Didik</td>
                    <td>:</td>
                    <td>{{ $siswa->nama }}</td>
                </tr>
                <tr>
                    <td>Kelas</td>
                    <td>:</td>
                    <td>{{ $siswa->kelas->nama }}</td>
                </tr>
                
                <tr>
                    <td>Kompetensi Keahlian</td>
                    <td>:</td>
                    <td>{{ $siswa->kelas->konsentrasi->jurusan->nama }}</td>
                </tr>
                <tr>
                    <td>Nama Industri</td>
                    <td>:</td>
                    <td>

                        {{ $perusahaan->nama }}

                    </td>
                </tr>
                <tr>
                    <td>Nama Instruktur</td>
                    <td>:</td>
                    <td>

                       {{$pembimbing->nama}}

                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br>

    <div style="margin-left: 70px">
        <table border="1" style="border-collapse:collapse;">
            <tbody>
                <tr>
                    <td class="table-secondary" style="height: 10px; width:30px; text-align:center">NO</td>
                    <td class="table-secondary" style="text-align: center; width: 300px">KOMPONEN</td>
                    <td style="width:100px; text-align:center;">SKOR<br>(0-100)</td>
                    <td class="table-secondary">KETERANGAN</td>

                </tr>

                @foreach ($kriteria as $item)
                <tr>
                    <td rowspan="1" style="background-color: rgb(236, 236, 236); height: 10px"><p style="text-align: center">{{ $loop -> iteration
                        }}</p></td>
                    <td style="background-color: rgb(236, 236, 236)"><p style="text-align: center">{{ $item -> nama_kriteria }}</p></td>
                    <td style="background-color: rgb(236, 236, 236)"><p style="text-align: center">

                        @if ($nilai)
                        {{ $nilai -> where('id_kriteria', $item -> id) ->  first() ? $nilai -> where('id_kriteria', $item -> id) -> first() -> point : ''}}
                        @else
                        @endif
                        </p>
                    </td>
                    <td style="background-color: rgb(236, 236, 236)">
                        @if ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first())

                        @if ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point >= 90)
                            <div style="text-align: center">A</div>
                        @elseif ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point < 90)
                            <div style="text-align: center">B</div>
                        @elseif ($nilai -> where('id_kriteria', $item -> id) -> where('id_siswa', $siswa -> nis)-> first() -> point < 80)
                            <div style="text-align: center">C</div>
                        @else
                            <div style="text-align: center">D</div>
                        @endif
                    @else

                    @endif
                    </td>
                </tr>

                @endforeach
            </tbody>
        </table>
    </div>
    <div class="page_break"></div>

    <div style="text-align: center">
        <h4>CATATAN SISWA DAN PEMBIMBING TEMPAT PKL</h4>
        <p>(Digunakan sebagai catatan siswa dan pembimbing sebagai masukan bagi pihak sekolah)</p>

        <div class="table-responsive" style="margin-left: 70px">
            <table border="1" class="table table-primary" style="border-collapse: collapse;">
                <tbody>
                    <tr class="">
                        <td style="text-align: center">TANGGAL</td>
                        <td style="width: 400px; text-align: center">CATATAN</td>
                        <td style="width: 70px; text-align: center">PARAF</td>
                    </tr>
                    {{-- <tr style="line-height: 0px">
                        <td><p style="text-align: center">1</p></td>
                        <td><p style="text-align: center">2</p></td>
                        <td><p style="text-align: center">3</p></td>
                    </tr> --}}
                    @foreach ($siswa -> catatan as $item )
                    <tr>
                        <td>
                            {{ $item -> tanggal}}
                        </td>
                        <td>
                            {{ $item -> catatan}}
                        </td>
                        <td></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="page_break"></div>

    <h4 style="text-align: center">DAFTAR KEHADIRAN SISWA DI TEMPAT PKL</h4>
    <div style="margin-left: 80px">
        {{-- Main table --}}
        <table border="1" style="border-collapse: collapse">
            <tr>
                <td rowspan="2" style="width: 30px; text-align: center">NO</td>
                <td rowspan="2" style="width: 100px; text-align: center">BULAN</td>
                <td colspan="11" rowspan="2" style="text-align: center">TANGGAL</td>
                <td colspan="3" style="text-align: center">REKAP</td>
            </tr>
            <tr>
                <td style="width: 30; text-align: center">S</td>
                <td style="width: 30; text-align: center">I</td>
                <td style="width: 30px; text-align: center">HADIR</td>
            </tr>
            @foreach ($all_bulan as $key => $value)

            <tr>
                <td rowspan="6" style="text-align: center">{{ $loop->iteration }}</td>
                <td rowspan="6" style="text-align: center">{{ $key }}</td>
                <td style="text-align: center; width: 20px">1</td>
                <td style="text-align: center; width: 20px">2</td>
                <td style="text-align: center; width: 20px">3</td>
                <td style="text-align: center; width: 20px">4</td>
                <td style="text-align: center; width: 20px">5</td>
                <td style="text-align: center; width: 20px">6</td>
                <td style="text-align: center; width: 20px">7</td>
                <td style="text-align: center; width: 20px">8</td>
                <td style="text-align: center; width: 20px">9</td>
                <td style="text-align: center; width: 20px">10</td>
                <td rowspan="4"></td>
                <td rowspan="6" style="text-align: center">{{ $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->all() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('status', 'Sakit')->count() : null }}</td>
                <td rowspan="6" style="text-align: center">{{ $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->all() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('status', 'Izin')->count() : null }}</td>
                <td rowspan="6" style="text-align: center">{{ $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->all() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('status', 'Hadir')->count() : null }}</td>
            </tr>
            <tr>
                {{-- Isi Tanggal Kehadiran --}}
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 1)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 1)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 1)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 1)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 1)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 1)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl',2)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl',2)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl',2)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl',2)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl',2)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl',2)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 3)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 3)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 3)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 3)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 3)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 3)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 4)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 4)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 4)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 4)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 4)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 4)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 5)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 5)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 5)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 5)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 5)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 5)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 6)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 6)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 6)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 6)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 6)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 6)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 7)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 7)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 7)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 7)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 7)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 7)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 8)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 8)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 8)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 8)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 8)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 8)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 9)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 9)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 9)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 9)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 9)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 9)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 10)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 10)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 10)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 10)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 10)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 10)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                {{-- Isi Tanggal Kehadiran (Sampai Sini) --}}

                {{-- Rekap Sakit, Izin, Alpha --}}
                {{-- Rekap Sakit, Izin, Alpha (Sampai Sini) --}}

                {{-- Jumlah Tidak Masuk, Jumlah Harus Masuk, Persentase Hadir --}}
                {{-- Jumlah Tidak Masuk, Jumlah Harus Masuk, Persentase Hadir (Sampai Sini) --}}
            </tr>
            <tr>
                <td style="text-align: center">11</td>
                <td style="text-align: center">12</td>
                <td style="text-align: center">13</td>
                <td style="text-align: center">14</td>
                <td style="text-align: center">15</td>
                <td style="text-align: center">16</td>
                <td style="text-align: center">17</td>
                <td style="text-align: center">18</td>
                <td style="text-align: center">19</td>
                <td style="text-align: center">20</td>
            </tr>
            <tr>
                {{-- Isi Tanggal Kehadiran --}}
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 11)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 11)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 11)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 11)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 11)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 11)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 12)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 12)->first() -> status == "Hadir" : null )
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 12)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 12)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 12)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 12)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 13)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 13)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 13)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 13)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 13)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 13)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 14)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 14)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 14)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 14)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 14)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 14)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 15)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 15)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 15)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 15)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 15)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 15)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 16)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 16)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 16)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 16)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 16)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 16)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 17)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 17)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 17)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 17)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 17)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 17)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 18)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 18)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 18)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 18)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 18)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 18)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 19)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 19)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 19)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 19)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 19)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 19)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 20)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 20)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 20)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 20)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 20)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 20)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                {{-- Isi Tanggal Kehadiran (Sampai Sini) --}}

                {{-- Rekap Sakit, Izin, Alpha --}}
                {{-- Rekap Sakit, Izin, Alpha (Sampai Sini) --}}

                {{-- Jumlah Tidak Masuk, Jumlah Harus Masuk, Persentase Hadir --}}
                {{-- Jumlah Tidak Masuk, Jumlah Harus Masuk, Persentase Hadir (Sampai Sini) --}}
            </tr>
            <tr>
                <td style="text-align: center">21</td>
                <td style="text-align: center">22</td>
                <td style="text-align: center">23</td>
                <td style="text-align: center">24</td>
                <td style="text-align: center">25</td>
                <td style="text-align: center">26</td>
                <td style="text-align: center">27</td>
                <td style="text-align: center">28</td>
                <td style="text-align: center">29</td>
                <td style="text-align: center">30</td>
                <td style="text-align: center; width: 20px">31</td>
            </tr>
            <tr>
                {{-- Isi Tanggal Kehadiran --}}
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 21)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 21)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 21)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 21)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 21)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 21)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl',22)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl',22)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl',22)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl',22)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl',22)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl',22)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 23)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 23)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 23)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 23)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 23)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 23)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 24)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 24)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 24)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 24)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 24)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 24)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 25)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 25)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 25)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 25)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 25)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 25)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 26)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 26)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 26)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 26)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 26)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 26)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 27)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 27)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 27)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 27)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 27)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 27)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 28)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 28)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 28)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 28)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 28)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 28)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 29)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 29)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 29)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 29)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 29)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 29)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 30)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 30)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 30)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 30)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 30)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 30)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif
                @if ($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 31)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 31)->first()->status == "Hadir" : null)
                    <td height="15"><div class="hadir"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 31)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 31)->first()->status == "Sakit" : null)
                    <td height="15"><div class="sakit"></div></td>
                @elseif($siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 31)->first() ? $siswa -> absen -> where('bulan', $key)->where('id_perusahaan', $perusahaan->id)->where('tgl', 31)->first()->status == "Izin" : null)
                    <td height="15"><div class="izin"></div></td>
                @else
                    <td height="15"><div></div></td>
                @endif

            </tr>
            @endforeach
        </table>
    </div>

    <div style="margin-left: 90px">
        <table border="0">
            <tr>
                <td rowspan="1">KET :</td>
                <td style="width: 100px">Hadir : <div class="kethadir"></div></td>
            </tr>
            <tr>
                <td></td>
                <td style="width: 100px">Sakit : <div class="ketsakit"></div></td>
            </tr>
            <tr>
                <td></td>
                <td style="width: 100px">Izin : <div class="ketizin"></div></td>
            </tr>
        </table>




































    </div>
</body>
</html>
