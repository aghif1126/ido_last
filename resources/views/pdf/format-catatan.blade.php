<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div style="text-align: center">
        <h4>CATATAN SISWA DAN PEMBIMBING TEMPAT PKL</h4>
        <p>(Digunakan sebagai catatan siswa dan pembimbing sebagai masukan bagi pihak sekolah)</p>

        <div class="table-responsive" style="margin-left: 60px">
            <table border="1" class="table table-primary" style="border-collapse: collapse;">
                <tbody>
                    <tr class="">
                        <td>TANGGAL</td>
                        <td style="width: 400px;"><p style="text-align: center">CATATAN</p></td>
                        <td style="width: 70px;"><p style="text-align: center">PARAF</p></td>
                    </tr>
                    <tr>
                        <td><p style="text-align: center">1</p></td>
                        <td><p style="text-align: center">2</p></td>
                        <td><p style="text-align: center">3</p></td>
                    </tr>
                    <tr>
                        <td>&nbsp;<br><br><br><br><br><br></td>
                        <td>&nbsp;<br><br><br><br><br><br></td>
                        <td>&nbsp;<br><br><br><br><br><br></td>
                    </tr>
                    <tr>
                        <td>&nbsp;<br><br><br><br><br><br></td>
                        <td>&nbsp;<br><br><br><br><br><br></td>
                        <td>&nbsp;<br><br><br><br><br><br></td>
                    </tr>
                    <tr>
                        <td>&nbsp;<br><br><br><br><br><br></td>
                        <td>&nbsp;<br><br><br><br><br><br></td>
                        <td>&nbsp;<br><br><br><br><br><br></td>
                    </tr>
                    <tr>
                        <td>&nbsp;<br><br><br><br><br><br></td>
                        <td>&nbsp;<br><br><br><br><br><br></td>
                        <td>&nbsp;<br><br><br><br><br><br></td>
                    </tr>
                </tbody>
            </table>
        </div>
        
    </div>
</body>
</html>