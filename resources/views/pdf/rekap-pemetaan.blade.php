<style>
#pemetaan{
    border-collapse: collapse;
}

#pemetaan td, #pemetaan th {
    border: 1px solid #ddd;
    padding: 8px;
}
</style>
<h4 style="text-align: center">Rekap Data Pemetaan Tahun {{ $tahunrekap }}</h4>
<div style="margrin-left: 30px">
    <table id="pemetaan">
        <tr>
            <th style="width: 200px; text-align: center">Perusahaan</th>
            <th style="width: 350px; text-align: center">Jurusan</th>
            <th style="width: 70px; text-align: center">Jumlah</th>
        </tr>
        @foreach ($perusahaan as $item)

        <tr>
            <td style="text-align: center">{{ $item->nama }}</td>
            <td style="text-align: center">
            @foreach ($item->jurusan as $item1)
                    {{ $item1->nama }} <br>
                    @endforeach
                </td>
            {{-- @foreach ($item->siswa as $item2) --}}
                @if ($item->siswa == null)
                    <td style="text-align: center"></td>
                @else
                    <td style="text-align: center">{{ $item->siswa->count() }}</td>
                @endif
            {{-- @endforeach --}}
            {{-- <td style="text-align: center">&nbsp;</td> --}}
        </tr>
        @endforeach
    </table>
</div>
