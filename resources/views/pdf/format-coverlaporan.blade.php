<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .b{
            width: 140px;
            height: 140px;
            background-color: transparent;
            border: 2px solid;
            border-radius: 100%;
        }
    </style>
</head>
<body>
    <p style="margin-left: 10%">Cover Laporan PKL</p>
    <br>
    <div style="text-align: center">
        <h3>Laporan Hasil Praktik Kerja Lapangan</h3>
        <h3>Di PT. ............</h3>
        <br><br><br>
        <div class="b" style="margin-left: 285px"></div>
        <br>
        <p>Disusun oleh ...........</p>
        <table style="margin-left: 200px">
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>NPSN</td>
                <td>:</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Kelas</td>
                <td>:</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Konsentrasi Keahlian</td>
                <td>:</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <br>
        <div style="margin-top: 200px">
            <h3>PEMERINTAH DAERAH PROVINSI JAWA BARAT <br> DINAS PENDIDIKAN <br> CABANG DINAS PENDIDIKAN WILAYAH VII <br> SMK NEGERI 11 BANDUNG</h3>
            <p>Bisinis Manajemen - Teknologi Informatika dan Komunikasi <br> Jl Budhi Cilember (022) 6652442 Bandung 40175 <br> http://smkn11bdg.net E-mail:smkn11bdg@gmail.com</p>
        </div>
    </div>
</body>
</html>