<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h4 style="text-align: center;">TEMPAT PRAKTEK KERJA LAPANGAN (PKL)</h4>
    <br>
    
    <div class="table-responsive" style="margin-left: 40px">
        <table class="table table-primary">
            <tbody>
                <tr class="">
                    <td style="width: 200px">TEMPAT PKL</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>ALAMAT</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>TELEPON</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>FAX</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>E-MAIL</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>WEBSITE</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="margin-left: 43px"><p>PIMPINAN INDUSTRI &nbsp;&nbsp;:</p></div>
    
    <div class="table-responsive" style="margin-left: 70px">
        <table class="table table-primary">
            <tbody>
                <tr class="">
                    <td style="width: 170px">NAMA JABATAN</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>NAMA</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>PANGKAT</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>NIP</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="margin-left: 43px"><p>PEMBIMBING INDUSTRI &nbsp;&nbsp;:</p></div>

    <div class="table-responsive" style="margin-left: 70px">
        <table class="table table-primary">
            <tbody>
                <tr class="">
                    <td style="width: 170px">NAMA</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>JABATAN</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="">
                    <td>NIP</td>
                    <td>:</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
    <br>

    <p style="margin-left: 255px">Bandung, ....................., 20.....<br>Siswa<br><br><br><br><br></p>
    <div style="margin-left: 255px"><p>..................................................<br>NIS.</p></div>
    
</body>
</html>