<style>
    .hadir{
        width: 9px;
        height: 9px;
        background-color: lime;
        position: absolute;
        margin-top: 5px;
        margin-left: 5px;
    }

    .sakit{
        width: 9px;
        height: 9px;
        background-color: yellow;
        position: absolute;
        margin-top: 5px;
        margin-left: 5px;
    }

    .izin{
        width: 9px;
        height: 9px;
        background-color: orange;
        position: absolute;
        margin-top: 5px;
        margin-left: 5px;
    }

    .alpha{
        width: 9px;
        height: 9px;
        background-color: red;
        position: absolute;
        margin-top: 5px;
        margin-left: 5px;
    }
    .kethadir{
        width: 9px;
        height: 9px;
        background-color: lime;
        position: absolute;
        margin-top: 7px;
    }

    .ketsakit{
        width: 9px;
        height: 9px;
        background-color: yellow;
        position: absolute;
        margin-top: 7px;
    }

    .ketizin{
        width: 9px;
        height: 9px;
        background-color: orange;
        position: absolute;
        margin-top: 7px;
    }

    .ketalpha{
        width: 9px;
        height: 9px;
        background-color: red;
        position: absolute;
        margin-top: 7px;
    }
</style>

<h4 style="text-align: center">DAFTAR KEHADIRAN SISWA DI TEMPAT PKL</h4>
<div style="margin-left: 60px">
    {{-- Main table --}}
    <table border="1" style="border-collapse: collapse">
        <tr>
            <td rowspan="2"><p style="width: 30px; text-align: center">NO</p></td>
            <td rowspan="2"><p style="width: 80px; text-align: center">BULAN</p></td>
            <td colspan="11" rowspan="2"><p style="text-align: center">TANGGAL</p></td>
            <td colspan="6"><p style="text-align: center">REKAP</p></td>
        </tr>
        <tr>
            <td><p style="width: 15; text-align: center">S</p></td>
            <td><p style="width: 15; text-align: center">I</p></td>
            <td><p style="width: 15; text-align: center">A</p></td>
            <td style="width: 30px; text-align: center">JML TDK MASUK</td>
            <td style="width: 30px; text-align: center">JML HRS MSK</td>
            <td style="width: 30px; text-align: center">% HADIR</td>
        </tr>
        <tr>
            <td rowspan="6"></td>
            <td rowspan="6"></td>
            <td style="text-align: center; width: 20px">1</td>
            <td style="text-align: center; width: 20px">2</td>
            <td style="text-align: center; width: 20px">3</td>
            <td style="text-align: center; width: 20px">4</td>
            <td style="text-align: center; width: 20px">5</td>
            <td style="text-align: center; width: 20px">6</td>
            <td style="text-align: center; width: 20px">7</td>
            <td style="text-align: center; width: 20px">8</td>
            <td style="text-align: center; width: 20px">9</td>
            <td style="text-align: center; width: 20px">10</td>
            <td rowspan="4"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            {{-- Isi Tanggal Kehadiran --}}
            <td height="19px"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            {{-- Isi Tanggal Kehadiran (Sampai Sini) --}}

            {{-- Rekap Sakit, Izin, Alpha --}}
            <td></td>
            <td></td>
            <td></td>
            {{-- Rekap Sakit, Izin, Alpha (Sampai Sini) --}}

            {{-- Jumlah Tidak Masuk, Jumlah Harus Masuk, Persentase Hadir --}}
            <td></td>
            <td></td>
            <td></td>
            {{-- Jumlah Tidak Masuk, Jumlah Harus Masuk, Persentase Hadir (Sampai Sini) --}}
        </tr>
        <tr>
            <td style="text-align: center">11</td>
            <td style="text-align: center">12</td>
            <td style="text-align: center">13</td>
            <td style="text-align: center">14</td>
            <td style="text-align: center">15</td>
            <td style="text-align: center">16</td>
            <td style="text-align: center">17</td>
            <td style="text-align: center">18</td>
            <td style="text-align: center">19</td>
            <td style="text-align: center">20</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            {{-- Isi Tanggal Kehadiran --}}
            <td height="19px"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            {{-- Isi Tanggal Kehadiran (Sampai Sini) --}}

            {{-- Rekap Sakit, Izin, Alpha --}}
            <td></td>
            <td></td>
            <td></td>
            {{-- Rekap Sakit, Izin, Alpha (Sampai Sini) --}}

            {{-- Jumlah Tidak Masuk, Jumlah Harus Masuk, Persentase Hadir --}}
            <td></td>
            <td></td>
            <td></td>
            {{-- Jumlah Tidak Masuk, Jumlah Harus Masuk, Persentase Hadir (Sampai Sini) --}}
        </tr>
        <tr>
            <td style="text-align: center">21</td>
            <td style="text-align: center">22</td>
            <td style="text-align: center">23</td>
            <td style="text-align: center">24</td>
            <td style="text-align: center">25</td>
            <td style="text-align: center">26</td>
            <td style="text-align: center">27</td>
            <td style="text-align: center">28</td>
            <td style="text-align: center">29</td>
            <td style="text-align: center">30</td>
            <td style="text-align: center; width: 20px">31</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            {{-- Isi Tanggal Kehadiran --}}
            <td height="19px"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            {{-- Isi Tanggal Kehadiran (Sampai Sini) --}}

            {{-- Rekap Sakit, Izin, Alpha --}}
            <td></td>
            <td></td>
            <td></td>
            {{-- Rekap Sakit, Izin, Alpha (Sampai Sini) --}}

            {{-- Jumlah Tidak Masuk, Jumlah Harus Masuk, Persentase Hadir --}}
            <td></td>
            <td></td>
            <td></td>
            {{-- Jumlah Tidak Masuk, Jumlah Harus Masuk, Persentase Hadir (Sampai Sini) --}}
        </tr>
    </table>
</div>

<div>
    <table border="0">
        <tr>
            <td rowspan="1">KET :</td>
            <td style="width: 100px">Hadir : <div class="kethadir"></div></td>
        </tr>
        <tr>
            <td></td>
            <td style="width: 100px">Sakit : <div class="ketsakit"></div></td>
        </tr>
        <tr>
            <td></td>
            <td style="width: 100px">Izin : <div class="ketizin"></div></td>
        </tr>
        <tr>
            <td></td>
            <td style="width: 100px">Alpha : <div class="ketalpha"></div></td>
        </tr>
    </table>
</div>