<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nilai</title>
</head>
<body style="margin-left: 5%">
    <p>Format Penilaian PKL.</p>
    <br>

    <div class="table-responsive">
        <table class="table table-primary">
            <tbody>
                <tr>
                    <td>Nama Peserta Didik</td>
                    <td>:</td>
                    <td>{{ $siswa->nama }}</td>
                </tr>
                <tr>
                    <td>Kelas</td>
                    <td>:</td>
                    <td>{{ $siswa->kelas->nama }}</td>
                </tr>
                <tr>
                    <td>Semester</td>
                    <td>:</td>
                    <td>{{ $siswa->semester }}</td>
                </tr>
                <tr>
                    <td>Kompetensi Keahlian</td>
                    <td>:</td>
                    <td>{{ $siswa->kelas->konsentrasi->jurusan->nama }}</td>
                </tr>
                <tr>
                    <td>Nama Industri</td>
                    <td>:</td>
                    <td>{{ $siswa->perusahaan->nama }}</td>
                </tr>
                <tr>
                    <td>Nama Instruktur</td>
                    <td>:</td>
                    <td>{{ $siswa->perusahaan->pembimbing->nama }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <br>

    <div style="margin-left: 30px">
        <table border="1" style="border-collapse:collapse;">
            <tbody>
                <tr>
                    <td class="table-secondary" style="height: 10px; width:30px; text-align:center"><p>No</p></td>
                    <td class="table-secondary" style="text-align: center">Komponen</p></td>
                    <td style="width:100px; text-align:center;">Skor<br>(0-100)</p></td>
                    <td class="table-secondary">Keterangan</p></td>

                </tr>

                <tr>
                    <td rowspan="1">1</td>
                    <td>Aspek Sikap</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td rowspan="6"></td>
                    <td>a. Penampilan dan kerapihan pakaian</td>
                    <td style="text-align:center;">{{ $siswa->nilai->penampilan }}</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>b. Komitmen dan integritas</td>
                    <td style="text-align:center;">{{ $siswa->nilai->komitmen }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>c. Menghargai dan menghormati (kesopanan)</td>
                    <td style="text-align:center;">{{ $siswa->nilai->kesopanan }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>d. Kreativitas</td>
                    <td style="text-align:center;">{{ $siswa->nilai->kreativitas }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>e. Kerja sama tim</td>
                    <td style="text-align:center;">{{ $siswa->nilai->kerjasamatim }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>f. Disiplin dan tanggung jawab</td>
                    <td style="text-align:center;">{{ $siswa->nilai->disiplin }}</td>
                    <td></td>
                </tr>

                <tr>
                    <td rowspan="1">2</td>
                    <td>Aspek Pengetahuan</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td rowspan="3"></td>
                    <td>a. Penguasaan keilmuan</td>
                    <td style="text-align:center;">{{ $siswa->nilai->penguasaan_keilmuan }}</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>b. Kemampuan mengidentifikasi masalah</td>
                    <td style="text-align:center;">{{ $siswa->nilai->kemampuan_identifikasi }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>c. Kemampuan menemukan alternatif solusi secara kreatif</td>
                    <td style="text-align:center;">{{ $siswa->nilai->kemampuan_alternatif }}</td>
                    <td></td>
                </tr>

                <tr>
                    <td rowspan="1">3</td>
                    <td>Aspek Pengetahuan</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td rowspan="4"></td>
                    <td>a. Keahlian dan keterampilan</td>
                    <td style="text-align:center;">{{ $siswa->nilai->keterampilan }}</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>b. Inovasi dan kreatifitas</td>
                    <td style="text-align:center;">{{ $siswa->nilai->inovasi }}</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>c. Produktifitas dan penyelesaian tugas</td>
                    <td style="text-align:center;">{{ $siswa->nilai->produktivitas }}</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>d. Penguasaan alat kerja</td>
                    <td style="text-align:center;">{{ $siswa->nilai->penguasaan_alatkerja }}</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
    
</body>
</html>