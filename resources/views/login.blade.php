<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{{ asset('login/assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('login/assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('login/assets/css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('/logo/Logo.png') }}" rel="icon">
    <title>Login</title>
  </head>
  <body bgcolor="#f6f9ff">
    {{-- ini splash screen --}}
        <style>
            .spinner {
                width: 100px;
                height: 100px;
                display: grid;
                border: 7px solid #0000;
                border-radius: 50%;
                border-color: #3b71ca #0000;
                animation: spinner-e04l1k 0.8s infinite linear;
            }

            .spinner::before,
            .spinner::after {
                content: "";
                grid-area: 1/1;
                margin: 3.5px;
                border: inherit;
                border-radius: 50%;
            }

            .spinner::before {
                border-color: #9fa6b2 #0000;
                animation: inherit;
                animation-duration: 0.4s;
                animation-direction: reverse;
            }

            .spinner::after {
                margin: 14px;
            }

            @keyframes spinner-e04l1k {
                100% {
                    transform: rotate(1turn);
                }
            }
            .preloader {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                z-index: 9999;
                /* background-color: #fff; */
            }
            .preloader .loading {
                position: absolute;
                left: 50%;
                top: 50%;
                transform: translate(-50%,-50%);
                font: 14px arial;
            }
            #spinner-overlay {
                display: none;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(255, 255, 255, 1); /* White overlay */
                z-index: 9999;
                opacity: 0.9;
            }

            #spinner-container {
                display: none;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                display: flex;
                align-items: center;
                justify-content: center;
                z-index: 10000;
            }
            .pagination {
                display: inline-block;
            }

            .pagination a {
                color: black;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }
        </style>

        <div id="spinner-overlay">
            <div id="spinner-container">
                <div id="spinner" class="bg-white d-flex align-items-center justify-content-center">
                    <div class="spinner"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            function showSpinner() {
                document.getElementById('spinner-overlay').style.display = 'block';
                document.getElementById('spinner-container').style.display = 'flex';
            }

            // Fungsi untuk menyembunyikan spinner
            function hideSpinner() {
                document.getElementById('spinner-overlay').style.display = 'none';
                document.getElementById('spinner-container').style.display = 'none';
            }

            // Event listener saat halaman sedang dimuat
            window.addEventListener('beforeunload', function () {
                showSpinner();
            });

            // Event listener saat halaman sudah dimuat
            window.addEventListener('unload', function () {
                hideSpinner();
            });
        </script>
    {{-- ini splash screen --}}

    @if (Session::has('status'))
        <div class="alert alert-danger align align-center" role="alert">
            {{ Session::get('message') }}
        </div>
    @endif

      <section class="form-01-main">
        <div class="logo">
            <img src="{{asset('/logo/Logo.png')}}" alt="Logo" width="75" height="75"
                class="d-inline-block align-text-top">
        </div>
      <div class="form-cover">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="form-sub-main">
            <form action="/login/auth" method="post">
                @csrf
                <div class="d-flex mb-3">
                    <div class="me-auto p-2">
                        <p class="headersign" style="font-size: 200%; font-family: Rum Raisin;">Sign Here!</p>
                    </div>
                    <div class="llogo p2 mt-3">
                        <img src="{{asset('/logo/Logo.png')}}" alt="Logo" width="50" height="50">{{-- I-Do --}}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-11">
                        <label class="form-label" for="form3Example3">Username</label>
                        <input type="text" name="email" id="form3Example3" class="form-control"
            style="border: 2px solid #899bbd; background: #f6f9ff;"
            placeholder="Username" onfocus="focusevent()" onblur="blurevent()"
                required />
                        {{-- <input style="border: 2px solid #899bbd;" id="email" name="email" class="form-control" type="text" placeholder="Username" aria-required="true" required> --}}
                    </div>
                    {{-- <label class="form-label" for="form3Example3">Username</label>
                    <input style="border: 2px solid #899bbd;" id="email" name="email" class="form-control _ge_de_ol" type="text" placeholder="Username" aria-required="true" required> --}}
                </div>

                <div class="form-group">
                    <div class="col-md-11">
                        <label class="form-label" for="form3Example3">Password</label>
                        <input type="password" name="password" id="inputPassword"
                            style="border: 2px solid #899bbd;"
                            class="form-control" placeholder="*********"
                            onfocus="focusevent1()" onblur="blurevent1()" required />
                    </div>
                    {{-- <input style="border: 2px solid #899bbd;" id="password" type="password" class="form-control" name="password" placeholder="********" required="required"> --}}
                </div>

                <div class="liaht">
                        <div class="form-group">
                        <div class="col-sm-12">
                            <input type="checkbox" class="form-check-input" id="show-password"
                            onclick="myFunction()"> Show Password
                            <script>
                                function focusevent() {
                                    document.getElementById("form3Example3").style.background = "#dcdcdc";
                                }
                                function myFunction() {
                                    var x = document.getElementById("inputPassword");
                                    if (x.type === "password") {
                                        x.type = "text";
                                    } else {
                                        x.type = "password";
                                    }
                                }
                            </script>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="btn_uy">
                        <div class="p-2">
                            <button type="submit" class="btn btn-secondary btn-lg ">
                                <font style=" font-family: Rum Raisin;">Login</font>
                            </button>

                        </div>
                    </div>
                </div>
            </form>
            </div>
          </div>
        </div>
      </div>
      </div>
      <footer class="footer">
          <font face="WildWest" size="3" color="#ffffff"> © 2023 I-Do web pkl untuk anda</font>
      </footer>
    </section>
  </body>
</html>
