<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('guru_perusahaan', function (Blueprint $table) {
            $table->id();
            $table->UnsignedbigInteger('id_guru')->nullable();
            $table->foreign('id_guru')->references('id')->on('gurus');
            $table->UnsignedbigInteger('id_perusahaan')->nullable();
            $table->foreign('id_perusahaan')->references('id')->on('perusahaans');
            $table->UnsignedbigInteger('id_jurusan')->nullable();
            $table->foreign('id_jurusan')->references('id')->on('jurusan');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('guru_perusahaans');
    }
};
