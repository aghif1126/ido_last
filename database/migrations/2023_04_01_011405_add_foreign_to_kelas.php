<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('kelas', function (Blueprint $table) {
            // $table->unsignedBigInteger('id_guru')->nullable();
            // $table->foreign('id_guru')->references('id')->on('gurus')->onUpdate('cascade')->onDelete('restrict');
            $table->unsignedBigInteger('id_konsentrasi_keahlian');
            $table->foreign('id_konsentrasi_keahlian')->references('id')->on('konsentrasi_keahlian')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('kelas', function (Blueprint $table) {
            $table->dropForeign('id_konsentrasi_keahlian');
            $table->dropColumn('id_konsentrasi_keahlian');
        });
    }
};
