<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('jurusan_perusahaans', function (Blueprint $table) {
            $table->id();
            $table->integer('kuota')->nullable();
            $table->UnsignedbigInteger('id_perusahaan')->nullable();
            $table->foreign('id_perusahaan')->references('id')->on('perusahaans')->onDelete('restrict')->onUpdate('cascade');
            $table->UnsignedbigInteger('id_jurusan')->nullable();
            $table->foreign('id_jurusan')->references('id')->on('jurusan')->onDelete('restrict')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('jurusan_perusahaans');
    }
};
