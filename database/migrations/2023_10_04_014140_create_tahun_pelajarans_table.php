<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tahun_pelajaran', function (Blueprint $table) {
            $table->id();
            $table->string('tahun');
            $table->enum('status', ['aktif', 'tidak aktif'])->nullable()->default('tidak aktif');
            $table->timestamps();
        });

        Schema::table('siswas', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun')->nullable();
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('perusahaans', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('pimpinan_perusahaans', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('pembimbing_perusahaans', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('aktivitas', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('nilais', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('gurus', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('roles', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('jurusan', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('kelas', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('konsentrasi_keahlian', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('catatans', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('jurusan_perusahaans', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('monitorings', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('siswa_perusahaans', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('absen', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
        Schema::table('kriteria', function (Blueprint $table) {
            $table->UnsignedbigInteger('id_tahun');
            $table->foreign('id_tahun')->references('id')->on('tahun_pelajaran');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tahun_pelajarans');
    }
};
