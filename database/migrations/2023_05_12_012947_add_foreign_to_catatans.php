<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('catatans', function (Blueprint $table) {
            $table->string('id_siswa')->nullable();
            $table->foreign('id_siswa')->references('nis')->on('siswas')->onDelete('cascade')->onUpdate('cascade');
            $table->UnsignedbigInteger('id_perusahaan')->nullable();
            $table->foreign('id_perusahaan')->references('id')->on('perusahaans')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('catatans', function (Blueprint $table) {
            //
        });
    }
};
