<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('siswa_perusahaans', function (Blueprint $table) {
            $table->id();
            $table->string('id_siswa')->nullable();
            $table->foreign('id_siswa')->references('nis')->on('siswas')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('id_perusahaan')->nullable();
            $table->foreign('id_perusahaan')->references('id')->on('perusahaans')->onDelete('cascade')->onUpdate('cascade');
            $table->date('awal_periode')->nullable();
            $table->date('akhir_periode')->nullable();
            $table->string('periode', 100)->nullable();
            $table->enum('status', ['Sudah', 'Belum'])->nullable()->default('Belum');
            $table->enum('confirm', ['Sudah', 'Belum'])->nullable()->default('Belum');
            $table->unsignedBigInteger('id_jurusan')->nullable();
            $table->foreign('id_jurusan')->references('id')->on('jurusan')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('siswa_perusahaans');
    }
};
