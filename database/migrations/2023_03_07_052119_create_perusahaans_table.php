<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('perusahaans', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('alamat');
            $table->string('email');
            $table->string('fax')->default('Tidak Tersedia');
            $table->string('telp');
            $table->integer('kuota');
            $table->string('website')->default('Tidak Tersedia');
            $table->string('ket')->default('Tidak Tersedia');
            $table->string('lokasi', 1000)->default('Tidak Tersedia');
            $table->enum('mou', ['ya', 'tidak'])->default('tidak');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('perusahaans');
    }
};
