<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('absen', function (Blueprint $table) {
            $table->id();
            $table -> string('bulan');
            $table -> date('tanggal');
            $table -> string('jam_masuk');
            $table -> string('poto_masuk');
            $table -> string('jam_pulang')->default('Tidak Ada');
            $table -> string('poto_pulang')->default('Tidak Ada');
            $table->enum('status', ['Hadir', 'Izin', 'Sakit']);
            $table -> string('tgl');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('absen');
    }
};
