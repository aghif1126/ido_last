<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kriteria', function (Blueprint $table) {
            $table->id();
            $table -> string('nama_kriteria');
            $table->unsignedBigInteger('id_pembimbing')->nullable();
            $table->foreign('id_pembimbing')->references('id')->on('pembimbing_perusahaans')->onDelete('restrict')->onUpdate('cascade');
            $table->timestamps();
        });

        Schema::table('nilais', function (Blueprint $table) {


            $table->string('id_siswa')->nullable();
            $table->foreign('id_siswa')->references('nis')->on('siswas')->onDelete('restrict')->onUpdate('cascade');
            $table->unsignedBigInteger('id_kriteria')->nullable();
            $table->foreign('id_kriteria')->references('id')->on('kriteria')->onDelete('restrict')->onUpdate('cascade');
            $table->unsignedBigInteger('id_pembimbing')->nullable();
            $table->foreign('id_pembimbing')->references('id')->on('pembimbing_perusahaans')->onDelete('restrict')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kriteria');
    }
};
