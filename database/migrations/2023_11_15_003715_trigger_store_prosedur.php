<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::unprepared('CREATE TRIGGER siswa_user AFTER INSERT ON `siswas` FOR EACH ROW
        BEGIN
           INSERT INTO `users` (`name`, `email`, `role_id`, `id_tahun`) VALUES (NEW.nama, NEW.nis, 4, NEW.id_tahun);
        END');
        DB::unprepared('CREATE TRIGGER delete_user AFTER DELETE ON `siswas` FOR EACH ROW
        BEGIN
           DELETE from `users` where id = OLD.id_user;
           DELETE from `siswa_perusahaans` where id_siswa = OLD.nis;
        END;');
        DB::unprepared('CREATE TRIGGER delete_perusahaan AFTER DELETE ON `perusahaans` FOR EACH ROW
        BEGIN
           DELETE from `pembimbing_perusahaans` where id_perusahaan = OLD.id;
           DELETE from `pimpinan_perusahaans` where id_perusahaan = OLD.id;
           DELETE from `jurusan_perusahaans` where id_perusahaan = OLD.id;
           DELETE from `siswa_perusahaans` where id_perusahaan = OLD.id;
        END;');

        $procedure = "DROP PROCEDURE IF EXISTS `show_kelas`;
        CREATE PROCEDURE `show_kelas` (IN id_tahun INT)
        BEGIN
        SELECT kelas.id as id, kelas.nama as nama, konsentrasi_keahlian.nama as konsentrasi, jurusan.nama as jurusan  from kelas INNER JOIN konsentrasi_keahlian ON kelas.id_konsentrasi_keahlian = konsentrasi_keahlian.id INNER JOIN jurusan ON konsentrasi_keahlian.id_jurusan=jurusan.id WHERE kelas.id_tahun = id_tahun;
        END;
        ";

        DB::unprepared($procedure);
    }
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
