<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('siswas', function (Blueprint $table) {
            // $table->string('id')->primary();

            $table->string('nis')->primary();
            $table->string('nama');
            // $table->string('kelas');
            $table->enum('jenkel', ['P', 'L']);
            $table->string('telp')->default('Tidak tersedia');
            $table->string('alamat')->default('Tidak tersedia');
            $table->date('tanggal_lahir')->default('2023-02-02');
            $table->string('foto')->default('Tidak tersedia');
            $table->string('nama_ortu')->default('Tidak tersedia');
            $table->string('alamat_ortu')->default('Tidak tersedia');
            $table->string('telp_ortu')->default('Tidak tersedia');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('siswas');
    }
};
