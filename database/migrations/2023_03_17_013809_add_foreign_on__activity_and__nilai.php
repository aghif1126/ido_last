<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('aktivitas', function (Blueprint $table) {
            $table->string('id_siswa');
            $table->foreign('id_siswa')->references('nis')->on('siswas')->onDelete('restrict')->onUpdate('cascade');
            $table->unsignedBigInteger('id_perusahaan');
            $table->foreign('id_perusahaan')->references('id')->on('perusahaans')->onDelete('restrict')->onUpdate('cascade');

        });

        // Schema::table('nilais', function (Blueprint $table) {

        //     $table->unsignedBigInteger('id_siswa')->nullable();
        //     $table->foreign('id_siswa')->references('id')->on('siswas')->onDelete('restrict')->onUpdate('cascade');
        //     $table->unsignedBigInteger('id_perusahaan')->nullable();
        //     $table->foreign('id_perusahaan')->references('id')->on('perusahaans')->onDelete('restrict')->onUpdate('cascade');

        // });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('aktivitas', function (Blueprint $table) {
            $table->dropForeign('id_siswa');
        });
        Schema::table('nilais', function (Blueprint $table) {
            $table->dropForeign('id_siswa');
        });
    }
};
