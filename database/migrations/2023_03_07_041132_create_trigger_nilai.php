<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // DB::unprepared('
        // CREATE TRIGGER isi_nilai AFTER INSERT ON `siswas` FOR EACH ROW
        //     BEGIN
        //         INSERT INTO nilais (`id_siswa`)
        //         VALUES (New.id);
        //     END
        // ');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // DB::unprepared('DROP TRIGGER `tr_User_Default_Member_Role`');
    }
};
