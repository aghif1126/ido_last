<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::unprepared('DROP VIEW IF EXISTS `view_monitoring`;
        CREATE VIEW view_monitoring AS
    SELECT  monitorings.id as id, kehadiran, sikap, kompetensi, tanggal, photo, gurus.nama as guru, perusahaans.nama as perusahaans, gurus.id as id_guru, perusahaans.id as id_perusahaan from monitorings  INNER JOIN gurus ON monitorings.id_guru=gurus.id INNER JOIN perusahaans on perusahaans.id=monitorings.id_perusahaan

');

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
