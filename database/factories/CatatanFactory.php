<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as faker;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class CatatanFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker = faker::create('id_ID');
        return [
            'catatan' => $faker -> paragraphs(3, true),
            'tanggal' => $faker -> date(),
            'id_siswa' => mt_rand(1,476),
            'id_perusahaan' => mt_rand(1,119)
        ];
    }
}
