<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as faker;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Nilai>
 */
class NilaiFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker = faker::create('id_ID');
        return [
            'penampilan' => mt_rand(1,100),
            'komitmen' => mt_rand(1,100),
            'kesopanan' => mt_rand(1,100),
            'kreativitas' => mt_rand(1,100),
            'kerjasamatim' => mt_rand(1,100),
            'disiplin' => mt_rand(1,100),
            'penguasaan_keilmuan' => mt_rand(1,100),
            'kemampuan_identifikasi' => mt_rand(1,100),
            'kemampuan_alternatif' => mt_rand(1,100),
            'keterampilan' => mt_rand(1,100),
            'inovasi' => mt_rand(1,100),
            'produktivitas' => mt_rand(1,100),
            'penguasaan_alatkerja' => mt_rand(1,100),

            'id_perusahaan' => mt_rand(1,119)
        ];
    }
}
