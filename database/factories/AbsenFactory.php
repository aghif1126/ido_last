<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as faker;


/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Absen>
 */
class AbsenFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker = faker::create('id_ID');
        $bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli"];
        $status = ["Hadir", "Izin", "Sakit"];
        return [
            'bulan' => $bulan[mt_rand(0,6)],
            'tgl' => mt_rand(1,31),
            'tanggal' => $faker -> date(),
            'jam_masuk' => $faker -> time('H:i:s'),
            'poto_masuk' => '',
            'jam_pulang' => $faker -> time('H:i:s'),
            'status' => $status[mt_rand(0,2)],
            'poto_pulang' => '',
            'id_siswa' => '001',
            'id_tahun' => 2
        ];
    }
}
