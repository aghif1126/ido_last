<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as faker;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class PembimbingPerusahaanFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $jenkel = ['P', 'L'];
        $faker = faker::create('id_ID');
        return [
            'nama' => $faker -> name(),
            'nip' => mt_rand(0000000001, 9999999999),
            'alamat' => $faker -> streetAddress(),
            'telp' => $faker -> phoneNumber(),
            'jenkel' => $jenkel[mt_rand(0,1)],
            'id_tahun' => '2'
            // 'id_perusahaan' => mt_rand(1,10)
        ];
    }
}
