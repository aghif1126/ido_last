<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as faker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Perusahaan>
 */
class PerusahaanFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker = faker::create('id_ID');
        return [
            'nama' => $faker -> company(),
            'alamat' => $faker -> streetAddress(),
            'email' => $faker->email(),
            'fax' => $faker->PhoneNumber(),
            'telp' => $faker -> phoneNumber(),
            'id_tahun' => 2
        ];
    }
}
