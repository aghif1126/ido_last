<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as faker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Siswa>
 */
class SiswaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $jenkel = ['P', 'L'];
        $faker = faker::create('id_ID');
        // $point = mt_rand(0,2);
        return [
            'nama' => $faker -> name(),
            'nis' => mt_rand(0000000001, 9999999999),

            // 'kelas' => $kelas[$point],
            'telp' => $faker -> phoneNumber(),
            'jenkel' => $jenkel[mt_rand(0,1)],
            'alamat' => $faker -> streetAddress(),
            'tanggal_lahir' => $faker -> date('Y-m-d'),
            'semester' => mt_rand(5,6),
            'tahun_pelajaran' => '2023/2024',
            // 'periode' => $periode[array_rand($periode)],
            // 'program_keahlian' => 'TIK',
            // 'konsentrasi_keahlian' => $kelas[$point],
            'nama_ortu' => $faker -> name(),
            'alamat_ortu' => $faker -> streetAddress(),
            'telp_ortu' => $faker -> phoneNumber()
            // 'id_kelas' => mt_rand(1,14)

        ];
    }
}
