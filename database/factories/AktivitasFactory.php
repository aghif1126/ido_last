<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as faker;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Aktivitas>
 */
class AktivitasFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $faker = faker::create('id_ID');
        return [
            'kegiatan' => $faker -> paragraphs(2, true),
            'divisi' => '-',
            'tanggal' => $faker -> date(),
            'mulai' => $faker -> time('H:i:s'),
            'selesai' => $faker -> time('H:i:s'),
            'id_siswa' => mt_rand(1,476),
            'id_perusahaan' => mt_rand(1,119)
        ];
    }
}
