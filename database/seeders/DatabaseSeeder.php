<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use Carbon\Carbon;
use App\Models\Guru;
use App\Models\Role;
use App\Models\User;
use App\Models\Kelas;

use App\Models\Siswa;
use App\Models\Kaprog;
use App\Models\Jurusan;
use App\Models\Aktivitas;
use App\Models\Catatan;
use App\Models\JurusanPerusahaan;
use App\Models\Perusahaan;
use Illuminate\Database\Seeder;
use App\Models\KonsentrasiKeahlian;
use App\Models\Nilai;
use App\Models\PembimbingPerusahaan;
use App\Models\PimpinanPerusahaan;
use App\Models\TahunPelajaran;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        TahunPelajaran::create([
            'tahun' => '2022/2023',
            'status' => 'tidak aktif'
        ]);
        TahunPelajaran::create([
            'tahun' => '2023/2024',
            'status' => 'aktif'
        ]);
        TahunPelajaran::create([
            'tahun' => '2024/2025',
            'status' => 'tidak aktif'
        ]);
        // Perusahaan::factory() -> count(119) -> create();
        // Guru::create([
        //     'nama' => 'Fajar Fatkur',
        //     'nip' => mt_rand(0000000001, 9999999999),
        //     'jenkel' => 'P',
        //     'alamat' => 'Melong',
        //     'telp' => '03838823455',
        //     'status' => 'kaprog',
        //     'id_tahun' => '2'
        // ]);
        // Guru::create([
        //     'nama' => 'Dedi Nurhamdani',
        //     'nip' => mt_rand(0000000001, 9999999999),
        //     'jenkel' => 'P',
        //     'alamat' => 'Melong',
        //     'telp' => '03838823455',
        //     'status' => 'kaprog',
        //     'id_tahun' => '2'
        // ]);
        // Guru::create([
        //     'nama' => 'Fathir Alif',
        //     'nip' => mt_rand(0000000001, 9999999999),
        //     'jenkel' => 'P',
        //     'alamat' => 'Melong',
        //     'telp' => '03838823455',
        //     'status' => 'kaprog',
        //     'id_tahun' => '2'
        // ]);
        // Guru::create([
        //     'nama' => 'Monica Redhita',
        //     'nip' => mt_rand(0000000001, 9999999999),
        //     'jenkel' => 'P',
        //     'alamat' => 'Melong',
        //     'telp' => '03838823455',
        //     'status' => 'kaprog',
        //     'id_tahun' => '2'
        // ]);
        // Guru::create([
        //     'nama' => 'Siti Nur',
        //     'nip' => mt_rand(0000000001, 9999999999),
        //     'jenkel' => 'P',
        //     'alamat' => 'Melong',
        //     'telp' => '03838823455',
        //     'status' => 'kaprog',
        //     'id_tahun' => '2'
        // ]);
        // Guru::create([
        //     'nama' => 'Tsalas Khatulistiwa',
        //     'nip' => mt_rand(0000000001, 9999999999),
        //     'jenkel' => 'P',
        //     'alamat' => 'Melong',
        //     'telp' => '03838823455',
        //     'status' => 'kaprog',
        //     'id_tahun' => '2'
        // ]);
        // Guru::factory() -> count(30) -> create();


        // Kaprog::factory() -> count(10) -> create();

        $data = [
            ['name' => 'Pak Dedi'],
            ['name' => 'Pak Zimzim'],
            ['name' => 'Pak Ibrahim'],
            ['name' => 'Bu Santi'],
            ['name' => 'Pak Masyudi'],
        ];

        // foreach ($data as $value) {
        //     Kaprog::insert([
        //         'name' => $value['name'],
        //         'created_at' => Carbon::now(),
        //         'updated_at' => Carbon::now()

        //     ]);
        // }

        User::insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => password_hash('admin', PASSWORD_DEFAULT),
            'pass' => 'admin',
            'id_tahun' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);






        $data = [
            ['name' => 'Admin'],
            ['name' => 'Pembimbing'],
            ['name' => 'GuruPembimbing'],
            ['name' => 'Siswa'],
            ['name' => 'Hubin'],
            ['name' => 'Kaprog'],
        ];

        foreach ($data as $value) {
            Role::insert([
                'name' => $value['name'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'id_tahun' => 2

            ]);
        }

        # isi data jurusan
        $data = [
            ['nama' => 'Pengembangan Perangkat Lunak dan Gim', 'id_guru' => '1'],
            ['nama' => 'Desain Komunikasi Visual', 'id_guru' => '2'],
            ['nama' => 'Teknik Jaringan Komputer dan Telekomunikasi', 'id_guru' => '3'],
            ['nama' => 'Pemasaran', 'id_guru' => '4'],
            ['nama' => 'Akuntansi dan Keuangan Lembaga', 'id_guru' => '5'],
            ['nama' => 'Manajemen Perkantoran dan Layanan Bisnis', 'id_guru' => '6'],
        ];
        foreach ($data as $value) {
            Jurusan::insert([
                'nama' => $value['nama'],
                // 'id_kaprog' => $value['id_kaprog'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'id_tahun' => 2

            ]);
        }

        #isi data konsentrasi keahlian

        $data = [
            ['nama' => 'Rekayasa Perangkat Lunak', 'id_jurusan' => '1'],
            ['nama' => 'Teknik Komputer dan Jaringan', 'id_jurusan' => '3'],
            ['nama' => 'Bisnis Retail', 'id_jurusan' => '4'],
            ['nama' => 'Manajemen Perkantoran', 'id_jurusan' => '6'],
            ['nama' => 'Manajemen Logistik', 'id_jurusan' => '6'],
            ['nama' => 'Akutansi', 'id_jurusan' => '5'],
            ['nama' => 'Desain Komunikasi Visual', 'id_jurusan' => '2'],
        ];
        foreach ($data as $value) {
            KonsentrasiKeahlian::insert([
                'nama' => $value['nama'],
                'id_jurusan' => $value['id_jurusan'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'id_tahun' => 2

            ]);
        }



        # isi data kelas

        $data = [

            ['nama' => '11 PPLG 1', 'id_jurusan' => '1'],
            ['nama' => '11 PPLG 2', 'id_jurusan' => '1'],
            ['nama' => '11 KDKV 1', 'id_jurusan' => '7'],
            ['nama' => '11 KDKV 2', 'id_jurusan' => '7'],
            ['nama' => '11 TJKT 1', 'id_jurusan' => '2'],
            ['nama' => '11 TJKT 2', 'id_jurusan' => '2'],
            ['nama' => '11 AKL 1', 'id_jurusan' => '6'],
            ['nama' => '11 AKL 2', 'id_jurusan' => '6'],
            ['nama' => '11 MLOG 1', 'id_jurusan' => '5'],
            ['nama' => '11 MLOG 2', 'id_jurusan' => '5'],
            ['nama' => '11 BR 1', 'id_jurusan' => '3'],
            ['nama' => '11 BR 2', 'id_jurusan' => '3'],
            ['nama' => '11 MP 1', 'id_jurusan' => '4'],
            ['nama' => '11 MP 2', 'id_jurusan' => '4']

        ];
        foreach ($data as $value) {
            Kelas::insert([
                'nama' => $value['nama'],
                'id_konsentrasi_keahlian' => $value['id_jurusan'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'id_tahun' => 2

            ]);
        }

        // Siswa::factory() -> count(476) -> create();

        // $i = 1;
        // $o = 1;
        // #update data user di siswa
        // for ($j=1; $j < 15; $j++) {
        //     $batas = $i + 34;
        //     for ($i; $i < $batas; $i++) {

        //             $flight = Siswa::findorFail($i);

        //             $flight->id_kelas = $j;

        //             $flight->save();

        //         $o += 1;

        //     }


        // }


        // $confirm = ['Sudah', 'Belum'];
        // for ($i=1; $i < 200; $i++) {
        //     $flight = Siswa::findorFail($i);

        //     $flight->id_perusahaan = mt_rand(1,10);
        //     $flight->status = 'Sudah';
        //     $flight->confirm = $confirm[mt_rand(0,1)];
        //     $flight->save();
        // }
        $flight = User::findorFail(1);

        $flight->role_id = 1;

        $flight->save();


        // Aktivitas::factory() -> count(500) -> create();
        // Catatan::factory() -> count(500) -> create
        // PembimbingPerusahaan::factory()->count(119)->create();
        // for ($i=1; $i < 120; $i++) {
        //     $flight = PembimbingPerusahaan::find($i);

        //     $flight->id_perusahaan = $i;

        //     $flight->save();
        // }
        // PimpinanPerusahaan::factory()->count(119)->create();
        // for ($i=1; $i < 120; $i++) {
        //     $flight = PimpinanPerusahaan::find($i);

        //     $flight->id_perusahaan = $i;

        //     $flight->save();
        // }
        // Nilai::factory()->count(477)->create();
        // for ($i=1; $i < 477; $i++) {
        //     $flight = Nilai::find($i);

        //     $flight->id_siswa = $i;

        //     $flight->save();
        // }
        // $a = 0;
        // for ($i=1; $i < 120; $i++) {
        //     // if ($i > 10) {
        //     //     $a = $i - 10;
        //     // } else {
        //     //     $a = $i;
        //     // }

        //     JurusanPerusahaan::insert([
        //         'id_perusahaan' => $i,
        //         'id_jurusan' => mt_rand(1, 6),
        //         'id_tahun' => 2
        //     ]);

        // }



    }
}
