<?php

namespace Database\Seeders;

use App\Models\Absen;
use App\Models\Aktivitas;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class AbsenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $status = ["Hadir", "Hadir", "Hadir", "Hadir", "Hadir", "Hadir", "Sakit",  "Hadir", "Hadir", "Hadir", "Hadir", "Hadir", "Hadir","Izin", "Hadir", "Hadir"];

        for ($i=1; $i < 31 ; $i++) {
            if ($i != 3 && $i != 4 &&  $i != 10 && $i != 11 && $i != 17 && $i != 18 && $i != 24 && $i != 25) {


                if($i > 9){
                // Aktivitas::create([
                //     'kegiatan' => 'Belajar Menggunakan Framwork Flashk menamatkan Video yang diberikan',
                //     'tanggal' => '2023-10-'.$i,
                //     'mulai' => '12:00:00',
                //     'selesai' => '12:00:00',
                //     'id_siswa' => 2106510353,
                //     'id_perusahaan' => 11,
                //     'id_tahun' => 2

                // ]);
                    Absen::create([
                    'bulan' => "Oktober",
                'tgl' => $i,
                'tanggal' => '2023-10-'.$i,
                'jam_masuk' => '17:20:15',
                'poto_masuk' => 'Tidak Tersedia',
                'jam_pulang' => '17:20:15',
                'status' => $status[mt_rand(0,15)],
                'poto_pulang' => 'Tidak Tersedia',
                'id_siswa' => '2106510353',
                'id_tahun' => 2,
                'id_perusahaan' => 11
            ]);
            }else if($i < 10){
            //    Aktivitas::create([
            //     'kegiatan' => 'Belajar Menggunakan Framwork Flashk menamatkan Video yang diberikan',
            //     'tanggal' => '2023-10-0'.$i,
            //     'mulai' => '12:00:00',
            //     'selesai' => '12:00:00',
            //     'id_siswa' => '21065103',
            //     'id_perusahaan' => 11,
            //     'id_tahun' => 2

            //    ]);
                Absen::create([
                    'bulan' => "Oktober",
                'tgl' => $i,
                'tanggal' => '2023-10-0'.$i,
                'jam_masuk' => '17:20:15',
                'poto_masuk' => 'Tidak Tersedia',
                'jam_pulang' => '17:20:15',
                'status' => $status[mt_rand(0,15)],
                'poto_pulang' => 'Tidak Tersedia',
                'id_siswa' => '2106510353',
                'id_tahun' => 2,
                'id_perusahaan' => 11
            ]);
            }
        }
            if ($i != 5 && $i != 6 &&  $i != 12 && $i != 13 && $i != 19 && $i != 20 && $i != 26 && $i != 27) {


                if($i > 9){
                // Aktivitas::create([
                //     'kegiatan' => 'Belajar Menggunakan Framwork Flashk menamatkan Video yang diberikan',
                //      'tanggal' => '2023-11-'.$i,
                //      'mulai' => '12:00:00',
                //      'selesai' => '12:00:00',
                //      'id_siswa' => '2106510353',
                //      'id_perusahaan' => 11,
                //      'id_tahun' => 2

                // ]);
                    Absen::create([
                    'bulan' => "November",
                'tgl' => $i,
                'tanggal' => '2023-11-'.$i,
                'jam_masuk' => '17:20:15',
                'poto_masuk' => 'Tidak Tersedia',
                'jam_pulang' => '17:20:15',
                'status' => $status[mt_rand(0,15)],
                'poto_pulang' => 'Tidak Tersedia',
                'id_siswa' => '2106510353',
                'id_tahun' => 2,
                'id_perusahaan' => 11
            ]);
            }else if($i < 10){
            //    Aktivitas::create([
            //     'kegiatan' => 'Belajar Menggunakan Framwork Flashk menamatkan Video yang diberikan',
            //     'tanggal' => '2023-11-0'.$i,
            //     'mulai' => '12:00:00',
            //     'selesai' => '12:00:00',
            //     'id_siswa' => '21065103',
            //     'id_perusahaan' => 11,
            //     'id_tahun' => 2

            //    ]);
                Absen::create([
                    'bulan' => "November",
                'tgl' => $i,
                'tanggal' => '2023-11-0'.$i,
                'jam_masuk' => '17:20:15',
                'poto_masuk' => 'Tidak Tersedia',
                'jam_pulang' => '17:20:15',
                'status' => $status[mt_rand(0,15)],
                'poto_pulang' => 'Tidak Tersedia',
                'id_siswa' => '2106510353',
                'id_tahun' => 2,
                'id_perusahaan' => 11
            ]);
            }
        }
            if ($i != 7 && $i != 8 &&  $i != 14 && $i != 15 && $i != 21 && $i != 22 && $i != 28 && $i != 29) {


                if($i > 9){
                // Aktivitas::create([
                //     'kegiatan' => 'Belajar Menggunakan Framwork Flashk menamatkan Video yang diberikan',
                //      'tanggal' => '2023-12-'.$i,
                //      'mulai' => '12:00:00',
                //      'selesai' => '12:00:00',
                //      'id_siswa' => '2106510353',
                //      'id_perusahaan' => 11,
                //      'id_tahun' => 2

                // ]);
                    Absen::create([
                    'bulan' => "Desember",
                'tgl' => $i,
                'tanggal' => '2023-12-'.$i,
                'jam_masuk' => '17:20:15',
                'poto_masuk' => 'Tidak Tersedia',
                'jam_pulang' => '17:20:15',
                'status' => $status[mt_rand(0,15)],
                'poto_pulang' => 'Tidak Tersedia',
                'id_siswa' => '2106510353',
                'id_tahun' => 2,
                'id_perusahaan' => 11
            ]);
            }else if($i < 10){
            //    Aktivitas::create([
            //     'kegiatan' => 'Belajar Menggunakan Framwork Flashk menamatkan Video yang diberikan',
            //     'tanggal' => '2023-12-0'.$i,
            //     'mulai' => '12:00:00',
            //     'selesai' => '12:00:00',
            //     'id_siswa' => '21065103',
            //     'id_perusahaan' => 11,
            //     'id_tahun' => 2

            //    ]);
                Absen::create([
                    'bulan' => "Desember",
                'tgl' => $i,
                'tanggal' => '2023-12-0'.$i,
                'jam_masuk' => '17:20:15',
                'poto_masuk' => 'Tidak Tersedia',
                'jam_pulang' => '17:20:15',
                'status' => $status[mt_rand(0,15)],
                'poto_pulang' => 'Tidak Tersedia',
                'id_siswa' => '2106510353',
                'id_tahun' => 2,
                'id_perusahaan' => 11
            ]);
            }
        }
            if ($i != 3 && $i != 4 &&  $i != 10 && $i != 11 && $i != 17 && $i != 18 && $i != 24 && $i != 25) {


                if($i > 9){
                // Aktivitas::create([
                //     'kegiatan' => 'Belajar Menggunakan Framwork Flashk menamatkan Video yang diberikan',
                //      'tanggal' => '2024-01-'.$i,
                //      'mulai' => '12:00:00',
                //      'selesai' => '12:00:00',
                //      'id_siswa' => '2106510353',
                //      'id_perusahaan' => 11,
                //      'id_tahun' => 2

                // ]);
                    Absen::create([
                    'bulan' => "Januari",
                'tgl' => $i,
                'tanggal' => '2024-01-'.$i,
                'jam_masuk' => '17:20:15',
                'poto_masuk' => 'Tidak Tersedia',
                'jam_pulang' => '17:20:15',
                'status' => $status[mt_rand(0,15)],
                'poto_pulang' => 'Tidak Tersedia',
                'id_siswa' => '2106510353',
                'id_tahun' => 2,
                'id_perusahaan' => 11
            ]);
            }else if($i < 10){
            //    Aktivitas::create([
            //     'kegiatan' => 'Belajar Menggunakan Framwork Flashk menamatkan Video yang diberikan',
            //     'tanggal' => '2024-01-0'.$i,
            //     'mulai' => '12:00:00',
            //     'selesai' => '12:00:00',
            //     'id_siswa' => '21065103',
            //     'id_perusahaan' => 11,
            //     'id_tahun' => 2

            //    ]);
                Absen::create([
                    'bulan' => "Januari",
                'tgl' => $i,
                'tanggal' => '2024-01-0'.$i,
                'jam_masuk' => '17:20:15',
                'poto_masuk' => 'Tidak Tersedia',
                'jam_pulang' => '17:20:15',
                'status' => $status[mt_rand(0,15)],
                'poto_pulang' => 'Tidak Tersedia',
                'id_siswa' => '2106510353',
                'id_tahun' => 2,
                'id_perusahaan' => 11
            ]);
            }
        }
            if ($i != 5 && $i != 6 &&  $i != 12 && $i != 13 && $i != 19 && $i != 20 && $i != 26 && $i != 27) {


                if($i > 9 && $i < 30){
                // Aktivitas::create([
                //     'kegiatan' => 'Belajar Menggunakan Framwork Flashk menamatkan Video yang diberikan',
                //      'tanggal' => '2024-02-'.$i,
                //      'mulai' => '12:00:00',
                //      'selesai' => '12:00:00',
                //      'id_siswa' => '2106510353',
                //      'id_perusahaan' => 11,
                //      'id_tahun' => 2

                // ]);
                    Absen::create([
                    'bulan' => "Februari",
                'tgl' => $i,
                'tanggal' => '2024-02-'.$i,
                'jam_masuk' => '17:20:15',
                'poto_masuk' => 'Tidak Tersedia',
                'jam_pulang' => '17:20:15',
                'status' => $status[mt_rand(0,15)],
                'poto_pulang' => 'Tidak Tersedia',
                'id_siswa' => '2106510353',
                'id_tahun' => 2,
                'id_perusahaan' => 11
            ]);
            }else if($i < 10){
            //    Aktivitas::create([
            //     'kegiatan' => 'Belajar Menggunakan Framwork Flashk menamatkan Video yang diberikan',
            //     'tanggal' => '2024-02-0'.$i,
            //     'mulai' => '12:00:00',
            //     'selesai' => '12:00:00',
            //     'id_siswa' => '21065103',
            //     'id_perusahaan' => 11,
            //     'id_tahun' => 2

            //    ]);
                Absen::create([
                    'bulan' => "Februari",
                'tgl' => $i,
                'tanggal' => '2024-02-0'.$i,
                'jam_masuk' => '17:20:15',
                'poto_masuk' => 'Tidak Tersedia',
                'jam_pulang' => '17:20:15',
                'status' => $status[mt_rand(0,15)],
                'poto_pulang' => 'Tidak Tersedia',
                'id_siswa' => '2106510353',
                'id_tahun' => 2,
                'id_perusahaan' => 11
            ]);
            }
        }
            if ($i != 7 && $i != 8 &&  $i != 14 && $i != 15 && $i != 21 && $i != 22 && $i != 28 && $i != 29) {


                if($i > 9){
                // Aktivitas::create([
                //     'kegiatan' => 'Belajar Menggunakan Framwork Flashk menamatkan Video yang diberikan',
                //      'tanggal' => '2024-03-'.$i,
                //      'mulai' => '12:00:00',
                //      'selesai' => '12:00:00',
                //      'id_siswa' => '2106510353',
                //      'id_perusahaan' => 11,
                //      'id_tahun' => 2

                // ]);
                    Absen::create([
                    'bulan' => "Maret",
                'tgl' => $i,
                'tanggal' => '2024-03-'.$i,
                'jam_masuk' => '17:20:15',
                'poto_masuk' => 'Tidak Tersedia',
                'jam_pulang' => '17:20:15',
                'status' => $status[mt_rand(0,15)],
                'poto_pulang' => 'Tidak Tersedia',
                'id_siswa' => '2106510353',
                'id_tahun' => 2,
                'id_perusahaan' => 11
            ]);
            }else if($i < 10){
            //    Aktivitas::create([
            //     'kegiatan' => 'Belajar Menggunakan Framwork Flashk menamatkan Video yang diberikan',
            //     'tanggal' => '2024-03-0'.$i,
            //     'mulai' => '12:00:00',
            //     'selesai' => '12:00:00',
            //     'id_siswa' => '21065103',
            //     'id_perusahaan' => 11,
            //     'id_tahun' => 2

            //    ]);
                Absen::create([
                    'bulan' => "Maret",
                'tgl' => $i,
                'tanggal' => '2024-03-0'.$i,
                'jam_masuk' => '17:20:15',
                'poto_masuk' => 'Tidak Tersedia',
                'jam_pulang' => '17:20:15',
                'status' => $status[mt_rand(0,15)],
                'poto_pulang' => 'Tidak Tersedia',
                'id_siswa' => '2106510353',
                'id_tahun' => 2,
                'id_perusahaan' => 11
            ]);
            }
        }

        }
    }
}
