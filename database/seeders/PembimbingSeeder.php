<?php

namespace Database\Seeders;

use App\Models\Guru;
use App\Models\Role;
use App\Models\User;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\Kaprog;
use App\Models\Jurusan;
use App\Models\Aktivitas;
use App\Models\Perusahaan;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use App\Models\KonsentrasiKeahlian;
use App\Models\PembimbingPerusahaan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PembimbingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {


        PembimbingPerusahaan::factory()->count(10)->create();
    }
}