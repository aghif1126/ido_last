<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Kelas;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [

            ['nama' => '11 PPLG 1', 'id_jurusan' => '1'],
            ['nama' => '11 PPLG 2', 'id_jurusan' => '1'],
            ['nama' => '11 KDKV 1', 'id_jurusan' => '7'],
            ['nama' => '11 KDKV 2', 'id_jurusan' => '7'],
            ['nama' => '11 TJKT 1', 'id_jurusan' => '2'],
            ['nama' => '11 TJKT 2', 'id_jurusan' => '2'],
            ['nama' => '11 AKL 1', 'id_jurusan' => '6'],
            ['nama' => '11 AKL 2', 'id_jurusan' => '6'],
            ['nama' => '11 MLOG 1', 'id_jurusan' => '5'],
            ['nama' => '11 MLOG 2', 'id_jurusan' => '5'],
            ['nama' => '11 BR 1', 'id_jurusan' => '3'],
            ['nama' => '11 BR 2', 'id_jurusan' => '3'],
            ['nama' => '11 MP 1', 'id_jurusan' => '4'],
            ['nama' => '11 MP 2', 'id_jurusan' => '4']

        ];
        foreach ($data as $value) {
            Kelas::insert([
                'nama' => $value['nama'],
                'id_konsentrasi_keahlian' => $value['id_jurusan'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'id_tahun' => 1

            ]);
        }
    }
}
