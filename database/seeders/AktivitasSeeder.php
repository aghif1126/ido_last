<?php

namespace Database\Seeders;

use App\Models\Aktivitas;
use App\Models\Absen;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class AktivitasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Aktivitas::factory() -> count(500) -> create();
        // Absen::factory()->count(100)->create();

        // $bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli"];
        $status = ["Hadir", "Izin", "Sakit"];
       for ($i=1; $i < 32 ; $i++) {
        Absen::create([
            'bulan' => "November",
            'tgl' => $i,
            'tanggal' => '2023-11-01',
            'jam_masuk' => '17:20:15',
            'poto_masuk' => '',
            'jam_pulang' => '17:20:15',
            'status' => $status[mt_rand(0,2)],
            'poto_pulang' => '',
            'id_siswa' => '2106510351',
            'id_tahun' => 2
        ]);
       }
    }
}
