<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // User::factory() -> count(20) -> create();
        for ($i=1; $i < 21; $i++) {
            $flight = User::find($i);

            $flight->role_id = 4;

            $flight->save();
        }
        for ($i=21; $i < 26; $i++) {
            $flight = User::find($i);

            $flight->role_id = 3;

            $flight->save();
        }
        for ($i=26; $i < 31; $i++) {
            $flight = User::find($i);

            $flight->role_id = 2;

            $flight->save();
        }



    }
}
