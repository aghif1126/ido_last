<?php

namespace Database\Seeders;

use App\Models\Siswa;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Siswa::factory() -> count(20) -> create();
        for ($i=1; $i < 21; $i++) {
            $flight = Siswa::find($i);

            $flight->id_user = $i;

            $flight->save();
        }
    }
}
