<?php
// tes dikit
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\GuruController;
use App\Http\Controllers\GuruPembimbing;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ExcelController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\JurusanController;
use App\Http\Controllers\KriteriaController;
use App\Http\Controllers\PemetaanController;
use App\Http\Controllers\AktivitasController;
use App\Http\Controllers\MonitoringController;
use App\Http\Controllers\PembimbingController;
use App\Http\Controllers\PerusahaanController;
use App\Http\Controllers\KonsentrasiController;
use App\Http\Controllers\GuruPembimbingController;
use App\Http\Controllers\PimpinanPerusahaanController;
use App\Http\Controllers\PembimbingPerusahaanController;
use App\Http\Controllers\TahunPelajaranController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// test branch
Route::get('/dashboard', function () {
        return redirect('/guru/dashboard');
    });
Route::get('/', [GuruController::class, 'dashboard']);
Route::get('/fitur-absen', function () {
    return view('/absen');
});
Route::get('/fitur-jurnal-kegiatan', function () {
    return view('/kegiatan');
});
Route::get('/fitur-nilai', function () {
    return view('/nilai');
});

Route::get('/home', function () {
    return redirect('/dashboard');
});

Route::get('/log-in', function () {
    return view('login');
})->name('login');

Route::get('/log-in', [AuthController::class, 'login'])->name('login')->middleware('guest');
Route::post('/login/auth', [AuthController::class, 'validasi'])->middleware('guest');
Route::get('/logout', [AuthController::class, 'logout'])->middleware('auth');
Route::get('/register', function () { return view('register'); });

Route::get('/siswa/dashboard', function () {
    return view('siswa.dashboard');
})->middleware('auth');

// Route::get('/dashboard/profile', [SiswaController::class, 'profile']);

Route::get('/penilaian', [PembimbingPerusahaanController::class, 'penilaian'])->middleware('auth');


// Route::get('/guru', function () {
//     return redirect('/guru/dashboard');
// })->middleware('auth');

// Route::get('/guru/dashboard', function () {
//     return view('guru.dashboard');
// })->middleware('auth');

// Route Guru

// Route::get('/admin/dashboard', [AdminController::class, 'index'])->middleware('auth');

// Route Guru Table siswa

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/guru/dashboard', [GuruController::class, 'index']);
    Route::get('/guru/show-siswa', [SiswaController::class, 'show']);
    Route::get('/guru/detail-siswa/{id}', [SiswaController::class, 'detail']);
    Route::get('/guru/add-siswa', [SiswaController::class, 'create']);
    Route::get('/guru/pilih-kelas-siswa', [SiswaController::class, 'kelas']);
    Route::post('/guru/import-siswa', [ExcelController::class, 'import']);
    // Route::get('/guru/download-template', [ExcelController::class, 'format']);
    Route::get('/guru/edit-siswa/{id}', [SiswaController::class, 'edit']);
    Route::post('/guru/ubah-siswa', [SiswaController::class, 'ubah']);
    Route::post('/guru/store-siswa', [SiswaController::class, 'store']);
    Route::get('/guru/delete-siswa', [SiswaController::class, 'delete']);
    Route::post('/guru/destroy-siswa', [SiswaController::class, 'destroy']);
});

// Route Guru Table perushaan

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/guru/show-perusahaan', [PerusahaanController::class, 'show']);
    Route::get('/guru/add-perusahaan', [PerusahaanController::class, 'create']);
    Route::post('/guru/store-perusahaan', [PerusahaanController::class, 'store']);
    Route::get('/guru/template-perusahaan', [ExcelController::class, 'format_perusahaan']);
    Route::post('/guru/import-perusahaan', [ExcelController::class, 'import_perusahaan']);
    Route::get('/guru/delete-perusahaan', [PerusahaanController::class, 'delete']);
    Route::post('/guru/destroy-perusahaan', [PerusahaanController::class, 'destroy']);
    Route::get('/guru/detail-perusahaan/{id}', [PerusahaanController::class, 'detail'])->name('detailper');
    Route::get('/guru/edit-perusahaan/{id}', [PerusahaanController::class, 'edit']);
    Route::get('/guru/edit-perusahaan/{id}/add-jurusan', [PerusahaanController::class, 'tambahjurusan']);
    Route::post('/guru/edit-perusahaan/store-jurusan', [PerusahaanController::class, 'storejurusan']);
    Route::get('/guru/edit-perusahaan/{id}/delete-jurusan', [PerusahaanController::class, 'hapusjurusan']);
    Route::post('/guru/edit-perusahaan/destroy-jurusan', [PerusahaanController::class, 'destroyjurusan']);
    Route::post('/guru/update-perusahaan', [PerusahaanController::class, 'update']);
});

// Route Guru Table User

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/guru/show-user', [UserController::class, 'show']);
    Route::get('/guru/add-user/guru/{id}', [UserController::class, 'createGuru']);
    Route::get('/guru/add-user/siswa/{id}', [UserController::class, 'createSiswa']);
    Route::get('/guru/add-user/pembimbing/{id}', [UserController::class, 'createPembimbing']);
    Route::get('/guru/pilih-user', [UserController::class, 'pilih']);
    Route::get('/guru/pilih-user/guru', [UserController::class, 'pilihGuru']);
    Route::get('/guru/pilih-user/siswa', [UserController::class, 'pilihSiswa']);
    Route::get('/guru/pilih-user/pembimbing', [UserController::class, 'pilihPembimbing']);
    Route::post('/guru/pembimbing/store-user', [UserController::class, 'PembimbingStore']);
    Route::post('/guru/guru/store-user', [UserController::class, 'GuruStore']);
    Route::post('/guru/siswa/store-user', [UserController::class, 'SiswaStore']);
    Route::get('/guru/delete-user', [UserController::class, 'delete']);
    Route::post('/guru/destroy-user', [UserController::class, 'destroy']);
    Route::get('/guru/detail-user/{id}', [UserController::class, 'detail']);
    Route::get('/guru/edit-user/{id}', [UserController::class, 'edit']);
    Route::post('/guru/update-user', [UserController::class, 'update']);
});

// Route Guru Table Konsentrasi

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/guru/show-konsentrasi', [KonsentrasiController::class, 'show']);
    Route::get('/guru/add-konsentrasi', [KonsentrasiController::class, 'create']);
    Route::post('/guru/store-konsentrasi', [KonsentrasiController::class, 'store']);
    Route::get('/guru/detail-konsentrasi/{id}', [KonsentrasiController::class, 'detail']);
    Route::get('/guru/edit-konsentrasi/{id}', [KonsentrasiController::class, 'edit']);
    Route::post('/guru/update-konsentrasi', [KonsentrasiController::class, 'update']);
});

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/guru/show-tahun-pelajaran', [TahunPelajaranController::class, 'show']);
    Route::get('/guru/add-tahun-pelajaran', [TahunPelajaranController::class, 'create']);
    Route::post('/guru/store-tahun-pelajaran', [TahunPelajaranController::class, 'store']);
    Route::get('/guru/delete-tahun-pelajaran', [TahunPelajaranController::class, 'delete']);
    Route::post('/guru/destroy-tahun-pelajaran', [TahunPelajaranController::class, 'destroy']);
    Route::get('/guru/ganti-tahun-pelajaran/{id}', [TahunPelajaranController::class, 'ganti_status']);
    Route::get('/guru/edit-tahun-pelajaran/{id}', [TahunPelajaranController::class, 'edit']);
    Route::post('/guru/update-tahun-pelajaran', [TahunPelajaranController::class, 'update']);
});

// Route Guru Table Jurusan

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/guru/show-jurusan', [JurusanController::class, 'show']);
    Route::get('/guru/add-jurusan', [JurusanController::class, 'create']);
    Route::post('/guru/store-jurusan', [JurusanController::class, 'store']);
    Route::get('/guru/detail-jurusan/{id}', [JurusanController::class, 'detail']);
    Route::get('/guru/edit-jurusan/{id}', [JurusanController::class, 'edit']);
    Route::post('/guru/update-jurusan', [JurusanController::class, 'update']);
});

// Route Guru Table Kelas

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/guru/show-kelas', [KelasController::class, 'show']);
    Route::get('/guru/add-kelas', [KelasController::class, 'create']);
    Route::post('/guru/store-kelas', [KelasController::class, 'store']);
    Route::get('/guru/detail-kelas/{id}', [KelasController::class, 'detail']);
    Route::get('/guru/detail-kelas/{id}/siswa', [KelasController::class, 'siswa']);
    Route::get('/guru/detail-kelas/pilihperusahaan', [KelasController::class, 'pilihperusahaan']);
    Route::get('/guru/edit-kelas/{id}', [KelasController::class, 'edit']);
    Route::post('/guru/update-kelas', [KelasController::class, 'update']);
});

// Route Pemetaan Siswa

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/guru/pemetaan-siswa', [PemetaanController::class, 'index']);
    Route::get('/guru/pemetaan-siswa/{id}/pilih-perusahaan', [PemetaanController::class, 'satu_pilih']);
    Route::post('/guru/pemetaan-siswa/pilih-perusahaan', [PemetaanController::class, 'pilih']);
    Route::post('/guru/pemetaan-siswa/pilih-perusahaan/confirm', [PemetaanController::class, 'confirm']);
    Route::post('/guru/pemetaan-siswa/pilih-perusahaan/periode', [PemetaanController::class, 'periode']);
    Route::post('/guru/pemetaan-siswa/pilih-perusahaan/konfirmasi', [PemetaanController::class, 'konfirmasi']);
    Route::post('/guru/pemetaan-siswa/pilih-perusahaan/batalkan', [PemetaanController::class, 'batalkan']);
});



//Pembimbing show siswa, detail siswa, beri nilai siswa, edit nilai siswa, rekap nilai siswa, profil, detail perusahaan, setting limit siswa
// Route::get('/pembimbing/show-siswa', [PembimbingPerusahaanController::class, ])->middleware('auth');

// Route::get('/guru/dashboard', [GuruController::class, 'index'])->middleware('auth');

//Route menu pembimbing

Route::middleware(['auth', 'pembimbing'])->group(function () {
    Route::get('/pembimbing/dashboard', [PembimbingPerusahaanController::class, 'index']);
    Route::post('/pembimbing/update-password', [PembimbingPerusahaanController::class, 'editpassword']);
    Route::get('/pembimbing/catatan/{id}', [PembimbingPerusahaanController::class, 'catatan']);
    Route::get('/pembimbing/add-catatan/{id}', [PembimbingPerusahaanController::class, 'addCatatan']);
    Route::post('/pembimbing/store-catatan', [PembimbingPerusahaanController::class, 'storeCatatan']);
    Route::get('/pembimbing/edit-catatan/{id}', [PembimbingPerusahaanController::class, 'editCatatan']);
    Route::get('/pembimbing/delete-catatan/{id}', [PembimbingPerusahaanController::class, 'deleteCatatan']);
    Route::post('/pembimbing/destroy-catatan', [PembimbingPerusahaanController::class, 'destroy']);
    Route::post('/pembimbing/update-catatan', [PembimbingPerusahaanController::class, 'updateCatatan']);
    Route::get('/pembimbing/nilai/{id}', [PembimbingPerusahaanController::class, 'nilai']);
    Route::get('/pembimbing/penilaian/{id}', [PembimbingPerusahaanController::class, 'addNilai']);
    Route::post('/pembimbing/store-nilai', [PembimbingPerusahaanController::class, 'storeNilai']);
    Route::get('/pembimbing/edit-nilai/{id}', [PembimbingPerusahaanController::class, 'editNilai']);
    Route::post('/pembimbing/update-nilai', [PembimbingPerusahaanController::class, 'updateNilai']);
    Route::get('/pembimbing/detail-siswa/{id}', [PembimbingPerusahaanController::class, 'detailsiswa']);

    Route::get('/pembimbing/kriteria', [KriteriaController::class, 'index']);
    Route::get('/pembimbing/add-kriteria', [KriteriaController::class, 'create']);
    Route::post('/pembimbing/store-kriteria', [KriteriaController::class, 'store']);
    Route::get('/pembimbing/delete-kriteria/{kriteria}', [KriteriaController::class, 'destroy']);
    Route::get('/pembimbing/edit-kriteria/{kriteria}', [KriteriaController::class, 'edit']);
    Route::post('/pembimbing/update-kriteria/{kriteria}', [KriteriaController::class, 'update']);
});

Route::middleware(['auth', 'guru'])->group(function () {
    Route::get('/guru/pembimbing/dashboard', [GuruPembimbingController::class, 'index']);
    Route::post('/guru/pembimbing/update-password', [GuruPembimbingController::class, 'editpassword']);
    Route::get('/guru/pembimbing/perusahaan', [GuruPembimbingController::class, 'perusahaan']);
    Route::get('/guru/pembimbing/detail-siswa/{id}', [GuruPembimbingController::class, 'detailsiswa']);
    Route::post('/guru/pembimbing/detail-siswa/', [GuruPembimbingController::class, 'detailsiswa']);
    Route::get('/guru/pembimbing/detail-pembimbing/{id}', [GuruPembimbingController::class, 'detailpembimbing']);
    // Route::get('/guru/pembimbing/kelas', [GuruPembimbingController::class, 'kelas']);
    Route::get('/guru/pembimbing/detail-perusahaan/{id}/{idjur}', [GuruPembimbingController::class, 'detailperusahaan']);
    Route::get('/guru/pembimbing/monitoring', [GuruPembimbingController::class, 'form']);
    // Route::get('/guru/pembimbing/monitoring-form/{id}', [GuruPembimbingController::class, 'form']);
    Route::post('/guru/pembimbing/store-monitoring', [GuruPembimbingController::class, 'storeform']);
    Route::get('/guru/pembimbing/riwayat', [GuruPembimbingController::class, 'riwayat']);
    Route::get('/guru/pembimbing/profile', [GuruPembimbingController::class, 'profile']);
});



// Route::get('/guru/penilaian', function () {
//     return view('guru.penilaian');
// })->middleware('auth');



// Route Siswa mulai disini

Route::middleware(['auth', 'siswa'])->group(function () {
    Route::get('/siswa/dashboard', [SiswaController::class, 'index'])-> name('siswa-index');
    Route::get('/siswa/form', [SiswaController::class, 'form']);
    Route::get('/siswa/profile', [SiswaController::class, 'profile']);
    Route::get('/siswa/aktivitas', [SiswaController::class, 'aktivitas']);
    Route::post('/siswa/absen', [SiswaController::class, 'absen']);
    Route::post('/siswa/absenpulang', [SiswaController::class, 'absenpulang']);
    Route::post('/siswa/aktivitas/isi', [SiswaController::class, 'isi_aktivitas']);
    Route::get('/siswa/riwayat', [SiswaController::class, 'riwayat']);
    Route::get('/siswa/perusahaan', [SiswaController::class, 'perusahaan']);
    Route::get('/siswa/nilai', [SiswaController::class, 'nilai']);
    Route::post('/siswa/update-data', [SiswaController::class, 'update']);
    Route::post('/siswa/update-poto', [SiswaController::class, 'poto']);
    Route::post('/siswa/update-password', [SiswaController::class, 'editpassword']);

});


// Route pimpinan perusahaan

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/guru/show-pimpinan', [PimpinanPerusahaanController::class, 'show']);
    Route::get('/guru/pilih-perusahaan', [PimpinanPerusahaanController::class, 'pilih_perusahaan']);
    Route::post('/guru/add-pimpinan', [PimpinanPerusahaanController::class, 'create']);
    Route::post('/guru/store-pimpinan', [PimpinanPerusahaanController::class, 'store']);
    Route::get('/guru/delete-pimpinan', [PimpinanPerusahaanController::class, 'delete']);
    Route::post('/guru/destroy-pimpinan', [PimpinanPerusahaanController::class, 'destroy']);
    Route::get('/guru/edit-pimpinan/{id}', [PimpinanPerusahaanController::class, 'edit']);
    Route::get('/guru/detail-pimpinan/{id}', [PimpinanPerusahaanController::class, 'detail']);
    Route::post('/guru/update-pimpinan', [PimpinanPerusahaanController::class, 'update']);
});


//Route admin

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/guru/show-pembimbing', [PembimbingController::class, 'show']);
    Route::get('/guru/pembimbing-pilih-perusahaan', [PembimbingController::class,'pilih_perusahaan']);
    Route::post('/guru/add-pembimbing', [PembimbingController::class, 'create']);
    Route::post('/guru/store-pembimbing', [PembimbingController::class, 'store']);
    Route::get('/guru/delete-pembimbing', [PembimbingController::class, 'delete']);
    Route::post('/guru/destroy-pembimbing', [PembimbingController::class, 'destroy']);
    Route::get('/guru/detail-pembimbing/{id}', [PembimbingController::class, 'detail']);
    Route::get('/guru/edit-pembimbing/{id}', [PembimbingController::class, 'edit']);
    Route::post('/guru/update-pembimbing', [PembimbingController::class, 'update']);
});

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/guru/show-guru', [GuruController::class, 'show']);
    Route::get('/guru/add-guru', [GuruController::class, 'create']);
    Route::get('/guru/down-template-guru', [ExcelController::class, 'format_guru']);
    Route::post('/guru/import-guru', [ExcelController::class, 'import_guru']);
    Route::post('/guru/store-guru', [GuruController::class, 'store']);
    Route::get('/guru/delete-guru', [GuruController::class, 'delete']);
    Route::post('/guru/destroy-guru', [GuruController::class, 'destroy']);
    Route::get('/guru/detail-guru/{id}', [GuruController::class, 'detail']);
    Route::get('/guru/edit-guru/{id}', [GuruController::class, 'edit']);
    Route::post('/guru/update-guru', [GuruController::class, 'update']);
    Route::get('/guru/edit-guru/{id}/add-perusahaan', [GuruController::class, 'tambahperusahaan']);
    Route::post('/guru/edit-guru/store-perusahaan', [GuruController::class, 'storeperusahaan']);
    Route::get('/guru/edit-guru/{id}/delete-perusahaan', [GuruController::class, 'hapusperusahaan']);
    Route::post('/guru/edit-guru/destroy-perusahaan', [GuruController::class, 'destroyperusahaan']);
});

//Route PDF

Route::post('/guru/pembimbing/download/monitoring', [PDFController::class, 'monitoring'])->middleware('auth')->name('download.monitoring');
Route::post('siswa/download/jurnalsiswa', [PDFController::class, 'jurnalsiswa'])->middleware('auth')->name('download.jurnalsiswa');
Route::post('admin/download/rekappemetaan', [PDFController::class, 'rekappemetaan'])->middleware('auth')->name('download.rekappemetaan');

//nanti hapus
// Route::get('siswa/download/format-nilai', [PDFController::class, 'formatnilai'])->middleware('auth')->name('download.format-nilai');
// Route::get('siswa/download/format-riwayat', [PDFController::class, 'formatriwayat'])->middleware('auth')->name('download.format-riwayat');
Route::get('siswa/download/format-cover', [PDFController::class, 'formatcover']);
// Route::get('siswa/download/format-identitas', [PDFController::class, 'formatidentitas'])->middleware('auth')->name('download.format-identitas');
// Route::get('siswa/download/format-tempatpkl', [PDFController::class, 'formattempatpkl'])->middleware('auth')->name('download.format-tempatpkl');
// Route::get('siswa/download/format-catatan', [PDFController::class, 'formatcatatan'])->middleware('auth')->name('download.format-catatan');
// Route::get('siswa/download/format-coverlaporan', [PDFController::class, 'formatcoverlaporan'])->middleware('auth')->name('download.format-coverlaporan');
// Route::get('siswa/download/format-catatanmonitoring', [PDFController::class, 'formatcatatanmonitoring'])->middleware('auth')->name('download.format-catatanmonitoring');
// Route::get('siswa/download/format-monitoring', [PDFController::class, 'formatmonitoring'])->middleware('auth')->name('download.format-monitoring');
// Route::get('siswa/download/format-absen', [PDFController::class, 'formatabsen']);

// Route Excel

Route::get('/guru/export-absen', [ExcelController::class, 'export'])->middleware('auth');


// Route Riwayat Monitoring

Route::middleware(['auth', 'admin'])->group(function () {
    Route ::get('/guru/show-monitoring', [MonitoringController::class, 'show']);
    Route::post('/guru/download-monitoring/{id}', [MonitoringController::class, 'downloadmonitoring']);
});

Route::middleware(['auth', 'admin'])->group(function () {
    Route::post('/guru/backup-db', [GuruController::class, 'backup_db']);
    Route::post('/guru/restore-db', [GuruController::class, 'restore_db']);
});

